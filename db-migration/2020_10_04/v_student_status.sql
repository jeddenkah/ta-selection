CREATE VIEW `v_student_status` AS
    SELECT 
        CONCAT(`s`.`id`,
                `ts`.`optionNum`,
                `ts`.`period_id`) AS `id`,
        `s`.`id` AS `student`,
        `g`.`id` AS `group`,
        `ts`.`optionNum` AS `optionNum`,
        `ts`.`status` AS `status`,
        `ts`.`period_id` AS `period`
    FROM
        (((`student` `s`
        LEFT JOIN `group_student` `gs` ON ((`gs`.`student_id` = `s`.`id`)))
        LEFT JOIN `group` `g` ON ((`gs`.`group_id` = `g`.`id`)))
        LEFT JOIN `topic_selection` `ts` ON ((`ts`.`group_id` = `g`.`id`)))
    WHERE
        (`gs`.`id` = (SELECT 
                MAX(`group_student`.`id`)
            FROM
                `group_student`
            WHERE
                (`group_student`.`student_id` = `s`.`id`)))
    ORDER BY `s`.`id`