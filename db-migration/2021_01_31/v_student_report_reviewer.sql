-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 31 Jan 2021 pada 08.22
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.3.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `taproposal`
--

-- --------------------------------------------------------

--
-- Struktur untuk view `v_student_report_reviewer`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_student_report_reviewer`  AS  select concat(`s`.`id`,`ts`.`optionNum`,`ts`.`period_id`) AS `id`,`s`.`id` AS `student`,`g`.`id` AS `group`,`ts`.`optionNum` AS `optionNum`,`ts`.`status` AS `status`,`ts`.`period_id` AS `period`,`l`.`id` AS `lecturer`,`gs`.`lecturer_id` AS `reviewer` from (((((`student` `s` left join `group_student` `gs` on(`gs`.`student_id` = `s`.`id`)) left join `group` `g` on(`gs`.`group_id` = `g`.`id`)) left join `topic_selection` `ts` on(`ts`.`group_id` = `g`.`id`)) left join `topic` `t` on(`ts`.`topic_id` = `t`.`id`)) left join `lecturer` `l` on(`t`.`lecturer_id` = `l`.`id`)) where `gs`.`id` = (select max(`group_student`.`id`) from `group_student` where `group_student`.`student_id` = `s`.`id`) and `ts`.`status` = 'APPROVED_LAB_RISET' order by `s`.`id` ;

--
-- VIEW  `v_student_report_reviewer`
-- Data: Tidak ada
--

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
