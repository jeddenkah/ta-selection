-- MySQL dump 10.13  Distrib 8.0.23, for macos10.15 (x86_64)
--
-- Host: localhost    Database: taproposal
-- ------------------------------------------------------
-- Server version	8.0.25-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `hashed_password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'Admin','$2b$10$7qkhnwlWJeTEcif9/KnaGuOthg8gAlTTRD70WZMeBXQ7B.hewgsfe');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_setting`
--

DROP TABLE IF EXISTS `app_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `app_setting` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `setting_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_setting`
--

LOCK TABLES `app_setting` WRITE;
/*!40000 ALTER TABLE `app_setting` DISABLE KEYS */;
INSERT INTO `app_setting` VALUES (3,'period_id','5');
/*!40000 ALTER TABLE `app_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive`
--

DROP TABLE IF EXISTS `archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `archive` (
  `id` int NOT NULL AUTO_INCREMENT,
  `createdAt` bigint DEFAULT NULL,
  `fromModel` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `originalRecord` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `originalRecordId` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive`
--

LOCK TABLES `archive` WRITE;
/*!40000 ALTER TABLE `archive` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `backup_topicselection_nolecturer`
--

DROP TABLE IF EXISTS `backup_topicselection_nolecturer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `backup_topicselection_nolecturer` (
  `id` int NOT NULL DEFAULT '0',
  `optionNum` double DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `topic_id` int DEFAULT NULL,
  `period_id` int DEFAULT NULL,
  `group_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `backup_topicselection_nolecturer`
--

LOCK TABLES `backup_topicselection_nolecturer` WRITE;
/*!40000 ALTER TABLE `backup_topicselection_nolecturer` DISABLE KEYS */;
INSERT INTO `backup_topicselection_nolecturer` VALUES (159,1,'APPROVED_LAB_RISET',71,5,116),(161,1,'APPROVED',66,5,118),(165,1,'APPROVED_LAB_RISET',65,5,122);
/*!40000 ALTER TABLE `backup_topicselection_nolecturer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bobot_dosen`
--

DROP TABLE IF EXISTS `bobot_dosen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bobot_dosen` (
  `id` int NOT NULL AUTO_INCREMENT,
  `dosen_pembimbing` int NOT NULL,
  `dosen_penguji` int NOT NULL,
  `period_id` int NOT NULL,
  `prodi_id` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bobot_dosen`
--

LOCK TABLES `bobot_dosen` WRITE;
/*!40000 ALTER TABLE `bobot_dosen` DISABLE KEYS */;
INSERT INTO `bobot_dosen` VALUES (1,60,40,5,1);
/*!40000 ALTER TABLE `bobot_dosen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clo`
--

DROP TABLE IF EXISTS `clo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `clo_code` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  `plo_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clo`
--

LOCK TABLES `clo` WRITE;
/*!40000 ALTER TABLE `clo` DISABLE KEYS */;
INSERT INTO `clo` VALUES (101,'CLO1','Mampu menetukan permasalahaan, dan solusi yang tepat dari proposal TA',0,102),(102,'CLO2','Mahasiswa mampu memberikan penjelasan mengenai latar belakang (pentingnya permasalahan) proposal TA berdasarkan sumber refernsi yang dapat dipercaya',0,102),(103,'CLO3','Mampu menghubungkan permasalahan pada prosposal TA dengan referensi yang sesuai ',0,102),(104,'CLO4','Mampu menentukan metode yang sesuai untuk mencapai tujuan pada proposal TA dan disertai dengan literatur sebagai dasar',0,102),(105,'CLO5','Mahasiswa mampu membuat Laporan proposal TA sesuai format dan panduan penulisan dengan bahas dan struktur yang baik',0,106);
/*!40000 ALTER TABLE `clo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eprt`
--

DROP TABLE IF EXISTS `eprt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `eprt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `file_name` varchar(50) NOT NULL,
  `score` double DEFAULT NULL,
  `eprtUrl` varchar(255) DEFAULT NULL,
  `eprtFd` varchar(255) DEFAULT NULL,
  `student_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `student_id` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eprt`
--

LOCK TABLES `eprt` WRITE;
/*!40000 ALTER TABLE `eprt` DISABLE KEYS */;
INSERT INTO `eprt` VALUES (3,'1202183291_EPRT_Ria Rahmawati',560,'/get-eprt/417/1202183291_EPRT_Ria Rahmawati','/var/www/aplikasita1/.tmp/uploads/28452a77-1337-4cf6-b704-6e86abaf7d44.pdf',417),(4,'EPRT Shafa Tathya Larasati',633,'/get-eprt/412/EPRT Shafa Tathya Larasati','/var/www/aplikasita1/.tmp/uploads/6e463c96-9d1c-4795-abb3-a4e5d134083c.pdf',412),(5,'EPRT_Arddhana Zhafran Amanullah_SI42GABx',587,'/get-eprt/427/EPRT_Arddhana Zhafran Amanullah_SI42GABx','/var/www/aplikasita1/.tmp/uploads/33c0addf-94ab-4d47-83ac-405b4bcaecbf.pdf',427),(6,'EPRT-1202184316-Yessy Permatasari',483,'/get-eprt/437/EPRT-1202184316-Yessy Permatasari','/var/www/aplikasita1/.tmp/uploads/72b35a5c-2e6f-49ff-b373-6b2d30308840.pdf',437),(7,'FATMA KURNIA FEBRIANTI_1202184297_EPrT',560,'/get-eprt/435/FATMA KURNIA FEBRIANTI_1202184297_EPrT','/var/www/aplikasita1/.tmp/uploads/26ca2fea-3c70-4069-9e9a-9e0dc7b55120.pdf',435),(8,'EPRT Ikhsan',450,'/get-eprt/434/EPRT Ikhsan','/var/www/aplikasita1/.tmp/uploads/2ab3e1fe-caf9-4038-a9d1-c22c3fc20cb3.pdf',434),(9,'Sonya Risnentri_Eprt',483,'/get-eprt/429/Sonya Risnentri_Eprt','/var/www/aplikasita1/.tmp/uploads/c8940a13-e2c7-4018-a8b0-e6082ec794fd.pdf',429),(10,'Ilma Nur Hidayati_EPRT',510,'/get-eprt/428/Ilma Nur Hidayati_EPRT','/var/www/aplikasita1/.tmp/uploads/177f4e0a-f8ea-4171-8960-78d4d8996042.pdf',428),(11,'zahwa_eprt',617,'/get-eprt/416/zahwa_eprt','/var/www/aplikasita1/.tmp/uploads/3e830076-73c4-426d-91a4-0733a11e1cc5.pdf',416),(12,'1202184310 - EPrT - Telkom University',560,'/get-eprt/436/1202184310 - EPrT - Telkom University','/var/www/aplikasita1/.tmp/uploads/ff6e6e4c-64dc-428a-a181-9660db74202e.pdf',436),(13,'EPrT_Nanda Arfan Hakim_1202184077',567,'/get-eprt/425/EPrT_Nanda Arfan Hakim_1202184077','/var/www/aplikasita1/.tmp/uploads/4a81f917-8af3-4820-9b67-d5980b9bf4a8.pdf',425),(14,'eprt_siti nihayatul choiriyah',493,'/get-eprt/418/eprt_siti nihayatul choiriyah','/var/www/aplikasita1/.tmp/uploads/6e702996-78e6-4c7b-b90b-1d67d10aa043.pdf',418),(15,'Pusat Bahasa _ Telkom University',527,'/get-eprt/419/Pusat Bahasa _ Telkom University','/var/www/aplikasita1/.tmp/uploads/a0dbeddb-7166-423b-bc82-613231ea54fe.pdf',419),(16,'Anastassya Gustirani_Sertifikat EPRT',597,'/get-eprt/422/Anastassya Gustirani_Sertifikat EPRT','/var/www/aplikasita1/.tmp/uploads/3ce5f761-3cef-428a-bedc-9ccc453164d8.pdf',422),(17,'Nilai EPRT_Izza Ariani_SI42GABX',507,'/get-eprt/430/Nilai EPRT_Izza Ariani_SI42GABX','/var/www/aplikasita1/.tmp/uploads/eb3357b2-9b85-4764-a70d-0d68406a8e6e.pdf',430),(18,'Nilai EPRT_1202184272',520,'/get-eprt/433/Nilai EPRT_1202184272','/var/www/aplikasita1/.tmp/uploads/f51f1517-c2cd-41dc-b5f1-fd5e8585f5ff.pdf',433),(19,'ISI4C2_EPrT_1202184209_Titisari Ramadhane',467,'/get-eprt/455/ISI4C2_EPrT_1202184209_Titisari Ramadhane','/var/www/aplikasita1/.tmp/uploads/c1c1b13d-20d7-4ad3-a1ca-b60723a19cee.pdf',455),(20,'SERTIFIKAT EPRT_TAUFIQ MAULANA FIRDAUS',567,'/get-eprt/415/SERTIFIKAT EPRT_TAUFIQ MAULANA FIRDAUS','/var/www/aplikasita1/.tmp/uploads/f6944582-a365-494e-855d-028d812e2e48.pdf',415),(21,'EPRT_TITOK AJI CAKRA',480,'/get-eprt/438/EPRT_TITOK AJI CAKRA','/var/www/aplikasita1/.tmp/uploads/175a96de-8bd0-4585-a0b2-d2b31322930b.pdf',438),(22,'1202184178 EPRT 2021',543,'/get-eprt/432/1202184178 EPRT 2021','/var/www/aplikasita1/.tmp/uploads/e5a5ba2e-5dbd-484a-9eca-6a4ce3e3b2c3.pdf',432),(23,'EPRT_Jasmine Aurely Salshabillah_1202183324',570,'/get-eprt/420/EPRT_Jasmine Aurely Salshabillah_1202183324','/var/www/aplikasita1/.tmp/uploads/d3ba8b74-4a52-4c9c-91bc-4433edc63520.pdf',420),(24,'MuhamadRidwanFirdaus_EPRT2021',443,'/get-eprt/421/MuhamadRidwanFirdaus_EPRT2021','/var/www/aplikasita1/.tmp/uploads/ac01ee55-009f-49fa-8d86-d56be0020894.pdf',421),(25,'EPRT_Yovita Margaret Abigail Purba_1202184112',537,'/get-eprt/426/EPRT_Yovita Margaret Abigail Purba_1202184112','/var/www/aplikasita1/.tmp/uploads/365fdced-d76b-460b-b3e2-bc494c09ba7f.pdf',426),(26,'eprt_1202180182',447,'/get-eprt/413/eprt_1202180182','/var/www/aplikasita1/.tmp/uploads/a88c71b7-55f0-4e46-87e1-d0f5a3f5df59.pdf',413),(27,'Nilai EPRT_Shahnaz Kamilah',487,'/get-eprt/423/Nilai EPRT_Shahnaz Kamilah','/var/www/aplikasita1/.tmp/uploads/1442346f-5725-4e89-8ff2-aaf49bf87cb7.pdf',423),(28,'Sertifikat EPrT',540,'/get-eprt/414/Sertifikat EPrT','/var/www/aplikasita1/.tmp/uploads/c99deb1e-08f5-4806-925e-e9e2190ffbd3.pdf',414),(29,'Tes EPRT - Risma Nur Damayanti',513,'/get-eprt/431/Tes EPRT - Risma Nur Damayanti','/var/www/aplikasita1/.tmp/uploads/a8d23a5b-c86a-41ff-a5a6-54aca453899e.pdf',431),(30,'EPRT April 2021',517,'/get-eprt/424/EPRT April 2021','/var/www/aplikasita1/.tmp/uploads/6c30a076-5335-4a62-8cc0-931acbb2d7a9.pdf',424),(31,'ISI4C2_EPrT_1202144167_LUTHFIANINGRUM',441,'/get-eprt/440/ISI4C2_EPrT_1202144167_LUTHFIANINGRUM','/var/www/aplikasita1/.tmp/uploads/2a138ab1-81f5-4aa0-9bc5-b648508684b8.pdf',440);
/*!40000 ALTER TABLE `eprt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group`
--

DROP TABLE IF EXISTS `group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `total_students` double DEFAULT NULL,
  `period_id` int DEFAULT NULL,
  `peminatan_id` int DEFAULT NULL,
  `owner` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group`
--

LOCK TABLES `group` WRITE;
/*!40000 ALTER TABLE `group` DISABLE KEYS */;
INSERT INTO `group` VALUES (116,1,5,4,425),(117,1,5,1,422),(118,3,5,3,427),(119,4,5,3,417),(120,1,5,5,438),(121,2,5,5,412),(122,2,5,4,426),(123,2,5,5,434),(124,3,5,4,437),(127,6,5,1,430),(129,3,5,5,455),(130,1,5,4,440),(131,1,5,5,442),(132,1,5,5,444),(133,1,5,4,446),(134,1,5,5,447),(135,1,5,5,448),(136,1,5,4,451),(137,1,5,5,452),(138,1,5,4,453),(139,1,5,4,454),(140,1,5,4,456),(141,2,5,4,416),(142,2,5,4,426),(143,3,5,3,427);
/*!40000 ALTER TABLE `group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_student`
--

DROP TABLE IF EXISTS `group_student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `group_student` (
  `id` int NOT NULL AUTO_INCREMENT,
  `group_id` int DEFAULT NULL,
  `student_id` int DEFAULT NULL,
  `lecturer_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_student`
--

LOCK TABLES `group_student` WRITE;
/*!40000 ALTER TABLE `group_student` DISABLE KEYS */;
INSERT INTO `group_student` VALUES (85,117,422,NULL),(86,118,428,NULL),(87,118,413,NULL),(88,118,427,NULL),(89,119,418,110),(90,119,419,111),(91,119,436,116),(92,119,417,116),(94,116,425,97),(95,116,416,97),(96,121,424,76),(97,121,412,76),(98,122,414,97),(99,122,426,97),(100,120,438,119),(101,120,429,119),(102,123,423,132),(103,123,434,132),(111,127,420,91),(112,127,435,91),(113,127,415,91),(114,127,422,91),(115,127,431,91),(116,127,430,91),(123,129,432,121),(124,129,433,121),(125,129,455,121),(126,124,421,124),(127,124,437,124),(128,130,440,115),(129,131,442,135),(130,132,444,135),(131,133,446,115),(132,134,447,87),(133,135,448,87),(134,136,451,127),(135,137,452,87),(136,138,453,124),(137,139,454,124),(138,140,456,127),(139,141,425,97),(140,141,416,97),(141,142,414,97),(142,142,426,97),(145,143,428,110),(146,143,413,134),(147,143,427,110);
/*!40000 ALTER TABLE `group_student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guidance_form`
--

DROP TABLE IF EXISTS `guidance_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `guidance_form` (
  `id` int NOT NULL AUTO_INCREMENT,
  `file_name` varchar(50) NOT NULL,
  `formUrl` varchar(255) DEFAULT NULL,
  `formFd` varchar(255) DEFAULT NULL,
  `student_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `student_id` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guidance_form`
--

LOCK TABLES `guidance_form` WRITE;
/*!40000 ALTER TABLE `guidance_form` DISABLE KEYS */;
INSERT INTO `guidance_form` VALUES (4,'1202183291_Form Bimbingan_Ria (1)','/get-form/417/1202183291_Form Bimbingan_Ria (1)','/var/www/aplikasita1/.tmp/uploads/652843d8-7c3e-49a9-92ef-5fca900f92f3.pdf',417),(5,'Ikhsan Tasef_FORM BERITA ACARA BIMBINGAN TA','/get-form/434/Ikhsan Tasef_FORM BERITA ACARA BIMBINGAN TA','/var/www/aplikasita1/.tmp/uploads/8d86140e-6089-4548-b917-9985655dda83.pdf',434),(6,'FORM BERITA ACARA BIMBINGAN TA1_SONYA RISNENTRI','/get-form/429/FORM BERITA ACARA BIMBINGAN TA1_SONYA RISNENTRI','/var/www/aplikasita1/.tmp/uploads/7d50d876-9103-4afd-9633-b0b8c0a1993b.pdf',429),(7,'1202183317_FormBimbingan_Siti Nihayatul Choiriyah','/get-form/418/1202183317_FormBimbingan_Siti Nihayatul Choiriyah','/var/www/aplikasita1/.tmp/uploads/be4028cb-1709-47f8-9ab2-c1e3555bc55f.pdf',418),(8,'FORMBERITAACARABIMBINGANTA-MuhamadRidwanFirdaus','/get-form/421/FORMBERITAACARABIMBINGANTA-MuhamadRidwanFirdaus','/var/www/aplikasita1/.tmp/uploads/bf925acd-2c14-4945-8db0-56976754f78c.pdf',421),(9,'Form Bimbingan TA Anastassya Gustirani','/get-form/422/Form Bimbingan TA Anastassya Gustirani','/var/www/aplikasita1/.tmp/uploads/3aca19dc-6296-4a46-ae90-85d4550029e0.pdf',422),(10,'FORM BIMBINGAN TA Izza Ariani','/get-form/430/FORM BIMBINGAN TA Izza Ariani','/var/www/aplikasita1/.tmp/uploads/1f44bedd-0b4e-46b9-96c2-7970c59ffd55.pdf',430),(11,'FORM BERITA ACARA BIMBINGAN TA1','/get-form/432/FORM BERITA ACARA BIMBINGAN TA1','/var/www/aplikasita1/.tmp/uploads/1dfc8f97-6744-4396-9302-86e974ffb467.pdf',432),(12,'ISI4C2_FormBimbingan_1202184209_Titisari Ramadhane','/get-form/455/ISI4C2_FormBimbingan_1202184209_Titisari Ramadhane','/var/www/aplikasita1/.tmp/uploads/f1144fb3-fb6d-490c-bb9e-17cc04085e0c.pdf',455),(13,'Form Bimbingan_1202184272','/get-form/433/Form Bimbingan_1202184272','/var/www/aplikasita1/.tmp/uploads/511e8ba3-75c4-4b4c-88c2-23e498c6cb38.pdf',433),(14,'Form Bimbingan TA Taufiq Maulana Firdaus','/get-form/415/Form Bimbingan TA Taufiq Maulana Firdaus','/var/www/aplikasita1/.tmp/uploads/8f69df38-c3a9-48f5-aa92-506a72063d4b.pdf',415),(15,'FORM_BIMBINGAN_ILMA NUR HIDAYATI','/get-form/428/FORM_BIMBINGAN_ILMA NUR HIDAYATI','/var/www/aplikasita1/.tmp/uploads/d931b67f-151e-4235-a3c7-361c999c0873.pdf',428),(16,'Bimbingan_1202184316','/get-form/437/Bimbingan_1202184316','/var/www/aplikasita1/.tmp/uploads/a1a307bd-8344-416f-bfc8-015c6a176409.pdf',437),(17,'FORM_BIMBINGAN_TA_ARDDHANA_1202184143','/get-form/427/FORM_BIMBINGAN_TA_ARDDHANA_1202184143','/var/www/aplikasita1/.tmp/uploads/8b8d30f3-099b-45ac-b672-229aad3e56aa.pdf',427),(18,'1202184310_Form Bimbingan TA_Muhammad Difagama I','/get-form/436/1202184310_Form Bimbingan TA_Muhammad Difagama I','/var/www/aplikasita1/.tmp/uploads/1b54f640-e68e-4fe0-bc12-6df4e4dec016.pdf',436),(19,'FormBimbinganTA1_Shafa Tathya Larasati','/get-form/412/FormBimbinganTA1_Shafa Tathya Larasati','/var/www/aplikasita1/.tmp/uploads/dcbed071-a3b6-42fe-b6ef-4db8f3024e66.pdf',412),(20,'FORM BERITA ACARA BIMBINGAN TA1_TITOK AJI CAKRA','/get-form/438/FORM BERITA ACARA BIMBINGAN TA1_TITOK AJI CAKRA','/var/www/aplikasita1/.tmp/uploads/feb39877-ae9b-459e-a000-463594aca4b8.pdf',438),(21,'Form Bimbingan_Nanda Arfan Hakim_1202184077','/get-form/425/Form Bimbingan_Nanda Arfan Hakim_1202184077','/var/www/aplikasita1/.tmp/uploads/13d9feed-cc84-44ff-9bd5-e9e38db17054.pdf',425),(22,'Form_Jasmine Aurely Salshabillah_1202183324','/get-form/420/Form_Jasmine Aurely Salshabillah_1202183324','/var/www/aplikasita1/.tmp/uploads/a7229f5d-9072-4099-b5e7-b53ccdaf6cd4.pdf',420),(23,'Form Bimbingan_Yovita Margaret Abigail_1202184112','/get-form/426/Form Bimbingan_Yovita Margaret Abigail_1202184112','/var/www/aplikasita1/.tmp/uploads/0a0f6db8-459d-4440-b73f-41ee935ba6dc.pdf',426),(24,'FORM_BIMGBINGAN','/get-form/413/FORM_BIMGBINGAN','/var/www/aplikasita1/.tmp/uploads/a53e3140-0c1b-476c-ad0c-d42b589803f6.pdf',413),(25,'Shahnaz Kamilah _ FORM BERITA ACARA BIMBINGAN TA','/get-form/423/Shahnaz Kamilah _ FORM BERITA ACARA BIMBINGAN TA','/var/www/aplikasita1/.tmp/uploads/11efe8ff-3a06-406b-9b99-fa65163d4046.pdf',423),(26,'FORM_BIMBINGAN_ZAHWA','/get-form/416/FORM_BIMBINGAN_ZAHWA','/var/www/aplikasita1/.tmp/uploads/1301bcef-b906-4354-beaa-e4567ccdf8ec.pdf',416),(27,'Form Bimbingan TA_Risma Nur Damayanti','/get-form/431/Form Bimbingan TA_Risma Nur Damayanti','/var/www/aplikasita1/.tmp/uploads/235b552f-7bb5-47fc-98fe-a85c0ea2a814.pdf',431),(28,'FormBimbinganTA1_I Gede Pasek Punia A.','/get-form/424/FormBimbinganTA1_I Gede Pasek Punia A.','/var/www/aplikasita1/.tmp/uploads/b8002036-9253-490e-93d3-ddb6e05daca4.pdf',424),(29,'ISI4C2_FormBimbingan_1202144167_LUTHFIANINGRUMK','/get-form/440/ISI4C2_FormBimbingan_1202144167_LUTHFIANINGRUMK','/var/www/aplikasita1/.tmp/uploads/d40b9f24-5e0a-478b-9804-ac20446cc1c7.pdf',440),(30,'FORM BIMBINGAN METLIT final','/get-form/446/FORM BIMBINGAN METLIT final','/var/www/aplikasita1/.tmp/uploads/d41ae769-72c4-48c1-90f5-bae91ef89bb6.pdf',446),(31,'1202184297_FATMA KURNIA FEBRIANTI_FORM BIMBINGAN','/get-form/435/1202184297_FATMA KURNIA FEBRIANTI_FORM BIMBINGAN','/var/www/aplikasita1/.tmp/uploads/3c389a1d-aa79-4908-af9a-9957ddb6c337.pdf',435),(40,'Form Bimbingan_Ifen Faridian Rahmadan_1202180229','/get-form/414/Form Bimbingan_Ifen Faridian Rahmadan_1202180229','/var/www/aplikasita1/.tmp/uploads/fdd91e76-c1bc-4442-bc08-d66bd1eb2463.PDF',414);
/*!40000 ALTER TABLE `guidance_form` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jfa`
--

DROP TABLE IF EXISTS `jfa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jfa` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `abbrev` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `abbrev` (`abbrev`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jfa`
--

LOCK TABLES `jfa` WRITE;
/*!40000 ALTER TABLE `jfa` DISABLE KEYS */;
INSERT INTO `jfa` VALUES (6,'Non Jabatan Fungsional','NJFA'),(7,'Asisten Ahli','AA'),(8,'Lektor','L'),(9,'Lektor Kepala','LK'),(10,'Professor','Prof');
/*!40000 ALTER TABLE `jfa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kk`
--

DROP TABLE IF EXISTS `kk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `kk` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `abbrev` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `abbrev` (`abbrev`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kk`
--

LOCK TABLES `kk` WRITE;
/*!40000 ALTER TABLE `kk` DISABLE KEYS */;
INSERT INTO `kk` VALUES (3,'Cybernetics','Cyb'),(4,'Enterprise and Industrial Systems','Eins'),(5,'Fakultas Rekayasa Industri','FRI');
/*!40000 ALTER TABLE `kk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lab_riset`
--

DROP TABLE IF EXISTS `lab_riset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lab_riset` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `abbrev` varchar(255) DEFAULT NULL,
  `kk_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `abbrev` (`abbrev`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lab_riset`
--

LOCK TABLES `lab_riset` WRITE;
/*!40000 ALTER TABLE `lab_riset` DISABLE KEYS */;
INSERT INTO `lab_riset` VALUES (1,'System Architecture and Governance','SAG',4),(2,'Enterprise Data Engineering','EDE',3),(3,'Enterprise System Development','ESD',3),(4,'Enterprise Infrastructure Management','EIM',4),(6,'Enterprise Intelligent System','EIS',3),(7,'Enterprise System and Solution','ESS',4),(8,'E-Logistic and Suplplay Chain','REALISM',4),(9,'KP Fakultas Rekayasa Industri','KP FRI',5);
/*!40000 ALTER TABLE `lab_riset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lecturer`
--

DROP TABLE IF EXISTS `lecturer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lecturer` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nik` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `hashed_password` varchar(255) DEFAULT NULL,
  `new_password_token` varchar(255) DEFAULT NULL,
  `new_password_token_expires_at` longtext,
  `lecturer_code` varchar(255) DEFAULT NULL,
  `jfa_id` int DEFAULT NULL,
  `lab_riset_id` int DEFAULT NULL,
  `prodi_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `nik` (`nik`)
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lecturer`
--

LOCK TABLES `lecturer` WRITE;
/*!40000 ALTER TABLE `lecturer` DISABLE KEYS */;
INSERT INTO `lecturer` VALUES (67,'123','Test',NULL,'$2b$10$7gcsOenClsfHDq2ybkDc8unvPV5psgFu9EGUzb7b2s7cnhu1/pvdO',NULL,NULL,'',NULL,NULL,NULL),(69,'16880020','ASTI AMALIA NUR FAJRILLAH','astiamalia@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'NFL',7,1,1),(70,'10760011','DR. BASUKI RAHMAD','basuki@transforma-institute.biz','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'BSR',8,1,1),(71,'18930095','BERLIAN MAULIDYA IZZATI_1','berlianmi@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'BMZ',7,1,1),(72,'10760007','RIDHA HANAFI','ridhanafi@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'RDF',7,1,1),(73,'10790021','YULI ADAM PRASETYO','adam@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'YAP',8,1,1),(74,'18880133','IQBAL SANTOSA_1','iqbals@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'IQO',7,1,1),(75,'12630005','LUKMAN ABDURRAHMAN','abdural@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'LMN',8,1,1),(76,'15890025','LUTHFI RAMADANI','luthfi@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'LRI',7,1,1),(77,'5820066','MURAHARTAWATY','murahartawaty@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'MHY',7,1,1),(78,'10780050','RAHMAT MULYANA','rahmatmoelyana@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'RMM',6,1,1),(79,'18820089','ROKHMAN FAUZI_1','rokhmanfauzi@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'RKZ',7,1,1),(80,'18860125','MUHARMAN LUBIS_1','muharmanlubis@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'MRL',8,1,1),(81,'13710054','ADITYAS WIDJAJARTO_1','adtwjrt@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'AWJ',7,4,1),(82,'17890112','AHMAD ALMAARIF_1','ahmadalmaarif@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'LIF',7,4,1),(83,'13860085','M. TEGUH KURNIAWAN','teguhkurniawan@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'MTK',8,4,1),(84,'760016','RD. ROHMAT SAEDUDIN','rdrohmat@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'ROH',8,4,1),(85,'14840067','UMAR YUNAN KURNIA SEPTO HEDIYANTO','umaryunan@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'UMY',7,4,1),(86,'20910013','M FATHINUDDIN','muhammadfathinuddin@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'MFA',6,4,1),(87,'14660049','ARI FAJAR SANTOSO','arifajar@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'ARJ',7,7,1),(88,'20750005','AVON BUDIONO','avonbudi@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'AVB',8,7,1),(89,'18890136','MUHARDI SAPUTRA_1','muhardi@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'UHS',7,7,1),(90,'14690010','R. WAHJOE WITJAKSONO','wahyuwicaksono@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'RWW',7,7,1),(91,'10820005','WARIH PUSPITASARI','warihpuspitasari@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'WRP',7,7,1),(92,'20610001','IR. AHMAD MUSNANSYAH','ahmadanc@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'MSN',8,6,1),(93,'14900050','FAISHAL MUFIED AL ANSHARY','faishalmufied@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'FMA',7,3,1),(94,'10740059','ILHAM PERDANA','ilhamp@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'ILD',8,3,1),(95,'14770014','NIA AMBARSARI','niaambarsari@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'NAS',8,3,1),(96,'15830041','NUR ICHSAN UTAMA','nichsan@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'NIU',7,2,1),(97,'19900023','RAHMAT FAUZI','rahmatfauzi@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'RFZ',7,3,1),(98,'10780051','SENO ADI PUTRA','adiputra@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'SNP',8,6,1),(99,'14790008','TIEN FABRIANTI KUSUMASARI','tienkusumasari@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'TFD',7,3,1),(100,'18890133','ALVI SYAHRINA','syahrina@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'ALV',6,3,1),(101,'15900004','PUTRA FAJAR ALAM','putrafajaralam@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'PFA',7,3,1),(102,'14830024','TAUFIK NUR ADI','taufikna@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'TNA',7,2,1),(103,'20920013','EKKY NOVRIZA ALAM','ekkynovrizalam@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'ENA',6,3,1),(104,'13840043','ALBI FITRANSYAH','albifit@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'ABF',6,2,1),(105,'14720028','DEDEN WITARSYAH','dedenw@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'DWH',8,2,1),(106,'17890109','EDI SUTOYO_1','edisutoyo@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'ETO',7,2,1),(107,'14850055','MUHAMMAD AZANI HASIBUAN','muhammadazani@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'MAZ',7,2,1),(108,'14890057','RACHMADITA ANDRESWARI','andreswari@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'RHA',7,2,1),(109,'8760034','RIZA AGUSTIANSYAH','rizaagustiansyah@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'RIZ',7,2,1),(110,'20870013','OKTARIANI NURUL PRATIWI','onurulp@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'ONP',8,2,1),(111,'20890010','FAQIH HAMAMI','faqihhamami@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'FQH',6,2,1),(112,'20920012','DITA PRAMESTI','ditapramesti@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'DTP',6,2,1),(113,'20820007','SINUNG SUAKANTO','sinung@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'SNG',8,3,1),(114,'20800011','HANIF FAKHRURROJA','haniff@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'HFR',6,2,1),(115,'20910042','MARGARETA HARDIYANTI','margareta@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'MGH',6,3,1),(116,'20900032','RISKA YANU FA\'RIFAH','riskayanu@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'RYF',7,1,1),(117,'20900034','VANDHA PRADWIYASMA WIDARTHA','vandhapw@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'VPW',6,2,1),(118,'14750043','SONI FAJAR SURYA GUMILANG','mustonie@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'SFJ',8,1,1),(119,'20920014','FITRIYANA DEWI','fitriyanadewi@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'FDE',6,1,1),(120,'20940008','FALAHAH','falahah@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'FLH',8,1,1),(121,'20940031','WIDYATASYA AGUSTIKA NURTRISHA','widyatasya@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'WDG',6,1,1),(122,'20830006','RYAN ADHITYA NUGRAHA','ranugraha@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'RUA',6,1,1),(123,'14780007','ARI YANUAR RIDWAN','ariyanuar@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'YRW',8,8,1),(124,'15620009','TATANG MULYANA','tatangmulyana@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'TTG',8,6,1),(125,'17890111','HILMAN DWI ANGGANA','hilmandwianggana@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'HDG',7,6,1),(126,'19870001','MOHAMMAD DENI AKBAR','denimath@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'MDE',6,6,1),(127,'15840079','NOPENDRI, S.SI.,M.SI','nopendri@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'NPD',7,6,1),(128,'20930054','LUTFIA SEPTININGRUM','lutfiaseptiningrum@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'LFS',6,7,1),(130,'20820004','ROKHMAN FAUZI','rokhmanfauzi@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'RKZ',7,1,1),(131,'19860002','MUHARMAN LUBIS','muharmanlubis@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'MRL',8,1,1),(132,'20930041','BERLIAN MAULIDYA IZZATI','berlianmi@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'BMZ',7,1,1),(133,'20890020','AHMAD ALMAARIF','ahmadalmaarif@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'LIF',7,4,1),(134,'19890015','EDI SUTOYO','edisutoyo@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'ETO',7,2,1),(135,'20880001','IQBAL SANTOSA','iqbals@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'IQO',7,1,1),(136,'20710005','ADITYAS WIDJAJARTO','adtwjrt@telkomuniversity.ac.id','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,'AWJ',7,4,1),(137,'20760002','RIDHA HANAFI','ridhanafi@telkomuniversity.ac.id',NULL,NULL,NULL,'RDF',NULL,NULL,NULL),(138,'20890019','MUHARDI SAPUTRA','muhardi@telkomuniversity.ac.id',NULL,NULL,NULL,'UHS',7,7,1);
/*!40000 ALTER TABLE `lecturer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lecturer_roles__masterrole_roles_masterrole`
--

DROP TABLE IF EXISTS `lecturer_roles__masterrole_roles_masterrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lecturer_roles__masterrole_roles_masterrole` (
  `id` int NOT NULL AUTO_INCREMENT,
  `lecturer_roles` int DEFAULT NULL,
  `masterrole_roles_masterrole` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lecturer_roles__masterrole_roles_masterrole`
--

LOCK TABLES `lecturer_roles__masterrole_roles_masterrole` WRITE;
/*!40000 ALTER TABLE `lecturer_roles__masterrole_roles_masterrole` DISABLE KEYS */;
INSERT INTO `lecturer_roles__masterrole_roles_masterrole` VALUES (59,76,9),(60,76,10),(61,68,9),(62,68,10),(63,99,10),(64,99,9),(65,134,9),(66,134,10);
/*!40000 ALTER TABLE `lecturer_roles__masterrole_roles_masterrole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lecturerquota`
--

DROP TABLE IF EXISTS `lecturerquota`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lecturerquota` (
  `id` int NOT NULL AUTO_INCREMENT,
  `lecturer_quota` double DEFAULT NULL,
  `lecturer_id` int DEFAULT NULL,
  `period_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lecturerquota`
--

LOCK TABLES `lecturerquota` WRITE;
/*!40000 ALTER TABLE `lecturerquota` DISABLE KEYS */;
/*!40000 ALTER TABLE `lecturerquota` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_role`
--

DROP TABLE IF EXISTS `master_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `master_role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `resourceId` double DEFAULT NULL,
  `resourceType` varchar(255) DEFAULT NULL,
  `isCoordinator` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_role`
--

LOCK TABLES `master_role` WRITE;
/*!40000 ALTER TABLE `master_role` DISABLE KEYS */;
INSERT INTO `master_role` VALUES (9,'Koordinator Metlit',NULL,'metlit',1),(10,'Ketua LabRiset',NULL,'labriset',1);
/*!40000 ALTER TABLE `master_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metlit`
--

DROP TABLE IF EXISTS `metlit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `metlit` (
  `id` int NOT NULL AUTO_INCREMENT,
  `class` varchar(255) DEFAULT NULL,
  `period_id` int DEFAULT NULL,
  `prodi_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metlit`
--

LOCK TABLES `metlit` WRITE;
/*!40000 ALTER TABLE `metlit` DISABLE KEYS */;
INSERT INTO `metlit` VALUES (14,'SI-42-GABX',5,1),(15,'SI-42-GAB1',5,1),(16,'SI-42-GAB1	',5,2),(17,'TI-42-GAB',5,2);
/*!40000 ALTER TABLE `metlit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `run_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'/20210704160502-add-score-id-auto-increment','2021-07-04 23:36:45'),(2,'/20210704234702-score-pembimbing','2021-07-05 09:49:26'),(3,'/20210704234745-score-penguji','2021-07-05 09:49:26');
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mycontroller`
--

DROP TABLE IF EXISTS `mycontroller`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mycontroller` (
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mycontroller`
--

LOCK TABLES `mycontroller` WRITE;
/*!40000 ALTER TABLE `mycontroller` DISABLE KEYS */;
/*!40000 ALTER TABLE `mycontroller` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `peminatan`
--

DROP TABLE IF EXISTS `peminatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `peminatan` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `abbrev` varchar(255) DEFAULT NULL,
  `prodi_id` int DEFAULT NULL,
  `labRisetInduk` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `abbrev` (`abbrev`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `peminatan`
--

LOCK TABLES `peminatan` WRITE;
/*!40000 ALTER TABLE `peminatan` DISABLE KEYS */;
INSERT INTO `peminatan` VALUES (1,'Enterprise Resource Planning','ERP',1,7),(2,'Enterprise Infrastructure Management','EIM',1,4),(3,'Enterprise Data Engineering','EDE',1,2),(4,'Enterprise Intelligent System Development','EISD',1,3),(5,'System Architecture and Governance','SAG',1,1),(8,'Prodi S1 Teknik Industri','S1 TI',2,9),(9,'Prodi S1 Sistem Informasi','S1 SI',2,9);
/*!40000 ALTER TABLE `peminatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `period`
--

DROP TABLE IF EXISTS `period`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `period` (
  `id` int NOT NULL AUTO_INCREMENT,
  `semester` varchar(255) DEFAULT NULL,
  `academic_year` varchar(255) DEFAULT NULL,
  `per_lecturer_quota` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `period`
--

LOCK TABLES `period` WRITE;
/*!40000 ALTER TABLE `period` DISABLE KEYS */;
INSERT INTO `period` VALUES (5,'GENAP','2020/2021',20);
/*!40000 ALTER TABLE `period` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plo`
--

DROP TABLE IF EXISTS `plo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `plo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `plo_code` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `period_id` int NOT NULL,
  `prodi_id` int NOT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plo`
--

LOCK TABLES `plo` WRITE;
/*!40000 ALTER TABLE `plo` DISABLE KEYS */;
INSERT INTO `plo` VALUES (102,'PLO2','Kemampuan menganalisis permasalahan, melakukan identifikasi dan mendefinisikan kebutuhan komputasi yang bersesuaian dengan solusi',5,1,0),(106,'PLO6','Kemampuan untuk berkomunikasi secara efektif dengan peserta yang beragam',5,1,0);
/*!40000 ALTER TABLE `plo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodi`
--

DROP TABLE IF EXISTS `prodi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prodi` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `abbrev` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodi`
--

LOCK TABLES `prodi` WRITE;
/*!40000 ALTER TABLE `prodi` DISABLE KEYS */;
INSERT INTO `prodi` VALUES (1,'S1 Sistem Informasi','S1 SI'),(2,'Fakultas Rekayasa Industri','FRI');
/*!40000 ALTER TABLE `prodi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project` (
  `id` int NOT NULL AUTO_INCREMENT,
  `topic_title` varchar(255) DEFAULT NULL,
  `topic_title_status` varchar(255) DEFAULT NULL,
  `file_link` varchar(255) DEFAULT NULL,
  `final_score` double DEFAULT NULL,
  `student_id` int DEFAULT NULL,
  `topic_id` int DEFAULT NULL,
  `period_id` int DEFAULT NULL,
  `reviewer_id` int DEFAULT NULL,
  `reviewerScoreInRubric` int DEFAULT NULL,
  `supervisorScoreInRubric` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `score`
--

DROP TABLE IF EXISTS `score`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `score` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_id` int NOT NULL,
  `lecturer_id` int NOT NULL,
  `rubric_id` int NOT NULL,
  `sub_clo_id` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=651 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `score`
--

LOCK TABLES `score` WRITE;
/*!40000 ALTER TABLE `score` DISABLE KEYS */;
INSERT INTO `score` VALUES (1,425,99,103025,302),(2,425,99,103054,305),(3,425,99,103015,301),(4,425,99,103035,303),(5,425,99,103045,304),(6,425,99,103065,306),(7,425,99,103074,307),(8,425,99,103084,308),(9,438,130,103025,302),(10,438,130,103035,303),(11,438,130,103055,305),(12,438,130,103045,304),(13,438,130,103014,301),(14,438,130,103064,306),(15,438,130,103074,307),(16,438,130,103084,308),(17,429,130,103025,302),(18,429,130,103035,303),(19,429,130,103045,304),(20,429,130,103055,305),(21,429,130,103014,301),(22,429,130,103064,306),(23,429,130,103074,307),(24,429,130,103084,308),(25,417,116,103025,302),(26,417,116,103044,304),(27,417,116,103015,301),(28,417,116,103054,305),(29,417,116,103034,303),(30,417,116,103065,306),(31,417,116,103074,307),(32,417,116,103085,308),(33,436,116,103034,303),(34,436,116,103044,304),(35,436,116,103054,305),(36,436,116,103065,306),(37,436,116,103024,302),(38,436,116,103015,301),(39,436,116,103074,307),(40,436,116,103085,308),(41,440,115,103022,302),(42,440,115,103052,305),(43,440,115,103013,301),(44,440,115,103034,303),(45,440,115,103043,304),(46,440,115,103064,306),(47,440,115,103073,307),(48,440,115,103082,308),(49,429,119,103023,302),(50,429,119,103033,303),(51,429,119,103043,304),(52,429,119,103052,305),(53,429,119,103061,306),(54,429,119,103012,301),(55,429,119,103073,307),(56,429,119,103083,308),(57,432,121,103024,302),(58,432,121,103035,303),(59,432,121,103044,304),(60,432,121,103064,306),(61,432,121,103054,305),(62,432,121,103014,301),(63,432,121,103074,307),(64,432,121,103085,308),(65,455,121,103024,302),(66,455,121,103035,303),(67,455,121,103044,304),(68,455,121,103054,305),(69,455,121,103015,301),(70,455,121,103064,306),(71,455,121,103074,307),(72,455,121,103085,308),(73,432,135,103025,302),(74,432,135,103035,303),(75,432,135,103055,305),(76,432,135,103045,304),(77,432,135,103015,301),(78,432,135,103064,306),(79,432,135,103084,308),(80,432,135,103075,307),(81,433,121,103024,302),(82,433,121,103035,303),(83,433,121,103044,304),(84,433,121,103054,305),(85,433,121,103014,301),(86,433,121,103063,306),(87,433,121,103074,307),(88,433,121,103085,308),(89,438,119,103023,302),(90,438,119,103043,304),(91,438,119,103034,303),(92,438,119,103054,305),(93,438,119,103013,301),(94,438,119,103063,306),(95,438,119,103072,307),(96,438,119,103083,308),(97,418,108,103033,303),(98,418,108,103023,302),(99,418,108,103044,304),(100,418,108,103054,305),(101,418,108,103013,301),(102,418,108,103064,306),(103,418,108,103073,307),(104,418,108,103084,308),(105,419,108,103024,302),(106,419,108,103034,303),(107,419,108,103045,304),(108,419,108,103054,305),(109,419,108,103065,306),(110,419,108,103014,301),(111,419,108,103073,307),(112,419,108,103084,308),(113,436,108,103025,302),(114,436,108,103034,303),(115,436,108,103044,304),(116,436,108,103014,301),(117,436,108,103055,305),(118,436,108,103065,306),(119,436,108,103074,307),(120,436,108,103084,308),(121,417,108,103025,302),(122,417,108,103034,303),(123,417,108,103055,305),(124,417,108,103013,301),(125,417,108,103064,306),(126,417,108,103044,304),(127,417,108,103074,307),(128,417,108,103084,308),(129,423,132,103022,302),(130,423,132,103032,303),(131,423,132,103052,305),(132,423,132,103012,301),(133,423,132,103062,306),(134,423,132,103042,304),(135,423,132,103072,307),(136,423,132,103081,308),(137,442,122,103044,304),(138,442,122,103014,301),(139,442,122,103074,307),(140,442,122,103034,303),(141,442,122,103064,306),(142,442,122,103024,302),(143,442,122,103054,305),(144,442,122,103084,308),(145,444,122,103054,305),(146,444,122,103064,306),(147,444,122,103034,303),(148,444,122,103044,304),(149,444,122,103024,302),(150,444,122,103014,301),(151,444,122,103074,307),(152,444,122,103084,308),(153,433,135,103025,302),(154,433,135,103035,303),(155,433,135,103045,304),(156,433,135,103064,306),(157,433,135,103015,301),(158,433,135,103055,305),(159,433,135,103075,307),(160,433,135,103084,308),(161,455,135,103025,302),(162,455,135,103035,303),(163,455,135,103045,304),(164,455,135,103055,305),(165,455,135,103015,301),(166,455,135,103064,306),(167,455,135,103075,307),(168,455,135,103084,308),(169,444,135,103013,301),(170,444,135,103024,302),(171,444,135,103034,303),(172,444,135,103044,304),(173,444,135,103052,305),(174,444,135,103064,306),(175,444,135,103073,307),(176,444,135,103083,308),(177,442,135,103024,302),(178,442,135,103034,303),(179,442,135,103045,304),(180,442,135,103053,305),(181,442,135,103013,301),(182,442,135,103064,306),(183,442,135,103072,307),(184,442,135,103083,308),(185,416,99,103025,302),(186,416,99,103035,303),(187,416,99,103044,304),(188,416,99,103055,305),(189,416,99,103014,301),(190,416,99,103063,306),(191,416,99,103084,308),(192,416,99,103073,307),(193,446,115,103032,303),(194,446,115,103043,304),(195,446,115,103013,301),(196,446,115,103023,302),(197,446,115,103062,306),(198,446,115,103051,305),(199,446,115,103073,307),(200,446,115,103083,308),(201,452,131,103023,302),(202,452,131,103043,304),(203,452,131,103062,306),(204,452,131,103053,305),(205,452,131,103032,303),(206,452,131,103013,301),(207,452,131,103072,307),(208,452,131,103082,308),(209,440,95,103023,302),(210,440,95,103074,307),(211,440,95,103084,308),(212,440,95,103034,303),(213,440,95,103043,304),(214,440,95,103063,306),(215,440,95,103053,305),(216,440,95,103013,301),(217,446,95,103023,302),(218,446,95,103063,306),(219,446,95,103084,308),(220,446,95,103073,307),(221,446,95,103034,303),(222,446,95,103044,304),(223,446,95,103014,301),(224,446,95,103053,305),(225,420,91,103033,303),(226,420,91,103023,302),(227,420,91,103044,304),(228,420,91,103064,306),(229,420,91,103053,305),(230,420,91,103013,301),(231,420,91,103084,308),(232,420,91,103074,307),(233,425,97,103035,303),(234,425,97,103025,302),(235,425,97,103044,304),(236,425,97,103063,306),(237,425,97,103054,305),(238,425,97,103014,301),(239,425,97,103073,307),(240,425,97,103084,308),(241,414,97,103023,302),(242,414,97,103044,304),(243,414,97,103054,305),(244,414,97,103033,303),(245,414,97,103012,301),(246,414,97,103062,306),(247,414,97,103072,307),(248,414,97,103084,308),(249,426,97,103024,302),(250,426,97,103044,304),(251,426,97,103054,305),(252,426,97,103063,306),(253,426,97,103034,303),(254,426,97,103014,301),(255,426,97,103073,307),(256,426,97,103084,308),(257,434,132,103052,305),(258,434,132,103042,304),(259,434,132,103012,301),(260,434,132,103062,306),(261,434,132,103032,303),(262,434,132,103022,302),(263,434,132,103072,307),(264,434,132,103081,308),(265,416,97,103023,302),(266,416,97,103034,303),(267,416,97,103043,304),(268,416,97,103013,301),(269,416,97,103054,305),(270,416,97,103063,306),(271,416,97,103074,307),(272,416,97,103084,308),(273,447,132,103033,303),(274,447,132,103023,302),(275,447,132,103013,301),(276,447,132,103061,306),(277,447,132,103044,304),(278,447,132,103053,305),(279,447,132,103073,307),(280,447,132,103083,308),(281,423,76,103044,304),(282,423,76,103024,302),(283,423,76,103034,303),(284,423,76,103051,305),(285,423,76,103013,301),(286,423,76,103063,306),(287,423,76,103074,307),(288,423,76,103083,308),(289,434,76,103024,302),(290,434,76,103051,305),(291,434,76,103013,301),(292,434,76,103063,306),(293,434,76,103044,304),(294,434,76,103034,303),(295,434,76,103074,307),(296,434,76,103083,308),(323,414,99,103024,302),(324,414,99,103034,303),(325,414,99,103044,304),(326,414,99,103054,305),(327,414,99,103013,301),(328,414,99,103064,306),(329,414,99,103073,307),(330,414,99,103084,308),(331,435,91,103064,306),(332,435,91,103013,301),(333,435,91,103033,303),(334,435,91,103044,304),(335,435,91,103023,302),(336,435,91,103053,305),(337,435,91,103084,308),(338,435,91,103074,307),(339,415,91,103053,305),(340,415,91,103044,304),(341,415,91,103013,301),(342,415,91,103023,302),(343,415,91,103033,303),(344,415,91,103064,306),(345,415,91,103074,307),(346,415,91,103084,308),(347,422,91,103013,301),(348,422,91,103023,302),(349,422,91,103033,303),(350,422,91,103044,304),(351,422,91,103053,305),(352,422,91,103064,306),(353,422,91,103074,307),(354,422,91,103084,308),(355,431,91,103053,305),(356,431,91,103013,301),(357,431,91,103044,304),(358,431,91,103064,306),(359,431,91,103023,302),(360,431,91,103033,303),(361,431,91,103074,307),(362,431,91,103084,308),(363,430,91,103044,304),(364,430,91,103023,302),(365,430,91,103064,306),(366,430,91,103033,303),(367,430,91,103013,301),(368,430,91,103053,305),(369,430,91,103084,308),(370,430,91,103074,307),(371,418,110,103054,305),(372,418,110,103064,306),(373,418,110,103034,303),(374,418,110,103024,302),(375,418,110,103014,301),(376,418,110,103044,304),(377,418,110,103074,307),(378,418,110,103084,308),(379,428,110,103014,301),(380,428,110,103054,305),(381,428,110,103044,304),(382,428,110,103034,303),(383,428,110,103024,302),(384,428,110,103065,306),(385,428,110,103074,307),(386,428,110,103084,308),(387,427,110,103034,303),(388,427,110,103023,302),(389,427,110,103013,301),(390,427,110,103054,305),(391,427,110,103044,304),(392,427,110,103064,306),(393,427,110,103074,307),(394,427,110,103084,308),(395,426,99,103014,301),(396,426,99,103024,302),(397,426,99,103045,304),(398,426,99,103034,303),(399,426,99,103054,305),(400,426,99,103064,306),(401,426,99,103074,307),(402,426,99,103084,308),(403,448,119,103023,302),(404,448,119,103033,303),(405,448,119,103043,304),(406,448,119,103053,305),(407,448,119,103013,301),(408,448,119,103063,306),(409,448,119,103073,307),(410,448,119,103083,308),(411,421,114,103024,302),(412,421,114,103034,303),(413,421,114,103055,305),(414,421,114,103014,301),(415,421,114,103045,304),(416,421,114,103065,306),(417,421,114,103074,307),(418,421,114,103084,308),(419,437,114,103025,302),(420,437,114,103015,301),(421,437,114,103034,303),(422,437,114,103045,304),(423,437,114,103055,305),(424,437,114,103065,306),(425,437,114,103085,308),(426,437,114,103075,307),(427,428,111,103025,302),(428,428,111,103035,303),(429,428,111,103045,304),(430,428,111,103055,305),(431,428,111,103015,301),(432,428,111,103065,306),(433,428,111,103075,307),(434,428,111,103085,308),(435,413,111,103025,302),(436,413,111,103035,303),(437,413,111,103045,304),(438,413,111,103055,305),(439,413,111,103015,301),(440,413,111,103064,306),(441,413,111,103084,308),(442,413,111,103075,307),(443,427,111,103025,302),(444,427,111,103035,303),(445,427,111,103045,304),(446,427,111,103054,305),(447,427,111,103013,301),(448,427,111,103064,306),(449,427,111,103075,307),(450,427,111,103085,308),(451,419,111,103025,302),(452,419,111,103035,303),(453,419,111,103055,305),(454,419,111,103045,304),(455,419,111,103015,301),(456,419,111,103065,306),(457,419,111,103075,307),(458,419,111,103085,308),(459,421,124,103024,302),(460,421,124,103054,305),(461,421,124,103034,303),(462,421,124,103044,304),(463,421,124,103014,301),(464,421,124,103074,307),(465,421,124,103064,306),(466,421,124,103084,308),(467,437,124,103045,304),(468,437,124,103024,302),(469,437,124,103055,305),(470,437,124,103014,301),(471,437,124,103064,306),(472,437,124,103034,303),(473,437,124,103074,307),(474,437,124,103084,308),(475,448,87,103023,302),(476,448,87,103033,303),(477,448,87,103044,304),(478,448,87,103063,306),(479,448,87,103053,305),(480,448,87,103014,301),(481,448,87,103074,307),(482,448,87,103084,308),(483,447,87,103053,305),(484,447,87,103013,301),(485,447,87,103044,304),(486,447,87,103024,302),(487,447,87,103033,303),(488,447,87,103064,306),(489,447,87,103073,307),(490,447,87,103084,308),(491,452,87,103024,302),(492,452,87,103044,304),(493,452,87,103014,301),(494,452,87,103034,303),(495,452,87,103064,306),(496,452,87,103053,305),(497,452,87,103084,308),(498,452,87,103074,307),(499,453,124,103024,302),(500,453,124,103034,303),(501,453,124,103044,304),(502,453,124,103053,305),(503,453,124,103014,301),(504,453,124,103063,306),(505,453,124,103073,307),(506,453,124,103084,308),(507,420,138,103024,302),(508,420,138,103045,304),(509,420,138,103034,303),(510,420,138,103054,305),(511,420,138,103014,301),(512,420,138,103064,306),(513,420,138,103075,307),(514,420,138,103085,308),(515,435,138,103024,302),(516,435,138,103054,305),(517,435,138,103044,304),(518,435,138,103035,303),(519,435,138,103014,301),(520,435,138,103065,306),(521,435,138,103074,307),(522,435,138,103085,308),(523,415,138,103054,305),(524,415,138,103034,303),(525,415,138,103024,302),(526,415,138,103044,304),(527,415,138,103014,301),(528,415,138,103085,308),(529,415,138,103075,307),(530,415,138,103065,306),(531,422,138,103035,303),(532,422,138,103024,302),(533,422,138,103055,305),(534,422,138,103045,304),(535,422,138,103014,301),(536,422,138,103064,306),(537,422,138,103084,308),(538,422,138,103075,307),(539,431,138,103034,303),(540,431,138,103055,305),(541,431,138,103045,304),(542,431,138,103024,302),(543,431,138,103065,306),(544,431,138,103014,301),(545,431,138,103085,308),(546,431,138,103074,307),(547,430,138,103034,303),(548,430,138,103024,302),(549,430,138,103045,304),(550,430,138,103054,305),(551,430,138,103014,301),(552,430,138,103064,306),(553,430,138,103085,308),(554,430,138,103075,307),(555,451,127,103035,303),(556,451,127,103062,306),(557,451,127,103054,305),(558,451,127,103014,301),(559,451,127,103044,304),(560,451,127,103024,302),(561,451,127,103074,307),(562,451,127,103083,308),(563,454,124,103033,303),(564,454,124,103023,302),(565,454,124,103043,304),(566,454,124,103053,305),(567,454,124,103014,301),(568,454,124,103064,306),(569,454,124,103073,307),(570,454,124,103084,308),(571,424,69,103024,302),(572,424,69,103034,303),(573,424,69,103055,305),(574,424,69,103044,304),(575,424,69,103015,301),(576,424,69,103064,306),(577,424,69,103074,307),(578,424,69,103084,308),(579,412,69,103055,305),(580,412,69,103025,302),(581,412,69,103065,306),(582,412,69,103034,303),(583,412,69,103044,304),(584,412,69,103015,301),(585,412,69,103085,308),(586,412,69,103074,307),(587,456,127,103045,304),(588,456,127,103014,301),(589,456,127,103054,305),(590,456,127,103035,303),(591,456,127,103064,306),(592,456,127,103024,302),(593,456,127,103084,308),(594,456,127,103074,307),(595,453,92,103024,302),(596,453,92,103044,304),(597,453,92,103034,303),(598,453,92,103054,305),(599,453,92,103014,301),(600,453,92,103064,306),(601,453,92,103074,307),(602,453,92,103084,308),(603,451,92,103034,303),(604,451,92,103024,302),(605,451,92,103054,305),(606,451,92,103044,304),(607,451,92,103014,301),(608,451,92,103064,306),(609,451,92,103074,307),(610,451,92,103084,308),(611,454,92,103054,305),(612,454,92,103034,303),(613,454,92,103024,302),(614,454,92,103044,304),(615,454,92,103014,301),(616,454,92,103064,306),(617,454,92,103074,307),(618,454,92,103084,308),(619,456,92,103034,303),(620,456,92,103024,302),(621,456,92,103064,306),(622,456,92,103054,305),(623,456,92,103014,301),(624,456,92,103084,308),(625,456,92,103074,307),(626,456,92,103044,304),(627,412,76,103025,302),(628,412,76,103035,303),(629,412,76,103044,304),(630,412,76,103055,305),(631,412,76,103015,301),(632,412,76,103065,306),(633,412,76,103074,307),(634,412,76,103084,308),(635,424,76,103025,302),(636,424,76,103044,304),(637,424,76,103035,303),(638,424,76,103055,305),(639,424,76,103015,301),(640,424,76,103065,306),(641,424,76,103074,307),(642,424,76,103083,308),(643,413,134,103024,302),(644,413,134,103053,305),(645,413,134,103014,301),(646,413,134,103063,306),(647,413,134,103034,303),(648,413,134,103044,304),(649,413,134,103074,307),(650,413,134,103084,308);
/*!40000 ALTER TABLE `score` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `score_pembimbing`
--

DROP TABLE IF EXISTS `score_pembimbing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `score_pembimbing` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_id` int NOT NULL,
  `lecturer_id` int NOT NULL,
  `rubric_id` int NOT NULL,
  `sub_clo_id` int NOT NULL,
  `period_id` int NOT NULL,
  `nilai_per_rubric` int NOT NULL,
  `portion` int NOT NULL,
  `nilai_calculated_portion` double NOT NULL,
  `nilai_calculated_bobot` double NOT NULL,
  `nilai_calculated_bobot_portion` double NOT NULL,
  `clo_id` int NOT NULL,
  `plo_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_score_pembimbing_student_id_idx` (`student_id`),
  KEY `fk_score_pembimbing_lecturer_id_idx` (`lecturer_id`),
  KEY `fk_score_pembimbing_rubric_id_idx` (`rubric_id`),
  KEY `fk_score_pembimbing_period_id_idx` (`period_id`),
  KEY `fk_score_pembimbing_clo_id_idx` (`clo_id`),
  KEY `fk_score_pembimbing_plo_id_idx` (`plo_id`),
  CONSTRAINT `fk_score_pembimbing_clo_id` FOREIGN KEY (`clo_id`) REFERENCES `clo` (`id`),
  CONSTRAINT `fk_score_pembimbing_lecturer_id` FOREIGN KEY (`lecturer_id`) REFERENCES `lecturer` (`id`),
  CONSTRAINT `fk_score_pembimbing_period_id` FOREIGN KEY (`period_id`) REFERENCES `period` (`id`),
  CONSTRAINT `fk_score_pembimbing_plo_id` FOREIGN KEY (`plo_id`) REFERENCES `plo` (`id`),
  CONSTRAINT `fk_score_pembimbing_rubric_id` FOREIGN KEY (`rubric_id`) REFERENCES `sub_clo_rubric` (`id`),
  CONSTRAINT `fk_score_pembimbing_student_id` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=343 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `score_pembimbing`
--

LOCK TABLES `score_pembimbing` WRITE;
/*!40000 ALTER TABLE `score_pembimbing` DISABLE KEYS */;
INSERT INTO `score_pembimbing` VALUES (31,414,99,103024,302,5,85,10,8.5,34,3.4,101,102),(32,414,99,103034,303,5,85,10,8.5,34,3.4,101,102),(33,414,99,103044,304,5,85,10,8.5,34,3.4,101,102),(34,414,99,103054,305,5,85,10,8.5,34,3.4,101,102),(35,414,99,103064,306,5,85,20,17,34,6.8,103,102),(36,414,99,103013,301,5,75,10,7.5,30,3,102,102),(37,414,99,103073,307,5,75,20,15,30,6,104,102),(38,414,99,103084,308,5,85,10,8.5,34,3.4,105,106),(39,423,76,103013,301,5,75,10,7.5,30,3,102,102),(40,423,76,103063,306,5,75,20,15,30,6,103,102),(41,423,76,103024,302,5,85,10,8.5,34,3.4,101,102),(42,423,76,103034,303,5,85,10,8.5,34,3.4,101,102),(43,423,76,103044,304,5,85,10,8.5,34,3.4,101,102),(44,423,76,103051,305,5,0,10,0,0,0,101,102),(45,423,76,103074,307,5,85,20,17,34,6.8,104,102),(46,423,76,103083,308,5,75,10,7.5,30,3,105,106),(47,426,99,103014,301,5,85,10,8.5,34,3.4,102,102),(48,426,99,103024,302,5,85,10,8.5,34,3.4,101,102),(49,426,99,103045,304,5,95,10,9.5,38,3.8,101,102),(50,426,99,103034,303,5,85,10,8.5,34,3.4,101,102),(51,426,99,103054,305,5,85,10,8.5,34,3.4,101,102),(52,426,99,103064,306,5,85,20,17,34,6.8,103,102),(53,426,99,103074,307,5,85,20,17,34,6.8,104,102),(54,426,99,103084,308,5,85,10,8.5,34,3.4,105,106),(55,418,108,103023,302,5,75,10,7.5,30,3,101,102),(56,418,108,103044,304,5,85,10,8.5,34,3.4,101,102),(57,418,108,103033,303,5,75,10,7.5,30,3,101,102),(58,418,108,103054,305,5,85,10,8.5,34,3.4,101,102),(59,418,108,103013,301,5,75,10,7.5,30,3,102,102),(60,418,108,103064,306,5,85,20,17,34,6.8,103,102),(61,418,108,103073,307,5,75,20,15,30,6,104,102),(62,418,108,103084,308,5,85,10,8.5,34,3.4,105,106),(63,419,108,103034,303,5,85,10,8.5,34,3.4,101,102),(64,419,108,103024,302,5,85,10,8.5,34,3.4,101,102),(65,419,108,103045,304,5,95,10,9.5,38,3.8,101,102),(66,419,108,103014,301,5,85,10,8.5,34,3.4,102,102),(67,419,108,103054,305,5,85,10,8.5,34,3.4,101,102),(68,419,108,103065,306,5,95,20,19,38,7.6,103,102),(69,419,108,103073,307,5,75,20,15,30,6,104,102),(70,419,108,103084,308,5,85,10,8.5,34,3.4,105,106),(71,436,108,103025,302,5,95,10,9.5,38,3.8,101,102),(72,436,108,103044,304,5,85,10,8.5,34,3.4,101,102),(73,436,108,103055,305,5,95,10,9.5,38,3.8,101,102),(74,436,108,103034,303,5,85,10,8.5,34,3.4,101,102),(75,436,108,103014,301,5,85,10,8.5,34,3.4,102,102),(76,436,108,103065,306,5,95,20,19,38,7.6,103,102),(77,436,108,103074,307,5,85,20,17,34,6.8,104,102),(78,436,108,103084,308,5,85,10,8.5,34,3.4,105,106),(79,417,108,103034,303,5,85,10,8.5,34,3.4,101,102),(80,417,108,103025,302,5,95,10,9.5,38,3.8,101,102),(81,417,108,103044,304,5,85,10,8.5,34,3.4,101,102),(82,417,108,103055,305,5,95,10,9.5,38,3.8,101,102),(83,417,108,103013,301,5,75,10,7.5,30,3,102,102),(84,417,108,103064,306,5,85,20,17,34,6.8,103,102),(85,417,108,103074,307,5,85,20,17,34,6.8,104,102),(86,417,108,103084,308,5,85,10,8.5,34,3.4,105,106),(87,438,130,103025,302,5,95,10,9.5,38,3.8,101,102),(88,438,130,103045,304,5,95,10,9.5,38,3.8,101,102),(89,438,130,103014,301,5,85,10,8.5,34,3.4,102,102),(90,438,130,103035,303,5,95,10,9.5,38,3.8,101,102),(91,438,130,103064,306,5,85,20,17,34,6.8,103,102),(92,438,130,103055,305,5,95,10,9.5,38,3.8,101,102),(93,438,130,103084,308,5,85,10,8.5,34,3.4,105,106),(94,438,130,103074,307,5,85,20,17,34,6.8,104,102),(95,429,130,103025,302,5,95,10,9.5,38,3.8,101,102),(96,429,130,103014,301,5,85,10,8.5,34,3.4,102,102),(97,429,130,103055,305,5,95,10,9.5,38,3.8,101,102),(98,429,130,103035,303,5,95,10,9.5,38,3.8,101,102),(99,429,130,103045,304,5,95,10,9.5,38,3.8,101,102),(100,429,130,103064,306,5,85,20,17,34,6.8,103,102),(101,429,130,103074,307,5,85,20,17,34,6.8,104,102),(102,429,130,103084,308,5,85,10,8.5,34,3.4,105,106),(103,434,76,103024,302,5,85,10,8.5,34,3.4,101,102),(104,434,76,103034,303,5,85,10,8.5,34,3.4,101,102),(105,434,76,103044,304,5,85,10,8.5,34,3.4,101,102),(106,434,76,103013,301,5,75,10,7.5,30,3,102,102),(107,434,76,103051,305,5,0,10,0,0,0,101,102),(108,434,76,103063,306,5,75,20,15,30,6,103,102),(109,434,76,103074,307,5,85,20,17,34,6.8,104,102),(110,434,76,103083,308,5,75,10,7.5,30,3,105,106),(111,447,132,103023,302,5,75,10,7.5,30,3,101,102),(112,447,132,103013,301,5,75,10,7.5,30,3,102,102),(113,447,132,103033,303,5,75,10,7.5,30,3,101,102),(114,447,132,103053,305,5,75,10,7.5,30,3,101,102),(115,447,132,103044,304,5,85,10,8.5,34,3.4,101,102),(116,447,132,103061,306,5,0,20,0,0,0,103,102),(117,447,132,103073,307,5,75,20,15,30,6,104,102),(118,447,132,103083,308,5,75,10,7.5,30,3,105,106),(119,432,135,103025,302,5,95,10,9.5,38,3.8,101,102),(120,432,135,103035,303,5,95,10,9.5,38,3.8,101,102),(121,432,135,103045,304,5,95,10,9.5,38,3.8,101,102),(122,432,135,103055,305,5,95,10,9.5,38,3.8,101,102),(123,432,135,103015,301,5,95,10,9.5,38,3.8,102,102),(124,432,135,103064,306,5,85,20,17,34,6.8,103,102),(125,432,135,103075,307,5,95,20,19,38,7.6,104,102),(126,432,135,103084,308,5,85,10,8.5,34,3.4,105,106),(127,433,135,103025,302,5,95,10,9.5,38,3.8,101,102),(128,433,135,103035,303,5,95,10,9.5,38,3.8,101,102),(129,433,135,103055,305,5,95,10,9.5,38,3.8,101,102),(130,433,135,103064,306,5,85,20,17,34,6.8,103,102),(131,433,135,103045,304,5,95,10,9.5,38,3.8,101,102),(132,433,135,103015,301,5,95,10,9.5,38,3.8,102,102),(133,433,135,103075,307,5,95,20,19,38,7.6,104,102),(134,433,135,103084,308,5,85,10,8.5,34,3.4,105,106),(135,455,135,103045,304,5,95,10,9.5,38,3.8,101,102),(136,455,135,103035,303,5,95,10,9.5,38,3.8,101,102),(137,455,135,103025,302,5,95,10,9.5,38,3.8,101,102),(138,455,135,103055,305,5,95,10,9.5,38,3.8,101,102),(139,455,135,103015,301,5,95,10,9.5,38,3.8,102,102),(140,455,135,103064,306,5,85,20,17,34,6.8,103,102),(141,455,135,103075,307,5,95,20,19,38,7.6,104,102),(142,455,135,103084,308,5,85,10,8.5,34,3.4,105,106),(143,440,95,103034,303,5,85,10,8.5,34,3.4,101,102),(144,440,95,103043,304,5,75,10,7.5,30,3,101,102),(145,440,95,103023,302,5,75,10,7.5,30,3,101,102),(146,440,95,103053,305,5,75,10,7.5,30,3,101,102),(147,440,95,103013,301,5,75,10,7.5,30,3,102,102),(148,440,95,103063,306,5,75,20,15,30,6,103,102),(149,440,95,103074,307,5,85,20,17,34,6.8,104,102),(150,440,95,103084,308,5,85,10,8.5,34,3.4,105,106),(151,446,95,103034,303,5,85,10,8.5,34,3.4,101,102),(152,446,95,103023,302,5,75,10,7.5,30,3,101,102),(153,446,95,103014,301,5,85,10,8.5,34,3.4,102,102),(154,446,95,103044,304,5,85,10,8.5,34,3.4,101,102),(155,446,95,103063,306,5,75,20,15,30,6,103,102),(156,446,95,103053,305,5,75,10,7.5,30,3,101,102),(157,446,95,103073,307,5,75,20,15,30,6,104,102),(158,446,95,103084,308,5,85,10,8.5,34,3.4,105,106),(159,442,122,103014,301,5,85,10,8.5,34,3.4,102,102),(160,442,122,103054,305,5,85,10,8.5,34,3.4,101,102),(161,442,122,103024,302,5,85,10,8.5,34,3.4,101,102),(162,442,122,103034,303,5,85,10,8.5,34,3.4,101,102),(163,442,122,103044,304,5,85,10,8.5,34,3.4,101,102),(164,442,122,103064,306,5,85,20,17,34,6.8,103,102),(165,442,122,103074,307,5,85,20,17,34,6.8,104,102),(166,442,122,103084,308,5,85,10,8.5,34,3.4,105,106),(167,444,122,103024,302,5,85,10,8.5,34,3.4,101,102),(168,444,122,103034,303,5,85,10,8.5,34,3.4,101,102),(169,444,122,103044,304,5,85,10,8.5,34,3.4,101,102),(170,444,122,103014,301,5,85,10,8.5,34,3.4,102,102),(171,444,122,103064,306,5,85,20,17,34,6.8,103,102),(172,444,122,103054,305,5,85,10,8.5,34,3.4,101,102),(173,444,122,103074,307,5,85,20,17,34,6.8,104,102),(174,444,122,103084,308,5,85,10,8.5,34,3.4,105,106),(175,452,131,103023,302,5,75,10,7.5,30,3,101,102),(176,452,131,103032,303,5,65,10,6.5,26,2.6,101,102),(177,452,131,103043,304,5,75,10,7.5,30,3,101,102),(178,452,131,103053,305,5,75,10,7.5,30,3,101,102),(179,452,131,103062,306,5,65,20,13,26,5.2,103,102),(180,452,131,103013,301,5,75,10,7.5,30,3,102,102),(181,452,131,103072,307,5,65,20,13,26,5.2,104,102),(182,452,131,103082,308,5,65,10,6.5,26,2.6,105,106),(183,425,99,103025,302,5,95,10,9.5,38,3.8,101,102),(184,425,99,103035,303,5,95,10,9.5,38,3.8,101,102),(185,425,99,103054,305,5,85,10,8.5,34,3.4,101,102),(186,425,99,103045,304,5,95,10,9.5,38,3.8,101,102),(187,425,99,103015,301,5,95,10,9.5,38,3.8,102,102),(188,425,99,103065,306,5,95,20,19,38,7.6,103,102),(189,425,99,103074,307,5,85,20,17,34,6.8,104,102),(190,425,99,103084,308,5,85,10,8.5,34,3.4,105,106),(191,416,99,103025,302,5,95,10,9.5,38,3.8,101,102),(192,416,99,103044,304,5,85,10,8.5,34,3.4,101,102),(193,416,99,103055,305,5,95,10,9.5,38,3.8,101,102),(194,416,99,103035,303,5,95,10,9.5,38,3.8,101,102),(195,416,99,103014,301,5,85,10,8.5,34,3.4,102,102),(196,416,99,103063,306,5,75,20,15,30,6,103,102),(197,416,99,103073,307,5,75,20,15,30,6,104,102),(198,416,99,103084,308,5,85,10,8.5,34,3.4,105,106),(199,448,119,103023,302,5,75,10,7.5,45,4.5,101,102),(200,448,119,103033,303,5,75,10,7.5,45,4.5,101,102),(201,448,119,103043,304,5,75,10,7.5,45,4.5,101,102),(202,448,119,103053,305,5,75,10,7.5,45,4.5,101,102),(203,448,119,103013,301,5,75,10,7.5,45,4.5,102,102),(204,448,119,103063,306,5,75,20,15,45,9,103,102),(205,448,119,103073,307,5,75,20,15,45,9,104,102),(206,448,119,103083,308,5,75,10,7.5,45,4.5,105,106),(207,421,114,103024,302,5,85,10,8.5,34,3.4,101,102),(208,421,114,103034,303,5,85,10,8.5,34,3.4,101,102),(209,421,114,103055,305,5,95,10,9.5,38,3.8,101,102),(210,421,114,103014,301,5,85,10,8.5,34,3.4,102,102),(211,421,114,103045,304,5,95,10,9.5,38,3.8,101,102),(212,421,114,103065,306,5,95,20,19,38,7.6,103,102),(213,421,114,103074,307,5,85,20,17,34,6.8,104,102),(214,421,114,103084,308,5,85,10,8.5,34,3.4,105,106),(215,437,114,103015,301,5,95,10,9.5,57,5.7,102,102),(216,437,114,103034,303,5,85,10,8.5,51,5.1,101,102),(217,437,114,103025,302,5,95,10,9.5,57,5.7,101,102),(218,437,114,103045,304,5,95,10,9.5,57,5.7,101,102),(219,437,114,103055,305,5,95,10,9.5,57,5.7,101,102),(220,437,114,103065,306,5,95,20,19,57,11.4,103,102),(221,437,114,103085,308,5,95,10,9.5,57,5.7,105,106),(222,437,114,103075,307,5,95,20,19,57,11.4,104,102),(223,428,111,103025,302,5,95,10,9.5,57,5.7,101,102),(224,428,111,103035,303,5,95,10,9.5,57,5.7,101,102),(225,428,111,103045,304,5,95,10,9.5,57,5.7,101,102),(226,428,111,103055,305,5,95,10,9.5,57,5.7,101,102),(227,428,111,103015,301,5,95,10,9.5,57,5.7,102,102),(228,428,111,103065,306,5,95,20,19,57,11.4,103,102),(229,428,111,103075,307,5,95,20,19,57,11.4,104,102),(230,428,111,103085,308,5,95,10,9.5,57,5.7,105,106),(231,413,111,103025,302,5,95,10,9.5,57,5.7,101,102),(232,413,111,103035,303,5,95,10,9.5,57,5.7,101,102),(233,413,111,103045,304,5,95,10,9.5,57,5.7,101,102),(234,413,111,103055,305,5,95,10,9.5,57,5.7,101,102),(235,413,111,103015,301,5,95,10,9.5,57,5.7,102,102),(236,413,111,103064,306,5,85,20,17,51,10.2,103,102),(237,413,111,103084,308,5,85,10,8.5,51,5.1,105,106),(238,413,111,103075,307,5,95,20,19,57,11.4,104,102),(239,427,111,103025,302,5,95,10,9.5,57,5.7,101,102),(240,427,111,103035,303,5,95,10,9.5,57,5.7,101,102),(241,427,111,103045,304,5,95,10,9.5,57,5.7,101,102),(242,427,111,103054,305,5,85,10,8.5,51,5.1,101,102),(243,427,111,103013,301,5,75,10,7.5,45,4.5,102,102),(244,427,111,103064,306,5,85,20,17,51,10.2,103,102),(245,427,111,103075,307,5,95,20,19,57,11.4,104,102),(246,427,111,103085,308,5,95,10,9.5,57,5.7,105,106),(247,420,138,103024,302,5,85,10,8.5,51,5.1,101,102),(248,420,138,103034,303,5,85,10,8.5,51,5.1,101,102),(249,420,138,103045,304,5,95,10,9.5,57,5.7,101,102),(250,420,138,103054,305,5,85,10,8.5,51,5.1,101,102),(251,420,138,103014,301,5,85,10,8.5,51,5.1,102,102),(252,420,138,103064,306,5,85,20,17,51,10.2,103,102),(253,420,138,103075,307,5,95,20,19,57,11.4,104,102),(254,420,138,103085,308,5,95,10,9.5,57,5.7,105,106),(255,435,138,103024,302,5,85,10,8.5,51,5.1,101,102),(256,435,138,103054,305,5,85,10,8.5,51,5.1,101,102),(257,435,138,103035,303,5,95,10,9.5,57,5.7,101,102),(258,435,138,103044,304,5,85,10,8.5,51,5.1,101,102),(259,435,138,103014,301,5,85,10,8.5,51,5.1,102,102),(260,435,138,103065,306,5,95,20,19,57,11.4,103,102),(261,435,138,103074,307,5,85,20,17,51,10.2,104,102),(262,435,138,103085,308,5,95,10,9.5,57,5.7,105,106),(263,415,138,103054,305,5,85,10,8.5,34,3.4,101,102),(264,415,138,103034,303,5,85,10,8.5,34,3.4,101,102),(265,415,138,103024,302,5,85,10,8.5,34,3.4,101,102),(266,415,138,103044,304,5,85,10,8.5,34,3.4,101,102),(267,415,138,103014,301,5,85,10,8.5,34,3.4,102,102),(268,415,138,103085,308,5,95,10,9.5,38,3.8,105,106),(269,415,138,103075,307,5,95,20,19,38,7.6,104,102),(270,415,138,103065,306,5,95,20,19,38,7.6,103,102),(271,422,138,103035,303,5,95,10,9.5,57,5.7,101,102),(272,422,138,103024,302,5,85,10,8.5,51,5.1,101,102),(273,422,138,103055,305,5,95,10,9.5,57,5.7,101,102),(274,422,138,103045,304,5,95,10,9.5,57,5.7,101,102),(275,422,138,103014,301,5,85,10,8.5,51,5.1,102,102),(276,422,138,103064,306,5,85,20,17,51,10.2,103,102),(277,422,138,103084,308,5,85,10,8.5,51,5.1,105,106),(278,422,138,103075,307,5,95,20,19,57,11.4,104,102),(279,431,138,103034,303,5,85,10,8.5,51,5.1,101,102),(280,431,138,103055,305,5,95,10,9.5,57,5.7,101,102),(281,431,138,103045,304,5,95,10,9.5,57,5.7,101,102),(282,431,138,103024,302,5,85,10,8.5,51,5.1,101,102),(283,431,138,103065,306,5,95,20,19,57,11.4,103,102),(284,431,138,103014,301,5,85,10,8.5,51,5.1,102,102),(285,431,138,103085,308,5,95,10,9.5,57,5.7,105,106),(286,431,138,103074,307,5,85,20,17,51,10.2,104,102),(287,430,138,103034,303,5,85,10,8.5,51,5.1,101,102),(288,430,138,103024,302,5,85,10,8.5,51,5.1,101,102),(289,430,138,103045,304,5,95,10,9.5,57,5.7,101,102),(290,430,138,103054,305,5,85,10,8.5,51,5.1,101,102),(291,430,138,103014,301,5,85,10,8.5,51,5.1,102,102),(292,430,138,103064,306,5,85,20,17,51,10.2,103,102),(293,430,138,103085,308,5,95,10,9.5,57,5.7,105,106),(294,430,138,103075,307,5,95,20,19,57,11.4,104,102),(295,424,69,103024,302,5,85,10,8.5,51,5.1,101,102),(296,424,69,103034,303,5,85,10,8.5,51,5.1,101,102),(297,424,69,103055,305,5,95,10,9.5,57,5.7,101,102),(298,424,69,103044,304,5,85,10,8.5,51,5.1,101,102),(299,424,69,103015,301,5,95,10,9.5,57,5.7,102,102),(300,424,69,103064,306,5,85,20,17,51,10.2,103,102),(301,424,69,103074,307,5,85,20,17,51,10.2,104,102),(302,424,69,103084,308,5,85,10,8.5,51,5.1,105,106),(303,412,69,103055,305,5,95,10,9.5,57,5.7,101,102),(304,412,69,103025,302,5,95,10,9.5,57,5.7,101,102),(305,412,69,103065,306,5,95,20,19,57,11.4,103,102),(306,412,69,103034,303,5,85,10,8.5,51,5.1,101,102),(307,412,69,103044,304,5,85,10,8.5,51,5.1,101,102),(308,412,69,103015,301,5,95,10,9.5,57,5.7,102,102),(309,412,69,103085,308,5,95,10,9.5,57,5.7,105,106),(310,412,69,103074,307,5,85,20,17,51,10.2,104,102),(311,453,92,103024,302,5,85,10,8.5,51,5.1,101,102),(312,453,92,103054,305,5,85,10,8.5,51,5.1,101,102),(313,453,92,103044,304,5,85,10,8.5,51,5.1,101,102),(314,453,92,103034,303,5,85,10,8.5,51,5.1,101,102),(315,453,92,103014,301,5,85,10,8.5,51,5.1,102,102),(316,453,92,103064,306,5,85,20,17,51,10.2,103,102),(317,453,92,103074,307,5,85,20,17,51,10.2,104,102),(318,453,92,103084,308,5,85,10,8.5,51,5.1,105,106),(319,451,92,103024,302,5,85,10,8.5,51,5.1,101,102),(320,451,92,103034,303,5,85,10,8.5,51,5.1,101,102),(321,451,92,103054,305,5,85,10,8.5,51,5.1,101,102),(322,451,92,103044,304,5,85,10,8.5,51,5.1,101,102),(323,451,92,103014,301,5,85,10,8.5,51,5.1,102,102),(324,451,92,103064,306,5,85,20,17,51,10.2,103,102),(325,451,92,103074,307,5,85,20,17,51,10.2,104,102),(326,451,92,103084,308,5,85,10,8.5,51,5.1,105,106),(327,454,92,103024,302,5,85,10,8.5,51,5.1,101,102),(328,454,92,103034,303,5,85,10,8.5,51,5.1,101,102),(329,454,92,103054,305,5,85,10,8.5,51,5.1,101,102),(330,454,92,103014,301,5,85,10,8.5,51,5.1,102,102),(331,454,92,103044,304,5,85,10,8.5,51,5.1,101,102),(332,454,92,103064,306,5,85,20,17,51,10.2,103,102),(333,454,92,103074,307,5,85,20,17,51,10.2,104,102),(334,454,92,103084,308,5,85,10,8.5,51,5.1,105,106),(335,456,92,103034,303,5,85,10,8.5,51,5.1,101,102),(336,456,92,103024,302,5,85,10,8.5,51,5.1,101,102),(337,456,92,103064,306,5,85,20,17,51,10.2,103,102),(338,456,92,103054,305,5,85,10,8.5,51,5.1,101,102),(339,456,92,103014,301,5,85,10,8.5,51,5.1,102,102),(340,456,92,103084,308,5,85,10,8.5,51,5.1,105,106),(341,456,92,103074,307,5,85,20,17,51,10.2,104,102),(342,456,92,103044,304,5,85,10,8.5,51,5.1,101,102);
/*!40000 ALTER TABLE `score_pembimbing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `score_penguji`
--

DROP TABLE IF EXISTS `score_penguji`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `score_penguji` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_id` int NOT NULL,
  `lecturer_id` int NOT NULL,
  `rubric_id` int NOT NULL,
  `sub_clo_id` int NOT NULL,
  `period_id` int NOT NULL,
  `nilai_per_rubric` int NOT NULL,
  `portion` int NOT NULL,
  `nilai_calculated_portion` double NOT NULL,
  `nilai_calculated_bobot` double NOT NULL,
  `nilai_calculated_bobot_portion` double NOT NULL,
  `clo_id` int NOT NULL,
  `plo_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_score_penguji_student_id_idx` (`student_id`),
  KEY `fk_score_penguji_lecturer_id_idx` (`lecturer_id`),
  KEY `fk_score_penguji_rubric_id_idx` (`rubric_id`),
  KEY `fk_score_penguji_period_id_idx` (`period_id`),
  KEY `fk_score_penguji_clo_id_idx` (`clo_id`),
  KEY `fk_score_penguji_plo_id_idx` (`plo_id`),
  CONSTRAINT `fk_score_penguji_clo_id` FOREIGN KEY (`clo_id`) REFERENCES `clo` (`id`),
  CONSTRAINT `fk_score_penguji_lecturer_id` FOREIGN KEY (`lecturer_id`) REFERENCES `lecturer` (`id`),
  CONSTRAINT `fk_score_penguji_period_id` FOREIGN KEY (`period_id`) REFERENCES `period` (`id`),
  CONSTRAINT `fk_score_penguji_plo_id` FOREIGN KEY (`plo_id`) REFERENCES `plo` (`id`),
  CONSTRAINT `fk_score_penguji_rubric_id` FOREIGN KEY (`rubric_id`) REFERENCES `sub_clo_rubric` (`id`),
  CONSTRAINT `fk_score_penguji_student_id` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=328 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `score_penguji`
--

LOCK TABLES `score_penguji` WRITE;
/*!40000 ALTER TABLE `score_penguji` DISABLE KEYS */;
INSERT INTO `score_penguji` VALUES (16,435,91,103033,303,5,75,10,7.5,30,3,101,102),(17,435,91,103044,304,5,85,10,8.5,34,3.4,101,102),(18,435,91,103064,306,5,85,20,17,34,6.8,103,102),(19,435,91,103013,301,5,75,10,7.5,30,3,102,102),(20,435,91,103023,302,5,75,10,7.5,30,3,101,102),(21,435,91,103053,305,5,75,10,7.5,30,3,101,102),(22,435,91,103074,307,5,85,20,17,34,6.8,104,102),(23,435,91,103084,308,5,85,10,8.5,34,3.4,105,106),(24,415,91,103053,305,5,75,10,7.5,30,3,101,102),(25,415,91,103013,301,5,75,10,7.5,30,3,102,102),(26,415,91,103023,302,5,75,10,7.5,30,3,101,102),(27,415,91,103044,304,5,85,10,8.5,34,3.4,101,102),(28,415,91,103033,303,5,75,10,7.5,30,3,101,102),(29,415,91,103064,306,5,85,20,17,34,6.8,103,102),(30,415,91,103074,307,5,85,20,17,34,6.8,104,102),(31,415,91,103084,308,5,85,10,8.5,34,3.4,105,106),(32,422,91,103013,301,5,75,10,7.5,30,3,102,102),(33,422,91,103023,302,5,75,10,7.5,30,3,101,102),(34,422,91,103033,303,5,75,10,7.5,30,3,101,102),(35,422,91,103044,304,5,85,10,8.5,34,3.4,101,102),(36,422,91,103053,305,5,75,10,7.5,30,3,101,102),(37,422,91,103064,306,5,85,20,17,34,6.8,103,102),(38,422,91,103074,307,5,85,20,17,34,6.8,104,102),(39,422,91,103084,308,5,85,10,8.5,34,3.4,105,106),(40,431,91,103053,305,5,75,10,7.5,30,3,101,102),(41,431,91,103013,301,5,75,10,7.5,30,3,102,102),(42,431,91,103044,304,5,85,10,8.5,34,3.4,101,102),(43,431,91,103064,306,5,85,20,17,34,6.8,103,102),(44,431,91,103023,302,5,75,10,7.5,30,3,101,102),(45,431,91,103033,303,5,75,10,7.5,30,3,101,102),(46,431,91,103084,308,5,85,10,8.5,34,3.4,105,106),(47,431,91,103074,307,5,85,20,17,34,6.8,104,102),(48,430,91,103044,304,5,85,10,8.5,34,3.4,101,102),(49,430,91,103023,302,5,75,10,7.5,30,3,101,102),(50,430,91,103064,306,5,85,20,17,34,6.8,103,102),(51,430,91,103033,303,5,75,10,7.5,30,3,101,102),(52,430,91,103053,305,5,75,10,7.5,30,3,101,102),(53,430,91,103013,301,5,75,10,7.5,30,3,102,102),(54,430,91,103084,308,5,85,10,8.5,34,3.4,105,106),(55,430,91,103074,307,5,85,20,17,34,6.8,104,102),(56,418,110,103054,305,5,85,10,8.5,34,3.4,101,102),(57,418,110,103064,306,5,85,20,17,34,6.8,103,102),(58,418,110,103024,302,5,85,10,8.5,34,3.4,101,102),(59,418,110,103014,301,5,85,10,8.5,34,3.4,102,102),(60,418,110,103034,303,5,85,10,8.5,34,3.4,101,102),(61,418,110,103044,304,5,85,10,8.5,34,3.4,101,102),(62,418,110,103074,307,5,85,20,17,34,6.8,104,102),(63,418,110,103084,308,5,85,10,8.5,34,3.4,105,106),(64,428,110,103014,301,5,85,10,8.5,34,3.4,102,102),(65,428,110,103054,305,5,85,10,8.5,34,3.4,101,102),(66,428,110,103044,304,5,85,10,8.5,34,3.4,101,102),(67,428,110,103024,302,5,85,10,8.5,34,3.4,101,102),(68,428,110,103065,306,5,95,20,19,38,7.6,103,102),(69,428,110,103034,303,5,85,10,8.5,34,3.4,101,102),(70,428,110,103074,307,5,85,20,17,34,6.8,104,102),(71,428,110,103084,308,5,85,10,8.5,34,3.4,105,106),(72,427,110,103034,303,5,85,10,8.5,34,3.4,101,102),(73,427,110,103023,302,5,75,10,7.5,30,3,101,102),(74,427,110,103013,301,5,75,10,7.5,30,3,102,102),(75,427,110,103044,304,5,85,10,8.5,34,3.4,101,102),(76,427,110,103054,305,5,85,10,8.5,34,3.4,101,102),(77,427,110,103064,306,5,85,20,17,34,6.8,103,102),(78,427,110,103074,307,5,85,20,17,34,6.8,104,102),(79,427,110,103084,308,5,85,10,8.5,34,3.4,105,106),(80,436,116,103034,303,5,85,10,8.5,34,3.4,101,102),(81,436,116,103024,302,5,85,10,8.5,34,3.4,101,102),(82,436,116,103054,305,5,85,10,8.5,34,3.4,101,102),(83,436,116,103044,304,5,85,10,8.5,34,3.4,101,102),(84,436,116,103065,306,5,95,20,19,38,7.6,103,102),(85,436,116,103015,301,5,95,10,9.5,38,3.8,102,102),(86,436,116,103074,307,5,85,20,17,34,6.8,104,102),(87,436,116,103085,308,5,95,10,9.5,38,3.8,105,106),(88,417,116,103025,302,5,95,10,9.5,38,3.8,101,102),(89,417,116,103034,303,5,85,10,8.5,34,3.4,101,102),(90,417,116,103015,301,5,95,10,9.5,38,3.8,102,102),(91,417,116,103054,305,5,85,10,8.5,34,3.4,101,102),(92,417,116,103044,304,5,85,10,8.5,34,3.4,101,102),(93,417,116,103065,306,5,95,20,19,38,7.6,103,102),(94,417,116,103085,308,5,95,10,9.5,38,3.8,105,106),(95,417,116,103074,307,5,85,20,17,34,6.8,104,102),(96,438,119,103023,302,5,75,10,7.5,30,3,101,102),(97,438,119,103013,301,5,75,10,7.5,30,3,102,102),(98,438,119,103043,304,5,75,10,7.5,30,3,101,102),(99,438,119,103034,303,5,85,10,8.5,34,3.4,101,102),(100,438,119,103054,305,5,85,10,8.5,34,3.4,101,102),(101,438,119,103063,306,5,75,20,15,30,6,103,102),(102,438,119,103072,307,5,65,20,13,26,5.2,104,102),(103,438,119,103083,308,5,75,10,7.5,30,3,105,106),(104,429,119,103033,303,5,75,10,7.5,30,3,101,102),(105,429,119,103043,304,5,75,10,7.5,30,3,101,102),(106,429,119,103052,305,5,65,10,6.5,26,2.6,101,102),(107,429,119,103023,302,5,75,10,7.5,30,3,101,102),(108,429,119,103012,301,5,65,10,6.5,26,2.6,102,102),(109,429,119,103061,306,5,0,20,0,0,0,103,102),(110,429,119,103073,307,5,75,20,15,30,6,104,102),(111,429,119,103083,308,5,75,10,7.5,30,3,105,106),(112,446,115,103032,303,5,65,10,6.5,26,2.6,101,102),(113,446,115,103043,304,5,75,10,7.5,30,3,101,102),(114,446,115,103051,305,5,0,10,0,0,0,101,102),(115,446,115,103023,302,5,75,10,7.5,30,3,101,102),(116,446,115,103062,306,5,65,20,13,26,5.2,103,102),(117,446,115,103013,301,5,75,10,7.5,30,3,102,102),(118,446,115,103073,307,5,75,20,15,30,6,104,102),(119,446,115,103083,308,5,75,10,7.5,30,3,105,106),(120,440,115,103013,301,5,75,10,7.5,30,3,102,102),(121,440,115,103034,303,5,85,10,8.5,34,3.4,101,102),(122,440,115,103022,302,5,65,10,6.5,26,2.6,101,102),(123,440,115,103064,306,5,85,20,17,34,6.8,103,102),(124,440,115,103052,305,5,65,10,6.5,26,2.6,101,102),(125,440,115,103043,304,5,75,10,7.5,30,3,101,102),(126,440,115,103073,307,5,75,20,15,30,6,104,102),(127,440,115,103082,308,5,65,10,6.5,26,2.6,105,106),(128,423,132,103022,302,5,65,10,6.5,26,2.6,101,102),(129,423,132,103032,303,5,65,10,6.5,26,2.6,101,102),(130,423,132,103042,304,5,65,10,6.5,26,2.6,101,102),(131,423,132,103052,305,5,65,10,6.5,26,2.6,101,102),(132,423,132,103012,301,5,65,10,6.5,26,2.6,102,102),(133,423,132,103062,306,5,65,20,13,26,5.2,103,102),(134,423,132,103072,307,5,65,20,13,26,5.2,104,102),(135,423,132,103081,308,5,0,10,0,0,0,105,106),(136,434,132,103042,304,5,65,10,6.5,26,2.6,101,102),(137,434,132,103022,302,5,65,10,6.5,26,2.6,101,102),(138,434,132,103052,305,5,65,10,6.5,26,2.6,101,102),(139,434,132,103012,301,5,65,10,6.5,26,2.6,102,102),(140,434,132,103032,303,5,65,10,6.5,26,2.6,101,102),(141,434,132,103062,306,5,65,20,13,26,5.2,103,102),(142,434,132,103081,308,5,0,10,0,0,0,105,106),(143,434,132,103072,307,5,65,20,13,26,5.2,104,102),(144,420,91,103023,302,5,75,10,7.5,30,3,101,102),(145,420,91,103033,303,5,75,10,7.5,30,3,101,102),(146,420,91,103064,306,5,85,20,17,34,6.8,103,102),(147,420,91,103053,305,5,75,10,7.5,30,3,101,102),(148,420,91,103044,304,5,85,10,8.5,34,3.4,101,102),(149,420,91,103013,301,5,75,10,7.5,30,3,102,102),(150,420,91,103074,307,5,85,20,17,34,6.8,104,102),(151,420,91,103084,308,5,85,10,8.5,34,3.4,105,106),(152,442,135,103024,302,5,85,10,8.5,34,3.4,101,102),(153,442,135,103034,303,5,85,10,8.5,34,3.4,101,102),(154,442,135,103045,304,5,95,10,9.5,38,3.8,101,102),(155,442,135,103013,301,5,75,10,7.5,30,3,102,102),(156,442,135,103053,305,5,75,10,7.5,30,3,101,102),(157,442,135,103064,306,5,85,20,17,34,6.8,103,102),(158,442,135,103072,307,5,65,20,13,26,5.2,104,102),(159,442,135,103083,308,5,75,10,7.5,30,3,105,106),(160,444,135,103024,302,5,85,10,8.5,34,3.4,101,102),(161,444,135,103034,303,5,85,10,8.5,34,3.4,101,102),(162,444,135,103044,304,5,85,10,8.5,34,3.4,101,102),(163,444,135,103064,306,5,85,20,17,34,6.8,103,102),(164,444,135,103013,301,5,75,10,7.5,30,3,102,102),(165,444,135,103052,305,5,65,10,6.5,26,2.6,101,102),(166,444,135,103073,307,5,75,20,15,30,6,104,102),(167,444,135,103083,308,5,75,10,7.5,30,3,105,106),(168,432,121,103024,302,5,85,10,8.5,34,3.4,101,102),(169,432,121,103035,303,5,95,10,9.5,38,3.8,101,102),(170,432,121,103044,304,5,85,10,8.5,34,3.4,101,102),(171,432,121,103054,305,5,85,10,8.5,34,3.4,101,102),(172,432,121,103014,301,5,85,10,8.5,34,3.4,102,102),(173,432,121,103064,306,5,85,20,17,34,6.8,103,102),(174,432,121,103074,307,5,85,20,17,34,6.8,104,102),(175,432,121,103085,308,5,95,10,9.5,38,3.8,105,106),(176,433,121,103024,302,5,85,10,8.5,34,3.4,101,102),(177,433,121,103035,303,5,95,10,9.5,38,3.8,101,102),(178,433,121,103044,304,5,85,10,8.5,34,3.4,101,102),(179,433,121,103054,305,5,85,10,8.5,34,3.4,101,102),(180,433,121,103063,306,5,75,20,15,30,6,103,102),(181,433,121,103014,301,5,85,10,8.5,34,3.4,102,102),(182,433,121,103085,308,5,95,10,9.5,38,3.8,105,106),(183,433,121,103074,307,5,85,20,17,34,6.8,104,102),(184,455,121,103024,302,5,85,10,8.5,34,3.4,101,102),(185,455,121,103044,304,5,85,10,8.5,34,3.4,101,102),(186,455,121,103054,305,5,85,10,8.5,34,3.4,101,102),(187,455,121,103015,301,5,95,10,9.5,38,3.8,102,102),(188,455,121,103035,303,5,95,10,9.5,38,3.8,101,102),(189,455,121,103064,306,5,85,20,17,34,6.8,103,102),(190,455,121,103074,307,5,85,20,17,34,6.8,104,102),(191,455,121,103085,308,5,95,10,9.5,38,3.8,105,106),(192,425,97,103054,305,5,85,10,8.5,34,3.4,101,102),(193,425,97,103025,302,5,95,10,9.5,38,3.8,101,102),(194,425,97,103035,303,5,95,10,9.5,38,3.8,101,102),(195,425,97,103063,306,5,75,20,15,30,6,103,102),(196,425,97,103014,301,5,85,10,8.5,34,3.4,102,102),(197,425,97,103044,304,5,85,10,8.5,34,3.4,101,102),(198,425,97,103073,307,5,75,20,15,30,6,104,102),(199,425,97,103084,308,5,85,10,8.5,34,3.4,105,106),(200,416,97,103023,302,5,75,10,7.5,30,3,101,102),(201,416,97,103043,304,5,75,10,7.5,30,3,101,102),(202,416,97,103034,303,5,85,10,8.5,34,3.4,101,102),(203,416,97,103054,305,5,85,10,8.5,34,3.4,101,102),(204,416,97,103063,306,5,75,20,15,30,6,103,102),(205,416,97,103013,301,5,75,10,7.5,30,3,102,102),(206,416,97,103074,307,5,85,20,17,34,6.8,104,102),(207,416,97,103084,308,5,85,10,8.5,34,3.4,105,106),(208,414,97,103023,302,5,75,10,7.5,30,3,101,102),(209,414,97,103033,303,5,75,10,7.5,30,3,101,102),(210,414,97,103044,304,5,85,10,8.5,34,3.4,101,102),(211,414,97,103054,305,5,85,10,8.5,34,3.4,101,102),(212,414,97,103062,306,5,65,20,13,26,5.2,103,102),(213,414,97,103012,301,5,65,10,6.5,26,2.6,102,102),(214,414,97,103072,307,5,65,20,13,26,5.2,104,102),(215,414,97,103084,308,5,85,10,8.5,34,3.4,105,106),(216,426,97,103024,302,5,85,10,8.5,34,3.4,101,102),(217,426,97,103054,305,5,85,10,8.5,34,3.4,101,102),(218,426,97,103034,303,5,85,10,8.5,34,3.4,101,102),(219,426,97,103044,304,5,85,10,8.5,34,3.4,101,102),(220,426,97,103063,306,5,75,20,15,30,6,103,102),(221,426,97,103014,301,5,85,10,8.5,34,3.4,102,102),(222,426,97,103073,307,5,75,20,15,30,6,104,102),(223,426,97,103084,308,5,85,10,8.5,34,3.4,105,106),(224,419,111,103025,302,5,95,10,9.5,38,3.8,101,102),(225,419,111,103035,303,5,95,10,9.5,38,3.8,101,102),(226,419,111,103055,305,5,95,10,9.5,38,3.8,101,102),(227,419,111,103045,304,5,95,10,9.5,38,3.8,101,102),(228,419,111,103015,301,5,95,10,9.5,38,3.8,102,102),(229,419,111,103065,306,5,95,20,19,38,7.6,103,102),(230,419,111,103075,307,5,95,20,19,38,7.6,104,102),(231,419,111,103085,308,5,95,10,9.5,38,3.8,105,106),(232,421,124,103024,302,5,85,10,8.5,34,3.4,101,102),(233,421,124,103034,303,5,85,10,8.5,34,3.4,101,102),(234,421,124,103044,304,5,85,10,8.5,34,3.4,101,102),(235,421,124,103054,305,5,85,10,8.5,34,3.4,101,102),(236,421,124,103014,301,5,85,10,8.5,34,3.4,102,102),(237,421,124,103074,307,5,85,20,17,34,6.8,104,102),(238,421,124,103064,306,5,85,20,17,34,6.8,103,102),(239,421,124,103084,308,5,85,10,8.5,34,3.4,105,106),(240,437,124,103045,304,5,95,10,9.5,38,3.8,101,102),(241,437,124,103024,302,5,85,10,8.5,34,3.4,101,102),(242,437,124,103055,305,5,95,10,9.5,38,3.8,101,102),(243,437,124,103014,301,5,85,10,8.5,34,3.4,102,102),(244,437,124,103064,306,5,85,20,17,34,6.8,103,102),(245,437,124,103034,303,5,85,10,8.5,34,3.4,101,102),(246,437,124,103074,307,5,85,20,17,34,6.8,104,102),(247,437,124,103084,308,5,85,10,8.5,34,3.4,105,106),(248,448,87,103033,303,5,75,10,7.5,30,3,101,102),(249,448,87,103044,304,5,85,10,8.5,34,3.4,101,102),(250,448,87,103023,302,5,75,10,7.5,30,3,101,102),(251,448,87,103063,306,5,75,20,15,30,6,103,102),(252,448,87,103053,305,5,75,10,7.5,30,3,101,102),(253,448,87,103014,301,5,85,10,8.5,34,3.4,102,102),(254,448,87,103074,307,5,85,20,17,34,6.8,104,102),(255,448,87,103084,308,5,85,10,8.5,34,3.4,105,106),(256,447,87,103053,305,5,75,10,7.5,30,3,101,102),(257,447,87,103013,301,5,75,10,7.5,30,3,102,102),(258,447,87,103024,302,5,85,10,8.5,34,3.4,101,102),(259,447,87,103033,303,5,75,10,7.5,30,3,101,102),(260,447,87,103044,304,5,85,10,8.5,34,3.4,101,102),(261,447,87,103064,306,5,85,20,17,34,6.8,103,102),(262,447,87,103073,307,5,75,20,15,30,6,104,102),(263,447,87,103084,308,5,85,10,8.5,34,3.4,105,106),(264,452,87,103024,302,5,85,10,8.5,34,3.4,101,102),(265,452,87,103044,304,5,85,10,8.5,34,3.4,101,102),(266,452,87,103014,301,5,85,10,8.5,34,3.4,102,102),(267,452,87,103034,303,5,85,10,8.5,34,3.4,101,102),(268,452,87,103053,305,5,75,10,7.5,30,3,101,102),(269,452,87,103064,306,5,85,20,17,34,6.8,103,102),(270,452,87,103084,308,5,85,10,8.5,34,3.4,105,106),(271,452,87,103074,307,5,85,20,17,34,6.8,104,102),(272,453,124,103024,302,5,85,10,8.5,34,3.4,101,102),(273,453,124,103034,303,5,85,10,8.5,34,3.4,101,102),(274,453,124,103044,304,5,85,10,8.5,34,3.4,101,102),(275,453,124,103053,305,5,75,10,7.5,30,3,101,102),(276,453,124,103014,301,5,85,10,8.5,34,3.4,102,102),(277,453,124,103063,306,5,75,20,15,30,6,103,102),(278,453,124,103073,307,5,75,20,15,30,6,104,102),(279,453,124,103084,308,5,85,10,8.5,34,3.4,105,106),(280,451,127,103035,303,5,95,10,9.5,38,3.8,101,102),(281,451,127,103062,306,5,65,20,13,26,5.2,103,102),(282,451,127,103054,305,5,85,10,8.5,34,3.4,101,102),(283,451,127,103014,301,5,85,10,8.5,34,3.4,102,102),(284,451,127,103044,304,5,85,10,8.5,34,3.4,101,102),(285,451,127,103024,302,5,85,10,8.5,34,3.4,101,102),(286,451,127,103074,307,5,85,20,17,34,6.8,104,102),(287,451,127,103083,308,5,75,10,7.5,30,3,105,106),(288,454,124,103033,303,5,75,10,7.5,30,3,101,102),(289,454,124,103023,302,5,75,10,7.5,30,3,101,102),(290,454,124,103043,304,5,75,10,7.5,30,3,101,102),(291,454,124,103053,305,5,75,10,7.5,30,3,101,102),(292,454,124,103064,306,5,85,20,17,34,6.8,103,102),(293,454,124,103014,301,5,85,10,8.5,34,3.4,102,102),(294,454,124,103073,307,5,75,20,15,30,6,104,102),(295,454,124,103084,308,5,85,10,8.5,34,3.4,105,106),(296,456,127,103045,304,5,95,10,9.5,38,3.8,101,102),(297,456,127,103014,301,5,85,10,8.5,34,3.4,102,102),(298,456,127,103054,305,5,85,10,8.5,34,3.4,101,102),(299,456,127,103035,303,5,95,10,9.5,38,3.8,101,102),(300,456,127,103064,306,5,85,20,17,34,6.8,103,102),(301,456,127,103024,302,5,85,10,8.5,34,3.4,101,102),(302,456,127,103084,308,5,85,10,8.5,34,3.4,105,106),(303,456,127,103074,307,5,85,20,17,34,6.8,104,102),(304,412,76,103025,302,5,95,10,9.5,38,3.8,101,102),(305,412,76,103035,303,5,95,10,9.5,38,3.8,101,102),(306,412,76,103044,304,5,85,10,8.5,34,3.4,101,102),(307,412,76,103055,305,5,95,10,9.5,38,3.8,101,102),(308,412,76,103015,301,5,95,10,9.5,38,3.8,102,102),(309,412,76,103065,306,5,95,20,19,38,7.6,103,102),(310,412,76,103074,307,5,85,20,17,34,6.8,104,102),(311,412,76,103084,308,5,85,10,8.5,34,3.4,105,106),(312,424,76,103025,302,5,95,10,9.5,38,3.8,101,102),(313,424,76,103044,304,5,85,10,8.5,34,3.4,101,102),(314,424,76,103035,303,5,95,10,9.5,38,3.8,101,102),(315,424,76,103055,305,5,95,10,9.5,38,3.8,101,102),(316,424,76,103015,301,5,95,10,9.5,38,3.8,102,102),(317,424,76,103065,306,5,95,20,19,38,7.6,103,102),(318,424,76,103074,307,5,85,20,17,34,6.8,104,102),(319,424,76,103083,308,5,75,10,7.5,30,3,105,106),(320,413,134,103024,302,5,85,10,8.5,34,3.4,101,102),(321,413,134,103053,305,5,75,10,7.5,30,3,101,102),(322,413,134,103014,301,5,85,10,8.5,34,3.4,102,102),(323,413,134,103063,306,5,75,20,15,30,6,103,102),(324,413,134,103034,303,5,85,10,8.5,34,3.4,101,102),(325,413,134,103044,304,5,85,10,8.5,34,3.4,101,102),(326,413,134,103074,307,5,85,20,17,34,6.8,104,102),(327,413,134,103084,308,5,85,10,8.5,34,3.4,105,106);
/*!40000 ALTER TABLE `score_penguji` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sessions` (
  `session_id` varchar(128) NOT NULL,
  `expires` int unsigned NOT NULL,
  `data` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nim` double DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `hashed_password` varchar(255) DEFAULT NULL,
  `new_password_token` varchar(255) DEFAULT NULL,
  `new_password_token_expires_at` longtext,
  `ipk` decimal(6,2) DEFAULT NULL,
  `peminatan_id` int DEFAULT NULL,
  `metlit_id` int DEFAULT NULL,
  `ta_id` int DEFAULT NULL,
  `eprt_id` int DEFAULT NULL,
  `form_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `nim` (`nim`)
) ENGINE=InnoDB AUTO_INCREMENT=459 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (412,1202180031,'SHAFA TATHYA LARASATI','shafatathya@student.telkomuniversity.ac.id','SI4207','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.83,5,14,NULL,NULL,NULL),(413,1202180182,'ACHMAD AKBAR HIDAYATULLAH','achmadakbar@student.telkomuniversity.ac.id','SI4209','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.60,3,14,NULL,NULL,NULL),(414,1202180229,'IFEN FARIDIAN RAHMADAN','ifenfaridian@student.telkomuniversity.ac.id','SI4206','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,0.00,4,14,NULL,NULL,NULL),(415,1202180268,'TAUFIQ MAULANA FIRDAUS','taufiqmaulanafirdaus@student.telkomuniversity.ac.id','SI4201','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.80,1,14,NULL,NULL,NULL),(416,1202181065,'ZAHWA ALIFAH AMMATULLAH','zahwaaalifah@student.telkomuniversity.ac.id','SI4208','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.69,4,14,NULL,NULL,NULL),(417,1202183291,'RIA RAHMAWATI','riarahmawati@student.telkomuniversity.ac.id','SI4203','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.92,3,14,NULL,NULL,NULL),(418,1202183317,'SITI NIHAYATUL CHOIRIYAH','choiryaya@student.telkomuniversity.ac.id','SI4203','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.93,3,14,NULL,NULL,NULL),(419,1202183322,'RIZALRASYD DWISELIA RIDWANAH','rizalrasyd@student.telkomuniversity.ac.id','SI4202','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.86,3,14,NULL,NULL,NULL),(420,1202183324,'JASMINE AURELY SALSHABILLAH','jasmineaurely@student.telkomuniversity.ac.id','SI4207','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.81,1,14,NULL,NULL,NULL),(421,1202183343,'MUHAMAD RIDWAN FIRDAUS','ridwanfirdaus@student.telkomuniversity.ac.id','SI42INT','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.75,4,14,NULL,NULL,NULL),(422,1202183361,'ANASTASSYA GUSTIRANI','anastassya@student.telkomuniversity.ac.id','SI4205','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.81,1,14,NULL,NULL,NULL),(423,1202184019,'SHAHNAZ KAMILAH','shahnazk@student.telkomuniversity.ac.id','SI4208','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.82,5,14,NULL,NULL,NULL),(424,1202184040,'I GEDE PASEK PUNIA ATMAJA','pasekpuniaatmaja@student.telkomuniversity.ac.id','SI4202','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.78,5,14,NULL,NULL,NULL),(425,1202184077,'NANDA ARFAN HAKIM','nandarfan@student.telkomuniversity.ac.id','SI4202','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.62,4,14,NULL,NULL,NULL),(426,1202184112,'YOVITA MARGARET ABIGAIL PURBA','yovitamargaretap@student.telkomuniversity.ac.id','SI4209','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.73,4,14,NULL,NULL,NULL),(427,1202184143,'ARDDHANA ZHAFRAN AMANULLAH','arddhanazhafran@student.telkomuniversity.ac.id','SI4209','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.96,3,14,NULL,NULL,NULL),(428,1202184159,'ILMA NUR HIDAYATI','ilmanurhidayati@student.telkomuniversity.ac.id','SI4209','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.94,3,14,NULL,NULL,NULL),(429,1202184170,'SONYA RISNENTRI','sonyarisnentri@student.telkomuniversity.ac.id','SI4208','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.66,5,14,NULL,NULL,NULL),(430,1202184173,'IZZA ARIANI','izzaariani@student.telkomuniversity.ac.id','SI4202','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.72,1,14,NULL,NULL,NULL),(431,1202184174,'RISMA NUR DAMAYANTI','rismanurdamayanti@student.telkomuniversity.ac.id','SI4203','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.90,1,14,NULL,NULL,NULL),(432,1202184178,'AULINA MEIDINAH KARIM','aulinameidinah@student.telkomuniversity.ac.id','SI4207','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.70,5,14,NULL,NULL,NULL),(433,1202184272,'ZAFIRA CHAIRUNNISA HERIANSJAH PUTRI','zafirachairunnisa@student.telkomuniversity.ac.id','SI4206','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.70,5,14,NULL,NULL,NULL),(434,1202184276,'IKHSAN TASEF NUR FIKRI','ikhsantasef@student.telkomuniversity.ac.id','SI4208','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.73,5,14,NULL,NULL,NULL),(435,1202184297,'FATMA KURNIA FEBRIANTI','byeolyn@student.telkomuniversity.ac.id','SI4208','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.81,1,14,NULL,NULL,NULL),(436,1202184310,'MUHAMMAD DIFAGAMA IVANKA','difagama@student.telkomuniversity.ac.id','SI4208','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.77,3,14,NULL,NULL,NULL),(437,1202184316,'YESSY PERMATASARI','yessypermatasari@student.telkomuniversity.ac.id','SI-42-INT','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.94,4,14,NULL,NULL,NULL),(438,1202184336,'TITOK AJI CAKRA','titokaji@student.telkomuniversity.ac.id','SI4208','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,3.75,5,14,NULL,NULL,NULL),(439,123,'Test','','','$2b$10$3LSGMsWWW4GnCzG99lYMwefpNaS76sXylbXsgMhe8oa.PjkXu9GOi',NULL,NULL,0.00,NULL,NULL,NULL,NULL,NULL),(440,1202144167,'LUTHFIANINGRUM KHRISTANTI MAHARDHIKA','','SI-38-05','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,0.00,4,14,NULL,NULL,NULL),(441,1202160232,'HARDITYO NUR KUNCORO','','SI-40-06','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,0.00,3,14,NULL,NULL,NULL),(442,1202160355,'SHIDRATUL KHODRI','','SI-40-04','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,0.00,5,14,NULL,NULL,NULL),(443,1202164152,'KAZAN ALFAN IHSAN','','SI-40-08','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,0.00,3,14,NULL,NULL,NULL),(444,1202164393,'M. AIDIL SYAHPUTRA ARITONANG','','SI-40-04','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,0.00,5,14,NULL,NULL,NULL),(445,1202170190,'MUHAMMAD YUSUF TANTU','','SI-41-03','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,0.00,2,14,NULL,NULL,NULL),(446,1202170346,'MUJA KALAGUN BARAYA','','SI-41-06','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,0.00,4,14,NULL,NULL,NULL),(447,1202172176,'MUHAMMAD SATRIADI DESTIAN','','SI-41-03','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,0.00,5,14,NULL,NULL,NULL),(448,1202172358,'IRFAN WIRA ADHITAMA','','SI-41-06','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,0.00,5,14,NULL,NULL,NULL),(449,1202173304,'MUHAMMAD FADWA MUFRIZ','','SI-41-06','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,0.00,3,14,NULL,NULL,NULL),(450,1202174002,'THALITA SHAFFAH BRATANDARI','','SI-41-01','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,0.00,5,14,NULL,NULL,NULL),(451,1202174209,'ALDA ANDITA NURANI','','SI-41-01','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,0.00,4,14,NULL,NULL,NULL),(452,1202183389,'ABDESSALAM M. OTHMANE','','EX-SI-42-INT','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,0.00,5,14,NULL,NULL,NULL),(453,1202184126,'GHUFRON FIKRIANTO','','SI-42-03','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,0.00,4,14,NULL,NULL,NULL),(454,1202184135,'DEWA MADE SURYA PERMANA MASTRA','','SI-42-03','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,0.00,4,14,NULL,NULL,NULL),(455,1202184209,'TITISARI RAMADHANE','','SI-42-05','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,0.00,5,14,NULL,NULL,NULL),(456,1202192366,'HAFIDH ZUHDI','','SI-43-INT','$2b$10$vg.A59NjZJ2l01CVfuBJT.yWvn1fSc7Xc66almtj/IhhknIHrzTOm',NULL,NULL,0.00,4,14,NULL,NULL,NULL),(457,1202180341,'BERLIAN AKBAR RUSMANA','berlianakbar@student.telkomuniversity.ac.id','','$2b$10$3LSGMsWWW4GnCzG99lYMwefpNaS76sXylbXsgMhe8oa.PjkXu9GOi',NULL,NULL,0.00,NULL,NULL,NULL,NULL,NULL),(458,1202198381,'YOSIFA GIANINDA','ygianinda@student.telkomuniversity.ac.id','','$2b$10$3LSGMsWWW4GnCzG99lYMwefpNaS76sXylbXsgMhe8oa.PjkXu9GOi',NULL,NULL,0.00,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `studentservice`
--

DROP TABLE IF EXISTS `studentservice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `studentservice` (
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studentservice`
--

LOCK TABLES `studentservice` WRITE;
/*!40000 ALTER TABLE `studentservice` DISABLE KEYS */;
/*!40000 ALTER TABLE `studentservice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_clo`
--

DROP TABLE IF EXISTS `sub_clo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sub_clo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `portion` double DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  `clo_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=309 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_clo`
--

LOCK TABLES `sub_clo` WRITE;
/*!40000 ALTER TABLE `sub_clo` DISABLE KEYS */;
INSERT INTO `sub_clo` VALUES (301,'Bab I (Latar belakang)',10,0,102),(302,'Bab I (Rumusan Masalah)',10,0,101),(303,'Bab I (Tujuan Tugas Akhir)',10,0,101),(304,'Bab I (Manfaat Tugas Akhir)',10,0,101),(305,'Bab I (Batasan Masalah)',10,0,101),(306,'Bab II (Literature review)',20,0,103),(307,'Bab III (Metode Penelitian)',20,0,104),(308,'Penulisan laporan',10,0,105);
/*!40000 ALTER TABLE `sub_clo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_clo_rubric`
--

DROP TABLE IF EXISTS `sub_clo_rubric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sub_clo_rubric` (
  `id` int NOT NULL AUTO_INCREMENT,
  `rubric_code` double DEFAULT NULL,
  `score_per_rubric` double DEFAULT NULL,
  `description` varchar(900) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  `sub_clo_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103086 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_clo_rubric`
--

LOCK TABLES `sub_clo_rubric` WRITE;
/*!40000 ALTER TABLE `sub_clo_rubric` DISABLE KEYS */;
INSERT INTO `sub_clo_rubric` VALUES (103011,1,0,'tidak ada ',0,301),(103012,2,65,'ada',0,301),(103013,3,75,'terdapat fakta sesuai dengan konteks masalah saja yang diambil dari sumber literatur yang dapat dipertanggungjawabkan',0,301),(103014,4,85,'terdapat fakta sesuai konteks masalah dari literatur dan analisis dari data tersebut sehingga mendukung pentingnya dilakukan penelitian TA',0,301),(103015,5,95,'terdapat fakta sesuai konteks masalah dari literatur dan analisis dari data tersebut sehingga mendukung pentingnya dilakukan penelitian TA, serta didukung dengan pengambilan kesimpulan yang logis terdapat fakta, analisis masalah, pengambilan kesimpulan yang logis',0,301),(103021,1,0,'tidak ada ',0,302),(103022,2,65,'ada dan mengulangi judul',0,302),(103023,3,75,'terdapat relasi dengan latar belakang',0,302),(103024,4,85,'1. relasi dengan permasalahan di latar belakang 2. dapat menyelesaikan masalah secara keseluruhan termasuk framework.metodologi yang akan digunakan 3. tidak mengulangi judul',0,302),(103025,5,95,'1. relasi dengan permasalahan di latar belakang 2. berupa pertanyaan-pertanyaan masalah penelitian 3. dapat menyelesaikan masalah secara keseluruhan termasuk framework.metodologi yang akan digunakan 4. tidak mengulangi judul',0,302),(103031,1,0,'tidak ada ',0,303),(103032,2,65,'tidak sesuai dengan jumlah rumusan masalah',0,303),(103033,3,75,'sesuai dengan jumlah rumusan masalah',0,303),(103034,4,85,'1. sesuai jumlah rumusan masalah 2. menjawab permasalahan penelitian dalam domain SI',0,303),(103035,5,95,'1. sesuai jumlah rumusan masalah 2. menjawab permasalahan penelitian 3. menjawab permasalahan penelitian dan menggambarkan penyelesaian penelitian secara tuntas pada domain SI',0,303),(103041,1,0,'tidak ada ',0,304),(103042,2,65,'manfaat yang disampaikan hanya untuk manfaat mahasiswa saja',0,304),(103043,3,75,'manfaat yang disampaikan hanya: 1, untuk mahasiswa 2. untuk kampus',0,304),(103044,4,85,'manfaat yang disampaikan hanya: 1, untuk mahasiswa 2. untuk kampus3. memberikan gambaran manfaat untuk industri atau obyek penelitian',0,304),(103045,5,95,'manfaat yang disampaikan hanya: 1, untuk mahasiswa 2. untuk kampus 3. memberikan gambaran manfaat untuk industri atau obyek penelitian 4. menggambarkan manfaat kontribusi penelitian pada keilmuan SI ',0,304),(103051,1,0,'tidak ada ',0,305),(103052,2,65,'ada namun isinya mengulang judul penelitian',0,305),(103053,3,75,'tidak mengulang judul penelitian',0,305),(103054,4,85,'1. tidak mengulang judul penelitian 2. menggambarkan eksepsi dengan benar sesuai lingkup penelitian',0,305),(103055,5,95,'1. tidak mengulang judul penelitian 2. menggambarkan eksepsi dengan benar sesuai lingkup penelitian 3. disertai penjelasan asumsi atau dasar dari setiap batasan penelitiannya',0,305),(103061,1,0,'tidak ada ',0,306),(103062,2,65,'1.Jumah paper / referensi yang digunakan <=5 (internasional) 2.Tidak terdapat elaborasi antar refernsi',0,306),(103063,3,75,'1.Jumah paper / referensi yang digunakan <=5 (internasional)2.Elaborasi antar refernsi dan keterkaitan dg permasalahan penelitian ',0,306),(103064,4,85,'1.Jumah paper / referensi yang digunakan <=10 (internasional) 2.Elaborasi antar refernsi dan keterkaitan dg permasalahan penelitian',0,306),(103065,5,95,'1.Jumah paper / referensi yang digunakan >=15 (internasional)2.Terdapat jejak-jejeak penelitian sebelmnya3.Elaborasi antar refernsi dan keterkaitan dg permasalahan penelitian',0,306),(103071,1,0,'tidak ada ',0,307),(103072,2,65,'mampu menentukan metode penelitian dan memberikan penjelasan mengenai langkah-langkah TA',0,307),(103073,3,75,'mampu memilih dan menggunakan metode dengan menyertakan penjelasan mengapa memilih metode yang digunakan dalam membangun TA namun belum menyertakan metode evaluasi hasil TA atau tidak menyertakan penjelasan metode secara jelas',0,307),(103074,4,85,'mampu memilih dan menggunakan metode dengan menyertakan penjelasan mengapa memilih metode yang digunakan dalam membangun dan evaluasi hasil TA',0,307),(103075,5,95,'mampu memilih dan menggunakan metode dengan menyertakan analisis metode-metode yang ada dan memberikan penjelasan mengapa memilih metode yang digunakan dalam membangun dan evaluasi hasil TA',0,307),(103081,1,0,'tidak ada ',0,308),(103082,2,65,'1. sesuai template 2. menggunakan font times new roman size 12, spasi 1.5 dan paragraph justify',0,308),(103083,3,75,'1. sesuai template 2. menggunakan font times new roman size 12, spasi 1.5 dan paragraph justify 3. Meniliki kohesi yang baik antar kalimat',0,308),(103084,4,85,'1. sesuai template 2. menggunakan font times new roman size 12, spasi 1.5 dan paragraph justify 3. Meniliki kohesi yang baik antar kalimat 4. Memiliki kohesi yang baik antar paragraf',0,308),(103085,5,95,'1. sesuai template 2. menggunakan font times new roman size 12, spasi 1.5 dan paragraph justify 3. Meniliki kohesi yang baik antar kalimat 4. Memiliki kohesi yang baik antar paragraf 5. memiliki kalimat yang ringkas, baku, dan mengikuti aturan tata tulis bahasa indonesia (koma, titik, dll) dengan baik dan benar',0,308);
/*!40000 ALTER TABLE `sub_clo_rubric` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ta`
--

DROP TABLE IF EXISTS `ta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ta` (
  `id` int NOT NULL AUTO_INCREMENT,
  `file_name` varchar(50) NOT NULL,
  `taUrl` varchar(255) DEFAULT NULL,
  `taFd` varchar(255) DEFAULT NULL,
  `student_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `student_id` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ta`
--

LOCK TABLES `ta` WRITE;
/*!40000 ALTER TABLE `ta` DISABLE KEYS */;
INSERT INTO `ta` VALUES (3,'1202184143_ARDDHANA ZHAFRAN A_SI42GABX_PROPOSAL','/get-ta/427/1202184143_ARDDHANA ZHAFRAN A_SI42GABX_PROPOSAL','/var/www/aplikasita1/.tmp/uploads/64e6623f-c110-410c-86ce-7289c90e8b2d.pdf',427),(4,'Proposal TA_Ikhsan Tasef Nur Fikri_1202184276','/get-ta/434/Proposal TA_Ikhsan Tasef Nur Fikri_1202184276','/var/www/aplikasita1/.tmp/uploads/25611fdb-50a6-4938-9355-97bede7c219b.pdf',434),(5,'Proposal TA_Ilma Nur Hidayati_1202184159','/get-ta/428/Proposal TA_Ilma Nur Hidayati_1202184159','/var/www/aplikasita1/.tmp/uploads/8d399876-1d4b-4e08-afc6-d9e856f4bf86.pdf',428),(6,'Zahwa Alifah Ammatullah_1202181065','/get-ta/416/Zahwa Alifah Ammatullah_1202181065','/var/www/aplikasita1/.tmp/uploads/edf31ae0-b23c-4f55-9b39-d32ad6591414.pdf',416),(7,'1202184310_Proposal TA_DE_Muhammad Difagama Ivanka','/get-ta/436/1202184310_Proposal TA_DE_Muhammad Difagama Ivanka','/var/www/aplikasita1/.tmp/uploads/d791828d-48a9-494a-a12a-c5ea61ad4ac6.pdf',436),(8,'Nanda Arfan Hakim_1202184077_SI42GABX','/get-ta/425/Nanda Arfan Hakim_1202184077_SI42GABX','/var/www/aplikasita1/.tmp/uploads/253f4262-36a1-412e-8049-d4f0224a6384.pdf',425),(9,'Proposal TA_Siti Nihayatul Choiriyah_1202183317','/get-ta/418/Proposal TA_Siti Nihayatul Choiriyah_1202183317','/var/www/aplikasita1/.tmp/uploads/c1639fd4-910f-4ca0-a726-e52bb9c736df.pdf',418),(10,'Proposal TA_Rizalrasyd Dwiselia R_1202183322','/get-ta/419/Proposal TA_Rizalrasyd Dwiselia R_1202183322','/var/www/aplikasita1/.tmp/uploads/c150d2e3-3613-4aa5-a604-9beae0df709c.pdf',419),(11,'PROPOSAL TA_IZZA ARIANI_1202184173_SI42GABX','/get-ta/430/PROPOSAL TA_IZZA ARIANI_1202184173_SI42GABX','/var/www/aplikasita1/.tmp/uploads/1d232930-ece2-401d-9344-3054be5adb5b.pdf',430),(12,'TA1_TAUFIQ MAULANA FIRDAUS_1202180268_SI42GABX','/get-ta/415/TA1_TAUFIQ MAULANA FIRDAUS_1202180268_SI42GABX','/var/www/aplikasita1/.tmp/uploads/43b66ed0-2c2e-4fd0-be72-937caf30253d.pdf',415),(13,'PROPOSAL_TITOK_1202184336','/get-ta/438/PROPOSAL_TITOK_1202184336','/var/www/aplikasita1/.tmp/uploads/9f4f7aef-fa05-4c1e-a0ef-abac9a253ac6.pdf',438),(14,'1202183291_RiaRahmawati','/get-ta/417/1202183291_RiaRahmawati','/var/www/aplikasita1/.tmp/uploads/dfa5fc5b-bd75-4d6b-a831-74ff65fdbe3c.pdf',417),(15,'TA1_Jasmine Aurely Salshabillah_1202183324','/get-ta/420/TA1_Jasmine Aurely Salshabillah_1202183324','/var/www/aplikasita1/.tmp/uploads/008145f9-34c2-428f-890c-6fa7fe110e6b.pdf',420),(16,'TA1_Yovita Margaret Abigail Purba_1202184112','/get-ta/426/TA1_Yovita Margaret Abigail Purba_1202184112','/var/www/aplikasita1/.tmp/uploads/45c12d31-e706-4964-9ba5-708c8f41355d.pdf',426),(17,'TA1_1202180182-compressed','/get-ta/413/TA1_1202180182-compressed','/var/www/aplikasita1/.tmp/uploads/4dd17d5c-fbfd-431e-a835-1692741ec50e.pdf',413),(18,'FileProposalTA1Update','/get-ta/432/FileProposalTA1Update','/var/www/aplikasita1/.tmp/uploads/89a3ff69-eaf0-469a-a1c4-c6ab03d02be8.pdf',432),(19,'Proposal Tugas Akhir_1202184272','/get-ta/433/Proposal Tugas Akhir_1202184272','/var/www/aplikasita1/.tmp/uploads/41163a5a-c172-4558-bf3c-30a9296ab856.pdf',433),(20,'TA1_TITISARI_RAMADHANE_1202184209','/get-ta/455/TA1_TITISARI_RAMADHANE_1202184209','/var/www/aplikasita1/.tmp/uploads/2067d897-47c5-4614-89ef-faf46dd6e16e.pdf',455),(21,'Proposal TA_Shahnaz Kamilah_1202184019','/get-ta/423/Proposal TA_Shahnaz Kamilah_1202184019','/var/www/aplikasita1/.tmp/uploads/c145fa25-34c7-4aee-8186-4e237590ae5e.pdf',423),(22,'Proposal_Sonya_1202184170','/get-ta/429/Proposal_Sonya_1202184170','/var/www/aplikasita1/.tmp/uploads/d25dd3fd-a11f-4487-8674-85855243f532.pdf',429),(23,'TA1_ShafaTathyaLarasati_1202180031','/get-ta/412/TA1_ShafaTathyaLarasati_1202180031','/var/www/aplikasita1/.tmp/uploads/1f14c6b6-d34a-47f3-9c53-06a990e85453.pdf',412),(24,'PROPOSAL_1202184316','/get-ta/437/PROPOSAL_1202184316','/var/www/aplikasita1/.tmp/uploads/6815b8ce-3197-45cd-9a6c-54c30cfe72b1.pdf',437),(25,'PROPOSAL TA Ifen Faridian Rahmadan_1202180229 fix','/get-ta/414/PROPOSAL TA Ifen Faridian Rahmadan_1202180229 fix','/var/www/aplikasita1/.tmp/uploads/76a8ade6-32fc-43c9-bb2b-97cb373b3dbf.pdf',414),(26,'PROPOSAL3_RISMA_NUR_DAMAYANTI_1202184174_SI42GABX','/get-ta/431/PROPOSAL3_RISMA_NUR_DAMAYANTI_1202184174_SI42GABX','/var/www/aplikasita1/.tmp/uploads/67b70972-4fbe-45a0-8cc3-570bf73a6620.pdf',431),(27,'TA1_IGedePasekPuniaA_1202184040','/get-ta/424/TA1_IGedePasekPuniaA_1202184040','/var/www/aplikasita1/.tmp/uploads/cd770cb5-7ebe-4c8b-b580-f60cc7bd3768.pdf',424),(28,'TA_1202183343_MRidwanF','/get-ta/421/TA_1202183343_MRidwanF','/var/www/aplikasita1/.tmp/uploads/4416bc0f-6062-443a-ada0-919e3b4177e9.pdf',421),(29,'ISI4C2_Poposal_1202144167_LUTHFIANINGRUM','/get-ta/440/ISI4C2_Poposal_1202144167_LUTHFIANINGRUM','/var/www/aplikasita1/.tmp/uploads/51ae3bd8-0f8a-4a10-95ce-54244d719677.pdf',440),(30,'Abdessalam','/get-ta/452/Abdessalam','/var/www/aplikasita1/.tmp/uploads/25f55645-7770-4775-bd09-2b7bb7b2feef.pdf',452),(31,'Dewa Made','/get-ta/454/Dewa Made','/var/www/aplikasita1/.tmp/uploads/41c706d4-a0a9-4d99-ba5d-78596e26a485.pdf',454),(32,'Ghufron','/get-ta/453/Ghufron','/var/www/aplikasita1/.tmp/uploads/4f8ba36c-6e96-4ef0-8f79-05b64b2d8b3f.pdf',453),(33,'Aidil','/get-ta/444/Aidil','/var/www/aplikasita1/.tmp/uploads/34b71a14-a7f2-42fb-9d1c-7817bdcadf17.pdf',444),(34,'Irfan','/get-ta/448/Irfan','/var/www/aplikasita1/.tmp/uploads/8fa918e5-bda2-40fb-96cf-e1fff242a11a.pdf',448),(35,'Alda','/get-ta/451/Alda','/var/www/aplikasita1/.tmp/uploads/1fe4179b-7445-47e6-8e69-788bbcf79bb7.pdf',451),(36,'Muja','/get-ta/446/Muja','/var/www/aplikasita1/.tmp/uploads/322420ff-9872-4382-82bb-6300399f82d5.pdf',446),(37,'Satriadi','/get-ta/447/Satriadi','/var/www/aplikasita1/.tmp/uploads/721a8c60-b63c-466f-8e17-c724667724c5.pdf',447),(38,'Hafidh','/get-ta/456/Hafidh','/var/www/aplikasita1/.tmp/uploads/684c8278-62db-4b7d-ab6a-580555fd73e0.pdf',456),(39,'Shidratul','/get-ta/442/Shidratul','/var/www/aplikasita1/.tmp/uploads/ef435660-884a-4f41-8fc8-20cecb0ac750.pdf',442),(40,'REVISI PROPOSAL 3_ANASTASSYA GUSTIRANI','/get-ta/422/REVISI PROPOSAL 3_ANASTASSYA GUSTIRANI','/var/www/aplikasita1/.tmp/uploads/0a71d4f2-98be-4722-982c-c9b34441bb66.pdf',422),(41,'1202184297_FATMA KURNIA FEBRIANTI','/get-ta/435/1202184297_FATMA KURNIA FEBRIANTI','/var/www/aplikasita1/.tmp/uploads/5d899f56-2f29-4014-9a59-a9a88853dec1.pdf',435);
/*!40000 ALTER TABLE `ta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `topic`
--

DROP TABLE IF EXISTS `topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `topic` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `quota` double DEFAULT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  `is_archived` tinyint(1) DEFAULT NULL,
  `peminatan_id` int DEFAULT NULL,
  `period_id` int DEFAULT NULL,
  `lecturer_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topic`
--

LOCK TABLES `topic` WRITE;
/*!40000 ALTER TABLE `topic` DISABLE KEYS */;
INSERT INTO `topic` VALUES (67,'Business Process Reengineering aplikasi iGracias',3,'-',0,1,5,5,130),(68,'Perancangan Arsitektur Referensi Industri Telekomunikasi',3,'-',0,1,5,5,135),(69,'Perbaikan Alur Sistem dan UI/UX pada Proses ABC pada Aplikasi SAP S/4 HANA PT. Telkom Indonesia',6,'-',0,1,1,5,138),(70,'Pengembangan Simulator Spillway Control Monitoring System',5,'-',0,1,4,5,114),(72,'Process mining aplikasi i-Gracias',4,'-',0,1,3,5,108),(73,'Sistem Pemerintahan Berbasis Elektronik (SPBE) Pemerintah Provinsi Jawa Barat',3,'-',0,1,5,5,69),(74,'Perancangan Arsitektur pada Sudinkes & Puskesmas Jakarta Timur',3,'-',0,1,5,5,76),(75,'Data Cleansing pada Master Data Pelanggan PT XYZ/ PT Telkom Indonesia_75',5,'Merging data customer',1,0,3,5,111),(76,'Cyber resilience application',1,'Cyber resilience application',0,1,5,5,131),(77,'Implementasi sistem pengawasan pemesanan secara bulk order dalam rantai pasok dingin',1,'Implementasi sistem pengawasan pemesanan secara bulk order dalam rantai pasok dingin',0,1,4,5,92),(78,'Pengembangan sistem manajemen gudang berpendingin berbasis web',1,'Pengembangan sistem manajemen gudang berpendingin berbasis web',0,1,4,5,92),(79,'Sistem informasi absensi online pada pondok pesantren modern',1,'Sistem informasi absensi online pada pondok pesantren modern',0,1,4,5,95),(80,'Analisis dan Perancangan Manajemen Layanan TI PT TIKI',2,'Analisis dan Perancangan Manajemen Layanan TI PT TIKI',0,1,5,5,122),(81,'Pengukuran tingkat kematangan model tata kelola TI ',1,'Pengukuran tingkat kematangan model tata kelola TI ',0,1,5,5,119),(82,'Hidroponik rumahan dengan sistem monitoring NodeMCU',1,'Hidroponik rumahan dengan sistem monitoring NodeMCU',0,1,4,5,92),(84,'Pengembangan aplikasi layanan pengangkutan sampah',1,'Pengembangan aplikasi layanan pengangkutan sampah',0,1,4,5,95),(85,'Analisis tingkat kematangan tata kelola TI PT Telkom Wiltel Bangka Belitung',1,'Analisis tingkat kematangan tata kelola TI PT Telkom Wiltel Bangka Belitung',0,1,5,5,132),(86,'Fuzzy logic water level control for IoT based spillway simulation',1,'Fuzzy logic water level control for IoT based spillway simulation',0,1,4,5,92),(87,'Data Cleansing pada Master Data Pelanggan PT XYZ/ PT Telkom Indonesia',3,'-',0,1,3,5,111),(88,'Aplikasi TA selection',2,'-',0,1,4,5,99),(89,'Analisis dan Perbaikan Kualitas Kode Platform Assessment Tool',2,'-',0,1,4,5,99),(91,'Perancangan Arsitektur Referensi Industri Telekomunikasi_x',3,'-',1,0,5,5,135);
/*!40000 ALTER TABLE `topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `topic_selection`
--

DROP TABLE IF EXISTS `topic_selection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `topic_selection` (
  `id` int NOT NULL AUTO_INCREMENT,
  `optionNum` double DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `topic_id` int DEFAULT NULL,
  `period_id` int DEFAULT NULL,
  `group_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=187 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topic_selection`
--

LOCK TABLES `topic_selection` WRITE;
/*!40000 ALTER TABLE `topic_selection` DISABLE KEYS */;
INSERT INTO `topic_selection` VALUES (160,1,'REJECTED',69,5,117),(162,1,'APPROVED_LAB_RISET',72,5,119),(163,1,'APPROVED_LAB_RISET',67,5,120),(164,1,'APPROVED_LAB_RISET',73,5,121),(166,1,'APPROVED_LAB_RISET',74,5,123),(167,1,'APPROVED_LAB_RISET',70,5,124),(170,1,'APPROVED_LAB_RISET',69,5,127),(172,1,'APPROVED_LAB_RISET',68,5,129),(173,1,'APPROVED_LAB_RISET',79,5,130),(174,1,'APPROVED_LAB_RISET',80,5,131),(175,1,'APPROVED_LAB_RISET',80,5,132),(176,1,'APPROVED_LAB_RISET',84,5,133),(177,1,'APPROVED_LAB_RISET',85,5,134),(178,1,'APPROVED_LAB_RISET',81,5,135),(179,1,'APPROVED_LAB_RISET',82,5,136),(180,1,'APPROVED_LAB_RISET',76,5,137),(181,1,'APPROVED_LAB_RISET',78,5,138),(182,1,'APPROVED_LAB_RISET',77,5,139),(183,1,'APPROVED_LAB_RISET',86,5,140),(184,1,'APPROVED_LAB_RISET',89,5,141),(185,1,'APPROVED_LAB_RISET',88,5,142),(186,1,'APPROVED_LAB_RISET',87,5,143);
/*!40000 ALTER TABLE `topic_selection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `v_reserach_lab_approve`
--

DROP TABLE IF EXISTS `v_reserach_lab_approve`;
/*!50001 DROP VIEW IF EXISTS `v_reserach_lab_approve`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_reserach_lab_approve` AS SELECT 
 1 AS `id`,
 1 AS `topic_selection`,
 1 AS `topic`,
 1 AS `lecturer`,
 1 AS `peminatan`,
 1 AS `group`,
 1 AS `student`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_score_pembimbing`
--

DROP TABLE IF EXISTS `v_score_pembimbing`;
/*!50001 DROP VIEW IF EXISTS `v_score_pembimbing`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_score_pembimbing` AS SELECT 
 1 AS `student_id`,
 1 AS `student_nim`,
 1 AS `student_name`,
 1 AS `lecturer_id`,
 1 AS `lecturer_nik`,
 1 AS `lecturer_name`,
 1 AS `rubric_id`,
 1 AS `rubric_description`,
 1 AS `rubric_code`,
 1 AS `sub_clo_id`,
 1 AS `sub_clo_description`,
 1 AS `clo_id`,
 1 AS `clo_code`,
 1 AS `clo_description`,
 1 AS `plo_id`,
 1 AS `plo_code`,
 1 AS `plo_description`,
 1 AS `period_id`,
 1 AS `nilai_per_rubric`,
 1 AS `portion`,
 1 AS `nilai_calculated_portion`,
 1 AS `nilai_calculated_bobot`,
 1 AS `nilai_calculated_bobot_portion`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_score_penguji`
--

DROP TABLE IF EXISTS `v_score_penguji`;
/*!50001 DROP VIEW IF EXISTS `v_score_penguji`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_score_penguji` AS SELECT 
 1 AS `student_id`,
 1 AS `student_nim`,
 1 AS `student_name`,
 1 AS `lecturer_id`,
 1 AS `lecturer_nik`,
 1 AS `lecturer_name`,
 1 AS `rubric_id`,
 1 AS `rubric_description`,
 1 AS `rubric_code`,
 1 AS `sub_clo_id`,
 1 AS `sub_clo_description`,
 1 AS `clo_id`,
 1 AS `clo_code`,
 1 AS `clo_description`,
 1 AS `plo_id`,
 1 AS `plo_code`,
 1 AS `plo_description`,
 1 AS `period_id`,
 1 AS `nilai_per_rubric`,
 1 AS `portion`,
 1 AS `nilai_calculated_portion`,
 1 AS `nilai_calculated_bobot`,
 1 AS `nilai_calculated_bobot_portion`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_student_report`
--

DROP TABLE IF EXISTS `v_student_report`;
/*!50001 DROP VIEW IF EXISTS `v_student_report`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_student_report` AS SELECT 
 1 AS `id`,
 1 AS `student`,
 1 AS `group`,
 1 AS `optionNum`,
 1 AS `status`,
 1 AS `period`,
 1 AS `lecturer`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_student_report_reviewer`
--

DROP TABLE IF EXISTS `v_student_report_reviewer`;
/*!50001 DROP VIEW IF EXISTS `v_student_report_reviewer`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_student_report_reviewer` AS SELECT 
 1 AS `id`,
 1 AS `student`,
 1 AS `group`,
 1 AS `optionNum`,
 1 AS `status`,
 1 AS `period`,
 1 AS `lecturer`,
 1 AS `reviewer`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_student_status`
--

DROP TABLE IF EXISTS `v_student_status`;
/*!50001 DROP VIEW IF EXISTS `v_student_status`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_student_status` AS SELECT 
 1 AS `id`,
 1 AS `student`,
 1 AS `group`,
 1 AS `optionNum`,
 1 AS `status`,
 1 AS `period`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_topic_selection`
--

DROP TABLE IF EXISTS `v_topic_selection`;
/*!50001 DROP VIEW IF EXISTS `v_topic_selection`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_topic_selection` AS SELECT 
 1 AS `id`,
 1 AS `optionNum`,
 1 AS `status`,
 1 AS `period`,
 1 AS `group`,
 1 AS `topic`,
 1 AS `peminatan`,
 1 AS `lecturer`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `z`
--

DROP TABLE IF EXISTS `z`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `z` (
  `id` int DEFAULT NULL,
  `c1` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `z`
--

LOCK TABLES `z` WRITE;
/*!40000 ALTER TABLE `z` DISABLE KEYS */;
/*!40000 ALTER TABLE `z` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `v_reserach_lab_approve`
--

/*!50001 DROP VIEW IF EXISTS `v_reserach_lab_approve`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_reserach_lab_approve` AS select concat(`s`.`id`,`ts`.`optionNum`,`ts`.`period_id`) AS `id`,`ts`.`id` AS `topic_selection`,`t`.`id` AS `topic`,`l`.`id` AS `lecturer`,`p`.`id` AS `peminatan`,`g`.`id` AS `group`,`s`.`id` AS `student` from ((((((`topic_selection` `ts` join `topic` `t` on((`ts`.`topic_id` = `t`.`id`))) join `group` `g` on((`ts`.`group_id` = `g`.`id`))) join `group_student` `gs` on((`gs`.`group_id` = `g`.`id`))) join `student` `s` on((`gs`.`student_id` = `s`.`id`))) join `lecturer` `l` on((`l`.`id` = `t`.`lecturer_id`))) join `peminatan` `p` on((`p`.`id` = `t`.`peminatan_id`))) where (`ts`.`status` = 'APPROVED') order by `g`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_score_pembimbing`
--

/*!50001 DROP VIEW IF EXISTS `v_score_pembimbing`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`mahasiswa_de`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_score_pembimbing` AS select `sp`.`student_id` AS `student_id`,`s`.`nim` AS `student_nim`,`s`.`name` AS `student_name`,`l`.`id` AS `lecturer_id`,`l`.`nik` AS `lecturer_nik`,`l`.`name` AS `lecturer_name`,`sp`.`rubric_id` AS `rubric_id`,`r`.`description` AS `rubric_description`,`r`.`rubric_code` AS `rubric_code`,`sp`.`sub_clo_id` AS `sub_clo_id`,`sc`.`description` AS `sub_clo_description`,`sp`.`clo_id` AS `clo_id`,`clo`.`clo_code` AS `clo_code`,`clo`.`description` AS `clo_description`,`sp`.`plo_id` AS `plo_id`,`plo`.`plo_code` AS `plo_code`,`plo`.`description` AS `plo_description`,`sp`.`period_id` AS `period_id`,`sp`.`nilai_per_rubric` AS `nilai_per_rubric`,`sp`.`portion` AS `portion`,`sp`.`nilai_calculated_portion` AS `nilai_calculated_portion`,`sp`.`nilai_calculated_bobot` AS `nilai_calculated_bobot`,`sp`.`nilai_calculated_bobot_portion` AS `nilai_calculated_bobot_portion` from ((((((`score_pembimbing` `sp` join `student` `s` on((`s`.`id` = `sp`.`student_id`))) join `lecturer` `l` on((`l`.`id` = `sp`.`lecturer_id`))) join `sub_clo_rubric` `r` on((`r`.`id` = `sp`.`rubric_id`))) join `sub_clo` `sc` on((`sc`.`id` = `sp`.`sub_clo_id`))) join `clo` on((`clo`.`id` = `sp`.`clo_id`))) join `plo` on((`plo`.`id` = `sp`.`plo_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_score_penguji`
--

/*!50001 DROP VIEW IF EXISTS `v_score_penguji`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`mahasiswa_de`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_score_penguji` AS select `sp`.`student_id` AS `student_id`,`s`.`nim` AS `student_nim`,`s`.`name` AS `student_name`,`l`.`id` AS `lecturer_id`,`l`.`nik` AS `lecturer_nik`,`l`.`name` AS `lecturer_name`,`sp`.`rubric_id` AS `rubric_id`,`r`.`description` AS `rubric_description`,`r`.`rubric_code` AS `rubric_code`,`sp`.`sub_clo_id` AS `sub_clo_id`,`sc`.`description` AS `sub_clo_description`,`sp`.`clo_id` AS `clo_id`,`clo`.`clo_code` AS `clo_code`,`clo`.`description` AS `clo_description`,`sp`.`plo_id` AS `plo_id`,`plo`.`plo_code` AS `plo_code`,`plo`.`description` AS `plo_description`,`sp`.`period_id` AS `period_id`,`sp`.`nilai_per_rubric` AS `nilai_per_rubric`,`sp`.`portion` AS `portion`,`sp`.`nilai_calculated_portion` AS `nilai_calculated_portion`,`sp`.`nilai_calculated_bobot` AS `nilai_calculated_bobot`,`sp`.`nilai_calculated_bobot_portion` AS `nilai_calculated_bobot_portion` from ((((((`score_penguji` `sp` join `student` `s` on((`s`.`id` = `sp`.`student_id`))) join `lecturer` `l` on((`l`.`id` = `sp`.`lecturer_id`))) join `sub_clo_rubric` `r` on((`r`.`id` = `sp`.`rubric_id`))) join `sub_clo` `sc` on((`sc`.`id` = `sp`.`sub_clo_id`))) join `clo` on((`clo`.`id` = `sp`.`clo_id`))) join `plo` on((`plo`.`id` = `sp`.`plo_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_student_report`
--

/*!50001 DROP VIEW IF EXISTS `v_student_report`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_student_report` AS select concat(`s`.`id`,`ts`.`optionNum`,`ts`.`period_id`) AS `id`,`s`.`id` AS `student`,`g`.`id` AS `group`,`ts`.`optionNum` AS `optionNum`,`ts`.`status` AS `status`,`ts`.`period_id` AS `period`,`l`.`id` AS `lecturer` from (((((`student` `s` left join `group_student` `gs` on((`gs`.`student_id` = `s`.`id`))) left join `group` `g` on((`gs`.`group_id` = `g`.`id`))) left join `topic_selection` `ts` on((`ts`.`group_id` = `g`.`id`))) left join `topic` `t` on((`ts`.`topic_id` = `t`.`id`))) left join `lecturer` `l` on((`t`.`lecturer_id` = `l`.`id`))) where ((`gs`.`id` = (select max(`group_student`.`id`) from `group_student` where (`group_student`.`student_id` = `s`.`id`))) and (`ts`.`status` = 'APPROVED')) order by `s`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_student_report_reviewer`
--

/*!50001 DROP VIEW IF EXISTS `v_student_report_reviewer`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_student_report_reviewer` AS select concat(`s`.`id`,`ts`.`optionNum`,`ts`.`period_id`) AS `id`,`s`.`id` AS `student`,`g`.`id` AS `group`,`ts`.`optionNum` AS `optionNum`,`ts`.`status` AS `status`,`ts`.`period_id` AS `period`,`l`.`id` AS `lecturer`,`gs`.`lecturer_id` AS `reviewer` from (((((`student` `s` left join `group_student` `gs` on((`gs`.`student_id` = `s`.`id`))) left join `group` `g` on((`gs`.`group_id` = `g`.`id`))) left join `topic_selection` `ts` on((`ts`.`group_id` = `g`.`id`))) left join `topic` `t` on((`ts`.`topic_id` = `t`.`id`))) left join `lecturer` `l` on((`t`.`lecturer_id` = `l`.`id`))) where ((`gs`.`id` = (select max(`group_student`.`id`) from `group_student` where (`group_student`.`student_id` = `s`.`id`))) and (`ts`.`status` = 'APPROVED_LAB_RISET')) order by `s`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_student_status`
--

/*!50001 DROP VIEW IF EXISTS `v_student_status`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_student_status` AS select concat(`s`.`id`,`ts`.`optionNum`,`ts`.`period_id`) AS `id`,`s`.`id` AS `student`,`g`.`id` AS `group`,`ts`.`optionNum` AS `optionNum`,`ts`.`status` AS `status`,`ts`.`period_id` AS `period` from (((`student` `s` left join `group_student` `gs` on((`gs`.`student_id` = `s`.`id`))) left join `group` `g` on((`gs`.`group_id` = `g`.`id`))) left join `topic_selection` `ts` on((`ts`.`group_id` = `g`.`id`))) where (`gs`.`id` = (select max(`group_student`.`id`) from `group_student` where (`group_student`.`student_id` = `s`.`id`))) order by `s`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_topic_selection`
--

/*!50001 DROP VIEW IF EXISTS `v_topic_selection`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`mahasiswa_de`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_topic_selection` AS select `topic_selection`.`id` AS `id`,`topic_selection`.`optionNum` AS `optionNum`,`topic_selection`.`status` AS `status`,`topic_selection`.`period_id` AS `period`,`topic_selection`.`group_id` AS `group`,`topic`.`id` AS `topic`,`topic`.`peminatan_id` AS `peminatan`,`lecturer`.`id` AS `lecturer` from ((`topic_selection` join `topic` on((`topic_selection`.`topic_id` = `topic`.`id`))) join `lecturer` on((`topic`.`lecturer_id` = `lecturer`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-09 16:02:32
