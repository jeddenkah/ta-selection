-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 31 Jan 2021 pada 08.23
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.3.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `taproposal`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `hashed_password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id`, `name`, `hashed_password`) VALUES
(1, 'admin', '$2b$10$IZ12TMj.AtK9tGj869e6HOe4eTFV2sK/po2ZZzRG5oCqcyYXw2khe');

-- --------------------------------------------------------

--
-- Struktur dari tabel `app_setting`
--

CREATE TABLE `app_setting` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `setting_value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `app_setting`
--

INSERT INTO `app_setting` (`id`, `name`, `setting_value`) VALUES
(1, 'period_id', '3'),
(2, 'last_group_id', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `archive`
--

CREATE TABLE `archive` (
  `id` int(11) NOT NULL,
  `createdAt` bigint(20) DEFAULT NULL,
  `fromModel` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `originalRecord` longtext COLLATE utf8_bin DEFAULT NULL,
  `originalRecordId` longtext COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktur dari tabel `clo`
--

CREATE TABLE `clo` (
  `id` int(11) NOT NULL,
  `clo_code` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  `plo_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `clo`
--

INSERT INTO `clo` (`id`, `clo_code`, `description`, `is_deleted`, `plo_id`) VALUES
(1, 'CLO1', 'ARRAY', 0, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `eprt`
--

CREATE TABLE `eprt` (
  `id` int(11) NOT NULL,
  `score` double DEFAULT NULL,
  `eprtUrl` varchar(255) DEFAULT NULL,
  `eprtFd` varchar(255) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `eprt`
--

INSERT INTO `eprt` (`id`, `score`, `eprtUrl`, `eprtFd`, `student_id`) VALUES
(1, 500, 'https://localhost:1337/get-eprt/1', 'C:\\Users\\HP\\Downloads\\project bu tien\\Aplikasi develoment\\.tmp\\uploads\\08c018d0-ba63-4124-ab20-6a1b50c5fe41.jpeg', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `group`
--

CREATE TABLE `group` (
  `id` int(11) NOT NULL,
  `total_students` double DEFAULT NULL,
  `period_id` int(11) DEFAULT NULL,
  `peminatan_id` int(11) DEFAULT NULL,
  `owner` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `group`
--

INSERT INTO `group` (`id`, `total_students`, `period_id`, `peminatan_id`, `owner`) VALUES
(1, 4, 1, 1, NULL),
(2, 4, 1, 1, NULL),
(3, 4, 1, 1, NULL),
(4, 4, 1, 1, NULL),
(5, 4, 1, 1, NULL),
(6, 4, 1, 1, NULL),
(7, 4, 1, 1, NULL),
(8, 4, 1, 1, NULL),
(9, 4, 1, 1, NULL),
(10, 4, 1, 1, NULL),
(11, 4, 1, 1, NULL),
(12, 4, 1, 1, NULL),
(13, 4, 1, 1, NULL),
(14, 4, 1, 1, NULL),
(15, 4, 1, 1, NULL),
(16, 4, 1, 1, NULL),
(17, 4, 1, 1, NULL),
(18, 4, 1, 1, NULL),
(19, 4, 1, 1, NULL),
(20, 4, 1, 1, NULL),
(21, 4, 1, 1, NULL),
(22, 4, 1, 1, NULL),
(23, 4, 1, 1, NULL),
(24, 4, 1, 1, NULL),
(25, 4, 1, 1, NULL),
(26, 4, 1, 1, NULL),
(27, 4, 1, 1, NULL),
(28, 4, 1, 1, NULL),
(29, 4, 1, 1, NULL),
(30, 4, 1, 1, NULL),
(31, 4, 1, 1, NULL),
(32, 4, 1, 1, NULL),
(33, 4, 1, 1, NULL),
(34, 4, 1, 1, NULL),
(35, 4, 1, 1, NULL),
(36, 4, 1, 1, NULL),
(37, 4, 1, 1, NULL),
(38, 4, 1, 1, NULL),
(39, 4, 1, 1, NULL),
(40, 4, 1, 1, NULL),
(41, 4, 1, 1, NULL),
(42, 4, 1, 1, NULL),
(43, 4, 1, 1, NULL),
(44, 4, 1, 1, NULL),
(45, 4, 1, 1, NULL),
(46, 4, 1, 1, NULL),
(47, 4, 1, 1, NULL),
(48, 4, 1, 1, NULL),
(49, 4, 1, 1, NULL),
(50, 4, 1, 1, NULL),
(51, 4, 1, 1, NULL),
(52, 4, 1, 1, NULL),
(53, 4, 1, 1, NULL),
(54, 4, 1, 1, NULL),
(55, 4, 1, 1, NULL),
(56, 4, 1, 1, NULL),
(57, 4, 1, 1, NULL),
(58, 4, 1, 1, NULL),
(59, 4, 1, 1, NULL),
(60, 4, 1, 1, NULL),
(61, 4, 1, 1, NULL),
(62, 4, 1, 1, NULL),
(63, 4, 1, 1, NULL),
(64, 4, 1, 1, NULL),
(65, 4, 1, 1, NULL),
(66, 4, 1, 1, NULL),
(67, 4, 1, 1, NULL),
(68, 4, 1, 1, NULL),
(69, 4, 1, 1, NULL),
(70, 4, 1, 1, NULL),
(71, 4, 1, 1, NULL),
(72, 4, 1, 1, NULL),
(73, 4, 1, 1, NULL),
(74, 4, 1, 1, NULL),
(75, 4, 1, 1, NULL),
(76, 4, 1, 1, NULL),
(77, 2, 3, 6, NULL),
(78, 2, 3, 6, NULL),
(79, 2, 3, 6, NULL),
(80, 3, 3, 3, NULL),
(81, 2, 3, 6, NULL),
(82, 2, 3, 6, NULL),
(83, 2, 3, 6, NULL),
(84, 2, 3, 6, NULL),
(85, 2, 3, 6, NULL),
(86, 1, 3, 6, NULL),
(87, 3, 3, 6, NULL),
(88, 2, 3, 6, NULL),
(89, 2, 3, 6, NULL),
(90, 2, 3, 6, NULL),
(91, 2, 3, 6, NULL),
(92, 2, 3, 6, NULL),
(93, 2, 3, 6, NULL),
(94, 1, 3, 6, NULL),
(95, 2, 3, 6, NULL),
(96, 2, 3, 5, NULL),
(98, 2, 3, 6, 5),
(99, 4, 3, 6, 4),
(100, 2, 3, 6, 34),
(101, 3, 3, 6, 25),
(102, 3, 3, 6, 41),
(103, 3, 3, 5, 8),
(104, 3, 3, 6, 28),
(105, 1, 3, 7, 52),
(106, 1, 3, 5, 355),
(107, 2, 3, 5, 355),
(108, 1, 3, 5, 355),
(109, 2, 3, 5, 355),
(110, 2, 3, 5, 355),
(111, 1, 3, 3, 22);

-- --------------------------------------------------------

--
-- Struktur dari tabel `group_student`
--

CREATE TABLE `group_student` (
  `id` int(11) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `lecturer_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `group_student`
--

INSERT INTO `group_student` (`id`, `group_id`, `student_id`, `lecturer_id`) VALUES
(1, 77, 3, NULL),
(7, 80, 306, NULL),
(8, 80, 323, NULL),
(9, 80, 345, NULL),
(27, 89, 14, NULL),
(29, 90, 14, 57),
(36, 94, 354, NULL),
(37, 95, 3, 14),
(38, 95, 1, 31),
(40, 96, 355, 15),
(47, 98, 10, 57),
(48, 98, 5, 57),
(49, 99, 23, 10),
(50, 99, 20, 22),
(51, 99, 16, NULL),
(52, 99, 4, NULL),
(53, 100, 33, NULL),
(54, 100, 34, NULL),
(56, 101, 28, NULL),
(57, 101, 25, NULL),
(58, 102, 35, NULL),
(59, 102, 51, NULL),
(60, 102, 41, NULL),
(61, 103, 64, NULL),
(62, 103, 19, NULL),
(63, 103, 8, 25),
(65, 104, 28, NULL),
(66, 104, 25, NULL),
(67, 105, 52, NULL),
(68, 106, 355, NULL),
(69, 107, 8, NULL),
(70, 107, 355, NULL),
(71, 108, 355, NULL),
(72, 109, 8, NULL),
(73, 109, 355, NULL),
(74, 110, 8, NULL),
(75, 110, 355, NULL),
(76, 111, 22, 15);

-- --------------------------------------------------------

--
-- Struktur dari tabel `guidance_form`
--

CREATE TABLE `guidance_form` (
  `id` int(11) NOT NULL,
  `formUrl` varchar(255) DEFAULT NULL,
  `formFd` varchar(255) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `guidance_form`
--

INSERT INTO `guidance_form` (`id`, `formUrl`, `formFd`, `student_id`) VALUES
(1, 'http://taproposal.bubat-dev.com/upload-form/2', '/home/taproposal/taproposal.bubat-dev.com/.tmp/uploads/092ab35b-a343-4b23-b561-cbeaf0ce5712.txt', 2),
(2, 'https://localhost:1337/get-form/1', 'C:\\Users\\HP\\Downloads\\project bu tien\\Aplikasi TA\\.tmp\\uploads\\5d54f248-49d2-4d85-88b1-2ba9ab63c998.jpeg', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jfa`
--

CREATE TABLE `jfa` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `abbrev` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `jfa`
--

INSERT INTO `jfa` (`id`, `name`, `abbrev`) VALUES
(1, 'Non Jabatan Fungsional', 'NJFA'),
(2, 'Asisten Ahli', 'AA'),
(3, 'Lektor', 'L'),
(4, 'Lektor Kepala', 'LK'),
(5, 'Profesor', 'Prof');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kk`
--

CREATE TABLE `kk` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `abbrev` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `kk`
--

INSERT INTO `kk` (`id`, `name`, `abbrev`) VALUES
(1, 'Enterprise and Industrial System', 'EINS'),
(2, 'Cybernetics', 'Cybernetics');

-- --------------------------------------------------------

--
-- Struktur dari tabel `lab_riset`
--

CREATE TABLE `lab_riset` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `abbrev` varchar(255) DEFAULT NULL,
  `kk_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `lab_riset`
--

INSERT INTO `lab_riset` (`id`, `name`, `abbrev`, `kk_id`) VALUES
(1, 'System Architecture and Governance', 'SAG', 1),
(2, 'Enterprise Data Engineering', 'EDE', 2),
(3, 'Enterprise System Development', 'ESD', 2),
(4, 'Enterprise Infrastructure Management', 'EIM', 1),
(5, 'Enterprise Resource Planning', 'ERP', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `lecturer`
--

CREATE TABLE `lecturer` (
  `id` int(11) NOT NULL,
  `nik` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `hashed_password` varchar(255) DEFAULT NULL,
  `new_password_token` varchar(255) DEFAULT NULL,
  `new_password_token_expires_at` longtext DEFAULT NULL,
  `lecturer_code` varchar(255) DEFAULT NULL,
  `jfa_id` int(11) DEFAULT NULL,
  `lab_riset_id` int(11) DEFAULT NULL,
  `prodi_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `lecturer`
--

INSERT INTO `lecturer` (`id`, `nik`, `name`, `email`, `hashed_password`, `new_password_token`, `new_password_token_expires_at`, `lecturer_code`, `jfa_id`, `lab_riset_id`, `prodi_id`) VALUES
(5, '1111', 'Dosen', 'naufalwwidyatama@gmail.com', '$2b$10$IZ12TMj.AtK9tGj869e6HOe4eTFV2sK/po2ZZzRG5oCqcyYXw2khe', NULL, NULL, 'AA', 5, 1, 1),
(6, '1000', 'Namama', 'naufalwwidyatama@gmail.com', '$2b$10$IZ12TMj.AtK9tGj869e6HOe4eTFV2sK/po2ZZzRG5oCqcyYXw2khe', NULL, NULL, 'CD', 1, 3, 1),
(10, '5555', 'Tien Fabrianti', 'naufalwwidyatama@gmail.com', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', 'Y3NoldW3JZhM6JdozTiQdXF4HzNbZ1Ms', '2019-07-12 10:58:21.000', 'DA', 1, 3, 1),
(11, '123456', 'AblehS', 'abc@abc.dom', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, '234567890', 5, NULL, NULL),
(14, '13710054', 'ADITYAS WIDJAJARTO, S.T., M.T.', 'adwjrt@gmail.com', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'AWJ', 1, NULL, NULL),
(15, '17890112', 'AHMAD ALMAARIF, S.Kom, MT', 'ahmad.almaarif@gmail.com', '$2b$10$IZ12TMj.AtK9tGj869e6HOe4eTFV2sK/po2ZZzRG5oCqcyYXw2khe', NULL, NULL, 'LIF', 1, 4, 1),
(16, '16610019', 'IR. AHMAD MUSNANSYAH, MSc.', 'ahmadanc@gmail.com', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'MSN', 3, NULL, NULL),
(17, '13840043', 'ALBI FITRANSYAH, S.Si., M.T.', 'albi_fit@yahoo.co.id', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'ABF', 1, NULL, NULL),
(18, '18890133', 'ALVI SYAHRINA, MSc.', 'syahrina@telkomuniversity.ac.id', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'ALV', 1, NULL, NULL),
(19, '17910086', 'ANIK HANIFATUL AZIZAH, S.Kom, MIM', NULL, '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'HAZ', 1, NULL, NULL),
(20, '14660049', 'ARI FAJAR SANTOSO, IR, M.T.', 'arifajar@telkomuniversity.ac.id', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'ARJ', 2, NULL, NULL),
(21, '16880020', 'ASTI AMALIA NUR FAJRILLAH, B.MM., M.SC', 'astiamalianf@gmail.com', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'NFL', 2, NULL, NULL),
(22, '18750077', 'AVON BUDIONO, S.T., M.T.', NULL, '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'AVB', 3, NULL, NULL),
(23, '10760011', 'DR. BASUKI RAHMAD, CISA.,CISM.,CRISC', 'basuki@transforma-institute.biz', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'BSR', 1, NULL, NULL),
(24, '18930095', 'BERLIAN MAULIDYA IZZATI, S.Kom., M.Kom.', 'berlianmi@telkomuniversity.ac.id', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'BMZ', 1, NULL, NULL),
(25, '14720028', 'DEDEN WITARSYAH, ST., M.ENG Ph.D', 'dedenw@telkomuniversity.ac.id', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'DWH', 3, NULL, NULL),
(26, '17890109', 'EDI SUTOYO, S.Kom., M.Comp.Sc.', 'edisutoyo89@gmail.com', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'ETO', 2, NULL, NULL),
(27, '14900050', 'FAISHAL MUFIED AL ANSHARY, S.Kom., M.Kom., M.Sc', 'anshary90@gmail.com', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'FMA', 1, NULL, NULL),
(28, '10860013', 'FERDIAN, S.T., M.T.', 'mr.ferdian@gmail.com', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'FED', 1, NULL, NULL),
(29, '10740059', 'ILHAM PERDANA, S.T., M.T.', 'lhamdana@gmail.com', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'ILD', 3, NULL, NULL),
(30, '18880133', 'IQBAL SANTOSA', 'iqbals@telkomuniversity.ac.id', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'IQO', 1, NULL, NULL),
(31, '14750038', 'Dr. IRFAN DARMAWAN, ST., M.T.', 'dirfand@gmail.com', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'IFD', 4, NULL, NULL),
(32, '12630005', 'Dr. Ir. LUKMAN ABDURRAHMAN, MIS', 'abdural@telkomuniversity.ac.id', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'LMN', 3, NULL, NULL),
(33, '15890025', 'LUTHFI RAMADANI, S.T., M.T.', 'luthfiramadani@gmail.com', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'LRI', 2, NULL, NULL),
(34, '13860085', 'M. TEGUH KURNIAWAN, S.T,.M.T', 'teguhkurniawan@telkomuniversity.ac.id', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'MTK', 3, NULL, NULL),
(35, '14850055', 'MUHAMMAD AZANI HASIBUAN, S.Kom., M.T.I', 'muhammadazani@telkomuniversity.ac.id', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'MAZ', 2, NULL, NULL),
(36, '18890136', 'MUHARDI SAPUTRA, S.ST.,M.T.', NULL, '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'UHS', 1, NULL, NULL),
(37, '18860125', 'MUHARMAN LUBIS, B.IT., M.IT., PhD.IT', NULL, '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'MRL', 1, NULL, NULL),
(38, '5820066', 'MURAHARTAWATY, S.T., M.T.', 'murahartawaty@telkomuniversity.ac.id', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'MHY', 2, NULL, NULL),
(39, '14770014', 'NIA AMBARSARI, S.Si., M.T.', 'niaambarsari@telkomuniversity.ac.id', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'NAS', 3, NULL, NULL),
(40, '15830041', 'NUR ICHSAN UTAMA, S.T., M.T.', 'ichsan83@gmail.com', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'NIU', 1, NULL, NULL),
(41, '15900004', 'PUTRA FAJAR ALAM, S.SI, M.T.', 'putrafajaralam@gmail.com', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'PFA', 2, NULL, NULL),
(42, '14690010', 'R. WAHJOE WITJAKSONO , S.T, M.M', 'rwahyuwicaksono@gmail.com', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'RWW', 2, NULL, NULL),
(43, '14890057', 'RACHMADITA ANDRESWARI, S.KOM., M.KOM.', 'andreswari@gmail.com', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'RHA', 2, NULL, NULL),
(44, '17900092', 'RAHMAT FAUZI, S.T., M.T.', 'rahmatfauzi.its@gmail.com', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'RFZ', 2, NULL, NULL),
(45, '10780050', 'RAHMAT MULYANA, S.T., M.T., M.B.A., PMP, CISA, CISM', 'rahmatmoelyana@telkomuniversity.ac.id', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'RMM', 1, NULL, NULL),
(46, '760016', 'RD. ROHMAT SAEDUDIN S.T., M. T.', 'rdrohmat@telkomuniversity.ac.id', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'ROH', 3, NULL, NULL),
(47, '10760007', 'RIDHA HANAFI, S.T., M.T.', 'ridhanafi@yahoo.com', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'RDF', 2, NULL, NULL),
(48, '8760034', 'RIZA AGUSTIANSYAH, S.T., M.Kom', 'rizaagustiansyah@telkomuniversity.ac.id', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'RIZ', 2, NULL, NULL),
(49, '18820089', 'ROKHMAN FAUZI, S.T., M.T.', NULL, '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'RKZ', 1, NULL, NULL),
(50, '10780051', 'SENO ADI PUTRA, S.Si., M.T.', 'seno_ap@yahoo.com', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'SNP', 3, NULL, NULL),
(51, '14750043', 'SONI FAJAR SURYA GUMILANG, S.T., M.T.', 'sonifajar@gmail.com', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'SFJ', 3, NULL, NULL),
(52, '14830024', 'TAUFIK NUR ADI, S.Kom., M.T.', 'taufik.nur.adi@gmail.com', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'TNA', 2, NULL, NULL),
(53, '14790008', 'Dr. TIEN FABRIANTI KUSUMASARI', 'tien_kusumasari@telkomuniversity.ac.id', '$2y$12$O2OK9wVVyNXZnnVj7QJXJunWexgkcUU.dr8Js75t7i2CoPMAal4CS ', 'Q64qBrtTWGa82lI9jK8cSK9GEZT5XSQR', '2020-09-13 19:19:23.343', 'TFD', 2, 3, 1),
(54, '14840067', 'UMAR YUNAN KURNIA SEPTO HEDIYANTO, S.T., M.T', 'umar.yunan.ksh@gmail.com', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'UMY', 1, NULL, NULL),
(55, '10820005', 'WARIH PUSPITASARI, S.PSI., M.PSI.', 'indigophie@yahoo.com', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'WRP', 2, NULL, NULL),
(56, '10790021', 'YULI ADAM PRASETYO, S.T., M.T.', 'adam@telkomuniversity.ac.id', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, 'YAP', 3, NULL, NULL),
(57, '14790008-1', 'TIEN FABRIANTI KUSUMASARI', 'tienkusumasari@telkomuniversity.ac.id', '$2b$10$IZ12TMj.AtK9tGj869e6HOe4eTFV2sK/po2ZZzRG5oCqcyYXw2khe', NULL, NULL, 'TFK', 3, 3, 1),
(58, '20890020-2', 'AHMAD ALMAARIF', 'ahmadalmaarif@telkomuniversity.ac.id', '$2b$10$ea4MhjibybZkN9b.ywZZ9.eqbzRZDcBaEEHlV6kipI2pyTso3LTs6', NULL, NULL, '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `lecturerquota`
--

CREATE TABLE `lecturerquota` (
  `id` int(11) NOT NULL,
  `lecturer_quota` double DEFAULT NULL,
  `lecturer_id` int(11) DEFAULT NULL,
  `period_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `lecturer_roles__masterrole_roles_masterrole`
--

CREATE TABLE `lecturer_roles__masterrole_roles_masterrole` (
  `id` int(11) NOT NULL,
  `lecturer_roles` int(11) DEFAULT NULL,
  `masterrole_roles_masterrole` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `lecturer_roles__masterrole_roles_masterrole`
--

INSERT INTO `lecturer_roles__masterrole_roles_masterrole` (`id`, `lecturer_roles`, `masterrole_roles_masterrole`) VALUES
(3, 5, 1),
(7, 53, 1),
(8, 53, 2),
(9, 53, 3),
(10, 53, 4),
(22, 6, 8),
(28, 57, 8),
(29, 57, 3),
(30, 57, 4),
(31, 15, 3),
(32, 15, 4),
(33, 15, 8);

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_role`
--

CREATE TABLE `master_role` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `resourceId` double DEFAULT NULL,
  `resourceType` varchar(255) DEFAULT NULL,
  `isCoordinator` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `master_role`
--

INSERT INTO `master_role` (`id`, `name`, `resourceId`, `resourceType`, `isCoordinator`) VALUES
(1, 'Koordinator Metlit', NULL, '', 1),
(2, 'Ketua Lab Riset', 2, 'labriset', 1),
(3, 'Dosen Pembina', 3, '', 1),
(4, 'Dosen Penguji', 4, '', 1),
(5, 'Dosen Metlit', NULL, '', 0),
(8, 'Ketua KK', NULL, '', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `metlit`
--

CREATE TABLE `metlit` (
  `id` int(11) NOT NULL,
  `class` varchar(255) DEFAULT NULL,
  `period_id` int(11) DEFAULT NULL,
  `prodi_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `metlit`
--

INSERT INTO `metlit` (`id`, `class`, `period_id`, `prodi_id`) VALUES
(1, 'SI-41-01', 3, 1),
(2, 'SI-41-02', 3, 1),
(3, 'SI-41-03', 3, 1),
(4, 'SI-41-04', 3, 1),
(5, 'SI-41-05', 3, 1),
(6, 'SI-41-06', 3, 1),
(8, 'SI-41-07', 3, 1),
(9, 'SI-41-08', 3, 1),
(10, 'SI-41-09', 3, 1),
(11, 'SI-41-INT', 3, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `mycontroller`
--

CREATE TABLE `mycontroller` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminatan`
--

CREATE TABLE `peminatan` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `abbrev` varchar(255) DEFAULT NULL,
  `prodi_id` int(11) DEFAULT NULL,
  `labRisetInduk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `peminatan`
--

INSERT INTO `peminatan` (`id`, `name`, `abbrev`, `prodi_id`, `labRisetInduk`) VALUES
(1, 'Enterprise Resource Planning', 'ERP', 1, 5),
(2, 'Enterprise Architecture', 'EA', 1, 1),
(3, 'Enterprise Infrastructure Management', 'EIM', 1, 4),
(4, 'Information System Management', 'ISM', 1, 1),
(5, 'Enterprise Data Engineering', 'EDE', 1, 2),
(6, 'Enterprise Intelligent System Development', 'EISD', 1, 3),
(7, 'System Architecture and Governance', 'SAG', 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `period`
--

CREATE TABLE `period` (
  `id` int(11) NOT NULL,
  `semester` varchar(255) DEFAULT NULL,
  `academic_year` varchar(255) DEFAULT NULL,
  `per_lecturer_quota` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `period`
--

INSERT INTO `period` (`id`, `semester`, `academic_year`, `per_lecturer_quota`) VALUES
(1, 'GANJIL', '2018-2019', 0),
(2, 'GENAP', '2018-2019', 0),
(3, 'GANJIL', '2020/2021', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `plo`
--

CREATE TABLE `plo` (
  `id` int(11) NOT NULL,
  `plo_code` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `plo`
--

INSERT INTO `plo` (`id`, `plo_code`, `description`, `is_deleted`) VALUES
(1, '', '', 1),
(2, '1', 'asdfg', 1),
(3, 'PLO1', 'Computasi dan matematis', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `prodi`
--

CREATE TABLE `prodi` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `abbrev` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `prodi`
--

INSERT INTO `prodi` (`id`, `name`, `abbrev`) VALUES
(1, 'S1 Sistem Informasi', 'S1 SI');

-- --------------------------------------------------------

--
-- Struktur dari tabel `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `topic_title` varchar(255) DEFAULT NULL,
  `topic_title_status` varchar(255) DEFAULT NULL,
  `file_link` varchar(255) DEFAULT NULL,
  `final_score` double DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `period_id` int(11) DEFAULT NULL,
  `reviewer_id` int(11) DEFAULT NULL,
  `reviewerScoreInRubric` int(11) DEFAULT NULL,
  `supervisorScoreInRubric` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sessions`
--

CREATE TABLE `sessions` (
  `session_id` varchar(128) NOT NULL,
  `expires` int(11) UNSIGNED NOT NULL,
  `data` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `nim` double DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `hashed_password` varchar(255) DEFAULT NULL,
  `new_password_token` varchar(255) DEFAULT NULL,
  `new_password_token_expires_at` longtext DEFAULT NULL,
  `ipk` decimal(6,2) DEFAULT NULL,
  `peminatan_id` int(11) DEFAULT NULL,
  `metlit_id` int(11) DEFAULT NULL,
  `ta_id` int(11) DEFAULT NULL,
  `eprt_id` int(11) DEFAULT NULL,
  `form_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `student`
--

INSERT INTO `student` (`id`, `nim`, `name`, `email`, `class`, `hashed_password`, `new_password_token`, `new_password_token_expires_at`, `ipk`, `peminatan_id`, `metlit_id`, `ta_id`, `eprt_id`, `form_id`) VALUES
(1, 1202174146, 'RADEN HASNA FADHILA', 'rhasnafadhila@student.telkomuniversity.ac.id', 'SI-41-GAB', '$2b$10$IZ12TMj.AtK9tGj869e6HOe4eTFV2sK/po2ZZzRG5oCqcyYXw2khe', NULL, NULL, '2.00', 6, 5, NULL, NULL, NULL),
(3, 1202170069, 'DIMAS NASHIRUDDIN AL FARUQ', 'dimasna@student.telkomuniversity.ac.id', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '3.77', 6, 1, NULL, NULL, NULL),
(4, 1202170366, 'MOHAMMAD AQIEL ILHAMY', 'maqiel@student.telkomuniversity.ac.id', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 4, NULL, NULL, NULL),
(5, 1202150083, 'FADHLURRAHMAN AFIF', '', 'SI-39-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 1, NULL, NULL, NULL),
(6, 1202170015, 'M IKHSAN PRATAMA P', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 1, NULL, NULL, NULL),
(7, 1202170025, 'AFIFAH YUSRI SALSABILA', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 1, NULL, NULL, NULL),
(8, 1202170080, 'ARDISA TSANIYA PUTRI', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 1, NULL, NULL, NULL),
(9, 1202170093, 'MAULIDA RAHMI', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 1, NULL, NULL, NULL),
(10, 1202170136, 'DWI YASTI FACHRUNNISA SUHERMAN', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 1, NULL, NULL, NULL),
(11, 1202170154, 'ALVAN ATHALLAH KAMAL', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 1, NULL, NULL, NULL),
(12, 1202170162, 'JOCIKA CHANDRA DEWI', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 1, NULL, NULL, NULL),
(13, 1202170173, 'HARYO DEWANTO', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 1, NULL, NULL, NULL),
(14, 1202170187, 'BIMA SYAFTIAAN', '', 'SI-41-01', '$2b$10$IZ12TMj.AtK9tGj869e6HOe4eTFV2sK/po2ZZzRG5oCqcyYXw2khe', NULL, NULL, '0.00', 6, 1, NULL, NULL, NULL),
(15, 1202170249, 'IRFANI FAHRI', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 1, NULL, NULL, NULL),
(16, 1202170257, 'RIZKI AL FATWA', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 1, NULL, NULL, NULL),
(17, 1202170262, 'TRAHTANDWINA LINA AYWANDARI', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 1, NULL, NULL, NULL),
(18, 1202170283, 'SYIFA ARIA SALSABILA', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 1, NULL, NULL, NULL),
(19, 1202170329, 'SANDY FINANDRA', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 1, NULL, NULL, NULL),
(20, 1202170367, 'RIZKY AZIS JAYASUTISNA', '', 'SI-41-01', '$2b$10$IZ12TMj.AtK9tGj869e6HOe4eTFV2sK/po2ZZzRG5oCqcyYXw2khe', NULL, NULL, '0.00', 6, 1, NULL, NULL, NULL),
(21, 1202172001, 'MISBACHUL FIQKRI', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 1, NULL, NULL, NULL),
(22, 1202172041, 'DIPA LASMAWARDHANA', '', 'SI-41-01', '$2b$10$IZ12TMj.AtK9tGj869e6HOe4eTFV2sK/po2ZZzRG5oCqcyYXw2khe', NULL, NULL, '0.00', 3, 1, NULL, NULL, NULL),
(23, 1202172055, 'MUHAMMAD ARDAN AL FAREZI ARIFIN', '', 'SI-41-01', '$2b$10$IZ12TMj.AtK9tGj869e6HOe4eTFV2sK/po2ZZzRG5oCqcyYXw2khe', NULL, NULL, '0.00', 6, 1, NULL, NULL, NULL),
(24, 1202172094, 'MUHAMAD GILANG GINANJAR', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 1, NULL, NULL, NULL),
(25, 1202172276, 'FAKHRI RIFQI FIRDAUS', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 1, NULL, NULL, NULL),
(26, 1202172350, 'FAIZ SUKMA DANII', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 1, NULL, NULL, NULL),
(27, 1202172391, 'MOHAMAD KHALIF NOER', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 1, NULL, NULL, NULL),
(28, 1202173112, 'NAURATUL ULFAH', '', 'SI-41-01', '$2b$10$IZ12TMj.AtK9tGj869e6HOe4eTFV2sK/po2ZZzRG5oCqcyYXw2khe', NULL, NULL, '0.00', 6, 1, NULL, NULL, NULL),
(29, 1202173297, 'MUHAMMAD FAHRIZAL RAHMAWAN', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 1, NULL, NULL, NULL),
(30, 1202174002, 'THALITA SHAFFAH BRATANDARI', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 1, NULL, NULL, NULL),
(31, 1202174016, 'RANGGA RAY IRAWAN', '', 'SI-41-01', '$2b$10$IZ12TMj.AtK9tGj869e6HOe4eTFV2sK/po2ZZzRG5oCqcyYXw2khe', NULL, NULL, '0.00', 6, 1, NULL, NULL, NULL),
(32, 1202174059, 'KINTAN ERIDANI PUTRI', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 1, NULL, NULL, NULL),
(33, 1202174082, 'UMI ZAHROH', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 1, NULL, NULL, NULL),
(34, 1202174083, 'TANIA AYU SEKAR ARUM', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 1, NULL, NULL, NULL),
(35, 1202174087, 'HANAVING', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 1, NULL, NULL, NULL),
(36, 1202174096, 'FAIDATUL HIKMAH', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 1, NULL, NULL, NULL),
(37, 1202174104, 'MUHAMMAD ABIYYU MUNIF', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 1, NULL, NULL, NULL),
(38, 1202174125, 'RAHMAT FAUZI PANE', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 1, NULL, NULL, NULL),
(39, 1202174127, 'FIKRI ABDULLAH', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 1, NULL, NULL, NULL),
(40, 1202174129, 'FARHANDI MAULANA PUTRA', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 1, NULL, NULL, NULL),
(41, 1202174185, 'PRAMESTI MAUDIPUTRI', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 1, NULL, NULL, NULL),
(42, 1202174199, 'YUNAN SUKHO ADITYA', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 1, NULL, NULL, NULL),
(43, 1202174218, 'KEVIN H NAINGGOLAN', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 1, NULL, NULL, NULL),
(44, 1202174227, 'RIZKIA SITI AFIFAH', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 1, NULL, NULL, NULL),
(45, 1202174233, 'AHMAD ZEN FADILAH', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 1, NULL, NULL, NULL),
(46, 1202174235, 'FRISCA FEBRIYANI KURNIAWAN', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 1, NULL, NULL, NULL),
(47, 1202174282, 'AZIFAH DHAFIN RIZQULLAH ALDI', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 1, NULL, NULL, NULL),
(48, 1202174295, 'ROSANICHA', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 1, NULL, NULL, NULL),
(49, 1202174299, 'ROBI DEWI ASIH PRAMESTI', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 1, NULL, NULL, NULL),
(50, 1202174311, 'VALEN DEANDRA', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 1, NULL, NULL, NULL),
(51, 1202174321, 'ALIFIA ZAHRA FIRDAUS', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 1, NULL, NULL, NULL),
(52, 1202174355, 'TANIA NIELSANY SIMBOLON', '', 'SI-41-01', '$2b$10$IZ12TMj.AtK9tGj869e6HOe4eTFV2sK/po2ZZzRG5oCqcyYXw2khe', NULL, NULL, '0.00', 7, 1, NULL, NULL, NULL),
(53, 1202174378, 'HENA HAFRANI', '', 'SI-41-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 1, NULL, NULL, NULL),
(54, 1202170023, 'SITI RAFIQAH BALQIS', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 2, NULL, NULL, NULL),
(55, 1202170026, 'RIRI ANISA ARISDILA', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 2, NULL, NULL, NULL),
(56, 1202170038, 'ABDULLAH', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 2, NULL, NULL, NULL),
(57, 1202170042, 'MUHAMMAD RAFI RAHARJO', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 2, NULL, NULL, NULL),
(58, 1202170063, 'TIKA ASTRIANI', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 2, NULL, NULL, NULL),
(59, 1202170077, 'RAGA HARITZ PRATAMA', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 2, NULL, NULL, NULL),
(60, 1202170117, 'MUHAMMAD IQBAL MUFLIHUDDIN', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 2, NULL, NULL, NULL),
(61, 1202170156, 'LUTHFAN ALFAJRI PATRA', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 2, NULL, NULL, NULL),
(62, 1202170188, 'FAUZAN RADIFAN SHIDIQ KAMALUDIN', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 2, NULL, NULL, NULL),
(63, 1202170236, 'M ADITYA PRATAMA CHAN', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 2, NULL, NULL, NULL),
(64, 1202170250, 'RAHADIAN ALDI NUGROHO', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 2, NULL, NULL, NULL),
(65, 1202170280, 'DITA APRILLIA RAHMANI', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 2, NULL, NULL, NULL),
(66, 1202170298, 'MUKHAMAD FURQON', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 2, NULL, NULL, NULL),
(67, 1202170330, 'MADE DARMA WIJAYA', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 2, NULL, NULL, NULL),
(68, 1202170356, 'WILFA DWI RAHMI', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 2, NULL, NULL, NULL),
(69, 1202170369, 'IQBAL TIARA PUTRA', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 2, NULL, NULL, NULL),
(70, 1202172017, 'DUSEF HAGANTA PANDIA', '', 'SI-41-02', '$2b$10$IZ12TMj.AtK9tGj869e6HOe4eTFV2sK/po2ZZzRG5oCqcyYXw2khe', NULL, NULL, '0.00', 1, 2, NULL, NULL, NULL),
(71, 1202172047, 'NARITA AYU PRAHASTIWI', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 2, NULL, NULL, NULL),
(72, 1202172341, 'MOHAMMAD AJIE SUKMA WINANGUN', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 2, NULL, NULL, NULL),
(73, 1202173263, 'AULIA FERINA SENDHITASARI', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 2, NULL, NULL, NULL),
(74, 1202173277, 'YUSUF HUSEN', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 2, NULL, NULL, NULL),
(75, 1202173383, 'SHINTA SARAHAZNA ULFA', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 2, NULL, NULL, NULL),
(76, 1202174003, 'MUH. FAWAZ NAJIB RISAL', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 2, NULL, NULL, NULL),
(77, 1202174004, 'WINDY ASTUTI', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 2, NULL, NULL, NULL),
(78, 1202174032, 'FERNANDA ADITYA NUGRAHA', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 2, NULL, NULL, NULL),
(79, 1202174068, 'ALWAN ALYAFI MULYAWAN', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 2, NULL, NULL, NULL),
(80, 1202174084, 'FAIRUZ ZAHIRAH LIDANTA', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 2, NULL, NULL, NULL),
(81, 1202174103, 'AVEICENA KEMAL ADRIANSYAH', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 2, NULL, NULL, NULL),
(82, 1202174113, 'SKOLASTIKA KURNIA DEWANTI', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 2, NULL, NULL, NULL),
(83, 1202174138, 'LAYLA SYIRIANI AMBARSARI', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 2, NULL, NULL, NULL),
(84, 1202174142, 'I GEDE ARYA DEVA NARAYANA', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 2, NULL, NULL, NULL),
(85, 1202174164, 'INDRIYANI', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 2, NULL, NULL, NULL),
(86, 1202174171, 'MUHAMMAD RICKY CHANDRA DINATA', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 2, NULL, NULL, NULL),
(87, 1202174175, 'WASIS WARDANA', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 2, NULL, NULL, NULL),
(88, 1202174201, 'NURAHDIATNA SUBAGYA', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 2, NULL, NULL, NULL),
(89, 1202174210, 'SUCI NUR ALIFA', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 2, NULL, NULL, NULL),
(90, 1202174219, 'GHERALDI MUHAMMAD SINALOAN', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 2, NULL, NULL, NULL),
(91, 1202174322, 'NADIA FITRI SAFIRA', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 2, NULL, NULL, NULL),
(92, 1202174351, 'REFZA KURNIAWAN', '', 'SI-41-02', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 2, NULL, NULL, NULL),
(93, 1202164346, 'HIRZAN GHUFRAN', '', 'SI-40-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 3, NULL, NULL, NULL),
(94, 1202170030, 'ALDO RIANTO', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 3, NULL, NULL, NULL),
(95, 1202170043, 'SAHRIAL HASAN WICAKSANA', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 3, NULL, NULL, NULL),
(96, 1202170118, 'LUKMAN RIYADI', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 3, NULL, NULL, NULL),
(97, 1202170130, 'YUDANTO EKA PUTRA', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 3, NULL, NULL, NULL),
(98, 1202170166, 'SABILA CHANIFAH', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 3, NULL, NULL, NULL),
(99, 1202170190, 'MUHAMMAD YUSUF TANTU', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 3, NULL, NULL, NULL),
(100, 1202170211, 'ADINDA INEZ SANG', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 3, NULL, NULL, NULL),
(101, 1202170251, 'PRADITYA AGUNG LAKSMANA', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 3, NULL, NULL, NULL),
(102, 1202170265, 'RIAN BIMO ANKHAL', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 3, NULL, NULL, NULL),
(103, 1202170279, 'RAFIK KUMALA ZAI', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 3, NULL, NULL, NULL),
(104, 1202170314, 'MUHAMMAD FARIZQI SETIAWAN', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 3, NULL, NULL, NULL),
(105, 1202170349, 'ENRICO LAZAWARDI', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 3, NULL, NULL, NULL),
(106, 1202170382, 'HATTA MURSYIDAL GHAIST', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 3, NULL, NULL, NULL),
(107, 1202171331, 'DENNY FITRIANSYAH PERDANA', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 3, NULL, NULL, NULL),
(108, 1202172019, 'FAHMI NURALIMI', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 3, NULL, NULL, NULL),
(109, 1202172141, 'ICCA MITA MONICA', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 3, NULL, NULL, NULL),
(110, 1202172176, 'MUHAMMAD SATRIADI DESTIAN', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 3, NULL, NULL, NULL),
(111, 1202172300, 'FIERI CAREZA', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 3, NULL, NULL, NULL),
(112, 1202172370, 'RENALDI TRI ANANDA MINKA', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 3, NULL, NULL, NULL),
(113, 1202172379, 'RANGGA ARDHI KESUMA', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 3, NULL, NULL, NULL),
(114, 1202172398, 'KEMAL INDRA KUSUMA', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 3, NULL, NULL, NULL),
(115, 1202173095, 'RIFKI AULIADA', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 3, NULL, NULL, NULL),
(116, 1202173102, 'NABILA ZAIRA ULFA ABABIL', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 3, NULL, NULL, NULL),
(117, 1202173194, 'EZRA CHRISMYRALDA SIANIPAR', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 3, NULL, NULL, NULL),
(118, 1202173222, 'DIMAS AJI WIRATAMA', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 3, NULL, NULL, NULL),
(119, 1202174005, 'NAVYA KIRANA SAFITRI', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 3, NULL, NULL, NULL),
(120, 1202174008, 'KOMANG GUSTIANA SUGOSHA', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 3, NULL, NULL, NULL),
(121, 1202174027, 'R. A. NADA TAQIYYA IZAZI', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 3, NULL, NULL, NULL),
(122, 1202174033, 'REZSHAL HIDAYAH', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 3, NULL, NULL, NULL),
(123, 1202174071, 'SYAFIRAH ABDULLAH', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 3, NULL, NULL, NULL),
(124, 1202174085, 'YOSEPHINE MAYAGITA TARIGAN', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 3, NULL, NULL, NULL),
(125, 1202174111, 'ARSIKE CIPTA PELANGI', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 3, NULL, NULL, NULL),
(126, 1202174114, 'KIRANA DHIATAMA AYUNDA', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 3, NULL, NULL, NULL),
(127, 1202174143, 'FADLAN FIKRI DZULFIKAR', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 3, NULL, NULL, NULL),
(128, 1202174205, 'MUHAMMAD IRWANDA IRAWAN', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 3, NULL, NULL, NULL),
(129, 1202174225, 'HANDAYANI OKTAVIA', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 3, NULL, NULL, NULL),
(130, 1202174264, 'WIWINDA JUNED PURBA', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 3, NULL, NULL, NULL),
(131, 1202174284, 'HILDA ATHIFAH FAISAL', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 3, NULL, NULL, NULL),
(132, 1202174324, 'NAFIDZAH KIASATI SHADRINA', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 3, NULL, NULL, NULL),
(133, 1202174342, 'JANGKAH WICAKSONO', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 3, NULL, NULL, NULL),
(134, 1202174352, 'M. ILHAM SETIA BUDI', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 3, NULL, NULL, NULL),
(135, 1202174359, 'DESITA NUR ROSYIDIANA', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 3, NULL, NULL, NULL),
(136, 1202174399, 'NYKE PUTRI GREACECITA RAUNG', '', 'SI-41-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 3, NULL, NULL, NULL),
(137, 1202150060, 'HAGANTA BREMA BANGUN', '', 'SI-39-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 4, NULL, NULL, NULL),
(138, 1202160338, 'BRILIANT MARISTA ZAKKA BILLY', '', 'SI-40-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 4, NULL, NULL, NULL),
(139, 1202170006, 'SHINTA EKA RISA SINURAYA', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 4, NULL, NULL, NULL),
(140, 1202170058, 'RANGGA AHSANA', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 4, NULL, NULL, NULL),
(141, 1202170066, 'MUH. DWI SATYA', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 4, NULL, NULL, NULL),
(142, 1202170097, 'WAFIUDDIN AKBAR', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 4, NULL, NULL, NULL),
(143, 1202170131, 'BAYU TRI ANGGORO', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 4, NULL, NULL, NULL),
(144, 1202170207, 'NANDA MILHAN MAKARIM', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 4, NULL, NULL, NULL),
(145, 1202170226, 'NURZAMI NOVRIAN', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 4, NULL, NULL, NULL),
(146, 1202170270, 'RAHARJO PUTRA KURNIADI', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 4, NULL, NULL, NULL),
(147, 1202170301, 'SIRAJUDDIN ABDUL HAKIM', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 4, NULL, NULL, NULL),
(148, 1202170317, 'RONALDO SUBIANTO PANJAITAN', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 4, NULL, NULL, NULL),
(149, 1202170333, 'SULTAN SYAH BILLAL M.A', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 4, NULL, NULL, NULL),
(150, 1202170343, 'RAFIF EQSA BUDI ANGGONO', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 4, NULL, NULL, NULL),
(151, 1202170353, 'JOSHUA BONARDO JUNIOR', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 4, NULL, NULL, NULL),
(152, 1202170360, 'NURUL ASYFA', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 4, NULL, NULL, NULL),
(153, 1202170371, 'VALDIS RAEVAN ALIF RENGKUNG', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 4, NULL, NULL, NULL),
(154, 1202172009, 'MUHAMMAD FAUZAN', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 4, NULL, NULL, NULL),
(155, 1202172073, 'ANNISA SYAFARANI CALLISTA', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 4, NULL, NULL, NULL),
(156, 1202172144, 'EKY CAHYA PUTRA WITJAKSANA', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 4, NULL, NULL, NULL),
(157, 1202172373, 'IRMALISTIA ALFIANI', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 4, NULL, NULL, NULL),
(158, 1202173247, 'MUHAMMAD KADDAFI NASUTION', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 4, NULL, NULL, NULL),
(159, 1202173307, 'WINDRI ALICIA SITOMPUL', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 4, NULL, NULL, NULL),
(160, 1202174034, 'BIMA SUSILA MUKTI', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 4, NULL, NULL, NULL),
(161, 1202174039, 'VENNY YERIA MONARES', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 4, NULL, NULL, NULL),
(162, 1202174086, 'KHOFIFAH RACHMA PRABAWATI', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 4, NULL, NULL, NULL),
(163, 1202174105, 'NORAYANTI SIHOMBING', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 4, NULL, NULL, NULL),
(164, 1202174145, 'NANCY CRESTASIA BR. HUTAURUK', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 4, NULL, NULL, NULL),
(165, 1202174159, 'MUHAMMAD RIZKI HARDIYANTO', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 4, NULL, NULL, NULL),
(166, 1202174167, 'NUR AFDHALIYAH FAHRI', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 4, NULL, NULL, NULL),
(167, 1202174191, 'SUBHAN ARI RAKA PRADANA', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 4, NULL, NULL, NULL),
(168, 1202174212, 'AFINA RAMADHANI', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 4, NULL, NULL, NULL),
(169, 1202174238, 'NAILUL MAROM', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 4, NULL, NULL, NULL),
(170, 1202174252, 'HAMDAN DZIKRUROBBI', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 4, NULL, NULL, NULL),
(171, 1202174259, 'SHITA WIDYA NINGSIH NASIR', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 4, NULL, NULL, NULL),
(172, 1202174266, 'LARASATI VALENSIA', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 4, NULL, NULL, NULL),
(173, 1202174281, 'GHOZALI ANDRI WINADA', '', 'SI-41-04', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 4, NULL, NULL, NULL),
(174, 1202198370, 'ZAKIAH KHATAMI ROMADHONNA', '', 'SIX-43-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 4, NULL, NULL, NULL),
(175, 1202198381, 'YOSIFA GIANINDA', '', 'SIX-43-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 4, NULL, NULL, NULL),
(176, 1202198382, 'ZAHRANI KHAIRUNNISA', '', 'SIX-43-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 4, NULL, NULL, NULL),
(177, 1202198388, 'ANITA WIDYASTUTI', '', 'SIX-43-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 4, NULL, NULL, NULL),
(178, 1202170010, 'COKORDA AGUNG JAYA LAKSMANA PRAKASA', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 5, NULL, NULL, NULL),
(179, 1202170035, 'IVAN PRIYAMBUDI', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 5, NULL, NULL, NULL),
(180, 1202170044, 'HILDA ARIES WIDIYANTI', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 5, NULL, NULL, NULL),
(181, 1202170050, 'MUHAMMAD GHAZI RASYID', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 5, NULL, NULL, NULL),
(182, 1202170092, 'RADITA NAJMI KAMILIA', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 5, NULL, NULL, NULL),
(183, 1202170106, 'BELLA TRIYANNUR MAULIA', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 5, NULL, NULL, NULL),
(184, 1202170132, 'MUHAMMAD RAFLY ARACHMAN', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 5, NULL, NULL, NULL),
(185, 1202170148, 'MUHAMAD IBRAHIM', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 5, NULL, NULL, NULL),
(186, 1202170160, 'MUHAMMAD NOUFAL AJRIYA SIDDIK', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 5, NULL, NULL, NULL),
(187, 1202170169, 'DISA ALFANIA', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 5, NULL, NULL, NULL),
(188, 1202170213, 'DIMAS AGUNG WICAKSONO', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 5, NULL, NULL, NULL),
(189, 1202170240, 'LALA ARIF FATURAHMAN', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 5, NULL, NULL, NULL),
(190, 1202170285, 'ANGGIT MOCHAMMAD GALANG SAMUDRA', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 5, NULL, NULL, NULL),
(191, 1202170344, 'ANDI SETIAWAN', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 5, NULL, NULL, NULL),
(192, 1202170357, 'SALEH HASAN B', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 5, NULL, NULL, NULL),
(193, 1202170385, 'RAKHA LABIB RAMADHAN', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 5, NULL, NULL, NULL),
(194, 1202170389, 'MUHAMMAD DAFFA RAYANDI', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 5, NULL, NULL, NULL),
(195, 1202171098, 'MUHAMMAD ZAQLUL', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 5, NULL, NULL, NULL),
(196, 1202171116, 'NABILA ZHAFIRAH PUTRI', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 5, NULL, NULL, NULL),
(197, 1202171120, 'I MADE INDRA KUSUMA', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 5, NULL, NULL, NULL),
(198, 1202172362, 'WIDIA FEBRIYANI', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 5, NULL, NULL, NULL),
(199, 1202173328, 'SALMA JUMAIZAR HANIF', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 5, NULL, NULL, NULL),
(200, 1202173339, 'HABIEB AR-RACHMAN', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 5, NULL, NULL, NULL),
(201, 1202174007, 'EFLIN TRINOVA LIMBONG', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 5, NULL, NULL, NULL),
(202, 1202174072, 'AHMAD KURNIAWAN', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 5, NULL, NULL, NULL),
(203, 1202174075, 'INTANIA EKA YANTI', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 5, NULL, NULL, NULL),
(204, 1202174088, 'ANISA WIDIA DINI PUTRI', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 5, NULL, NULL, NULL),
(205, 1202174135, 'DELIMA AYU WULANDARI', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 5, NULL, NULL, NULL),
(206, 1202174179, 'EZZA RAMADHANTA MACHMUD RAZAQ', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 5, NULL, NULL, NULL),
(207, 1202174202, 'RAISYA AVRIANI ISKANDAR', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 5, NULL, NULL, NULL),
(208, 1202174220, 'YUNIS FEMILIA NUGRAINI', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 5, NULL, NULL, NULL),
(209, 1202174253, 'MUHAMMAD ZAKI', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 5, NULL, NULL, NULL),
(210, 1202174267, 'NURAINI IKA PRATIWI KALINGARA', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 5, NULL, NULL, NULL),
(212, 1202174288, 'NUR AZIZAH HARUN', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 5, NULL, NULL, NULL),
(213, 1202174309, 'NADYA AMALIA PURWANINGTYAS', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 5, NULL, NULL, NULL),
(214, 1202174335, 'ERADO FATA SANJAYA', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 5, NULL, NULL, NULL),
(215, 1202174374, 'JEREMIAH FERDINAND LOMBOGIA', '', 'SI-41-05', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 5, NULL, NULL, NULL),
(216, 1202170011, 'RAFIF FAKHRI RAMADHAN', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 6, NULL, NULL, NULL),
(217, 1202170062, 'THORIQ IKBAR', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 6, NULL, NULL, NULL),
(218, 1202170099, 'ALEXANDER VINI ADHITYA BAGASKARA', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 6, NULL, NULL, NULL),
(219, 1202170121, 'DIVA JIHAN SAFIRA', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 6, NULL, NULL, NULL),
(220, 1202170133, 'GEDE SUDANTA NETHAN KURU', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 6, NULL, NULL, NULL),
(221, 1202170180, 'IBRAM MOSZARDO', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 6, NULL, NULL, NULL),
(222, 1202170182, 'FEBI TITANIA', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 6, NULL, NULL, NULL),
(223, 1202170193, 'RENAL NUR RACHMAN', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 6, NULL, NULL, NULL),
(224, 1202170229, 'MUHAMMAD VALEN', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 6, NULL, NULL, NULL),
(225, 1202170268, 'MARYATI PUJI LESTARI', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 6, NULL, NULL, NULL),
(226, 1202170272, 'RIFQI ZAIN NAUFAL', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 6, NULL, NULL, NULL),
(227, 1202170289, 'NABILA NUR NAJMI LAILA', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 6, NULL, NULL, NULL),
(228, 1202170332, 'OCTAVIANA TENNISSA POETRY', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 6, NULL, NULL, NULL),
(229, 1202170336, 'MOCHAMMAD FIKRIANSYAH MAULANA', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 6, NULL, NULL, NULL),
(230, 1202170365, 'AURA NAFARENA SYARIFAH', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 6, NULL, NULL, NULL),
(231, 1202170386, 'HILMAN BUDIMAN', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 6, NULL, NULL, NULL),
(232, 1202171139, 'ANANDA FIQRI FIRDAUS', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 6, NULL, NULL, NULL),
(233, 1202172014, 'VINA DWIANA PUTRI', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 6, NULL, NULL, NULL),
(234, 1202172024, 'PRAISE SETIAWAN SARAGIH', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 6, NULL, NULL, NULL),
(235, 1202172045, 'MAIZIAH AZKA NURHAKIM', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 6, NULL, NULL, NULL),
(236, 1202172051, 'BAGUS RIZAL DARMAWAN', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 6, NULL, NULL, NULL),
(237, 1202172358, 'IRFAN WIRA ADHITAMA', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 6, NULL, NULL, NULL),
(238, 1202173147, 'SINDY ARTA SUTARMAN', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 6, NULL, NULL, NULL),
(239, 1202173287, 'FADHIL NAUFAL', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 6, NULL, NULL, NULL),
(240, 1202173304, 'MUHAMMAD FADWA MUFRIZ', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 6, NULL, NULL, NULL),
(241, 1202174036, 'WIBI ADITAMA', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 6, NULL, NULL, NULL),
(242, 1202174074, 'MUHAMMAD RIFKI OKTOVA', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 6, NULL, NULL, NULL),
(243, 1202174079, 'YANEU NURUL AZIZZA', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 6, NULL, NULL, NULL),
(244, 1202174089, 'KHADIJAH HALIMAH ASSA`DIYAH', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 6, NULL, NULL, NULL),
(245, 1202174107, 'RURI FADHILAH', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 6, NULL, NULL, NULL),
(246, 1202174122, 'I GEDE BAGUS PRASETYAWAN', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 6, NULL, NULL, NULL),
(247, 1202174149, 'IKHWAN HADI', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 6, NULL, NULL, NULL),
(248, 1202174153, 'PUTU ADIMERTHA KRISNAAJI DARSANA', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 6, NULL, NULL, NULL),
(249, 1202174161, 'ALDO PRAKASA AFDYA', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 6, NULL, NULL, NULL),
(250, 1202174170, 'AYTA BOANGMANALU', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 6, NULL, NULL, NULL),
(251, 1202174203, 'ALIFIANI SYAHRUNI QOTRUNNISA', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 6, NULL, NULL, NULL),
(252, 1202174221, 'PUTU MINIA DEWI', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 6, NULL, NULL, NULL),
(253, 1202174241, 'SINGGIH KRISMARWANTONI SADINO', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 6, NULL, NULL, NULL),
(254, 1202174245, 'NURAINI RACHMA ARDIANTI', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 6, NULL, NULL, NULL),
(255, 1202174254, 'DAMAS AGRYAN PERMANA', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 6, NULL, NULL, NULL),
(256, 1202174294, 'RATIH SYAIDAH', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 6, NULL, NULL, NULL),
(257, 1202174310, 'LASTRI M.A DOLOKSARIBU', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 6, NULL, NULL, NULL),
(258, 1202174320, 'MAJANGGA', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 6, NULL, NULL, NULL),
(259, 1202174375, 'MUHAMMAD LUTHFI TRIYANA', '', 'SI-41-06', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 6, NULL, NULL, NULL),
(260, 1202164033, 'MAS TAUFIQ DIRGA PRASTICARYA', '', 'SI-40-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 8, NULL, NULL, NULL),
(261, 1202170018, 'DINDA SONIA TIARA', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 8, NULL, NULL, NULL),
(262, 1202170046, 'TANIA TARYONO', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 8, NULL, NULL, NULL),
(263, 1202170168, 'SETYA BUDI PRADANA', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 8, NULL, NULL, NULL),
(264, 1202170242, 'NABIL DIRHAM GHIFARY', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 8, NULL, NULL, NULL),
(265, 1202170248, 'DWI KURNIA', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 8, NULL, NULL, NULL),
(266, 1202170305, 'ADHYAKSA SETIABUDI', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 8, NULL, NULL, NULL),
(267, 1202170323, 'JOSE ARMANDO PRATAMA', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 8, NULL, NULL, NULL),
(268, 1202170337, 'ROBBY ALEXANDER SIHOTANG', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 8, NULL, NULL, NULL),
(269, 1202172100, 'NOVAL ALTAF', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 8, NULL, NULL, NULL),
(270, 1202172230, 'MUHAMMAD RIFKI RUSANDI', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 8, NULL, NULL, NULL),
(271, 1202173123, 'RIZKI AULIA AKBAR LUBIS', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 8, NULL, NULL, NULL),
(272, 1202173150, 'WIGUNA RAMADHAN', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 8, NULL, NULL, NULL),
(273, 1202174012, 'MUHAMMAD AXL BAYU PRATAMA', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 8, NULL, NULL, NULL),
(274, 1202174028, 'HASAN MUHAMMAD DIPOHUTOMO', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 8, NULL, NULL, NULL),
(275, 1202174052, 'FARHAN HASAN AL-AMUDI', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 8, NULL, NULL, NULL),
(276, 1202174076, 'YUDHI SATRIO WIBOWO', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 8, NULL, NULL, NULL),
(277, 1202174134, 'DANNY NAUFAL PRATAMA', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 8, NULL, NULL, NULL),
(278, 1202174151, 'NASSYFA ALFIRDA RIANI', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 8, NULL, NULL, NULL),
(279, 1202174157, 'DEVITA WULANDARI', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 8, NULL, NULL, NULL),
(280, 1202174174, 'THESAPNA DWI ANGGITA OKVANTI', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 8, NULL, NULL, NULL),
(281, 1202174181, 'RAISUL AMIN ZULFIKRI', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 8, NULL, NULL, NULL),
(282, 1202174204, 'TIARA ADONIA', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 8, NULL, NULL, NULL),
(283, 1202174223, 'IKA RAHMAYANTI TAMBUNAN', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 8, NULL, NULL, NULL),
(284, 1202174255, 'JESSY ANUGERAH VINDRAGGETA PARTOSENTONO', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 8, NULL, NULL, NULL),
(285, 1202174269, 'GRACE R SIMANJUNTAK', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 8, NULL, NULL, NULL),
(286, 1202174292, 'CHIKA ENGGAR PUSPITA', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 8, NULL, NULL, NULL),
(287, 1202174296, 'YUDHA ALFARID', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 8, NULL, NULL, NULL),
(288, 1202174326, 'ILHAM NURYANTO', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 8, NULL, NULL, NULL),
(289, 1202174368, 'FATIN HANIFAH', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 8, NULL, NULL, NULL),
(290, 1202174376, 'MUHAMMAD NAUFAL', '', 'SI-41-07', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 8, NULL, NULL, NULL),
(291, 1202198367, 'ROBI GUSNAWAN', '', 'SIX-43-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 8, NULL, NULL, NULL),
(292, 1202198369, 'SANTI AL ARIF', '', 'SIX-43-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 8, NULL, NULL, NULL),
(293, 1202198371, 'ADE HANDIYANTO', '', 'SIX-43-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 8, NULL, NULL, NULL),
(294, 1202198372, 'FADHIL CAHYA KESUMA', '', 'SIX-43-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 8, NULL, NULL, NULL),
(295, 1202198373, 'ARVIN LAZAWARDI PRIAMBODO', '', 'SIX-43-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 8, NULL, NULL, NULL),
(296, 1202198374, 'SHAFRIAN DWITAMA', '', 'SIX-43-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 8, NULL, NULL, NULL),
(297, 1202198375, 'FAKHRUNNISA NUR AFRA', '', 'SIX-43-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 8, NULL, NULL, NULL),
(298, 1202198376, 'ERIN KARINA NUR FADILLAH', '', 'SIX-43-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 8, NULL, NULL, NULL),
(299, 1202198377, 'MEGA CANDRA DEWI', '', 'SIX-43-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 8, NULL, NULL, NULL),
(300, 1202198378, 'RINEZ ASPRINOLA', '', 'SIX-43-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 8, NULL, NULL, NULL),
(301, 1202198379, 'DESSY YUSSELA MOCHAMAD YUSUP', '', 'SIX-43-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 8, NULL, NULL, NULL),
(302, 1202198387, 'RISWAN ARDINATA', '', 'SIX-43-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 8, NULL, NULL, NULL),
(303, 1202198389, 'MUGHNY MUBARAK', '', 'SIX-43-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 8, NULL, NULL, NULL),
(304, 1202198390, 'FAKHRI NAUFAL ALI', '', 'SIX-43-01', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 8, NULL, NULL, NULL);
INSERT INTO `student` (`id`, `nim`, `name`, `email`, `class`, `hashed_password`, `new_password_token`, `new_password_token_expires_at`, `ipk`, `peminatan_id`, `metlit_id`, `ta_id`, `eprt_id`, `form_id`) VALUES
(305, 1202160019, 'FADLIL MUHAMMAD', '', 'SI-40-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 9, NULL, NULL, NULL),
(306, 1202160330, 'MUHAMMAD RIZKI SEPTIAWAN', '', 'SI-40-03', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 9, NULL, NULL, NULL),
(307, 1202170029, 'RANGGA SULTAN TAWAKKAL', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 9, NULL, NULL, NULL),
(308, 1202170081, 'NIRWANA ANGGIE SINAGA', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 9, NULL, NULL, NULL),
(309, 1202170101, 'ARIF MARZUQ SYAHBANI', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 9, NULL, NULL, NULL),
(310, 1202170109, 'INES INDAH PUTRI', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 9, NULL, NULL, NULL),
(311, 1202170232, 'TOMI MULHARTONO', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 9, NULL, NULL, NULL),
(312, 1202170256, 'M.HABIB JAUHARI', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 9, NULL, NULL, NULL),
(313, 1202170293, 'LULU FAIRUZ HUSNIAH PANGERANG', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 9, NULL, NULL, NULL),
(314, 1202170306, 'ARRIZAL FAUZAN BARLIANA', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 9, NULL, NULL, NULL),
(315, 1202170315, 'ZAKIYAH SALSABILA', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 9, NULL, NULL, NULL),
(316, 1202170364, 'M ADHI PRADANA', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 9, NULL, NULL, NULL),
(317, 1202172054, 'CHRISHERYANDA EKA SAPUTRA', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 9, NULL, NULL, NULL),
(318, 1202172152, 'SETYA AJI NURFAIZI', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 9, NULL, NULL, NULL),
(319, 1202172348, 'ANDIKA SIDDIQ GUMELAR', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 9, NULL, NULL, NULL),
(320, 1202173177, 'SHERLY FRATISTA', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 9, NULL, NULL, NULL),
(321, 1202173231, 'IRFAN NAUFAL ARIFDA', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 9, NULL, NULL, NULL),
(322, 1202173377, 'AFIF FATUR RAHMAN', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 9, NULL, NULL, NULL),
(323, 1202174020, 'QIRANI FADHILA RAMADHANI', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 9, NULL, NULL, NULL),
(324, 1202174065, 'MUHAMMAD FIKRI AZHAR', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 9, NULL, NULL, NULL),
(325, 1202174078, 'BINTANG BHARA MUKTI', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 9, NULL, NULL, NULL),
(326, 1202174091, 'NURUL FARIKHAH', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 9, NULL, NULL, NULL),
(327, 1202174124, 'FAHMI DWI ABDURRAHMAN', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 9, NULL, NULL, NULL),
(328, 1202174128, 'AINUNNISA IQLIMA IFFATUSYSYAKILA', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 9, NULL, NULL, NULL),
(329, 1202174155, 'NEILA FADHILA PUTRI', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 9, NULL, NULL, NULL),
(330, 1202174172, 'M.RANANTA LUTHFIKA', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 9, NULL, NULL, NULL),
(331, 1202174183, 'FAHRIZAL HANIF DARMAWAN', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 9, NULL, NULL, NULL),
(332, 1202174197, 'AMORY OCTOVIO', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 9, NULL, NULL, NULL),
(333, 1202174208, 'YUNI ARIYANI ARIFIN', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 9, NULL, NULL, NULL),
(334, 1202174216, 'M.FADHIL AFIF', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 9, NULL, NULL, NULL),
(335, 1202174224, 'ALLISYA NASHFATI JUSTINE', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 9, NULL, NULL, NULL),
(336, 1202174246, 'MUHAMMAD ZIKRI', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 9, NULL, NULL, NULL),
(337, 1202174274, 'MUHAMMAD OKI ANGGA FAIRUZ', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 9, NULL, NULL, NULL),
(338, 1202174278, 'ITSAR SALSABILA ALKHANZA', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 7, 9, NULL, NULL, NULL),
(339, 1202174354, 'PUTU PRIYANKA SONIA DEWI', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 9, NULL, NULL, NULL),
(340, 1202174388, 'CANDRA EKA LAKSANA', '', 'SI-41-08', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 9, NULL, NULL, NULL),
(341, 1202170110, 'SURYA SANJAYA', '', 'SI-41-INT', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 5, 10, NULL, NULL, NULL),
(342, 1202170163, 'RIYAN MUDA KARANA PUTRA', '', 'SI-41-INT', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 10, NULL, NULL, NULL),
(343, 1202170165, 'IRFANANDO WIRANATHA', '', 'SI-41-INT', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 10, NULL, NULL, NULL),
(344, 1202172361, 'F.X.ANDREW PRIYONO', '', 'SI-41-INT', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 10, NULL, NULL, NULL),
(345, 1202173184, 'MUHAMMAD RIZALDI FADILLAH', '', 'SI-41-INT', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 3, 10, NULL, NULL, NULL),
(346, 1202174061, 'CHYNTIA THENY EFY NANDA M', '', 'SI-41-INT', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 1, 10, NULL, NULL, NULL),
(347, 1202174318, 'AUDIA DITSYA RAMADHAN', '', 'SI-41-INT', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 10, NULL, NULL, NULL),
(348, 1202174390, 'RIFQI NAUFALHANIF SATRIO WIBOWO', '', 'SI-41-INT', '$2b$10$BFNuRdsPO2Kffdpz3hgFBO6U5S3mKEQ4zS6.J9XbB9xODu0AFrlN6', NULL, NULL, '0.00', 6, 10, NULL, NULL, NULL),
(353, 1202172192, 'SINGGIH AJI SASONGKO', 'singgihajisasongko@student.telkomuniversity.ac.id', 'SI-41-05', NULL, NULL, NULL, '0.00', 6, 5, NULL, NULL, NULL),
(354, 1202174275, 'IZHA MAHENDRA', 'izhamahendra@student.telkomuniversity.ac.id', 'SI-41-05', NULL, NULL, NULL, '0.00', 6, 5, NULL, NULL, NULL),
(355, 1202174037, 'MUFTI ALIE SATRIAWAN', 'muftialies@student.telkomuniversity.ac.id', 'SI-41-07', NULL, NULL, NULL, '0.00', 5, 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `studentservice`
--

CREATE TABLE `studentservice` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sub_clo`
--

CREATE TABLE `sub_clo` (
  `id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `portion` double DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  `clo_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sub_clo_rubric`
--

CREATE TABLE `sub_clo_rubric` (
  `id` int(11) NOT NULL,
  `rubric_code` double DEFAULT NULL,
  `score` double DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  `sub_clo_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ta`
--

CREATE TABLE `ta` (
  `id` int(11) NOT NULL,
  `taUrl` varchar(255) DEFAULT NULL,
  `taFd` varchar(255) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ta`
--

INSERT INTO `ta` (`id`, `taUrl`, `taFd`, `student_id`) VALUES
(1, 'https://localhost:1337/get-ta/1', 'C:\\Users\\HP\\Downloads\\project bu tien\\Aplikasi TA\\.tmp\\uploads\\d8c7d0ba-0b82-45c8-87be-f6458d4f8f64.jpeg', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `topic`
--

CREATE TABLE `topic` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `quota` double DEFAULT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  `is_archived` tinyint(1) DEFAULT NULL,
  `peminatan_id` int(11) DEFAULT NULL,
  `period_id` int(11) DEFAULT NULL,
  `lecturer_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `topic`
--

INSERT INTO `topic` (`id`, `name`, `quota`, `deskripsi`, `is_deleted`, `is_archived`, `peminatan_id`, `period_id`, `lecturer_id`) VALUES
(1, 'Topik 1 Topik Topik Topik Topik Topik Topik Topik Topik', 12, '', 1, 0, 2, 1, NULL),
(2, 'Topik 2 Topik Topik Topik Topik Topik Topik Topik Topik', 12, '', 1, 0, 4, 1, NULL),
(3, 'Topik 3 Topik Topik Topik Topik Topik Topik Topik Topik', 13, '', 1, 0, 1, 1, NULL),
(4, 'Topik 4 Topik Topik Topik Topik Topik Topik Topik Topik', 14, '', 1, 0, 1, 1, NULL),
(5, 'Topik 5 Topik Topik Topik Topik Topik Topik Topik Topik', 15, '', 1, 0, 1, 1, NULL),
(6, 'Topik 6 Topik Topik Topik Topik Topik Topik Topik Topik', 16, '', 1, 0, 1, 1, NULL),
(7, 'Topik 7 Topik Topik Topik Topik Topik Topik Topik Topik', 17, '', 1, 0, 1, 1, NULL),
(8, 'Topik 8 Topik Topik Topik Topik Topik Topik Topik Topik', 18, '', 1, 0, 1, 1, NULL),
(9, 'Bla bd e KE', 4, '', 1, 0, 1, 1, NULL),
(10, 'Abc', 0, '', 1, 0, 1, 1, NULL),
(11, 'Abcd Efgh Ijklm Nopqrs', 8, '', 1, 0, 1, 1, NULL),
(12, 'Cek 123', 4, '', 1, 0, 7, 1, NULL),
(13, 'ABjdfaljfla', 10, '', 1, 0, 6, 1, 10),
(14, 'QD EQRQLRN', 5, '', 1, 0, 5, 1, NULL),
(15, 'smartUKM', 3, '', 1, 0, 1, 1, NULL),
(16, 'topik x', 15, '', 1, 0, 4, 1, NULL),
(17, 'topik x', 10, '', 1, 0, 7, 1, NULL),
(18, 'topik y', 20, '', 1, 0, 5, 1, NULL),
(19, 'Aplikasi tool untuk Master Data Management (MDM) dalam rangka Data Governance', 3, '', 0, 0, 5, 1, 53),
(20, 'Aplikasi tool untuk Data Quality Management (DQM) dalam rangka Data Governance', 4, '', 0, 0, 5, 1, 53),
(21, 'Proses bisnis generic untuk pengelolaan Master Data Management dan Data Quality Management', 2, '', 0, 0, 4, 1, 53),
(22, 'Platform Assessment Tool & Team Recommendation ', 3, '', 0, 0, 6, 1, 53),
(23, 'Container : Kubernetes Technology', 3, '', 0, 0, 3, 1, 14),
(24, 'Devops : Ansible Technology ', 3, '', 0, 0, 3, 1, 14),
(25, 'SOC : Elements, Systems & Architecture', 6, '', 0, 0, 3, 1, 14),
(26, 'Forensics : Tools, System & Frameworks', 3, '', 0, 0, 3, 1, 14),
(27, 'Verification, Validation and Certification of Business Architecture', 3, '', 0, 0, 2, 1, 21),
(28, 'EA for manufacturing industry', 15, '', 0, 0, 2, 1, 21),
(29, 'Enterprise Architecture: Pemanfaatan Teknologi Informasi untuk Transformasi bagi Usaha Mikro Kecil Menengah (UMKM)', 12, '', 1, 0, 2, 1, 24),
(30, 'Enterprise Architecture: Pemanfaatan Teknologi Informasi untuk Transformasi bagi Usaha Mikro Kecil Menengah (UMKM)', 8, '', 0, 0, 2, 1, 24),
(31, 'Enterprise Architecture Validation', 4, '', 0, 0, 2, 1, 24),
(32, '10760007', 13, '', 1, 0, 2, 1, 47),
(33, 'Arsitektur PB SPBE khusus Jabar', 11, '', 1, 0, 2, 1, 47),
(34, 'EA for smart city', 13, '', 0, 0, 2, 1, 56),
(35, 'Arsitektur PB SPBE khusus Jabar', 8, '', 0, 0, 2, 1, 47),
(36, 'Perancangan Enterprise Architecture dengan pendekatan TOGAF ADM dan ITIL', 3, '', 0, 0, 2, 1, 47),
(37, 'MuhardiSaputra', 25, '', 0, 0, 1, 1, 36),
(38, 'Data Center Management (Focus in Deep Analysis User Need)', 5, '', 1, 0, 3, 1, 54),
(39, 'PERANCANGAN APLIKASI POINT OF SALES BERBASIS WEBSITE TOGUIDE STUDI KASUS UMKM LAUNDRY MENGGUNAKAN METODE ITERATIVE DAN INCREMENTAL', 10, 'Dapat melakukan penelitian sesuai tahap penelitian, mulai membuat model penelitian, membuat instrumen penelitian, menyebarkan kuisioner, serta mengolah dan menganalisis data.', 1, 0, 7, 1, 53),
(40, 'Aplikasi untuk project management', 5, 'pembuatan aplikasi android', 1, 0, 6, 3, 57),
(41, 'Metadata Management berbasis Opensource Tool A', 2, 'TA ini membuat tool otomatis untuk mengelola metadata di perusahaan', 0, 0, 5, 3, 57),
(42, 'Pembuatan Aplikasi Final Project Management (TA Selection)', 2, 'Menyempurnakan fitur pada modul TA Selection dan TA Desk Evaluation', 0, 0, 6, 3, 56),
(43, 'Aplikasi Performance Management', 3, 'Menambahkan fitur-fitur baru pada modul Group Management, Task Management dan Performance Management', 0, 0, 6, 3, 57),
(44, 'Topik Test 1', 3, 'ini topik buat nge tes', 0, 0, 6, 3, 57),
(45, 'Web Vulnerability Analysis', 12, 'Analisis kelemahan pada website', 0, 0, 3, 3, 58),
(46, 'Malware Classification Analysis', 3, 'Analisis klasifikasi malware dengan berbagai metode', 1, 0, 3, 3, 58),
(47, 'Wireless Vulnerability Analysis', 3, 'Analisis kelemahan jaringan wireless dengan berbagai metode', 1, 0, 3, 3, 58),
(48, '-', 5, '-', 1, 0, 3, 3, 58),
(49, 'Malware Classification Analysis', 3, 'Analisis Klasifikasi Malware dengan berbagai metode', 0, 0, 3, 3, 58),
(50, 'test1', 5, 'test1', 0, 0, 6, 3, 58),
(51, 'Implementasi Data Mining Telkomsel', 2, 'pengimplementasian data mining perusahaan Telkomsel', 0, 0, 5, 3, 57),
(52, '	Pengembangan Chatbot untuk Belajar Bahasa Asing pada Startup Haylingo', 5, '	Pengembangan Chatbot untuk Belajar Bahasa Asing', 0, 0, 6, 3, 18),
(53, 'Evaluasi dan Re-design Website Beramaljariyah.org untuk Meningkatkan Aksesibilitas pada Pengguna Lanjut Usia', 3, 'Menyusun laporan audit aksesibilitas, membuat usulan rancangan website baru yang sesuai dengan WCAG 2.1 dan sesuai kebutuhan pengguna lanjut usia', 0, 0, 6, 3, 18),
(54, 'Evaluasi dan Re-design Website Pelayanan Publik X untuk Meningkatkan Aksesibilitas pada Pengguna Lanjut Usia', 2, '	membuat audit aksesibilitas website publik kemudian merancang ulang agar sesuai WCAG 2.1', 0, 0, 6, 3, 18),
(55, 'Perancangan Aplikasi Mobile Open Library', 3, 'Melakukan evaluasi user interface kemudian mengusulkan rancangan sistem baru', 0, 0, 6, 3, 18),
(56, 'edsads', 5, 'esdfsfsf', 0, 0, 5, 3, 57),
(57, 'Ta Hasna', 5, 'hasna bisa', 0, 0, 6, 3, 57),
(58, 'Test Topic', 5, 'sdsd', 0, 0, 7, 3, 5),
(59, 'Data Mining untuk bank', 2, 'menganaisis perkembangan bank', 0, 0, 5, 3, 57),
(60, 'Segmentasi  Pelanggan Telkomsel', 5, 'melakukan segmentasi pelanggan telkomsel', 0, 0, 5, 3, 57),
(61, 'Test', 5, 'aaaa', 0, 0, 3, 3, 15);

-- --------------------------------------------------------

--
-- Struktur dari tabel `topic_selection`
--

CREATE TABLE `topic_selection` (
  `id` int(11) NOT NULL,
  `optionNum` double DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `period_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `topic_selection`
--

INSERT INTO `topic_selection` (`id`, `optionNum`, `status`, `topic_id`, `period_id`, `group_id`) VALUES
(28, 1, 'WAITING', 3, 1, 5),
(29, 1, 'WAITING', 3, 1, 6),
(30, 2, 'REJECTED', 4, 1, 6),
(31, 1, 'WAITING', 3, 1, 7),
(32, 1, 'WAITING', 3, 1, 8),
(33, 1, 'WAITING', 3, 1, 9),
(34, 1, 'WAITING', 3, 1, 10),
(35, 1, 'WAITING', 3, 1, 11),
(36, 1, 'WAITING', 3, 1, 12),
(37, 1, 'WAITING', 3, 1, 13),
(38, 1, 'WAITING', 3, 1, 14),
(39, 1, 'WAITING', 3, 1, 15),
(40, 1, 'WAITING', 3, 1, 16),
(41, 1, 'WAITING', 3, 1, 17),
(42, 1, 'WAITING', 3, 1, 18),
(43, 1, 'WAITING', 3, 1, 19),
(44, 1, 'WAITING', 3, 1, 20),
(45, 1, 'WAITING', 3, 1, 21),
(46, 1, 'WAITING', 3, 1, 22),
(47, 1, 'WAITING', 3, 1, 23),
(48, 1, 'WAITING', 3, 1, 24),
(49, 1, 'WAITING', 3, 1, 25),
(50, 1, 'WAITING', 3, 1, 26),
(51, 1, 'WAITING', 3, 1, 27),
(52, 1, 'WAITING', 3, 1, 28),
(53, 1, 'WAITING', 3, 1, 29),
(54, 1, 'WAITING', 3, 1, 30),
(55, 1, 'WAITING', 3, 1, 31),
(56, 1, 'WAITING', 3, 1, 32),
(57, 1, 'WAITING', 3, 1, 33),
(58, 1, 'WAITING', 3, 1, 34),
(59, 1, 'WAITING', 3, 1, 35),
(60, 1, 'WAITING', 3, 1, 36),
(61, 1, 'WAITING', 3, 1, 37),
(62, 1, 'WAITING', 3, 1, 38),
(63, 1, 'WAITING', 3, 1, 39),
(64, 1, 'WAITING', 3, 1, 40),
(65, 1, 'WAITING', 3, 1, 41),
(66, 1, 'WAITING', 3, 1, 42),
(67, 1, 'WAITING', 3, 1, 43),
(68, 1, 'WAITING', 3, 1, 44),
(69, 1, 'WAITING', 3, 1, 45),
(70, 1, 'WAITING', 3, 1, 46),
(71, 1, 'WAITING', 3, 1, 47),
(72, 1, 'WAITING', 3, 1, 48),
(73, 1, 'WAITING', 3, 1, 49),
(74, 1, 'APPROVED', 7, 1, 50),
(75, 1, 'WAITING', 7, 1, 51),
(76, 1, 'WAITING', 7, 1, 52),
(77, 1, 'WAITING', 7, 1, 53),
(78, 1, 'WAITING', 7, 1, 54),
(79, 1, 'WAITING', 7, 1, 55),
(80, 1, 'WAITING', 7, 1, 56),
(81, 1, 'WAITING', 7, 1, 57),
(82, 1, 'REJECTED', 7, 1, 58),
(83, 1, 'WAITING', 7, 1, 59),
(84, 1, 'WAITING', 7, 1, 60),
(85, 1, 'WAITING', 3, 1, 61),
(86, 1, 'WAITING', 3, 1, 62),
(87, 1, 'WAITING', 3, 1, 64),
(88, 1, 'WAITING', 3, 1, 63),
(89, 1, 'WAITING', 3, 1, 65),
(90, 1, 'REJECTED', 5, 1, 66),
(91, 1, 'WAITING', 5, 1, 67),
(92, 1, 'WAITING', 5, 1, 68),
(93, 1, 'WAITING', 5, 1, 69),
(94, 1, 'REJECTED', 4, 1, 70),
(95, 1, 'REJECTED', 4, 1, 71),
(96, 1, 'REJECTED', 4, 1, 72),
(98, 1, 'REJECTED', 4, 1, 74),
(99, 2, 'REJECTED', 6, 1, 74),
(100, 1, 'REJECTED', 8, 1, 75),
(101, 2, 'REJECTED', 9, 1, 75),
(102, 1, 'WAITING', 8, 1, 76),
(103, 2, 'WAITING', 9, 1, 76),
(104, 1, 'REJECTED', 44, 3, 77),
(105, 2, 'REJECTED', 43, 3, 77),
(106, 1, 'REJECTED', 42, 3, 78),
(107, 2, 'WAITING', 50, 3, 78),
(108, 1, 'WAITING', 22, 3, 79),
(109, 2, 'REJECTED', 42, 3, 79),
(110, 1, 'WAITING', 45, 3, 80),
(111, 2, 'WAITING', 49, 3, 80),
(112, 1, 'WAITING', 50, 3, 86),
(113, 2, 'REJECTED', 44, 3, 86),
(114, 1, 'REJECTED', 44, 3, 89),
(115, 2, 'REJECTED', 42, 3, 89),
(116, 1, 'APPROVED_LAB_RISET', 42, 3, 90),
(117, 2, 'AUTO_CANCELLED', 43, 3, 90),
(118, 1, 'WAITING', 50, 3, 91),
(119, 2, 'REJECTED', 44, 3, 91),
(120, 1, 'WAITING', 50, 3, 92),
(121, 2, 'WAITING', 44, 3, 92),
(122, 1, 'REJECTED', 42, 3, 93),
(123, 2, 'WAITING', 43, 3, 93),
(124, 1, 'WAITING', 44, 3, 94),
(125, 2, 'WAITING', 50, 3, 94),
(126, 1, 'REJECTED', 42, 3, 95),
(127, 2, 'APPROVED_LAB_RISET', 44, 3, 95),
(128, 1, 'REJECTED', 41, 3, 96),
(129, 2, 'REJECTED', 51, 3, 96),
(132, 1, 'APPROVED_LAB_RISET', 42, 3, 98),
(133, 1, 'APPROVED_LAB_RISET', 52, 3, 99),
(134, 1, 'WAITING', 54, 3, 100),
(135, 1, 'REJECTED', 53, 3, 101),
(136, 1, 'WAITING', 55, 3, 102),
(137, 1, 'APPROVED_LAB_RISET', 56, 3, 103),
(138, 1, 'REJECTED', 57, 3, 104),
(139, 2, 'AUTO_CANCELLED', 55, 3, 104),
(140, 1, 'APPROVED', 58, 3, 105),
(141, 1, 'REJECTED', 51, 3, 106),
(142, 2, 'REJECTED', 60, 3, 106),
(143, 1, 'REJECTED', 60, 3, 107),
(144, 2, 'REJECTED', 59, 3, 107),
(145, 1, 'REJECTED', 59, 3, 108),
(146, 2, 'REJECTED', 60, 3, 108),
(147, 1, 'REJECTED', 60, 3, 109),
(148, 2, 'REJECTED', 59, 3, 109),
(149, 1, 'APPROVED', 60, 3, 110),
(150, 2, 'REJECTED', 59, 3, 110),
(151, 1, 'APPROVED_LAB_RISET', 61, 3, 111);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_reserach_lab_approve`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_reserach_lab_approve` (
`id` varchar(44)
,`topic_selection` int(11)
,`topic` int(11)
,`lecturer` int(11)
,`peminatan` int(11)
,`group` int(11)
,`student` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_student_report`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_student_report` (
`id` varchar(44)
,`student` int(11)
,`group` int(11)
,`optionNum` double
,`status` varchar(255)
,`period` int(11)
,`lecturer` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_student_report_reviewer`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_student_report_reviewer` (
`id` varchar(44)
,`student` int(11)
,`group` int(11)
,`optionNum` double
,`status` varchar(255)
,`period` int(11)
,`lecturer` int(11)
,`reviewer` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_student_status`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_student_status` (
`id` varchar(44)
,`student` int(11)
,`group` int(11)
,`optionNum` double
,`status` varchar(255)
,`period` int(11)
);

-- --------------------------------------------------------

--
-- Struktur dari tabel `z`
--

CREATE TABLE `z` (
  `id` int(11) DEFAULT NULL,
  `c1` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_reserach_lab_approve`
--
DROP TABLE IF EXISTS `v_reserach_lab_approve`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_reserach_lab_approve`  AS  select concat(`s`.`id`,`ts`.`optionNum`,`ts`.`period_id`) AS `id`,`ts`.`id` AS `topic_selection`,`t`.`id` AS `topic`,`l`.`id` AS `lecturer`,`p`.`id` AS `peminatan`,`g`.`id` AS `group`,`s`.`id` AS `student` from ((((((`topic_selection` `ts` join `topic` `t` on(`ts`.`topic_id` = `t`.`id`)) join `group` `g` on(`ts`.`group_id` = `g`.`id`)) join `group_student` `gs` on(`gs`.`group_id` = `g`.`id`)) join `student` `s` on(`gs`.`student_id` = `s`.`id`)) join `lecturer` `l` on(`l`.`id` = `t`.`lecturer_id`)) join `peminatan` `p` on(`p`.`id` = `t`.`peminatan_id`)) where `ts`.`status` = 'APPROVED' order by `g`.`id` ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_student_report`
--
DROP TABLE IF EXISTS `v_student_report`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_student_report`  AS  select concat(`s`.`id`,`ts`.`optionNum`,`ts`.`period_id`) AS `id`,`s`.`id` AS `student`,`g`.`id` AS `group`,`ts`.`optionNum` AS `optionNum`,`ts`.`status` AS `status`,`ts`.`period_id` AS `period`,`l`.`id` AS `lecturer` from (((((`student` `s` left join `group_student` `gs` on(`gs`.`student_id` = `s`.`id`)) left join `group` `g` on(`gs`.`group_id` = `g`.`id`)) left join `topic_selection` `ts` on(`ts`.`group_id` = `g`.`id`)) left join `topic` `t` on(`ts`.`topic_id` = `t`.`id`)) left join `lecturer` `l` on(`t`.`lecturer_id` = `l`.`id`)) where `gs`.`id` = (select max(`group_student`.`id`) from `group_student` where `group_student`.`student_id` = `s`.`id`) and `ts`.`status` = 'APPROVED' order by `s`.`id` ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_student_report_reviewer`
--
DROP TABLE IF EXISTS `v_student_report_reviewer`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_student_report_reviewer`  AS  select concat(`s`.`id`,`ts`.`optionNum`,`ts`.`period_id`) AS `id`,`s`.`id` AS `student`,`g`.`id` AS `group`,`ts`.`optionNum` AS `optionNum`,`ts`.`status` AS `status`,`ts`.`period_id` AS `period`,`l`.`id` AS `lecturer`,`gs`.`lecturer_id` AS `reviewer` from (((((`student` `s` left join `group_student` `gs` on(`gs`.`student_id` = `s`.`id`)) left join `group` `g` on(`gs`.`group_id` = `g`.`id`)) left join `topic_selection` `ts` on(`ts`.`group_id` = `g`.`id`)) left join `topic` `t` on(`ts`.`topic_id` = `t`.`id`)) left join `lecturer` `l` on(`t`.`lecturer_id` = `l`.`id`)) where `gs`.`id` = (select max(`group_student`.`id`) from `group_student` where `group_student`.`student_id` = `s`.`id`) and `ts`.`status` = 'APPROVED_LAB_RISET' order by `s`.`id` ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_student_status`
--
DROP TABLE IF EXISTS `v_student_status`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_student_status`  AS  select concat(`s`.`id`,`ts`.`optionNum`,`ts`.`period_id`) AS `id`,`s`.`id` AS `student`,`g`.`id` AS `group`,`ts`.`optionNum` AS `optionNum`,`ts`.`status` AS `status`,`ts`.`period_id` AS `period` from (((`student` `s` left join `group_student` `gs` on(`gs`.`student_id` = `s`.`id`)) left join `group` `g` on(`gs`.`group_id` = `g`.`id`)) left join `topic_selection` `ts` on(`ts`.`group_id` = `g`.`id`)) where `gs`.`id` = (select max(`group_student`.`id`) from `group_student` where `group_student`.`student_id` = `s`.`id`) order by `s`.`id` ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indeks untuk tabel `app_setting`
--
ALTER TABLE `app_setting`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indeks untuk tabel `archive`
--
ALTER TABLE `archive`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indeks untuk tabel `clo`
--
ALTER TABLE `clo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indeks untuk tabel `eprt`
--
ALTER TABLE `eprt`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `student_id` (`student_id`);

--
-- Indeks untuk tabel `group`
--
ALTER TABLE `group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indeks untuk tabel `group_student`
--
ALTER TABLE `group_student`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indeks untuk tabel `guidance_form`
--
ALTER TABLE `guidance_form`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `student_id` (`student_id`);

--
-- Indeks untuk tabel `jfa`
--
ALTER TABLE `jfa`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `abbrev` (`abbrev`);

--
-- Indeks untuk tabel `kk`
--
ALTER TABLE `kk`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `abbrev` (`abbrev`);

--
-- Indeks untuk tabel `lab_riset`
--
ALTER TABLE `lab_riset`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `abbrev` (`abbrev`);

--
-- Indeks untuk tabel `lecturer`
--
ALTER TABLE `lecturer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `nik` (`nik`);

--
-- Indeks untuk tabel `lecturerquota`
--
ALTER TABLE `lecturerquota`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indeks untuk tabel `lecturer_roles__masterrole_roles_masterrole`
--
ALTER TABLE `lecturer_roles__masterrole_roles_masterrole`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indeks untuk tabel `master_role`
--
ALTER TABLE `master_role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indeks untuk tabel `metlit`
--
ALTER TABLE `metlit`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indeks untuk tabel `mycontroller`
--
ALTER TABLE `mycontroller`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indeks untuk tabel `peminatan`
--
ALTER TABLE `peminatan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `abbrev` (`abbrev`);

--
-- Indeks untuk tabel `period`
--
ALTER TABLE `period`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indeks untuk tabel `plo`
--
ALTER TABLE `plo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indeks untuk tabel `prodi`
--
ALTER TABLE `prodi`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indeks untuk tabel `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indeks untuk tabel `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`session_id`);

--
-- Indeks untuk tabel `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `nim` (`nim`);

--
-- Indeks untuk tabel `studentservice`
--
ALTER TABLE `studentservice`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indeks untuk tabel `sub_clo`
--
ALTER TABLE `sub_clo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indeks untuk tabel `sub_clo_rubric`
--
ALTER TABLE `sub_clo_rubric`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indeks untuk tabel `ta`
--
ALTER TABLE `ta`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `student_id` (`student_id`);

--
-- Indeks untuk tabel `topic`
--
ALTER TABLE `topic`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indeks untuk tabel `topic_selection`
--
ALTER TABLE `topic_selection`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `app_setting`
--
ALTER TABLE `app_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `archive`
--
ALTER TABLE `archive`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `clo`
--
ALTER TABLE `clo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `eprt`
--
ALTER TABLE `eprt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `group`
--
ALTER TABLE `group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT untuk tabel `group_student`
--
ALTER TABLE `group_student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT untuk tabel `guidance_form`
--
ALTER TABLE `guidance_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `jfa`
--
ALTER TABLE `jfa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `kk`
--
ALTER TABLE `kk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `lab_riset`
--
ALTER TABLE `lab_riset`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `lecturer`
--
ALTER TABLE `lecturer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT untuk tabel `lecturerquota`
--
ALTER TABLE `lecturerquota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `lecturer_roles__masterrole_roles_masterrole`
--
ALTER TABLE `lecturer_roles__masterrole_roles_masterrole`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT untuk tabel `master_role`
--
ALTER TABLE `master_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `metlit`
--
ALTER TABLE `metlit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `mycontroller`
--
ALTER TABLE `mycontroller`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `peminatan`
--
ALTER TABLE `peminatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `period`
--
ALTER TABLE `period`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `plo`
--
ALTER TABLE `plo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `prodi`
--
ALTER TABLE `prodi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=356;

--
-- AUTO_INCREMENT untuk tabel `studentservice`
--
ALTER TABLE `studentservice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `sub_clo`
--
ALTER TABLE `sub_clo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `sub_clo_rubric`
--
ALTER TABLE `sub_clo_rubric`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `ta`
--
ALTER TABLE `ta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `topic`
--
ALTER TABLE `topic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT untuk tabel `topic_selection`
--
ALTER TABLE `topic_selection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
