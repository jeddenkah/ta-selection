-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 17, 2020 at 09:15 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `taaa`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `hashed_password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `hashed_password`) VALUES
(1, 'admin', '$2b$10$IZ12TMj.AtK9tGj869e6HOe4eTFV2sK/po2ZZzRG5oCqcyYXw2khe');

-- --------------------------------------------------------

--
-- Table structure for table `app_setting`
--

CREATE TABLE `app_setting` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `setting_value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `app_setting`
--

INSERT INTO `app_setting` (`id`, `name`, `setting_value`) VALUES
(1, 'period_id', '3'),
(2, 'last_group_id', '0');

-- --------------------------------------------------------

--
-- Table structure for table `archive`
--

CREATE TABLE `archive` (
  `id` int(11) NOT NULL,
  `createdAt` bigint(20) DEFAULT NULL,
  `fromModel` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `originalRecord` longtext COLLATE utf8_bin DEFAULT NULL,
  `originalRecordId` longtext COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `clo`
--

CREATE TABLE `clo` (
  `id` int(11) NOT NULL,
  `clo_code` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  `plo_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `eprt`
--

CREATE TABLE `eprt` (
  `id` int(11) NOT NULL,
  `score` varchar(255) DEFAULT NULL,
  `eprtUrl` varchar(255) DEFAULT NULL,
  `eprtFd` varchar(255) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `group`
--

CREATE TABLE `group` (
  `id` int(11) NOT NULL,
  `total_students` double DEFAULT NULL,
  `period_id` int(11) DEFAULT NULL,
  `peminatan_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `group`
--

INSERT INTO `group` (`id`, `total_students`, `period_id`, `peminatan_id`) VALUES
(1, 4, 1, 1),
(2, 4, 1, 1),
(3, 4, 1, 1),
(4, 4, 1, 1),
(5, 4, 1, 1),
(6, 4, 1, 1),
(7, 4, 1, 1),
(8, 4, 1, 1),
(9, 4, 1, 1),
(10, 4, 1, 1),
(11, 4, 1, 1),
(12, 4, 1, 1),
(13, 4, 1, 1),
(14, 4, 1, 1),
(15, 4, 1, 1),
(16, 4, 1, 1),
(17, 4, 1, 1),
(18, 4, 1, 1),
(19, 4, 1, 1),
(20, 4, 1, 1),
(21, 4, 1, 1),
(22, 4, 1, 1),
(23, 4, 1, 1),
(24, 4, 1, 1),
(25, 4, 1, 1),
(26, 4, 1, 1),
(27, 4, 1, 1),
(28, 4, 1, 1),
(29, 4, 1, 1),
(30, 4, 1, 1),
(31, 4, 1, 1),
(32, 4, 1, 1),
(33, 4, 1, 1),
(34, 4, 1, 1),
(35, 4, 1, 1),
(36, 4, 1, 1),
(37, 4, 1, 1),
(38, 4, 1, 1),
(39, 4, 1, 1),
(40, 4, 1, 1),
(41, 4, 1, 1),
(42, 4, 1, 1),
(43, 4, 1, 1),
(44, 4, 1, 1),
(45, 4, 1, 1),
(46, 4, 1, 1),
(47, 4, 1, 1),
(48, 4, 1, 1),
(49, 4, 1, 1),
(50, 4, 1, 1),
(51, 4, 1, 1),
(52, 4, 1, 1),
(53, 4, 1, 1),
(54, 4, 1, 1),
(55, 4, 1, 1),
(56, 4, 1, 1),
(57, 4, 1, 1),
(58, 4, 1, 1),
(59, 4, 1, 1),
(60, 4, 1, 1),
(61, 4, 1, 1),
(62, 4, 1, 1),
(63, 4, 1, 1),
(64, 4, 1, 1),
(65, 4, 1, 1),
(66, 4, 1, 1),
(67, 4, 1, 1),
(68, 4, 1, 1),
(69, 4, 1, 1),
(70, 4, 1, 1),
(71, 4, 1, 1),
(72, 4, 1, 1),
(73, 4, 1, 1),
(74, 4, 1, 1),
(75, 4, 1, 1),
(76, 4, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `group_student`
--

CREATE TABLE `group_student` (
  `id` int(11) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `guidance_form`
--

CREATE TABLE `guidance_form` (
  `id` int(11) NOT NULL,
  `formUrl` varchar(255) DEFAULT NULL,
  `formFd` varchar(255) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `jfa`
--

CREATE TABLE `jfa` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `abbrev` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jfa`
--

INSERT INTO `jfa` (`id`, `name`, `abbrev`) VALUES
(1, 'Non Jabatan Fungsional', 'NJFA'),
(2, 'Asisten Ahli', 'AA'),
(3, 'Lektor', 'L'),
(4, 'Lektor Kepala', 'LK'),
(5, 'Profesor', 'Prof');

-- --------------------------------------------------------

--
-- Table structure for table `kk`
--

CREATE TABLE `kk` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `abbrev` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kk`
--

INSERT INTO `kk` (`id`, `name`, `abbrev`) VALUES
(1, 'Enterprise and Industrial System', 'EINS'),
(2, 'Cybernetics', 'Cybernetics');

-- --------------------------------------------------------

--
-- Table structure for table `lab_riset`
--

CREATE TABLE `lab_riset` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `abbrev` varchar(255) DEFAULT NULL,
  `kk_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `lecturer`
--

CREATE TABLE `lecturer` (
  `id` int(11) NOT NULL,
  `nik` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `hashed_password` varchar(255) DEFAULT NULL,
  `new_password_token` varchar(255) DEFAULT NULL,
  `new_password_token_expires_at` longtext DEFAULT NULL,
  `lecturer_code` varchar(255) DEFAULT NULL,
  `jfa_id` int(11) DEFAULT NULL,
  `lab_riset_id` int(11) DEFAULT NULL,
  `prodi_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lecturer`
--

INSERT INTO `lecturer` (`id`, `nik`, `name`, `email`, `hashed_password`, `new_password_token`, `new_password_token_expires_at`, `lecturer_code`, `jfa_id`, `lab_riset_id`, `prodi_id`) VALUES
(1, '1', 'Dosen1', 'naufalwwidyatama@gmail.com', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'NWW', 1, NULL, NULL),
(5, '1111', 'Dosen', 'naufalwwidyatama@gmail.com', '$2b$10$Nvd4Yj52.1uPG8MYFICYruU1RNhxSpCt5YhleYArJ24fvh.Exk9bC', NULL, NULL, 'AA', 5, NULL, NULL),
(6, '1000', 'Namama', 'naufalwwidyatama@gmail.com', '$2b$10$2ED11JxNKZY5czeVEB9qU.fwHPuRBhYU8M1s0UpkqRtEsdVQRns1m', NULL, NULL, 'CD', 1, NULL, NULL),
(10, '5555', 'Tien Fabrianti', 'naufalwwidyatama@gmail.com', '$2b$12$kVTuUVUo/x6NHtm9bz6bzugTWSi5EyruFXcbY/T9KgUhv6o2HTy92', 'Y3NoldW3JZhM6JdozTiQdXF4HzNbZ1Ms', '2019-07-12 10:58:21.000', 'DA', 1, NULL, NULL),
(11, '123456', 'AblehS', 'abc@abc.dom', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, '234567890', 5, NULL, NULL),
(14, '13710054', 'ADITYAS WIDJAJARTO, S.T., M.T.', 'adwjrt@gmail.com', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'AWJ', 1, NULL, NULL),
(15, '17890112', 'AHMAD ALMAARIF, S.Kom, MT', 'ahmad.almaarif@gmail.com', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'LIF', 1, NULL, NULL),
(16, '16610019', 'IR. AHMAD MUSNANSYAH, MSc.', 'ahmadanc@gmail.com', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'MSN', 3, NULL, NULL),
(17, '13840043', 'ALBI FITRANSYAH, S.Si., M.T.', 'albi_fit@yahoo.co.id', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'ABF', 1, NULL, NULL),
(18, '18890133', 'ALVI SYAHRINA, MSc.', 'syahrina@telkomuniversity.ac.id', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'ALV', 1, NULL, NULL),
(19, '17910086', 'ANIK HANIFATUL AZIZAH, S.Kom, MIM', NULL, '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'HAZ', 1, NULL, NULL),
(20, '14660049', 'ARI FAJAR SANTOSO, IR, M.T.', 'arifajar@telkomuniversity.ac.id', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'ARJ', 2, NULL, NULL),
(21, '16880020', 'ASTI AMALIA NUR FAJRILLAH, B.MM., M.SC', 'astiamalianf@gmail.com', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'NFL', 2, NULL, NULL),
(22, '18750077', 'AVON BUDIONO, S.T., M.T.', NULL, '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'AVB', 3, NULL, NULL),
(23, '10760011', 'DR. BASUKI RAHMAD, CISA.,CISM.,CRISC', 'basuki@transforma-institute.biz', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'BSR', 1, NULL, NULL),
(24, '18930095', 'BERLIAN MAULIDYA IZZATI, S.Kom., M.Kom.', 'berlianmi@telkomuniversity.ac.id', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'BMZ', 1, NULL, NULL),
(25, '14720028', 'DEDEN WITARSYAH, ST., M.ENG Ph.D', 'dedenw@telkomuniversity.ac.id', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'DWH', 3, NULL, NULL),
(26, '17890109', 'EDI SUTOYO, S.Kom., M.Comp.Sc.', 'edisutoyo89@gmail.com', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'ETO', 2, NULL, NULL),
(27, '14900050', 'FAISHAL MUFIED AL ANSHARY, S.Kom., M.Kom., M.Sc', 'anshary90@gmail.com', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'FMA', 1, NULL, NULL),
(28, '10860013', 'FERDIAN, S.T., M.T.', 'mr.ferdian@gmail.com', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'FED', 1, NULL, NULL),
(29, '10740059', 'ILHAM PERDANA, S.T., M.T.', 'lhamdana@gmail.com', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'ILD', 3, NULL, NULL),
(30, '18880133', 'IQBAL SANTOSA', 'iqbals@telkomuniversity.ac.id', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'IQO', 1, NULL, NULL),
(31, '14750038', 'Dr. IRFAN DARMAWAN, ST., M.T.', 'dirfand@gmail.com', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'IFD', 4, NULL, NULL),
(32, '12630005', 'Dr. Ir. LUKMAN ABDURRAHMAN, MIS', 'abdural@telkomuniversity.ac.id', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'LMN', 3, NULL, NULL),
(33, '15890025', 'LUTHFI RAMADANI, S.T., M.T.', 'luthfiramadani@gmail.com', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'LRI', 2, NULL, NULL),
(34, '13860085', 'M. TEGUH KURNIAWAN, S.T,.M.T', 'teguhkurniawan@telkomuniversity.ac.id', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'MTK', 3, NULL, NULL),
(35, '14850055', 'MUHAMMAD AZANI HASIBUAN, S.Kom., M.T.I', 'muhammadazani@telkomuniversity.ac.id', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'MAZ', 2, NULL, NULL),
(36, '18890136', 'MUHARDI SAPUTRA, S.ST.,M.T.', NULL, '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'UHS', 1, NULL, NULL),
(37, '18860125', 'MUHARMAN LUBIS, B.IT., M.IT., PhD.IT', NULL, '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'MRL', 1, NULL, NULL),
(38, '5820066', 'MURAHARTAWATY, S.T., M.T.', 'murahartawaty@telkomuniversity.ac.id', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'MHY', 2, NULL, NULL),
(39, '14770014', 'NIA AMBARSARI, S.Si., M.T.', 'niaambarsari@telkomuniversity.ac.id', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'NAS', 3, NULL, NULL),
(40, '15830041', 'NUR ICHSAN UTAMA, S.T., M.T.', 'ichsan83@gmail.com', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'NIU', 1, NULL, NULL),
(41, '15900004', 'PUTRA FAJAR ALAM, S.SI, M.T.', 'putrafajaralam@gmail.com', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'PFA', 2, NULL, NULL),
(42, '14690010', 'R. WAHJOE WITJAKSONO , S.T, M.M', 'rwahyuwicaksono@gmail.com', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'RWW', 2, NULL, NULL),
(43, '14890057', 'RACHMADITA ANDRESWARI, S.KOM., M.KOM.', 'andreswari@gmail.com', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'RHA', 2, NULL, NULL),
(44, '17900092', 'RAHMAT FAUZI, S.T., M.T.', 'rahmatfauzi.its@gmail.com', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'RFZ', 2, NULL, NULL),
(45, '10780050', 'RAHMAT MULYANA, S.T., M.T., M.B.A., PMP, CISA, CISM', 'rahmatmoelyana@telkomuniversity.ac.id', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'RMM', 1, NULL, NULL),
(46, '760016', 'RD. ROHMAT SAEDUDIN S.T., M. T.', 'rdrohmat@telkomuniversity.ac.id', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'ROH', 3, NULL, NULL),
(47, '10760007', 'RIDHA HANAFI, S.T., M.T.', 'ridhanafi@yahoo.com', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'RDF', 2, NULL, NULL),
(48, '8760034', 'RIZA AGUSTIANSYAH, S.T., M.Kom', 'rizaagustiansyah@telkomuniversity.ac.id', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'RIZ', 2, NULL, NULL),
(49, '18820089', 'ROKHMAN FAUZI, S.T., M.T.', NULL, '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'RKZ', 1, NULL, NULL),
(50, '10780051', 'SENO ADI PUTRA, S.Si., M.T.', 'seno_ap@yahoo.com', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'SNP', 3, NULL, NULL),
(51, '14750043', 'SONI FAJAR SURYA GUMILANG, S.T., M.T.', 'sonifajar@gmail.com', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'SFJ', 3, NULL, NULL),
(52, '14830024', 'TAUFIK NUR ADI, S.Kom., M.T.', 'taufik.nur.adi@gmail.com', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'TNA', 2, NULL, NULL),
(53, '14790008', 'Dr. TIEN FABRIANTI KUSUMASARI', 'tien_kusumasari@yahoo.com', '$2b$10$TMby9Ol0UQJoLW7/YY4xaOrRWbB26JKt.wxsR4YFwLF0tso2NXJmS', 'Q64qBrtTWGa82lI9jK8cSK9GEZT5XSQR', '2020-09-13 19:19:23.343', 'TFD', 2, NULL, NULL),
(54, '14840067', 'UMAR YUNAN KURNIA SEPTO HEDIYANTO, S.T., M.T', 'umar.yunan.ksh@gmail.com', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'UMY', 1, NULL, NULL),
(55, '10820005', 'WARIH PUSPITASARI, S.PSI., M.PSI.', 'indigophie@yahoo.com', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'WRP', 2, NULL, NULL),
(56, '10790021', 'YULI ADAM PRASETYO, S.T., M.T.', 'adam@telkomuniversity.ac.id', '$2b$12$7r37C4fVjzqnnbeVnX3q6O9YZm3/Uy.PvPak/ww9xqe8tGwsJhAuu', NULL, NULL, 'YAP', 3, NULL, NULL),
(57, '14790008-1', 'TIEN FABRIANTI KUSUMASARI', 'tienkusumasari@telkomuniversity.ac.id', NULL, NULL, NULL, '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lecturer_roles__masterrole_roles_masterrole`
--

CREATE TABLE `lecturer_roles__masterrole_roles_masterrole` (
  `id` int(11) NOT NULL,
  `lecturer_roles` int(11) DEFAULT NULL,
  `masterrole_roles_masterrole` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `master_role`
--

CREATE TABLE `master_role` (
  `id` int(11) NOT NULL,
  `role` varchar(255) DEFAULT NULL,
  `abbrev` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `metlit`
--

CREATE TABLE `metlit` (
  `id` int(11) NOT NULL,
  `class` varchar(255) DEFAULT NULL,
  `period_id` int(11) DEFAULT NULL,
  `prodi_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `metlit`
--

INSERT INTO `metlit` (`id`, `class`, `period_id`, `prodi_id`) VALUES
(1, 'metlit', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mycontroller`
--

CREATE TABLE `mycontroller` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `peminatan`
--

CREATE TABLE `peminatan` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `abbrev` varchar(255) DEFAULT NULL,
  `prodi_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `peminatan`
--

INSERT INTO `peminatan` (`id`, `name`, `abbrev`, `prodi_id`) VALUES
(1, 'Enterprise Resource Planning', 'ERP', NULL),
(2, 'Enterprise Architecture', 'EA', NULL),
(3, 'Enterprise Infrastructure Management', 'EIM', NULL),
(4, 'Information System Management', 'ISM', NULL),
(5, 'Enterprise Data Management', 'EDM', NULL),
(6, 'Enterprise Application Development', 'EAD', NULL),
(7, 'System Architecture and Governance', 'SAG', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `period`
--

CREATE TABLE `period` (
  `id` int(11) NOT NULL,
  `semester` varchar(255) DEFAULT NULL,
  `academic_year` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `period`
--

INSERT INTO `period` (`id`, `semester`, `academic_year`) VALUES
(1, 'GANJIL', '2018-2019'),
(2, 'GENAP', '2018-2019'),
(3, 'GANJIL', '2020/2021');

-- --------------------------------------------------------

--
-- Table structure for table `plo`
--

CREATE TABLE `plo` (
  `id` int(11) NOT NULL,
  `plo_code` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `prodi`
--

CREATE TABLE `prodi` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `abbrev` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `prodi`
--

INSERT INTO `prodi` (`id`, `name`, `abbrev`) VALUES
(1, 'S1 Sistem Informasi', 'S1 SI');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `topic_title` varchar(255) DEFAULT NULL,
  `topic_title_status` varchar(255) DEFAULT NULL,
  `file_link` varchar(255) DEFAULT NULL,
  `final_score` double DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `period_id` int(11) DEFAULT NULL,
  `reviewer_id` int(11) DEFAULT NULL,
  `reviewerScoreInRubric` int(11) DEFAULT NULL,
  `supervisorScoreInRubric` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `session_id` varchar(128) NOT NULL,
  `expires` int(11) UNSIGNED NOT NULL,
  `data` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `nim` double DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `hashed_password` varchar(255) DEFAULT NULL,
  `new_password_token` varchar(255) DEFAULT NULL,
  `new_password_token_expires_at` longtext DEFAULT NULL,
  `ipk` decimal(10,0) DEFAULT NULL,
  `peminatan_id` int(11) DEFAULT NULL,
  `metlit_id` int(11) DEFAULT NULL,
  `ta_id` int(11) DEFAULT NULL,
  `eprt_id` int(11) DEFAULT NULL,
  `form_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `nim`, `name`, `email`, `class`, `hashed_password`, `new_password_token`, `new_password_token_expires_at`, `ipk`, `peminatan_id`, `metlit_id`, `ta_id`, `eprt_id`, `form_id`) VALUES
(1, 1202174146, 'RADEN HASNA FADHILA', 'rhasnafadhila@student.telkomuniversity.ac.id', '', NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL),
(2, 1202172192, 'SINGGIH AJI SASONGKO', 'singgihajisasongko@student.telkomuniversity.ac.id', '', NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL),
(3, 1202170069, 'DIMAS NASHIRUDDIN AL FARUQ', 'dimasna@student.telkomuniversity.ac.id', '', NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sub_clo`
--

CREATE TABLE `sub_clo` (
  `id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `portion` double DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  `clo_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sub_clo_rubric`
--

CREATE TABLE `sub_clo_rubric` (
  `id` int(11) NOT NULL,
  `rubric_code` double DEFAULT NULL,
  `score` double DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  `sub_clo_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ta`
--

CREATE TABLE `ta` (
  `id` int(11) NOT NULL,
  `taUrl` varchar(255) DEFAULT NULL,
  `taFd` varchar(255) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `topic`
--

CREATE TABLE `topic` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `quota` double DEFAULT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  `is_archived` tinyint(1) DEFAULT NULL,
  `peminatan_id` int(11) DEFAULT NULL,
  `period_id` int(11) DEFAULT NULL,
  `lecturer_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `topic`
--

INSERT INTO `topic` (`id`, `name`, `quota`, `deskripsi`, `is_deleted`, `is_archived`, `peminatan_id`, `period_id`, `lecturer_id`) VALUES
(1, 'Topik 1 Topik Topik Topik Topik Topik Topik Topik Topik', 12, '', 1, 0, 2, 1, 1),
(2, 'Topik 2 Topik Topik Topik Topik Topik Topik Topik Topik', 12, '', 1, 0, 4, 1, 1),
(3, 'Topik 3 Topik Topik Topik Topik Topik Topik Topik Topik', 13, '', 1, 0, 1, 1, 1),
(4, 'Topik 4 Topik Topik Topik Topik Topik Topik Topik Topik', 14, '', 1, 0, 1, 1, 1),
(5, 'Topik 5 Topik Topik Topik Topik Topik Topik Topik Topik', 15, '', 1, 0, 1, 1, 1),
(6, 'Topik 6 Topik Topik Topik Topik Topik Topik Topik Topik', 16, '', 1, 0, 1, 1, 1),
(7, 'Topik 7 Topik Topik Topik Topik Topik Topik Topik Topik', 17, '', 1, 0, 1, 1, 1),
(8, 'Topik 8 Topik Topik Topik Topik Topik Topik Topik Topik', 18, '', 1, 0, 1, 1, 1),
(9, 'Bla bd e KE', 4, '', 1, 0, 1, 1, 1),
(10, 'Abc', 0, '', 1, 0, 1, 1, 1),
(11, 'Abcd Efgh Ijklm Nopqrs', 8, '', 1, 0, 1, 1, 1),
(12, 'Cek 123', 4, '', 1, 0, 7, 1, 1),
(13, 'ABjdfaljfla', 10, '', 1, 0, 6, 1, 10),
(14, 'QD EQRQLRN', 5, '', 1, 0, 5, 1, 1),
(15, 'smartUKM', 3, '', 1, 0, 1, 1, 1),
(16, 'topik x', 15, '', 1, 0, 4, 1, 1),
(17, 'topik x', 10, '', 1, 0, 7, 1, 1),
(18, 'topik y', 20, '', 1, 0, 5, 1, 1),
(19, 'Aplikasi tool untuk Master Data Management (MDM) dalam rangka Data Governance', 3, '', 0, 0, 5, 1, 53),
(20, 'Aplikasi tool untuk Data Quality Management (DQM) dalam rangka Data Governance', 4, '', 0, 0, 5, 1, 53),
(21, 'Proses bisnis generic untuk pengelolaan Master Data Management dan Data Quality Management', 2, '', 0, 0, 4, 1, 53),
(22, 'Platform Assessment Tool & Team Recommendation ', 3, '', 0, 0, 6, 1, 53),
(23, 'Container : Kubernetes Technology', 3, '', 0, 0, 3, 1, 14),
(24, 'Devops : Ansible Technology ', 3, '', 0, 0, 3, 1, 14),
(25, 'SOC : Elements, Systems & Architecture', 6, '', 0, 0, 3, 1, 14),
(26, 'Forensics : Tools, System & Frameworks', 3, '', 0, 0, 3, 1, 14),
(27, 'Verification, Validation and Certification of Business Architecture', 3, '', 0, 0, 2, 1, 21),
(28, 'EA for manufacturing industry', 15, '', 0, 0, 2, 1, 21),
(29, 'Enterprise Architecture: Pemanfaatan Teknologi Informasi untuk Transformasi bagi Usaha Mikro Kecil Menengah (UMKM)', 12, '', 1, 0, 2, 1, 24),
(30, 'Enterprise Architecture: Pemanfaatan Teknologi Informasi untuk Transformasi bagi Usaha Mikro Kecil Menengah (UMKM)', 8, '', 0, 0, 2, 1, 24),
(31, 'Enterprise Architecture Validation', 4, '', 0, 0, 2, 1, 24),
(32, '10760007', 13, '', 1, 0, 2, 1, 47),
(33, 'Arsitektur PB SPBE khusus Jabar', 11, '', 1, 0, 2, 1, 47),
(34, 'EA for smart city', 13, '', 0, 0, 2, 1, 56),
(35, 'Arsitektur PB SPBE khusus Jabar', 8, '', 0, 0, 2, 1, 47),
(36, 'Perancangan Enterprise Architecture dengan pendekatan TOGAF ADM dan ITIL', 3, '', 0, 0, 2, 1, 47),
(37, 'MuhardiSaputra', 25, '', 0, 0, 1, 1, 36),
(38, 'Data Center Management (Focus in Deep Analysis User Need)', 5, '', 1, 0, 3, 1, 54),
(39, 'PERANCANGAN APLIKASI POINT OF SALES BERBASIS WEBSITE TOGUIDE STUDI KASUS UMKM LAUNDRY MENGGUNAKAN METODE ITERATIVE DAN INCREMENTAL', 10, 'Dapat melakukan penelitian sesuai tahap penelitian, mulai membuat model penelitian, membuat instrumen penelitian, menyebarkan kuisioner, serta mengolah dan menganalisis data.', 1, 0, 7, 1, 53);

-- --------------------------------------------------------

--
-- Table structure for table `topic_selection`
--

CREATE TABLE `topic_selection` (
  `id` int(11) NOT NULL,
  `optionNum` double DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `period_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `topic_selection`
--

INSERT INTO `topic_selection` (`id`, `optionNum`, `status`, `topic_id`, `period_id`, `group_id`) VALUES
(28, 1, 'WAITING', 3, 1, 5),
(29, 1, 'WAITING', 3, 1, 6),
(30, 2, 'REJECTED', 4, 1, 6),
(31, 1, 'WAITING', 3, 1, 7),
(32, 1, 'WAITING', 3, 1, 8),
(33, 1, 'WAITING', 3, 1, 9),
(34, 1, 'WAITING', 3, 1, 10),
(35, 1, 'WAITING', 3, 1, 11),
(36, 1, 'WAITING', 3, 1, 12),
(37, 1, 'WAITING', 3, 1, 13),
(38, 1, 'WAITING', 3, 1, 14),
(39, 1, 'WAITING', 3, 1, 15),
(40, 1, 'WAITING', 3, 1, 16),
(41, 1, 'WAITING', 3, 1, 17),
(42, 1, 'WAITING', 3, 1, 18),
(43, 1, 'WAITING', 3, 1, 19),
(44, 1, 'WAITING', 3, 1, 20),
(45, 1, 'WAITING', 3, 1, 21),
(46, 1, 'WAITING', 3, 1, 22),
(47, 1, 'WAITING', 3, 1, 23),
(48, 1, 'WAITING', 3, 1, 24),
(49, 1, 'WAITING', 3, 1, 25),
(50, 1, 'WAITING', 3, 1, 26),
(51, 1, 'WAITING', 3, 1, 27),
(52, 1, 'WAITING', 3, 1, 28),
(53, 1, 'WAITING', 3, 1, 29),
(54, 1, 'WAITING', 3, 1, 30),
(55, 1, 'WAITING', 3, 1, 31),
(56, 1, 'WAITING', 3, 1, 32),
(57, 1, 'WAITING', 3, 1, 33),
(58, 1, 'WAITING', 3, 1, 34),
(59, 1, 'WAITING', 3, 1, 35),
(60, 1, 'WAITING', 3, 1, 36),
(61, 1, 'WAITING', 3, 1, 37),
(62, 1, 'WAITING', 3, 1, 38),
(63, 1, 'WAITING', 3, 1, 39),
(64, 1, 'WAITING', 3, 1, 40),
(65, 1, 'WAITING', 3, 1, 41),
(66, 1, 'WAITING', 3, 1, 42),
(67, 1, 'WAITING', 3, 1, 43),
(68, 1, 'WAITING', 3, 1, 44),
(69, 1, 'WAITING', 3, 1, 45),
(70, 1, 'WAITING', 3, 1, 46),
(71, 1, 'WAITING', 3, 1, 47),
(72, 1, 'WAITING', 3, 1, 48),
(73, 1, 'WAITING', 3, 1, 49),
(74, 1, 'APPROVED', 7, 1, 50),
(75, 1, 'WAITING', 7, 1, 51),
(76, 1, 'WAITING', 7, 1, 52),
(77, 1, 'WAITING', 7, 1, 53),
(78, 1, 'WAITING', 7, 1, 54),
(79, 1, 'WAITING', 7, 1, 55),
(80, 1, 'WAITING', 7, 1, 56),
(81, 1, 'WAITING', 7, 1, 57),
(82, 1, 'REJECTED', 7, 1, 58),
(83, 1, 'WAITING', 7, 1, 59),
(84, 1, 'WAITING', 7, 1, 60),
(85, 1, 'WAITING', 3, 1, 61),
(86, 1, 'WAITING', 3, 1, 62),
(87, 1, 'WAITING', 3, 1, 64),
(88, 1, 'WAITING', 3, 1, 63),
(89, 1, 'WAITING', 3, 1, 65),
(90, 1, 'REJECTED', 5, 1, 66),
(91, 1, 'WAITING', 5, 1, 67),
(92, 1, 'WAITING', 5, 1, 68),
(93, 1, 'WAITING', 5, 1, 69),
(94, 1, 'REJECTED', 4, 1, 70),
(95, 1, 'REJECTED', 4, 1, 71),
(96, 1, 'REJECTED', 4, 1, 72),
(98, 1, 'REJECTED', 4, 1, 74),
(99, 2, 'REJECTED', 6, 1, 74),
(100, 1, 'REJECTED', 8, 1, 75),
(101, 2, 'REJECTED', 9, 1, 75),
(102, 1, 'WAITING', 8, 1, 76),
(103, 2, 'WAITING', 9, 1, 76);

-- --------------------------------------------------------

--
-- Table structure for table `z`
--

CREATE TABLE `z` (
  `id` int(11) DEFAULT NULL,
  `c1` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `app_setting`
--
ALTER TABLE `app_setting`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `archive`
--
ALTER TABLE `archive`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `clo`
--
ALTER TABLE `clo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `eprt`
--
ALTER TABLE `eprt`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `student_id` (`student_id`);

--
-- Indexes for table `group`
--
ALTER TABLE `group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `group_student`
--
ALTER TABLE `group_student`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `guidance_form`
--
ALTER TABLE `guidance_form`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `student_id` (`student_id`);

--
-- Indexes for table `jfa`
--
ALTER TABLE `jfa`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `abbrev` (`abbrev`);

--
-- Indexes for table `kk`
--
ALTER TABLE `kk`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `abbrev` (`abbrev`);

--
-- Indexes for table `lab_riset`
--
ALTER TABLE `lab_riset`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `abbrev` (`abbrev`);

--
-- Indexes for table `lecturer`
--
ALTER TABLE `lecturer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `nik` (`nik`);

--
-- Indexes for table `lecturer_roles__masterrole_roles_masterrole`
--
ALTER TABLE `lecturer_roles__masterrole_roles_masterrole`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `master_role`
--
ALTER TABLE `master_role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `metlit`
--
ALTER TABLE `metlit`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `mycontroller`
--
ALTER TABLE `mycontroller`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `peminatan`
--
ALTER TABLE `peminatan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `abbrev` (`abbrev`);

--
-- Indexes for table `period`
--
ALTER TABLE `period`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `plo`
--
ALTER TABLE `plo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `prodi`
--
ALTER TABLE `prodi`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `nim` (`nim`);

--
-- Indexes for table `sub_clo`
--
ALTER TABLE `sub_clo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `sub_clo_rubric`
--
ALTER TABLE `sub_clo_rubric`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `ta`
--
ALTER TABLE `ta`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `student_id` (`student_id`);

--
-- Indexes for table `topic`
--
ALTER TABLE `topic`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `topic_selection`
--
ALTER TABLE `topic_selection`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `app_setting`
--
ALTER TABLE `app_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `archive`
--
ALTER TABLE `archive`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `clo`
--
ALTER TABLE `clo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `eprt`
--
ALTER TABLE `eprt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `group`
--
ALTER TABLE `group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `group_student`
--
ALTER TABLE `group_student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `guidance_form`
--
ALTER TABLE `guidance_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jfa`
--
ALTER TABLE `jfa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kk`
--
ALTER TABLE `kk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lab_riset`
--
ALTER TABLE `lab_riset`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lecturer`
--
ALTER TABLE `lecturer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `lecturer_roles__masterrole_roles_masterrole`
--
ALTER TABLE `lecturer_roles__masterrole_roles_masterrole`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `master_role`
--
ALTER TABLE `master_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `metlit`
--
ALTER TABLE `metlit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mycontroller`
--
ALTER TABLE `mycontroller`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `peminatan`
--
ALTER TABLE `peminatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `period`
--
ALTER TABLE `period`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `plo`
--
ALTER TABLE `plo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `prodi`
--
ALTER TABLE `prodi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sub_clo`
--
ALTER TABLE `sub_clo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sub_clo_rubric`
--
ALTER TABLE `sub_clo_rubric`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ta`
--
ALTER TABLE `ta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `topic`
--
ALTER TABLE `topic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `topic_selection`
--
ALTER TABLE `topic_selection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
