/**
 * Seed Function
 * (sails.config.bootstrap)
 *
 * A function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also create a hook.
 *
 * For more information on seeding your app with fake data, check out:
 * https://sailsjs.com/config/bootstrap
 */

const bcrypt = require('bcrypt');

module.exports.bootstrap = async function () {

  // By convention, this is a good place to set up fake data during development.
  //
  // For example:
  // ```
  // // Set up fake development data (or if we already have some, avast)
  // if (await User.count() > 0) {
  //   return;
  // }
  //
  // await User.createEach([
  //   { emailAddress: 'ry@example.com', fullName: 'Ryan Dahl', },
  //   { emailAddress: 'rachael@example.com', fullName: 'Rachael Shaw', },
  //   // etc.
  // ]);
  // ```
  const defaultPassword = await bcrypt.hash("123", 10);
  const adminPassword = await bcrypt.hash("fri#2020", 10);
  if (await Admin.count() == 0) {
    await Admin.create({ name: 'Test', hashedPassword: adminPassword });
  }
  await Lecturer.findOrCreate({ nik: "123" }, { name: "Test", nik: "123", hashedPassword: defaultPassword });
  await Student.findOrCreate({ nim: "123" }, { name: "Test", nim: "123", hashedPassword: defaultPassword });
};
