/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

  /***************************************************************************
  *                                                                          *
  * Default policy for all controllers and actions, unless overridden.       *
  * (`true` allows public access)                                            *
  *                                                                          *
  ***************************************************************************/

  '*': true,
  // 'account/*': 'isLoggedIn',
  'data/get-file-ta': 'isLoggedIn',
  'data/get-file-eprt': 'isLoggedIn',
  'data/get-file-formbimbingan': 'isLoggedIn',

  'student/*': 'isStudent',
  'student/find': true,
  'student/findOne': true,
  'student/create': 'isAdmin',
  'student/update': 'isAdmin',
  'student/destroy': 'isAdmin',
  'student/populate': true,

  'lecturer/*': 'isLecturer',
  'lecturer/find': true,
  'lecturer/findOne': true,
  'lecturer/create': 'isAdmin',
  'lecturer/update': 'isAdmin',
  'lecturer/destroy': 'isAdmin',
  'lecturer/populate': true,
  
  'admin/*': 'isAdmin',
  'appsetting/*': 'isAdmin',
  'clo/*': 'CloPolicy',
  'clo/find' : true,
  'group/*': 'isAdmin',
  'groupstudent/*': 'isAdmin',
  'jfa/*': 'isAdmin',
  'kk/*': 'isAdmin',
  'metlit/*': 'isAdmin',
  'peminatan/*': 'isAdmin',
  'labriset/*': 'isAdmin',
  'period/*': 'isAdmin',
  'period/find' : true,
  'plo/*': 'PloPolicy',
  'plo/find' : true,
  'prodi/*': 'isAdmin',
  'prodi/find' : true,
  'project/*': 'isAdmin',
  'subclo/*': 'isAdmin',
  'subclorubric/*': 'isAdmin',
  'topic/*': 'isAdmin',
  'topicselection/*': 'isAdmin',

};
