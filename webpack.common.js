const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const path = require('path');
const glob = require('glob');
const _ = require('lodash')

const REACT_SRC_DIR = "assets/src/pages/";
const REACT_DIST_DIR = 'dist/pages/';

const reactPages = glob.sync(REACT_SRC_DIR + "**/*.jsx");
const entryObject = _.reduce([{}, ...reactPages], (object, filename) => {
    const entryName = filename.replace(path.extname(filename), "").replace(REACT_SRC_DIR, REACT_DIST_DIR);
    object[entryName] = `./${filename}`;
    return object;

})

module.exports = {
    entry:
    {
        ...entryObject,
        main: './assets/main.js',
        'js/init-popover': './assets/js/init-popover.js',
        //'vendor': ['react', 'react-dom', 'prop-types']
    },
    output:
    {
        path: path.join(__dirname, 'www'),
        filename: '[name].js'
    },
    plugins:
        [
            new CleanWebpackPlugin(),
            new CopyPlugin({
                patterns: [
                    { from: 'assets/dependencies', to: 'dependencies' },
                    { from: 'assets/images', to: 'images' },
                ],
            }),
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                //'window.jQuery': 'jquery',
                //'window.$': 'jquery'
            })
        ],

    module: {
        rules: [
            {
                loader: 'babel-loader',
                test: /\.(js|jsx)$/,
                exclude: /node_modules/
            },
            {
                test: [/datatables\.net.*/, /jquery.dataTables.js/],
                loader: 'imports-loader',
                options: {
                    /*
                    imports: {
                      moduleName: 'jquery',
                      name: '$',
                    },*/
                    additionalCode: `window.$ = window.jQuery = require( 'jquery' );`,
                },
            },
            //{ test: /datatables\.net.*/, loader:"imports-loader?define=>false,exports=>false" },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader',
                ],
            },
            {
                test: /\.scss$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader'
                ],
            },
            {
                //images
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    'file-loader',
                ],
            },
            {
                test: /\.(svg|eot|woff|ttf|svg|woff2)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: "[path][name].[ext]"
                        }
                    }
                ]
            }
        ],
    },
    optimization: {
        splitChunks:
        {
            name: false,
            cacheGroups:
            {
                vendor:
                {
                    test: /node_modules/,
                    chunks: "initial",
                    name: "vendor",
                    enforce: true
                }
            }
        }
    }
}
