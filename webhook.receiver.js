const express = require("express");
const { promisify } = require("util");
const childProcess = require("child_process");
const exec = promisify(childProcess.exec);
const fs = require("fs");
const path = require("path");
const { default: Axios } = require("axios");
const {CronJob} = require("cron");
const { BUILDER_CONFIG, RECEIVER_CONFIG, APP_CONFIG, SECRET, BUILD_FILENAME } = require("./webhook.config");
const builder = Axios.create({
	baseURL: BUILDER_CONFIG.baseUrl,
});

const buildJob = new CronJob("0 */10 * * * *", async () => {
	await downloadBuild("production")
	await execReload("production");
	await downloadBuild("development")
	await execReload("development");
});
buildJob.start();

// const app = express();
// app.use(express.json());
// app.use(express.urlencoded({ extended: true }));

// app.post("/hook", handleHook);
// app.post("/build/finished", handleFinishedBuild);

// app.listen(RECEIVER_CONFIG.port, RECEIVER_CONFIG.hostname);
// console.log(`Webhook server running at ${RECEIVER_CONFIG.baseUrl}`);

async function handleHook(req, res) {
	res.status(200).end();
	if (req.headers['x-gitlab-event'] == "Push Hook" && req.headers['x-gitlab-token'] == SECRET) {
		console.debug(req.body);
		let env;
		if (req.body.ref == "refs/heads/qa")
			env = "development";
		else if (req.body.ref == "refs/heads/master")
			env = "production";
		builder.post("/build", {
			headers: { "x-env": env },
		}).catch(handleReqError);
	}
}

async function downloadBuild(env) { 
	let filename;
	if (env == "production") filename = BUILD_FILENAME.production;
	else filename = BUILD_FILENAME.development;
	console.log("Downloading build", env);
	const buildPath = path.resolve(__dirname, ".tmp", filename + ".tar.gz");
	if (fs.existsSync(buildPath)) fs.unlinkSync(buildPath);
	const writer = fs.createWriteStream(buildPath);
	const response = await builder.get("/build", { responseType: "stream", headers: { "x-env": env } })
		.catch(handleReqError);
	response.data.pipe(writer);
	return new Promise((resolve, reject) => {
		writer.on("finish", resolve);
		writer.on("error", reject);
	});
}

async function handleFinishedBuild(req, res) {
	res.status(200).end();
	const env = req.headers["x-env"];
	await downloadBuild(env);
	await execReload(env);
}

function handleReqError(error) {
	if (error.response) {
		console.log(error.response.data);
		console.log(error.response.status);
		console.log(error.response.headers);
	} else if (error.request) {
		console.log(error.request);
	} else {
		console.log('Error', error.message);
	}
}

async function execReload(env) {
	let appPath, processName, buildFilename
	if (env == "production") {
		appPath = APP_CONFIG.production.path;
		processName = APP_CONFIG.production.processName;
		buildFilename = BUILD_FILENAME.production
	} else {
		appPath = APP_CONFIG.development.path;
		processName = APP_CONFIG.development.processName;
		buildFilename = BUILD_FILENAME.development
	}
	buildFilename;
	const outputPath = path.resolve(appPath, "www");
	const reloadScript = [
		`tar -zxvf .tmp/${buildFilename}.tar.gz`,
		`rm -rf ${outputPath}`,
		`mv .tmp/${buildFilename} ${outputPath}`,
		"cd " + appPath,
		"git pull",
		"npm i",
		"pm2 restart " + processName
	];
	try {
		for (cmd of reloadScript) {
			console.log("exec: " + cmd);
			const process = await exec(cmd);
			process.stdout && console.log("stdout: " + process.stdout);
			process.stderr && console.log("stderr: " + process.stderr);
		}
	} catch (e) {
		return console.error(e);
	}
	console.log("App has been reloaded!");
}