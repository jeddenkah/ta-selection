import React, {useState, useEffect} from 'react'
import PropTypes from 'prop-types'
import { Formik, Field, ErrorMessage } from 'formik'
import { FormInput, FormGroup, Button, ButtonGroup, Form } from "shards-react"
import LecturerSchema from '../../schemas/Lecturer'
import ModelSelect from '../../components/ModelSelect'
import unpopulateRecord from '../../utils/unpopulateRecord'
import _ from 'lodash'
import axios from 'axios'
import Select from 'react-select'
import * as yup from 'yup';

const InputField = ({field, ...props}) => (<FormInput {...field} {...props} />);
const HelpBlock = ({children}) => (<p className="help-block">{children}</p>);

const emptyPlo = {
    ploCode: '',
    description: '',
    prodi: '',
    period: ''
}


const ploSchema = yup.object().shape({
    ploCode: yup.string().required(),
    description: yup.string().required(),
    prodi: yup.number(),
    period: yup.number(),
});

export default function PloForm({initialRecord, mode}) {
    PloForm.propTypes = {
        initialRecord: PropTypes.object,
        mode: PropTypes.oneOf(['NEW', 'EDIT', '']),
        onCancel: PropTypes.func,
        hidden: PropTypes.bool
    }
    const handleSubmit = (data) => {
        if (mode == 'NEW') {
            axios.post(`/admin/new-plo`, data)
                .then(res => {
                    location.reload();
                })
        } else if (mode == 'EDIT') {
            axios.post(`/admin/edit-plo`, data)
                .then(res => {
                    location.reload();
                })
        }
    }
    return (
        <Formik
            initialValues={unpopulateRecord(initialRecord) || emptyPlo}
            validationSchema={ploSchema}
            onSubmit={(values, { setSubmitting, resetForm }) => {
                handleSubmit(values);
                resetForm();
                setSubmitting(false);
            }}
        >
            {({ submitForm, handleChange, handleBlur, values }) => (
                <Form>
                    <FormGroup>
                        <label htmlFor="ploCode">Kode PLO</label>
                        <Field type="text" name="ploCode" component={InputField}/>
                        <ErrorMessage name="ploCode" component={HelpBlock} />
                    </FormGroup>
                    <FormGroup>
                        <label htmlFor="description">Deskripsi</label>
                        <Field type="text" name="description" component={InputField}/>
                        <ErrorMessage name="name" component={HelpBlock} />
                    </FormGroup>
                    <FormGroup>
                        <label htmlFor="prodi">Prodi</label>
                        <ModelSelect
                            name="prodi"
                            value={values.prodi}
                            modelName="prodi"
                            displayAttribute="name"
                            onChange={handleChange}
                        />
                        <ErrorMessage name="prodi" component={HelpBlock} />
                    </FormGroup>
                    <FormGroup>
                        <label htmlFor="period">Periode</label>
                        <ModelSelect
                            name="period"
                            value={values.period}
                            modelName="period"
                            displayAttribute="academicYear"
                            onChange={handleChange}
                            onBlur={handleBlur}
                        />
                        <ErrorMessage name="period" component={HelpBlock} />
                    </FormGroup>
                    <Button onClick={submitForm}>Submit</Button>
                </Form>
            )}
        </Formik>
)
}