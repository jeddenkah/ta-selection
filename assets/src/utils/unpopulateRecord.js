export default function unpopulateRecord(initialRecord) {
    const recordPairs = _.entries(initialRecord).map(([key, value]) => {
        if (typeof value == 'object') {
            if (Array.isArray(value)) return [key, value.map(v => v.id)]
            return [key, value?.id]
        }
        return [key, value]
    });
    return _.fromPairs(recordPairs);
    
}