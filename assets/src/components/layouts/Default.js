import React from "react";
import PropTypes from "prop-types";
import { Container, Row, Col } from "shards-react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import MainNavbar from "../layout/MainNavbar/MainNavbar";
import MainSidebar from "../layout/MainSidebar/MainSidebar";
import MainFooter from "../layout/MainFooter";
import {ToastContainer} from 'react-toastify';
import Cookies from "js-cookie";

const DefaultLayout = ({ children, noNavbar, noFooter}) => (
  <Router basename={process.env.REACT_APP_BASENAME || ""}>
  <Container fluid>
    <Row>
      <MainSidebar role={Cookies.get("role")} />
      <Col
        className="main-content p-0"
        lg={{ size: 10, offset: 2 }}
        md={{ size: 9, offset: 3 }}
        sm="12"
        tag="main"
      >
        {!noNavbar && <MainNavbar />}
        {children}
        {!noFooter && <MainFooter />}
      </Col>
    </Row>
    <ToastContainer
      position="top-center"
      autoClose={5000}
      hideProgressBar
      newestOnTop={false}
      closeOnClick
      rtl={false}
      pauseOnFocusLoss
      draggable
      pauseOnHover
    />
  </Container>
  </Router>
);

DefaultLayout.propTypes = {
  /**
   * Whether to display the navbar, or not.
   */
  noNavbar: PropTypes.bool,
  /**
   * Whether to display the footer, or not.
   */
  noFooter: PropTypes.bool
};

DefaultLayout.defaultProps = {
  noNavbar: false,
  noFooter: false
};

export default DefaultLayout;
