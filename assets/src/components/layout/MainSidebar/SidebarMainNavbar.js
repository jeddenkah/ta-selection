import React from "react";
import PropTypes from "prop-types";
import { Navbar, NavbarBrand } from "shards-react";

import { Dispatcher, Constants } from "../../flux";
import Cookies from 'js-cookie';


class SidebarMainNavbar extends React.Component {
  constructor(props) {
    super(props);
    this.handleToggleSidebar = this.handleToggleSidebar.bind(this);
  }

  handleToggleSidebar() {
    Dispatcher.dispatch({
      actionType: Constants.TOGGLE_SIDEBAR
    });
  }
  

  render() {
    const { hideLogoText, role } = this.props;
    const userName = Cookies.get('userName');
    const userType = Cookies.get('userType');
    return (
      <div className="main-navbar">
        <Navbar
          className="align-items-stretch flex-md-nowrap p-0"
          type="dark"
        >
          <NavbarBrand
            className="w-100 mr-0 justify-content-center"
            href="#"
            style={{ lineHeight: "250px" }}
          >
            <div className="d-table m-auto justify-content-center">

            <img
            className="user-navbar"
            src="/images/user.svg"
            alt="User Avatar"
          />

          <h4 className="nav-username">
          {userName}
          </h4>
          <h3 className="nav-usertype">
            {role}
          </h3>
            </div>
          </NavbarBrand>
          {/* eslint-disable-next-line */}
          <a
            className="toggle-sidebar d-sm-inline d-md-none d-lg-none"
            onClick={this.handleToggleSidebar}
          > 
            <i className="material-icons">&#xE5C4;</i>
          </a>
        </Navbar>
      </div>
    );
  }
}

SidebarMainNavbar.propTypes = {
  /**
   * Whether to hide the logo text, or not.
   */
  hideLogoText: PropTypes.bool
};

SidebarMainNavbar.defaultProps = {
  hideLogoText: false
};

export default SidebarMainNavbar;
