import React from 'react';
import PropTypes from 'prop-types';
import { Button, ButtonGroup } from "shards-react";
import MUIDatatable from "mui-datatables";

function Row({tuple, onEdit, onDelete, id}) {
    Row.propTypes = {
        tuple: PropTypes.array,
        onEdit: PropTypes.func,
        onDelete: PropTypes.func,
        id: PropTypes.number.isRequired
    }
    const handleEdit = () => onEdit(id);
    const handleDelete = () => onDelete(id);
    return (
        <tr>
            {tuple.map((data, i) => 
                (<td key={i}>{data? (data.abbrev || data.name || data.description || data.id || data) : '' }</td>)
            )}
            <td>
            <ButtonGroup>
                <Button theme="success" className="btn" onClick={handleEdit}>Ganti</Button>
                <Button theme="danger" className="btn" onClick={handleDelete}>Hapus</Button>
            </ButtonGroup>
            </td>
        </tr>
    )
}
/*
export default function EditableTable({tuples, rowIds, columns, onRowEdit, onRowDelete, id}) {
    EditableTable.propTypes = {
        tuples: PropTypes.arrayOf(PropTypes.array),
        rowIds: PropTypes.arrayOf(PropTypes.number.isRequired),
        columns: PropTypes.arrayOf(PropTypes.string),
        onRowEdit: PropTypes.func,
        onRowDelete: PropTypes.func,
        id: PropTypes.string
    }
    const header = columns.map((col, i) => <th key={i}>{col}</th>);
    const rows = tuples.map((tuple, i) => {
        return (
            <Row
                key={rowIds[i]}
                tuple={tuple}
                id={rowIds[i]}
                onEdit={onRowEdit}
                onDelete={onRowDelete}
            />
        )
    })
    return (
        <div>
            <table id={id} className="table table-responsive">
                <thead>
                    <tr>
                        {header}
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {rows}
                </tbody>
            </table>
        </div>
    )

}*/

export default function EditableTable({tuples, rowIds, columns, onRowEdit, onRowDelete, id, options}) {
    EditableTable.propTypes = {
        tuples: PropTypes.arrayOf(PropTypes.array),
        rowIds: PropTypes.arrayOf(PropTypes.number.isRequired),
        columns: PropTypes.arrayOf(PropTypes.string),
        onRowEdit: PropTypes.func,
        onRowDelete: PropTypes.func,
        id: PropTypes.string,
        options: PropTypes.object //MUIDatatable Config
    }
    const header = columns.map((col, i) => <th key={i}>{col}</th>);
    const rows = tuples.map((tuple, i) => {
        return (
            <Row
                key={rowIds[i]}
                tuple={tuple}
                id={rowIds[i]}
                onEdit={onRowEdit}
                onDelete={onRowDelete}
            />
        )
    })
    return (
        <MUIDatatable
            data={tuples}
            columns={columns}
            options={{
                ...options,
                selectableRows: "none",
                customRowRender: (data, dataIndex, rowIndex) => (
                    <Row
                        key={dataIndex}
                        tuple={data}
                        id={rowIds[dataIndex]}
                        onEdit={onRowEdit}
                        onDelete={onRowDelete}
                        dataIndex={dataIndex}
                        rowIndex={rowIndex}
                    /> 
                ),
            }}
        />
    )

}