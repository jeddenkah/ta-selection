import React from 'react';
import PropTypes from 'prop-types';
import {
    Form,
    FormInput,
    FormSelect,
    FormGroup,
    Button
} from 'shards-react';
var _ = require('lodash');
import unpopulateRecord from '../utils/unpopulateRecord';
import ModelSelect from './ModelSelect';

export default class ModelForm extends React.Component {
    constructor(props) {
        super(props);
        ModelForm.propTypes = {
            mode: PropTypes.oneOf(['NEW', 'EDIT', '']),
            modelName: PropTypes.string,
            attributes: PropTypes.object.isRequired,
            referencedModelNames: PropTypes.arrayOf(PropTypes.string),
            referencedModelRecords: PropTypes.object,
            initialRecord: PropTypes.object,
            hideColumns: PropTypes.arrayOf(PropTypes.string),
            renderColumn: PropTypes.object
        }
        this.state = {
            errorMessages: new Map(),
            ...unpopulateRecord(props.initialRecord)
        }
        
        this.handleChange = this.handleChange.bind(this);
        this.validate = this.validate.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }
    handleChange(e) {
        const target = e.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }
    validate() {
        this.setState((state, props) => {
            const errorMessages = getModelFormErrors(state);
            return {errorMessages}
        });
        const errorMessages = getModelFormErrors(state);
        return errorMessages.size ==  0;
    }
    handleSubmit(e) {
        e.preventDefault();
        if (!this.validate) return;
        const data = _.omit(this.state, ['errorMessages', ...this.props.hideColumns]);
        if (this.props.mode == 'NEW') {
            fetch(`/${this.props.modelName}`, {
                method: 'POST',
                body: JSON.stringify(data)
            }).then(res => {
                if (res.ok) {
                    window.location.reload(true);
                }
            })
        } else if (this.props.mode == 'EDIT') {
            fetch(`/${this.props.modelName}/${data.id}`, {
                method: 'PATCH',
                body: JSON.stringify(data)
            }).then(res => {
                if (res.ok) {
                    window.location.reload(true);
                }
            })
        }
    }
    render() {
        const {referencedModelRecords = {}, attributes, modelName} = this.props;
        const formGroups = Object.entries(attributes).map(([attributeName, attribute]) => {
                const value = this.state[attributeName];
                const refModelRecords = referencedModelRecords[attribute.model];
                const errorMessage = this.state.errorMessages.get(attributeName);
                const renderColumn = this.props.renderColumn?.[attributeName]
                const inputField = getInputField(
                    attributeName,
                    attribute,
                    value,
                    this.handleChange,
                    refModelRecords,
                    renderColumn
                );
                return (
                    <FormGroup key={attributeName}>
                        <label htmlFor={attributeName}>
                            {attributeName}
                        </label>
                        {inputField}
                        <p className="help-block">{errorMessage}</p>
                    </FormGroup>
                )
            })
            
        return (
            <div>
                <Form
                    method="POST"
                    onSubmit={this.handleSubmit}
                >
                    {formGroups}
                    <Button type="submit" theme="primary">Submit</Button>
                </Form>
            </div>
        )
    }
}

function FieldGroup({attributeName, errorMessage}) {
    return (
        <FormGroup key={attributeName}>
            <label htmlFor={attributeName}>
                {attributeName}
            </label>
            {inputField}
            <p className="help-block">{errorMessage}</p>
        </FormGroup>
    )
}

function getInputField(attributeName, attribute, value, onChange, modelList = [], renderColumn) {
    if (attribute.type == 'number') {
        return (
            <FormInput
                type="number"
                name={attributeName}
                value={value}
                mix={attribute.min}
                max={attribute.max}
                onChange={onChange}
            />
        );
    }
    if (attribute.type == 'string') {
        if (attribute.isIn) {
            return (
                <FormSelect
                    name={attributeName}
                    value={value}
                    onChange={onChange}
                >
                    <option value=""></option>
                    {attribute.isIn.map(opt => <option key={opt} value={opt}>{opt}</option>)}
                </FormSelect>
            );
        }
        return (
            <FormInput
                name={attributeName}
                value={value}
                onChange={onChange}
            />
        );
    }
    if (attribute.type == 'boolean') {
        return (
            <div className="form-check">
                <input
                    className="form-check-input"
                    name={attributeName}
                    type="checkbox"
                    checked={value}
                    onChange={onChange} />
            </div>
        )
    }
    if (attribute.model) {
        //pakai react-select
        /*
        const selectOptions = modelList
            .map(record => {
                const optionLabel = typeof renderColumn == 'function' ? renderColumn({[attributeName]: record}) : record.id
                return {value: record.id, label: optionLabel}
            });
        const handleChange = (selectedOption) => {
            onChange({target: {name: attributeName, value: selectedOption.value}})
        }
        const label = _.find(selectOptions, opt => opt.value == value)?.label;
        const selectedOption = value ? {value, label} : undefined;
        const emptyStyle = new Proxy({}, {
            get: (target, propKey) => () => {}
        });*/


        return (
            <ModelSelect 
                name={attributeName}  
                modelName={attribute.model} 
                value={value} 
                onChange={onChange} 
                displayAttribute={record => renderColumn({[attributeName]: record})}
                records={modelList}
            />
        );
    }
}