import React from 'react';
import PropTypes from 'prop-types';

export default function KelasSelect({ kelasList, value, onChange, name = 'kelasId' }) {
    KelasSelect.propTypes = {
        kelasList: PropTypes.arrayOf(PropTypes.object).isRequired,
        value: PropTypes.string,
        onChange: PropTypes.func,
        name: PropTypes.string
    }
    return (
        <select className="form-control" style={{maxWidth: 'fit-content'}} name={name} value={value || ''} onChange={onChange}>
            <option value=""></option>
            {kelasList.map(kelas => {
                return <option key={kelas.id} value={kelas.id}>{kelas.class}</option>
            })}
        </select>
    )
}
