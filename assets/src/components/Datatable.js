import React, {useEffect} from 'react'
import PropTypes from 'prop-types'
import '../../dependencies/datatables/DataTables-1.10.20/js/jquery.dataTables'
import '../../dependencies/datatables/DataTables-1.10.20/css/jquery.dataTables.min.css'

export default function Datatable({children, id, options}) {
    useEffect(() => {
        $(document).ready(() => {
            $('#'+id).DataTable(options);
        })
    }, [])
    return (
        children
    )
}
Datatable.propTypes = {
    children: PropTypes.element.isRequired,
    id: PropTypes.string.isRequired,
    options: PropTypes.object
}