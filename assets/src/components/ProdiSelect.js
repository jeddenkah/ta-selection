import React from 'react';
import PropTypes from 'prop-types';

export default function ProdiSelect({ prodiList, value, onChange, name = 'prodiId' }) {
    ProdiSelect.propTypes = {
        prodiList: PropTypes.arrayOf(PropTypes.object).isRequired,
        value: PropTypes.string,
        onChange: PropTypes.func,
        name: PropTypes.string
    }
    return (
        <select className="form-control" style={{maxWidth: 'fit-content'}} name={name} value={value || ''} onChange={onChange}>
            <option value=""></option>
            {prodiList.map(prodi => {
                return <option key={prodi.id} value={prodi.id}>{prodi.name}</option>
            })}
        </select>
    )
}
