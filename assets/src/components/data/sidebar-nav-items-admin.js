export default function() {
  return [
    {
      title: "Setting Periode",
      to: "/admin/period-setting",
      htmlBefore: '<i class="material-icons">dashboard</i>',
      htmlAfter: ""
    },
    {
      title: "Master Data",
      to: "/admin/master-data",
      htmlBefore: '<i class="material-icons">dashboard</i>',
      htmlAfter: ""
    },
    {
      title: "Topik",
      htmlBefore: '<i class="material-icons">filter_9_plus</i>',
      htmlAfter: '<i class="material-icons">arrow_drop_down</i>',
      to: "#",
      child: [
        {
          title: "Daftar Topik",
          to: "/topics"
        },
        {
          title: "Arsip Topik",
          to: "/admin/topic-archives"
        
        },
        {
          title: "Pemilihan Topik",
          to: "/admin/topic-selections"
        
        },
        {
          title: "Arsip Pemilihan Topik",
          to: "/admin/topic-selections-archives"
        }
      ]
    },
    {
      title: "Dosen",
      htmlBefore: '<i class="material-icons">filter_9_plus</i>',
      htmlAfter: '<i class="material-icons">arrow_drop_down</i>',
      to: "#",
      child: [
        {
          title: "Daftar Dosen",
          to: "/admin/lecturer-list"
        },
        {
          title: "Upload Dosen",
          to: "/admin/upload-user/lecturer"
        
        }
      ]
    },
    {
      title: "Mahasiswa",
      htmlBefore: '<i class="material-icons">filter_9_plus</i>',
      htmlAfter: '<i class="material-icons">arrow_drop_down</i>',
      to: "#",
      child: [
        {
          title: "Daftar Mahasiswa",
          to: "/admin/student-list"
        },
        {
          title: "Status Mahasiswa",
          to: "/admin/student-status"
        },
        {
          title: "Upload Mahasiswa",
          to: "/admin/upload-user/student"
        
        }
      ]
    },
    
    
  ];
}
