export default function() {
  return [
    {
      title: "Dashboard",
      to: "/student/dashboard",
      htmlBefore: '<i class="material-icons">dashboard</i>',
      htmlAfter: ""
    },
    
    {
      title: "TA Selection",
      htmlBefore: '<i class="material-icons">library_books</i>',
      htmlAfter: '<i class="material-icons">arrow_drop_down</i>',
      to: "#",
      child: [
        {
          title:"Daftar Topik",
          to:"/topics"
        },
        {
          title: "Pilih Topik",
          to: "/student/topic-selection/new"
        },
        {
          title: "Status",
          to: "/student/topic-selection-status"
        },
        {
          title: "Setting Group",
          to: "/student/group-setting"
        }
      ]
    },
    {
      title: "Bimbingan Online",
      htmlBefore: '<i class="material-icons">library_books</i>',
      to: "/student/bimbingan"
    },
    {
      title: "Desk Evaluation",
      htmlBefore: '<i class="material-icons">library_books</i>',
      to: "/student/dedashboard"
    },
    {
      title: "Notification",
      htmlBefore: '<i class="material-icons">notifications</i>',
      to: "/notification",
    }
  ];
}
