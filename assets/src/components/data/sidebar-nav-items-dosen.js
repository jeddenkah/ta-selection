import Cookies from 'js-cookie';
import { truncate } from 'lodash';

export default function() {
  const isKoorLabRiset = Cookies.get('koorLabRiset');
  const isKoorKk = Cookies.get('koorKk');
  const isKoorMetlit = Cookies.get('koorMetlit');
  const isDosenPembina = Cookies.get('dosenPembina');
  const isDosenPenguji = Cookies.get('dosenPenguji');
  const isDosenMetlit = Cookies.get('dosenMetlit');
  return [
     {
      title: "Dashboard",
      to: "/lecturer/dashboard",
      htmlBefore: '<i class="material-icons">dashboard</i>',
      htmlAfter: ""
    },
    {
      title: "Desk Evaluation",
      htmlBefore: '<i class="material-icons">filter_9_plus</i>',
      htmlAfter: '<i class="material-icons">arrow_drop_down</i>',
      to: "#",
      child: [
        {
          title: "Dosen Pembina",
          to: "/lecturer/dosen-pembimbing"
        },
        {
          title: "Dosen Reviewer",
          to: "/lecturer/dosen-penguji"
        }
      ]
    }, 
    {
      title: "TA Selection",
      htmlBefore: '<i class="material-icons">group</i>',
      htmlAfter: '<i class="material-icons">arrow_drop_down</i>',
      to: "#",
      child: [
        {
          title: "Daftar Topik Anda",
          to: "/lecturer/topics"
        },
        {
          title: "Buat Topik",
          to: "/lecturer/topic/new"
        },
        {
          title: "Setujui",
          to: "/lecturer/topic-approvals"
        },
        {
          title: "Daftar Topik Semua",
          to: "/topics"
        },
      ]
    },
    {
      title: "Bimbingan",
      htmlBefore: '<i class="material-icons">library_books</i>',
      to: "/lecturer/bimbingan"
    },
    (isKoorLabRiset || isKoorKk) ? {
      title: "Plotting",
      htmlBefore: '<i class="material-icons">group</i>',
      htmlAfter: '<i class="material-icons">arrow_drop_down</i>',
      to: "#",
      child: [
        {
          title: "Plot Dosen Pembimbing",
          to: "/lecturer/plotting-dosen-pembimbing"
        },
        {
          title: "Plot Dosen Penguji",
          to: "/lecturer/plotting-dosen-penguji"
        },
      ]
    } : {},
    isKoorMetlit=="true" ? {
      title: "Proposal TA",
      htmlBefore: '<i class="material-icons">library_books</i>',
      htmlAfter: '<i class="material-icons">arrow_drop_down</i>',
      to: "#",
      child: [
        {
          title: "Komponen Penilaian",
          to: "/lecturer/koor-metlit"
        },
        {
          title: "Nilai Proposal",
          to: "/lecturer/nilai-proposal"
        }
      ]
    } : {},
    isDosenMetlit=="true" ? {
      title: "Nilai Proposal",
      htmlBefore: '<i class="material-icons">library_books</i>',
      to: "/lecturer/nilai-proposal"
    } : {}
  ];
}
