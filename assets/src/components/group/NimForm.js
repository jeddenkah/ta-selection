import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {Row, Col, Button, Card, CardBody, FormInput, CardTitle} from 'shards-react';

export default class NimForm extends Component {
    static propTypes = {
        peminatanId: PropTypes.number.isRequired,
        onAddStudent: PropTypes.func.isRequired,
        hasStudent: PropTypes.func.isRequired ,// kalau hasStudent(nim) => true, onAddStudent tidak di panggil
        validation: PropTypes.func, //validation sebelum addStudent: validation(res.student) => errorMessage (string)
    }
    state = {
        nim: '',
        errorMessage: ''
    }
    handleNimChange = (e) => {
        const newNim = e.target.value;
        this.setState({ nim: newNim });
    }
    handleAddNim = (e) => {
        const newNim = this.state.nim;
        if (this.props.hasStudent(newNim)) return this.setState({ errorMessage: `Mahasiswa dengan nim ${newNim} sudah ditambahkan` });
        fetch(`/data/student/${newNim}`)
            .then(res => res.json())
            .then(res => {
                const errorMessage = this.props.validation(res.student, newNim) || this.commonValidation(res.student, newNim);
                if (!errorMessage) {
                    this.props.onAddStudent(res.student);
                    this.setState({ nim: '', errorMessage: '' });
                } else this.setState({errorMessage});
            });
    }
    commonValidation = (student, queryNim) => {
        if (!student) return `Mahasiswa dengan nim ${queryNim} tidak ditemukan`;
        if (student.hasGroup) return  `Mahasiswa dengan nim ${queryNim} sudah memiliki kelompok`;
        if (student.peminatan.id != this.props.peminatanId) return `Anggota harus memiliki peminatan yang sama dengan anda`;
    }
    render = () => {
        const { nim, errorMessage } = this.state;
        return (
            // <tr className={errorMessage ? 'has-error' : ''}>
            //     <td>
            //         <input
            //             type="number"
            //             className="form-control"
            //             name="nim"
            //             value={nim}
            //             onChange={this.handleNimChange} />
            //     </td>
            //     <td colSpan="3">{errorMessage}</td>
            //     <td><button type="button" className="btn-ijo" onClick={this.handleAddNim}>Tambah</button></td>
            // </tr>

            /*<Row style={{ paddingTop: '2em' }}>
                <Col md="12" lg="6">*/
                    <Card>
                        <CardBody>
                            <Row style={{ marginTop: '-1.09375rem' }}>
                                <Col style={{ alignSelf: 'center', textAlign: 'left' }}><CardTitle style={{ marginTop: '0' }}>Tambahkan Anggota</CardTitle></Col>
                            </Row>
                            <Row className="formComponentSpace">
                                <CardTitle>{errorMessage}</CardTitle>
                            </Row>
                            <Row className="formComponentSpace">
                                <FormInput
                                    type="number"
                                    className="form-control"
                                    name="nim"
                                    value={nim}
                                    onChange={this.handleNimChange}
                                />
                            </Row>
                            <Row className="formComponentSpace">
                                <button className="btn btn-ijo btn-block" onClick={this.handleAddNim} >Tambah</button>
                            </Row>
                        </CardBody>
                    </Card>
                /*</Col>
            </Row>*/
        );
    }
}