import React, {Component} from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import EditableGroupTable from './EditableGroupTable';
import NimForm from './NimForm';
import {Button, Card, CardBody, Row} from 'shards-react';
import {toast} from 'react-toastify';

export default class GroupControl extends Component {
    static propTypes = {
        students: PropTypes.arrayOf(PropTypes.object),
        user: PropTypes.object.isRequired,
        groupId: PropTypes.number,
    }
    state = {
        students: this.props.students ?? [],
        isSubmitting: false,
        isChanged: false,
        errorCode: ''
    }
    handleAddStudent = (student) => {
        this.setState(prevState  => {
            return {students: [...prevState.students, student], isChanged: true}
        });
    }
    handleDeleteStudent = (studentId) => {
        this.setState(prevState => {
            const newStudents = prevState.students.filter(s => s.id != studentId);
            return{students: newStudents, isChanged: true}
        });
    }
    hasStudent = (nim) => {
        const student = this.props.students.find(s => s.nim == nim);
        return !!student;
    }
    handleLeave = () => {
        axios.post('/student/leave-group')
            .then(res => {
                location.reload();
            }).catch(res => {
                const exitCode = err.response.headers["x-exit"];
                this.setState({isSubmitting: false, exitCode: exitCode});
            })
    }
    handleSubmit = () => {
        const studentIds = this.state.students.map(s => s.id);
        axios.post('/student/edit-group', {studentIds})
            .then(res => {
                location.reload();
            }).catch(err => {
                const exitCode = err.response.headers["x-exit"];
                this.setState({isSubmitting: false, errorCode: exitCode});
                alertLocked();
            })
    }
    discardChanges = () => {
        location.reload();
    }

    deleteGroup = () => {
        if (!confirm(`Apakah anda yakin akan menghapus Group#${this.props.groupId}?`)) return;
        this.setState({isSubmitting: true})
        axios.post('/student/delete-group')
            .then(res => {
                toast.success(`Berhasil menghapus Group#${this.props.groupId}!`)
                setTimeout(() => location.reload(), 2000);
            }).catch(err => {
                const exitCode = err.response.headers["x-exit"];
                this.setState({isSubmitting: false, errorCode: exitCode}); 
                alertLocked();
            })
    }

    render = () => {
        const {user} = this.props;
        
        return (
            <div>
                <NimForm
                    peminatanId={user.peminatan.id}
                    hasStudent={this.hasStudent}
                    onAddStudent={this.handleAddStudent}
                    validation={(student, queryNim) => {
                        if (student.waitingOrApprovedTopics && (student.waitingOrApprovedTopics.length > 0) ) {
                            const nama1 = student.name;
                            return `${nama1} / ${queryNim} sudah memiliki pengajuan Topik `;
                        }
                    }}
                />
                <Row className="mb-4"></Row>
                <EditableGroupTable
                    ownerId={user.id}
                    students={this.state.students}
                    onAddStudent={this.handleAddStudent}
                    onDeleteStudent={this.handleDeleteStudent}
                    peminatan={user.peminatan}
                />
                <div className="row">
                    
                </div>
                <div className="group-control-save-buttons mt-4">
                    <button className="mr-2 btn btn-ijo-outline" outline onClick={this.discardChanges} disabled={!this.state.isChanged}>Batalkan Perubahan</button>
                    <button className="btn btn-ijo" onClick={this.handleSubmit} disabled={!this.state.isChanged || this.state.isSubmitting}>Simpan</button>
                    <Button className="right" theme="danger float-right" outline onClick={this.deleteGroup}>Hapus Group</Button>
                </div>
            </div>
        )
    }
}

const alertLocked = () => {
    toast.error('Group sudah terkunci. Topik group sudah ada yang disetujui.', {autoClose: false})
}