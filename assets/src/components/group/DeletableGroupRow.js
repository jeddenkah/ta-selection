import React from 'react';
import PropTypes from 'prop-types';
import {Button} from 'shards-react';

export default function DeletableGroupRow({ student, hasDeleteButton = false, onDeleteStudent, step }) {
    DeletableGroupRow.propTypes = {
        student: PropTypes.object.isRequired,
        hasDeleteButton: PropTypes.bool,
        onDeleteStudent: PropTypes.func.isRequired
    }
    const handleDeleteClick = () => {
        onDeleteStudent(student.id);
    }
    const deleteButton = (
        <Button
            outline 
            theme="danger"
            type="button"
            className="btn btn-default"
            onClick={handleDeleteClick}
        >Hapus
        </Button>
    )
    return (
        <tr>
            <td>{student.nim}</td>
            <td>{student.name}</td>
            <td>{student.class}</td>
            <td>{student.peminatan.abbrev}</td>
            <td>
                {step === 1 && !!hasDeleteButton && deleteButton}
            </td>
        </tr>
    );


}