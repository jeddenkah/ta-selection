import React, {useState} from 'react'
import {Button} from 'shards-react'
import {toast} from 'react-toastify'

export default function LeaveButton() {
    const leave = () => {
        axios.post('/student/leave-group')
            .then(res => {
                location.reload();
            }).catch(err => {
                const exitCode = err.response.headers['x-exit'];
                if (exitCode == 'locked') toast.error('Tidak bisa melanjutkan. Topik group anda sudah disetujui.');
            })
    }
    const handleClick = () => {
        if (confirm('Apakah anda yakin ingin meninggalkan group')) leave();
    }
    return (
        <Button outline theme="danger" onClick={handleClick}>Leave</Button>
    )
}