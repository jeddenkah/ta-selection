import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { Col, Row, Card, CardBody, CardTitle, Button} from 'shards-react';
import DeletableGroupRow from './DeletableGroupRow';

export default class EditableGroupTable extends Component {
    static propTypes = {
        ownerId: PropTypes.number.isRequired,
        students: PropTypes.arrayOf(PropTypes.object).isRequired,
        onDeleteStudent: PropTypes.func.isRequired,
        peminatan: PropTypes.object
    }
    handleHasStudent = (nim) => {
        const student = this.props.students.find(student => student.nim == nim);
        return !!student;
    }
    render = () => {
        const { students, ownerId, onDeleteStudent, peminatan } = this.props;
        return (

            // <div className="main-content-container px-4 pb-4 container-fluid" style={{ paddingTop: '2em' }}>
            // </div>
            //<div /*className="main-content-container px-4 pb-4 container-fluid"*/ style={{ paddingTop: '2em' }}>
                <Card>
                    <CardBody>
                        <Row style={{ marginTop: '-1.09375rem' }}>
                            <Col style={{ alignSelf: 'center', textAlign: 'left' }}><CardTitle style={{ marginTop: '0' }}>Daftar Anggota Kelompok</CardTitle></Col>
                        </Row>
                        <Row className="formComponentSpace">
                            <div className="table-responsive-sm"><table className="table ">
                                <thead>
                                    <tr>
                                        <th>NIM</th>
                                        <th>Nama</th>
                                        <th>Kelas</th>
                                        <th>Peminatan</th>
                                        <th>Aksi</th>
                                    </tr>

                                </thead>
                                <tbody>
                                    {students.map(student => {
                                        const detailedStudent = {...student, peminatan}
                                        return (
                                            <DeletableGroupRow
                                                key={student.id}
                                                student={detailedStudent}
                                                onDeleteStudent={onDeleteStudent}
                                                hasDeleteButton={student.id != ownerId}
                                                step={1} />
                                        );
                                    })}
                                </tbody>
                            </table></div>
                        </Row>
                    </CardBody>
                </Card>
            //</div>
        );
    }
}