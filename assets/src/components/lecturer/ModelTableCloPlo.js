import React from 'react';
import PropTypes from 'prop-types';
import { Row, Button, Modal, ModalBody, ModalHeader } from "shards-react";
import EditableTable from './EditableTableCloPlo';
import ModelForm from '../ModelForm';
import axios from 'axios';
var _ = require('lodash');

/**
 * ModelTable
 * tabel untuk CRUD berbasis sails blueprint api
 * menyediakan form untuk create dan edit
 */

export default class ModelTable extends React.Component {
    constructor(props) {
        super(props);
        ModelTable.propTypes = {
            model: PropTypes.object.isRequired, //skema model sails
            modelName: PropTypes.string, //identitas model
            records: PropTypes.array,
            hideColumns: PropTypes.arrayOf(PropTypes.string), // hide kolom tipe collection biar bisa bekerja
            customForm: PropTypes.func,
            uploadForm: PropTypes.func,
            validationSchema: PropTypes.object, // hanya dipakai jika pakai default form
            renderColumn: PropTypes.object, // {[columnName]: (record) => string}
            shallowDelete: PropTypes.bool // kalau true: tombol hapus hanya akan mengeset isDeleted = true, tabel hanya memperlihatkan isDeleted = false
        }
        this.state = {
            mode: '',
            modelToEdit: {},
            records: props.records ?? [],
            referencedModelRecords: {}, //key: modelName, value: modelRecords
            isLoading: false,
        }
        this.fetchData = this.fetchData.bind(this);
        this.handleNew = this.handleNew.bind(this);
        this.handleUploadMetlit = this.handleUploadMetlit.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }
    fetchData() {
        const {records, model} = this.props
        if(!records) {
            this.setState({isLoading: true});
            const modelName = this.props.modelName;
            const query = this.props.shallowDelete ? '?where={"isDeleted":false}' : ''
            fetch(`/${modelName}${query}`, {
                method: 'GET',
            }).then(res => res.json())
            .then(records => this.setState({records, isLoading: false}))
        }
        _.entries(model.attributes).forEach(([attrName, props]) => {
            const modelName = props.model
            if (!modelName) return;
            axios(`/${modelName}`, {
                method: 'GET'
            })
            .then(res => {
                this.setState((prevState) => {
                    let referencedModelRecords = prevState.referencedModelRecords;
                    referencedModelRecords[modelName] = res.data;
                    return referencedModelRecords;
                })
            });

        });
    }
    componentDidMount() {
        this.fetchData();
    }
    componentDidUpdate(prevProps) {
        if (this.props.modelName != prevProps.modelName) {
            this.fetchData(); 
        }
        
    }
    handleNew() {
        this.setState({mode: 'NEW', modelToEdit: null});
    }
    handleUploadMetlit(){
        this.setState({mode: 'UPLOAD', modelToEdit: null})
    }
    handleEdit(modelId) {
        const modelToEdit = this.state.records.find(model => model.id == modelId);
        this.setState({modelToEdit, mode: 'EDIT'});
    }
    handleDelete(modelId) {
        const model = this.state.records.find(model => model.id == modelId);
        const {modelName} = this.props;
        const shouldDelete = confirm(`Apakah anda yakin akan menghapus ${modelName} dengan id ${model.id}?`);
        if (shouldDelete) {
            if (this.props.shallowDelete) {
                fetch(`/${modelName}/${modelId}`, {
                    method: 'PATCH',
                    body: JSON.stringify({isDeleted: true})
                }).then(res => {
                    if (res.ok) {
                        window.location.reload(true);
                    }
                })
            } else {
                fetch(`/${modelName}/${modelId}`, {
                    method: 'DELETE',
                }).then(res => {
                    if (res.ok) {
                        window.location.reload(true);
                    }
                })
            }
        }
    }
    closeModal() {
        this.setState({mode: '', modelToEdit: null});
    }
    toggleModal() {
        this.setState(state => ({mode: '', modelToEdit: null}));
    }
    render() {
        const {model, modelName, isLoading, shallowDelete} = this.props;
        const hideColumns = this.props.hideColumns ?? [];
        if (shallowDelete) hideColumns.push(['isDeleted']);
        const {records, referencedModelRecords} = this.state;
        const modelIds = records.map(model => model.id);
        model.attributes = _.omit(model.attributes, hideColumns);
        const columns = Object.keys(model.attributes);
        const modelTuples = records.map((record, i) => {
            const attributeValues = columns
            .map(column => {
                const renderThisColumn = this.props.renderColumn && this.props.renderColumn[column];
                return typeof renderThisColumn == 'function' ? renderThisColumn(record) : record[column];
            });
            return [modelIds[i], ...attributeValues] 
        });
        const columnHeaders = ['id', ...columns];
        const modalTitle = (this.state.mode == 'NEW' ? 'Tambah' : this.state.mode == 'UPLOAD' ? 'Upload' : 'Ganti') + ' ' + _.capitalize(modelName);
        return (
            <div className="table">
                <div style={{display: "flex", justifyContent: 'space-between'}}>
                    {/* <Button onClick={this.handleNew}>Tambah</Button> */}
                    {modelName == 'metlit'?<Button onClick={this.handleUploadMetlit} outline>Upload</Button>:<></>}
                </div>
                {isLoading && <p>Loading data ...</p>}
                <div className="model-table">
                    <EditableTable
                        id={this.props.id}
                        columns={columnHeaders}
                        tuples={modelTuples}
                        rowIds={modelIds}
                        onRowDelete={this.handleDelete}
                        onRowEdit={this.handleEdit}
                    />
                </div>
                <Modal size="lg" open={!!this.state.mode} toggle={this.closeModal} modalClassName="modal">
                <ModalHeader>{modalTitle}</ModalHeader>
                    <ModalBody>
                        { this.props.customForm ? <this.props.customForm mode={this.state.mode} initialRecord={this.state.modelToEdit} /> : this.state.mode == 'UPLOAD' ? <this.props.uploadForm mode={this.state.mode} initialRecord={this.state.modelToEdit} /> :
                            <ModelForm
                                mode={this.state.mode}
                                modelName={modelName}
                                attributes={model.attributes}
                                referencedModelRecords={referencedModelRecords}
                                initialRecord={this.state.modelToEdit}
                                validationSchema={this.props.validationSchema}
                                hideColumns={hideColumns}
                                renderColumn={this.props.renderColumn}
                        />}
                        
                    </ModalBody>
                </Modal>
            </div>
        )
    }
}