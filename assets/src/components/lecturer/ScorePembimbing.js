import React from 'react';
import PropTypes from 'prop-types';

export default class ScorePembimbing extends React.Component {
        constructor(props) {
            super(props);
            ScorePembimbing.propTypes = {
              nilaiList: PropTypes.arrayOf(PropTypes.array),
              student : PropTypes.number,
          }
            this.state = {
                nilaiPerClo:[]
            }
        }
        componentDidMount() {
            //getnilai
            const initNilaiPerClo = (state, props) => {
                let nilaiPerClo = {}
                props.nilaiList.forEach((nilai) => {
                    if(nilai.student.id==props.student){
                        console.log(nilai.student.id+'-'+props.student)
                        nilai.subCloRubricList.forEach((rubric) => {
                            nilaiPerClo[rubric.subClo.clo] = nilaiPerClo[rubric.subClo.clo] || 0;
                            nilaiPerClo[rubric.subClo.clo] += rubric.subClo.portion / 100 * rubric.score;
                        });
                    }
                    

                });
                return {nilaiPerClo}
            }
            this.setState(initNilaiPerClo);
        }
        render(){  
            let total=0;
               return(
                <>{Object.entries(this.state.nilaiPerClo).map(([key, value])=>{
                    total+=value
                })
                }
                <p>{total}</p>
                </>)
        }
}