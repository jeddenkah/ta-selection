import React from 'react';
import { Nav, NavItem } from "shards-react";
import {Link, useRouteMatch} from 'react-router-dom';

function NavigationItem({ label, to, activeOnlyWhenExact }) {
    let match = useRouteMatch({
      path: to,
      exact: activeOnlyWhenExact
    });
  
    return (
      <NavItem>
        <Link className={match ? 'nav-link active' : 'nav-link'} to={to}>{label}</Link>
      </NavItem>
    );
  }

export default function KoorMetlitNav() {
  const homePath = '/lecturer/koor-metlit'
  return (
    <Nav tabs className="master-data-nav">
        <NavigationItem label="PLO" to={homePath + "/plo"} />
        <NavigationItem label="CLO" to={homePath + "/clo"} />
        <NavigationItem label="Bobot Penilaian" to={homePath + "/bobot"} />
    </Nav>
  );
}