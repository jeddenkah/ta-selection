import React from 'react';
import PropTypes from 'prop-types';

export default function PeriodSelectMetlit({ periods, value, onChange, name = 'periodId' }) {
    PeriodSelectMetlit.propTypes = {
        periods: PropTypes.arrayOf(PropTypes.object).isRequired,
        value: PropTypes.string,
        onChange: PropTypes.func,
        name: PropTypes.string
    }
    return (
        <select className="form-control" style={{maxWidth: 'fit-content'}} name={name} value={value} onChange={onChange}>
            {/* <option value=""></option> */}
            {periods.map(period => {
                return <option key={period.id} value={period.id}>{period.academicYear} {period.semester}</option>
            })}
        </select>
    )
}
