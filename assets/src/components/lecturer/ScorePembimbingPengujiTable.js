import React from "react";

import '../../../dependencies/datatables/DataTables-1.10.20/js/jquery.dataTables'
import '../../../dependencies/datatables/DataTables-1.10.20/css/jquery.dataTables.min.css'



export default class ScoreTable extends React.Component {
    constructor(props) {
        super(props);
        //this.options = [];
        this.state = {
            Pembimbing: [],
            Penguji: [],
            studentList: [],
            headerList: [],
        };
		//console.dir(state);
        $(document).ready(() => {
            $('#score-table').DataTable()
        })

    }
    componentDidMount() {
        //this.setState({Penguji : getPenguji(this.props.records2, this.props.filterPenguji, this.props.bobot)})
        //this.setState({Pembimbing : getPembimbing(this.props.records1, this.props.filterPembimbing, this.props.bobot)})
        this.props.scoreList.map((score) => {
                this.setState(({ headerList }) => {
                    const newHeader = {
                        subCloId : score.subClo.id,
                        subClo: score.subClo.description,
                        clo: score.clo.description,
                        plo: score.plo.ploCode,
                    };
                    return {
                         headerList: [...headerList, newHeader],
                    };
             });
        });
    }
    render() {
        var header = [];
        header = this.state.headerList.filter(function (a) {
            var key = a.subCloId;
            if (!this[key]) {
                this[key] = true;
                return true;
            }
        }, Object.create(null));

        var sortedHeader = header.sort((a, b) => parseFloat(a.subCloId) - parseFloat(b.subCloId));

        var sortedScore = this.props.scoreList.sort((a, b) => parseFloat(a.subClo) - parseFloat(b.subClo));
        //var sortedPenguji = this.props.scorePenguji.sort((a, b) => parseFloat(a.subClo) - parseFloat(b.subClo));
        // const rows = sortedPembimbing.concat(sortedPenguji);
        

        // let scoreTotalAll = [];

        // rows.forEach(function (a) {
        //     if ( !this[a.subClo.id]) {
        //         this[a.subClo.id] = { student: a.student, subClo: a.subClo, nilaiCalculatedBobot: 0, nilaiCalculatedBobotPortion: 0 };
        //         scoreTotalAll.push(this[a.subClo.id]);
        //     } 
        //     this[a.subClo.id].nilaiCalculatedBobot += a.nilaiCalculatedBobot;
        //     this[a.subClo.id].nilaiCalculatedBobotPortion += a.nilaiCalculatedBobotPortion;
        // }, Object.create(null));
        
        

        // let scoreTotalCLO = []
        // rows.reduce(function (res, value) {
        //     if (!res[value.clo.id]) {
        //         res[value.clo.id] = {
        //             student: value.student,
        //             subClo: value.subClo,
        //             portion: 0,
        //             nilaiCalculatedBobotPortion:0
        //         };
        //         scoreTotalCLO.push(res[value.clo.id]);
        //     }
        //     res[value.clo.id].portion += value.portion;
        //     res[value.clo.id].nilaiCalculatedBobotPortion += value.nilaiCalculatedBobotPortion;
        //     return res;
        // }, {});

      

        // let groupedAll = _.mapValues(
        //     _.groupBy(scoreTotalAll, "student.nim"),
        //     (clist) => clist.map((nilai) => _.omit(nilai, "student.nim"))
        // );
        // let groupedAllCLO = _.mapValues(
        //     _.groupBy(scoreTotalCLO, "student.nim"),
        //     (clist) => clist.map((nilai) => _.omit(nilai, "student.nim"))
        // );
        let finalScore = 0;

        let groupedScore = _.mapValues(
            _.groupBy(sortedScore, "student.nim"),
            (clist) => clist.map((nilai) => _.omit(nilai, "student.nim"))
        );

        // let groupedPenguji = _.mapValues(
        //     _.groupBy(this.props.scorePenguji, "student.nim"),
        //     (clist) => clist.map((nilai) => _.omit(nilai, "student.nim"))
        // );

        // console.log(this.props.student)
        //console.log(sortedHeader)
        // console.log(rows)
        // console.log(scoreTotalAll)
        // console.log(groupedAll)
        // console.log(groupedPenguji)
        console.log(groupedScore)
        console.log(this.props.studentBimbingan)
       // console.log(sortedPenguji)

        const view = this.props.view;
        // if (view == "all") {
        //     var dataScore = groupedAll;
        // } else if (view == "pembimbing") {
        //     var dataScore = groupedPembimbing;
        // } else {
        //     var dataScore = groupedPenguji;
        // }
        let lastKey = Object.keys(groupedScore)[Object.keys(groupedScore).length-1];
        console.log('last : '+lastKey)
        return (
            <div>
                <table id="score-table" className="table table-responsive">
                    <thead>
                        <tr>
                            <th>NIM</th>
                            <th>Dosen {view}</th>
                            {sortedHeader.map((headerTable) => {
                                return (
                                    <th>
                                        {headerTable.plo +
                                            " - " +
                                            headerTable.clo +
                                            " " +
                                            headerTable.subClo}
                                    </th>
                                );
                            })}
                           
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                    view=='Pembimbing'? 
                    this.props.studentBimbingan.map((student)=>{
                        if(student.groupStudent!=null){
                            return student.groupStudent.map((gs)=>{
                                return gs.group.map((gp)=>{
                                   if(gp.topicSelection!=null){
                                    return gp.topicSelection.map((ts)=>{
                                        return ts.topic.map((tp)=>{
                                         // return tp.lecturer.map((lt)=>{
                                            return(
                                                <tr>
                                                <td>{student.nim}</td>
                                                {console.log(tp.lecturer)}
                                                <td>{tp.lecturer.name}</td>
                                                {Object.entries(groupedScore).map(([keys,nilaiList])=> {
                                                    if(student.nim==keys){
                                                        finalScore=0
                                                        return(
                                                            <>
                                                            
                                                            {
                                                                 Object.entries(nilaiList).map(([key,nilai])=>{
                                                                            //if(student.nim==keys){
                                                                            finalScore+=nilai.nilaiCalculatedPortion
                                                                            return(
                                                                                <td>{nilai.nilaiPerRubric}</td>
                                                                            )
                                                                        //}
                                                                        })
                                                                        
                                                                }
                                                                {
                                                                    finalScore!=0 ? <td>{finalScore}</td> : null
                                                                }
                                                            </>
                                                        )
                                                    }
                                                    else if (student.nim!=keys && keys==lastKey && 
                                                        Object.keys(groupedScore).filter(key => key == student.nim)<=0){
                                                        finalScore=0
                                                    }
                                                    })}
                                                    
                                                     {
                                                     sortedHeader.map(sorted=>{
                                                             if(finalScore==0){
                                                                 return(
                                                                     <td>Belum Dinilai</td>
                                                                 )
                                                             }
                                                         })
                                                     }
                                                    {finalScore ==0 ? <td>{finalScore}</td> : null }
                                                  
                                            </tr>
                                            )
                                          })
                                          
                                        })
                                    //})
                                   }
                                })
                            })
                        }
                    }) :
                    this.props.studentReview.map((student)=>{
                        return student.groupStudent.map((gs)=>{
                            return(
                                <tr>
                                <td>{student.nim}</td>
                               
                                <td>{gs.lecturer.name}</td>
                                {Object.entries(groupedScore).map(([keys,nilaiList])=> {
                                    if(student.nim==keys){
                                        finalScore=0
                                        return(
                                            <>
                                            
                                            {
                                                 Object.entries(nilaiList).map(([key,nilai])=>{
                                                            //if(student.nim==keys){
                                                            finalScore+=nilai.nilaiCalculatedPortion
                                                            return(
                                                                <td>{nilai.nilaiPerRubric}</td>
                                                            )
                                                        //}
                                                        })
                                                        
                                                }
                                                {
                                                    finalScore!=0 ? <td>{finalScore}</td> : null
                                                }
                                            </>
                                        )
                                    }
                                    else if (student.nim!=keys && keys==lastKey && 
                                        Object.keys(groupedScore).filter(key => key == student.nim)<=0){
                                        finalScore=0
                                    }
                                    })}
                                    
                                     {
                                     sortedHeader.map(sorted=>{
                                             if(finalScore==0){
                                                 return(
                                                     <td>Belum Dinilai</td>
                                                 )
                                             }
                                         })
                                     }
                                    {finalScore ==0 ? <td>{finalScore}</td> : null }
                                  
                            </tr>
                            )
                        })
                    }) 
                        
                    }
                    </tbody>
                </table>
            </div>
        );
    }
}
