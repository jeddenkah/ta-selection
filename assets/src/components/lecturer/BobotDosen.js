import React from 'react';
import { toast } from 'react-toastify';

import {
    Row, Card, CardBody, CardSubtitle, CardTitle, Col, FormInput, InputGroup,
    InputGroupText,
    InputGroupAddon, Button
} from 'shards-react';



export default class BobotDosen extends React.Component {

    constructor(props) {
        super(props);
    
        this.state = {
         
          edit: props.bobotPembina ? true : false,
          bobotPembina: props.bobotPembina,
          bobotPenguji: props.bobotPenguji,
          periodId : props.periodId,
    
        };
    
        this.handleChange = this.handleChange.bind(this);
        this.editChange = this.editChange.bind(this);
        this.simpanBobot = this.simpanBobot.bind(this);
        
      }

      handleChange(e) {
        this.setState({ [e.target.name]: e.target.value });
      }
      editChange() {
        if (!this.state.periodId) return toast.error("Periode wajib diisi!")
        this.setState({edit: false});
      }
    //   const handleApprove = () => {
    //     fetch('/lecturer/update-plotting-dosen-pembimbing', {
    //         method: 'POST',
    //         body: JSON.stringify({ id: topicSelectionId, status: 'APPROVED_LAB_RISET' })
    //     }).then(res => {
    //         if (res.redirected) location.href = res.url;
    //     })
    // }
      simpanBobot(){
        if (!this.state.bobotPembina || !this.state.bobotPenguji){ return toast.error("Bobot tidak boleh kosong!")}
        else{
            const fd = new FormData();
            fd.append('bobot_pembimbing', this.state.bobotPembina);
            fd.append('bobot_penguji', this.state.bobotPenguji);
            fd.append('period_id', this.state.periodId);
            console.log(this.state.periodId)
            axios.post('/lecturer/bobot-dosen', fd).then(res => {
                this.setState({ bobotPembina: res.data.dosenPembimbing,bobotPenguji: res.data.dosenPenguji, periodId: res.data.period, edit: true })
                console.log(res)    
            });
        }
        
      }

      render(){
        let {edit, bobotPembina, bobotPenguji, periodId } = this.state;
    return (
        <Row>
            <Col sm="12" lg="8">
                <Card>
                    <CardBody>
                        <Row >
                            <Col style={{ alignSelf: 'center', textAlign: 'left' }}><CardTitle style={{ marginTop: '0' }}>Input Presentase Pembimbing</CardTitle></Col>
                        </Row>
                        <Row >
                            <Col style={{ alignSelf: 'center', textAlign: 'left' }}><CardSubtitle style={{ marginTop: '0' }}>Untuk nilai hasil akhir dari mahasiswa</CardSubtitle></Col>
                        </Row>
                        <Row className="container">


                            <div style={{paddingTop: '1em'}}>
                            
                                <InputGroup className="mb-2">
                                {edit? 
                                    <FormInput placeholder="e.g: 20" name="bobotPembina" value={bobotPembina} onChange={this.handleChange} disabled/>
                                    :
                                    <FormInput placeholder="e.g: 20" name="bobotPembina" value={bobotPembina} onChange={this.handleChange} />
                                }
                                    <InputGroupAddon type="append">
                                        <InputGroupText>%</InputGroupText>
                                    </InputGroupAddon>
                                </InputGroup>
                            
                            </div>
                        </Row>
                        <Row >
                            <Col style={{ alignSelf: 'center', textAlign: 'left' }}><CardTitle style={{ marginTop: '0' }}>Input Presentase Reviewer</CardTitle></Col>
                        </Row>
                        <Row >
                            <Col style={{ alignSelf: 'center', textAlign: 'left' }}><CardSubtitle style={{ marginTop: '0' }}>Untuk nilai hasil akhir dari mahasiswa</CardSubtitle></Col>
                        </Row>
                        <Row className="container">


                            <div style={{paddingTop: '1em'}}>
                            
                                <InputGroup className="mb-2">
                                    {edit ?
                                     <FormInput placeholder="e.g: 20" name="bobotPenguji" value={bobotPenguji} onChange={this.handleChange} disabled/>
                                     :
                                     <FormInput placeholder="e.g: 20" name="bobotPenguji" value={bobotPenguji} onChange={this.handleChange}/>
                                }
                                   
                                    <InputGroupAddon type="append">
                                        <InputGroupText>%</InputGroupText>
                                    </InputGroupAddon>
                                </InputGroup>
                            
                            </div>
                            <div className="formComponentSpace container">
                              {edit? 
                              <Button block className="btn-ijo-outline" onClick={this.editChange} >Edit</Button>
                              :
                              <Button block className="btn-ijo" onClick={this.simpanBobot} >Simpan</Button>
                            }
                              
                            </div>
                        </Row>
                      
                    </CardBody>
                </Card>
            </Col>
            <Col sm="12" lg="4">


            </Col>
        </Row>
    )
      }
}
