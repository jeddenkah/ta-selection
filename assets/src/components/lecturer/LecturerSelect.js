import React from "react";
import PropTypes from "prop-types"
import MUIDataTable from "mui-datatables";
import {Button, Modal, ModalBody, ModalHeader, InputGroup, InputGroupAddon, InputGroupText, FormInput, FormSelect} from "shards-react";
import flattenObject from "../../utils/flattenObject"

const COLUMNS = [
	{
		name: "id",
		label: "ID"
	},
	{
		name: "name",
		label: "Nama"
	},
	{
		name: "nik",
		label: "NIK"
	},
	{
		name: "jfa.abbrev",
		label: "Jfa"
	},
	{
		name: "labRiset.abbrev",
		label: "Lab Riset"
	},
	{
		name: "prodi.abbrev",
		label: "Prodi"
	}
]

export default class LecturerSelect extends React.Component {
	state = {
		selectedLecturer: '',
		isOpen: false,
		records: [],
	}
	static propTypes = {
		name: PropTypes.string,
		value: PropTypes.object,
		onChange: PropTypes.func,
		disabled: PropTypes.bool,
		prodiId: PropTypes.number
	}
	static defaultProps = {
		name: 'lecturerId',
		value: {},
		onChange: () => null,
		disabled: false
	}
	componentDidMount = () => {
		let query = "/lecturer?limit=1000";
		//if (this.props.prodiId) query += "&prodi=" + this.props.prodiId  
		fetch(query)
			.then(res => res.json())
			.then(records => records.map(r => flattenObject(r)))
			.then(records => this.setState({records}))
	}
	openModal = () => {
		this.setState({isOpen: true});
	}
	closeModal = () => {
		this.setState({isOpen: false});
	}
	handleRowClick = (rowData) => {
		const lecturerId = rowData[0];
		const lecturerName = rowData[1];
		this.props.onChange({target: {name: this.props.name, value: {id: lecturerId, name: lecturerName}}});
		this.closeModal();
	}
	render() {
		const {records} = this.state;
		const {value} = this.props;
		const tableOptions = {
			onRowClick: this.handleRowClick,
			selectableRows: "single",
			selectableRowsHideCheckboxes: true,
			selectableRowsOnClick: false,
			download: false,
			print: false
		}
		return (
			<>
				<InputGroup>
					<FormInput value={value.name} readOnly/>
					<InputGroupAddon type="append">
						<Button
							onClick={this.openModal}
							disabled={this.props.disabled}
						>Pilih dosen</Button>
					</InputGroupAddon>
				</InputGroup>
				<Modal
					open={this.state.isOpen}
					toggle={this.state.isOpen ? this.closeModal : this.openModal}
				>
					<ModalHeader>
						Pilih Dosen
					</ModalHeader>
					<ModalBody>
						<MUIDataTable
							columns={COLUMNS}
							data={records}
							options={tableOptions}
						/>
					</ModalBody>
				</Modal>
			</>
		);
	}
}