import React from "react";

import '../../../dependencies/datatables/DataTables-1.10.20/js/jquery.dataTables'
import '../../../dependencies/datatables/DataTables-1.10.20/css/jquery.dataTables.min.css'
import {Button} from  "shards-react";
import Export from "react-html-table-to-excel";



export default class ScoreTable extends React.Component {
    constructor(props) {
        super(props);
        //this.options = [];
        this.state = {
            Pembimbing: [],
            Penguji: [],
            studentList: [],
            headerList: [],
        };
		//console.dir(state);
        $(document).ready(function() {
            $('#score-table').DataTable();
        })

    }
    componentDidMount() {
        //this.setState({Penguji : getPenguji(this.props.records2, this.props.filterPenguji, this.props.bobot)})
        //this.setState({Pembimbing : getPembimbing(this.props.records1, this.props.filterPembimbing, this.props.bobot)})
        this.props.scorePembimbing.map((score) => {
                this.setState(({ headerList }) => {
                    const newHeader = {
                        subCloId : score.subClo.id,
                        subClo: score.subClo.description,
                        clo: score.clo.description,
                        plo: score.plo.ploCode,
                    };
                    return {
                         headerList: [...headerList, newHeader],
                    };
             });
        });
    }
    render() {
        var header = [];
        header = this.state.headerList.filter(function (a) {
            var key = a.subCloId;
            if (!this[key]) {
                this[key] = true;
                return true;
            }
        }, Object.create(null));

        var sortedHeader = header.sort((a, b) => parseFloat(a.subCloId) - parseFloat(b.subCloId));

        var sortedPembimbing = this.props.scorePembimbing.sort((a, b) => parseFloat(a.subClo) - parseFloat(b.subClo));
        var sortedPenguji = this.props.scorePenguji.sort((a, b) => parseFloat(a.subClo) - parseFloat(b.subClo));
        const rows = sortedPembimbing.concat(sortedPenguji);
        

        let scoreTotalAll = [];


        scoreTotalAll = [...rows.reduce((r, o) => {
            const key = o.subClo.id + '-' + o.student.id;
            
            const item = r.get(key) || Object.assign({}, o, {
                nilaiCalculatedBobot: 0, 
                nilaiCalculatedBobotPortion: 0 
            });
            
            item.nilaiCalculatedBobot += o.nilaiCalculatedBobot;
            item.nilaiCalculatedBobotPortion += Math.round(o.nilaiCalculatedBobotPortion*100) / 100;
          
            return r.set(key, item);
          }, new Map).values()];
        
        

        let scoreTotalAllCLO = []
        
        
        scoreTotalAll.forEach(function (a) {
            if ( !this[a.clo]) {
                this[a.clo] = { clo:a.clo , student: a.student, portion: 0, nilaiCalculatedBobot: 0 };
                scoreTotalAllCLO.push(this[a.clo]);
            } 
            //let portionDec = a.portion/100;
            this[a.clo].portion += a.portion;
            this[a.clo].nilaiCalculatedBobot += a.nilaiCalculatedBobot;
        }, Object.create(null));
      

        let groupedAll = _.mapValues(
            _.groupBy(scoreTotalAll, "student.nim"),
            (clist) => clist.map((nilai) => _.omit(nilai, "student.nim"))
        );
        let groupedAllCLO = _.mapValues(
            _.groupBy(scoreTotalAllCLO, "student.nim"),
            (clist) => clist.map((nilai) => _.omit(nilai, "student.nim"))
        );
        
        let finalScore = 0;

        let lastKey = Object.keys(groupedAll)[Object.keys(groupedAll).length-1];
        console.log('last : '+lastKey)
        return (
            <div>
                <Button>
                    <Export 
                    className="btn"
                    table="score-table"
                    filename="data_penilaian_all"
                    sheet="sheet 1"
                    buttonText="DOWNLOAD"
                    />
                </Button>
               
                <table id="score-table" className="table table-responsive">
                    <thead>
                        <tr>
                            <th>NIM</th>
                            <th>Nama Mahasiswa</th>
                            {sortedHeader.map((headerTable) => {
                                return (
                                    <th>
                                        {headerTable.plo +
                                            " - " +
                                            headerTable.clo +
                                            " " +
                                            headerTable.subClo}
                                    </th>
                                );
                            })}
                            {/* {sortedHeader.map((headerTable) => {
                                return (
                                    <th>
                                        {headerTable.clo}
                                    </th>
                                );
                            })} */}
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.student.map((student)=>{
                            return(
                            <tr>
                            <td>{student.nim}</td>
                            <td>{student.name}</td>
                            {Object.entries(groupedAll).map(([keys, nilaiList]) => {
                                var sortedScore = nilaiList.sort((a, b) => parseFloat(a.subClo.id) - parseFloat(b.subClo.id));
                                            if(student.nim==keys){
                                                finalScore=0
                                                return(
                                                    <>
                                                    {
                                                        Object.entries(sortedScore).map(([key,nilai])=>{
                                               
                                                            finalScore+=Math.round(nilai.nilaiCalculatedBobotPortion*100) / 100
                                                                    
                                                                    return(
                                                                        <td>{nilai.nilaiCalculatedBobot}</td>
                                                                    )
                                                                
                                                               
                                                            })
                                                    }
                                                    {
                                                    finalScore!=0 ? <td>{finalScore}</td> : null
                                                    }
                                                    </>
                                                )
                                            }
                                            else if (student.nim!=keys && keys==lastKey && 
                                                Object.keys(groupedAll).filter(key => key == student.nim)<=0){
                                                finalScore=0
                                            }
                                            
                                    })}
                                 {
                                     sortedHeader.map(sorted=>{
                                             if(finalScore==0){
                                                 return(
                                                     <td>Belum Dinilai</td>
                                                 )
                                             }
                                         })
                                     }
                                    {finalScore ==0 ? <td>{finalScore}</td> : null }     
                             </tr>
                            )
                            
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}
