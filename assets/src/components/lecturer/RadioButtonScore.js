import React from 'react';
import PropTypes from 'prop-types';
import { Form } from 'shards-react';
import { Formik } from 'formik'
import axios from 'axios'
import { Button,Popup } from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css'

class Radio extends React.Component {
    constructor(props) {
      super(props);
      Radio.propTypes = { 
        onAdd: PropTypes.func.isRequired,
        rubric: PropTypes.number,
        subClo: PropTypes.number,
        label: PropTypes.number,
        description: PropTypes.string,
        onRemove: PropTypes.func.isRequired,
        selected: PropTypes.bool,
    }
      this.state = {
         selected : this.props.selected
      };
    }
    componentDidMount(){
      const {onAdd,rubric,subClo,nilaiperRubric,portion,clo,plo} = this.props;
      if(this.state.selected){
      onAdd(rubric,subClo,nilaiperRubric,portion,clo,plo);
     } 
    }
    toggle() {
      const {onChange} = this.context.radioGroup;
      const {onAdd,onRemove,rubric,subClo,nilaiperRubric,portion,clo,plo} = this.props;
      if(this.state.selected){
        this.setState({selected:false})
       onRemove(subClo)
      }
      else{
        const selected = !this.state.selected;
        this.setState({selected});
        onAdd(rubric,subClo,nilaiperRubric,portion,clo,plo);
        onChange(selected,this);
      }
    }
    
    setSelected(selected) {
      this.setState({selected});
    }
    
    render() {
      let classname = this.state.selected ? 'active' : ''
      
        return(
          <>
            
            <Popup 
            popperModifiers={{
              preventOverflow: {
                boundariesElement: "offsetParent"
              }
            }}
            content= {this.props.description} trigger={
              <button type="button" className={classname} onClick={this.toggle.bind(this)}
              style={{margin: '5px',padding: '15px',backgroundColor : this.state.selected ? '#CCFCCD' : 'white'}}> {this.props.label}
              </button>
              } 
              />
            {/* <ReactTooltip id="registerTip" place="top" effect="solid">
             {this.props.description}
            </ReactTooltip> */}
           </>
        )
    }
  }
  
  Radio.contextTypes = {
    radioGroup: PropTypes.object
  };
  
  class RadioGroup extends React.Component {
    constructor(props) {
      super(props);
      this.options = [];
      // this.state={
      //   selectedRubric:'',
      //   selectedSubClo:''
      // }
    }
  
    getChildContext() {
      const {name} = this.props;
      return {radioGroup: {
        name,
        onChange: this.onChange.bind(this)
      }};
    }
    
    onChange(selected, child) {
      this.options.forEach(option => {
        if (option !== child) {
          option.setSelected(!selected);
        }
      });
    }
    
    render() {
      let children = React.Children.map(this.props.children, child => {
        return React.cloneElement(child, {
          ref: (component => {this.options.push(component);}) 
        });
      });
      return <div className="radio-group">{children}</div>;
    }
  }
  
  RadioGroup.childContextTypes = {
    radioGroup: PropTypes.object
  };
  

  function RadioButton({subCloTuples,scoreTuples,mode,onAdd,onSubmit,onEdit,onRemove}) {
      RadioButton.propTypes = {
        subCloTuples: PropTypes.arrayOf(PropTypes.array),
        scoreTuples: PropTypes.arrayOf(PropTypes.array),
        mode: PropTypes.string, 
        onAdd: PropTypes.func.isRequired,
        onSubmit: PropTypes.func.isRequired,
        onEdit:PropTypes.func.isRequired,
        onRemove: PropTypes.func.isRequired,
    }
    console.log(scoreTuples);
    if(scoreTuples.length==0){
      return(
        <FormNew subCloTuples={subCloTuples} scoreTuples={scoreTuples} onSubmit={onSubmit} onRemove={onRemove} onAdd={onAdd}/>
      )
    }else{
      return(
        <FormEdit subCloTuples={subCloTuples} scoreTuples={scoreTuples} onSubmit={onEdit} onRemove={onRemove} onAdd={onAdd}/>
      )
    }   
}

function FormNew({subCloTuples,scoreTuples,onAdd,onSubmit,onRemove}) {
  FormNew.propTypes = {
    subCloTuples: PropTypes.arrayOf(PropTypes.array),
    scoreTuples: PropTypes.arrayOf(PropTypes.array),
    onAdd: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired,
}
return(
  <Formik
      //initialValues={unpopulateRecord(initialRecord) || emptyPlo}
      //validationSchema={ploSchema}
      onSubmit={(values, { setSubmitting, resetForm }) => {
          onSubmit(values);
          resetForm();
          setSubmitting(false);
      }}
  >
    {({ submitForm, handleChange, values }) => (
      <Form>
      {subCloTuples.map((tuple) => ( 
          <div style={{paddingTop: '2em'}}>
              <h5>{tuple[0].cloCode}</h5>
                {tuple[0].subCloList.map((subClo)=>(
                  <div style={{paddingTop: '1em'}}>
                  <h6>{subClo.description}</h6>
                  <RadioGroup name={subClo.id}>
                  {subClo.subCloRubricList.map((rubric)=>(
                    <Radio
                      rubric={rubric.id}
                      label={rubric.rubricCode}
                      subClo={subClo.id}
                      onAdd={onAdd}
                      onRemove={onRemove}
                      description={rubric.description}
                      selected={false}
                      nilaiperRubric={rubric.score}
                      portion={subClo.portion}
                      clo={tuple[0].id}
                      plo={tuple[0].plo} 
                    />
                  ))}
                  </RadioGroup>
                  
                  </div>
                ))}
              
          </div>
      ))}
  <div style={{paddingTop: '2em'}}>
  <Button onClick={submitForm}>Submit</Button>
  </div>   
  </Form>
  )}
  </Formik>
)
}
function FormEdit({subCloTuples,scoreTuples,onAdd,onSubmit,onRemove}) {
  FormEdit.propTypes = {
    subCloTuples: PropTypes.arrayOf(PropTypes.array),
    scoreTuples: PropTypes.arrayOf(PropTypes.array),
    onAdd: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired,
}
return(
  <Formik
            //initialValues={unpopulateRecord(initialRecord) || emptyPlo}
            //validationSchema={ploSchema}
            onSubmit={(values, { setSubmitting, resetForm }) => {
                onSubmit(values);
                resetForm();
                setSubmitting(false);
            }}
        >
          {({ submitForm }) => (
            <Form>
            {subCloTuples.map((tuple) => ( 
                <div style={{paddingTop: '2em'}}>
                    <h5>{tuple[0].cloCode}</h5>
                      {tuple[0].subCloList.map((subClo)=>(
                        <div style={{paddingTop: '1em'}}>
                        <h6>{subClo.description}</h6>
                        {scoreTuples.map((score)=>{
                        if(score[0].rubric.subClo==subClo.id){
                          return(<RadioGroup name={subClo.id}>
                            {subClo.subCloRubricList.map((rubric)=>{
                              return(
                                <Radio
                                  rubric={rubric.id}
                                  label={rubric.rubricCode}
                                  subClo={subClo.id}
                                  onAdd={onAdd}
                                  onRemove={onRemove}
                                  description={rubric.description}
                                  selected={score[0].rubric.id==rubric.id}
                                  nilaiperRubric={rubric.score}
                                  portion={subClo.portion}
                                  clo={tuple[0].id}
                                  plo={tuple[0].plo}
                                />
                              )
                              })}
                            </RadioGroup>
                            )
                        }
                        
                        })}
                        </div>
                      ))}
                    
                </div>
            ))}
        <div style={{paddingTop: '2em'}}>
        <Button onClick={submitForm}>Submit</Button>
        </div>   
        </Form>
        )}
        </Formik>
)
}

export default class RadioButtonScore extends React.Component {
  constructor(props) {
    super(props);
    //this.options = [];
    this.state={
      selectedList : [],
      lecturer : props.lecturer,
      student : props.student,
      sub_clo : []
    }
    this.handleAddScore = this.handleAddScore.bind(this);
    this.removeScore = this.removeScore.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
  }

  
  handleAddScore(rubric,subClo,nilaiPerRubric,portion,clo,plo) {
    const newRubric = {
      rubric : rubric,
      subClo : subClo,
      nilaiPerRubric : nilaiPerRubric,
      portion : portion,
      clo : clo,
      plo : plo
    }

    if(this.state.selectedList.length>0){
      this.state.selectedList.forEach((selectedList,index)=>{
        if(selectedList.subClo==subClo){
          this.setState(oldState => {
            //const detailedScore= newRubric
            let selectedList = oldState.selectedList;
            // const scoreIndex = selectedList.findIndex(sc => sc.subClo == subClo);
            selectedList.splice(index, 1, newRubric);
            return {selectedList}
           //console.log(scoreIndex)
           //console.log(subClo)
        })
        }
        else{
          this.setState(({selectedList}) => {
            return {
                selectedList: [...selectedList, newRubric],
            }
          })
  
        }
      })
    }
    else{
      this.setState(({selectedList}) => {
        return {
            selectedList: [...selectedList, newRubric],
        }
      })
    }
}

  removeScore(subClo) {
    var sameClo =[]
    this.state.selectedList.forEach((selectedList,index)=>{
      if(selectedList.subClo==subClo){
        sameClo.push(index)
    }
    })
 
    
      for(var i = this.state.selectedList.length; i >= 0; i--){
        sameClo.forEach((scl)=>{
          if(i==scl){
              this.setState(oldState => {
                let selectedList = oldState.selectedList;
                selectedList.splice(i, 1);
                return {selectedList}
            })
            
          }
        })
      }
  }

  handleSubmit(){
   
   var selectedData = [];
   selectedData = this.state.selectedList.filter(function (a) {
       var key = a.subClo;
       if (!this[key]) {
           this[key] = true;
           return true;
       }
   }, Object.create(null));
  
   
   const subcloList=[]
   selectedData.forEach(selected=>{
      subcloList.push(selected.subClo) 
   })

   if(JSON.stringify(subcloList.sort()) === JSON.stringify(this.state.sub_clo.sort())){
     
    const url = this.props.url;
    
    selectedData.forEach(newScore=>{
      
        axios.post(`/lecturer/input-score`, {...newScore, lecturer: this.state.lecturer, student: this.state.student })
      .then(res => {
          window.location.href=url;
      }) 
    
    })
   }
   else{
    alert("Nilai Harus Terisi Penuh ! ");
    window.location.reload(true)
   }
  }

  handleEdit(){
  
  var selectedData = [];
   selectedData = this.state.selectedList.filter(function (a) {
       var key = a.subClo;
       if (!this[key]) {
           this[key] = true;
           return true;
       }
   }, Object.create(null));
  
   
   const subcloList=[]
   selectedData.forEach(selected=>{
     
      subcloList.push(selected.subClo)
     
   })
 

   if(JSON.stringify(subcloList.sort()) === JSON.stringify(this.state.sub_clo.sort())){
     
    const url = this.props.url;
    selectedData.forEach(newScore => {
     
        axios.post(`/lecturer/update-score`, {...newScore, lecturer: this.state.lecturer, student: this.state.student })
        .then(res => {
            window.location.href=url;
        })
      
    })
              
    }
   else{
    alert("Nilai Harus Terisi Penuh ! ");
    window.location.reload(true)
   }
  }

componentDidMount(){
    this.props.subCloTuples.map((tuple, i) => ( 
      tuple[0].subCloList.map((subClo,i)=>(
        this.setState(({sub_clo}) => {
          return {
              sub_clo: [...sub_clo, subClo.id],
          }
        })
      ))
    ))
    console.log(this.props.url)
}
  
  render() {
    var mode ='';
    if(this.props.scoreTuples==0){
      mode='NEW'
    }
    else{
      mode='EDIT'
    }
    return (
    <RadioButton
      subCloTuples={this.props.subCloTuples}
      scoreTuples ={this.props.scoreTuples}
      mode={mode}
      onAdd={this.handleAddScore}
      onSubmit={this.handleSubmit}
      onEdit={this.handleEdit}
      onRemove={this.removeScore}
    /> 
    )
  }
}
