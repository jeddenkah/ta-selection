import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import { FormSelect } from "shards-react";
import Select from 'react-select'
import _ from 'lodash'

export default function ModelSelect({name, modelName, displayAttribute, value, onChange, records: initialRecord = [], isClearable = true}) {
    ModelSelect.propTypes = {
        name: PropTypes.string,
        modelName: PropTypes.string.isRequired,
        displayAttribute: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.func
        ]),
        onChange: PropTypes.func,
        records: PropTypes.array
    }
    ModelSelect.defaultProps = {
        displayAttribute: 'id'
    }
    const [records, setRecords] = useState(initialRecord);
    useEffect(() => {
        if (!initialRecord.length) {
            fetch(`/${modelName}?limit=1000`)
                .then(res => res.json(res))
                .then(res => setRecords(res));
        }
    }, []);
    const selectOptions = records
        .map(record => {
            let optionLabel;
            if (typeof displayAttribute == 'function') optionLabel = displayAttribute(record);
            else if (typeof displayAttribute == 'string') optionLabel = record[displayAttribute];
            else optionLabel = record.id;
            return {value: record.id, label: optionLabel}
        });
    const handleChange = (selectedOption) => {
        onChange({target: {name, value: selectedOption?.value ?? null}})
    }
    const label = _.find(selectOptions, opt => opt.value == value)?.label;
    const selectedOption = value ? {value, label} : undefined;
    return (
        <Select name={name} placeholder={''} value={selectedOption} options={selectOptions} onChange={handleChange} isClearable={isClearable} />
    );
}