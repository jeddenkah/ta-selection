import React from 'react';
import PropTypes from 'prop-types';
import { Form, FormGroup, FormInput, FormSelect, FormTextarea, Button } from "shards-react";

export default class CloForm extends React.Component {
    constructor(props) {
        super(props);
        CloForm.propTypes = {
            initialRecord: PropTypes.object,
            mode: PropTypes.oneOf(['NEW', 'EDIT', '']),
            onCancel: PropTypes.func,
            hidden: PropTypes.bool
        }
        this.state = {
            clo: props.initialRecord ? {...props.initialRecord} : {
                plo: {id: ''},
                cloCode: '',
                description: '',
                
            },
            errorMessages: new Map(),
            filterPeriodId: props.initialRecord ? props.initialRecord.plo.period : '',
            periodList: [],
            ploList: []
        }
       
        this.handleChange = this.handleChange.bind(this);
        this.handlePeriodChange = this.handlePeriodChange.bind(this);
        this.handlePloChange = this.handlePloChange.bind(this);
        this.validate = this.validate.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }
    componentDidMount() {
        fetch('/period')
            .then(res => res.json())
            .then(res => this.setState({periodList: res}));
        fetch('/plo')
            .then(res => res.json())
            .then(res => this.setState({ploList: res}));
    }
    handleChange(e) {
        const attributeName = e.target.name;
        const attributeValue = e.target.value;
        this.setState(({clo}) => {
            const newClo = {...clo, [attributeName]: attributeValue}
            return {clo: newClo}
        });
    }
    handlePloChange(e) {
        const ploId = e.target.value;
        const plo = this.state.ploList.find(plo => plo.id == ploId);
        this.setState(({clo}) => {
            return {clo: {...clo, plo}}
        })
    }
    handlePeriodChange(e) {
        const periodId = parseInt(e.target.value);
        this.setState(({clo}) => {
            return {filterPeriodId: periodId, clo: {...clo, plo: ''}}
        })
    }
    validate() {
        this.setState((state, props) => {
            const errorMessages = getCloFormErrors(state);
            return {errorMessages}
        });
        const errorMessages = getCloFormErrors(state);
        return errorMessages.size ==  0;
    }
    submitNewClo() {
        const unpopulatedClo = {...this.state.clo, plo: this.state.clo.plo.id,period: this.state.filterPeriodId};
        fetch('/clo', {
            method: 'POST',
            body: JSON.stringify(unpopulatedClo)
        }).then(res => window.location.reload(true))
    }
    submitEditClo() {
        const unpopulatedClo = {...this.state.clo, plo: this.state.clo.plo.id,period: this.state.filterPeriodId};
        fetch(`/clo/${this.state.clo.id}`, {
            method: 'PATCH',
            body: JSON.stringify(unpopulatedClo)
        }).then(res => window.location.reload(true))
    }
    handleSubmit(e) {
        e.preventDefault();
        if (!this.validate) return;
        if (this.props.mode == 'NEW') {
            this.submitNewClo();
        } else if (this.props.mode == 'EDIT') {
            this.submitEditClo();
        }
    }
    render() {
        const {ploList, periodList} = this.state;
        const periodSelectValue = this.state.filterPeriodId;
        const periodSelect = (
            <FormSelect className="form-control" name="filterPeriodId" value={periodSelectValue} onChange={this.handlePeriodChange}>
                <option value=""></option>
                {periodList.map(period => <option key={period.id} value={period.id}>{period.semester+'-'+period.academicYear}</option>)}
            </FormSelect>
        );
        const filteredPloList = ploList.filter(plo => plo.period.id == this.state.filterPeriodId)
        const ploSelect = (
            <FormSelect className="form-control" name="plo" value={this.state.clo.plo.id} onChange={this.handlePloChange} disabled={!this.state.filterPeriodId}>
                <option value=""></option>
                {
                    filteredPloList.map(plo => <option key={plo.id} value={plo.id}>{plo.ploCode}</option>)
                }
            </FormSelect>
        );
        return (
            <Form>
                <FormGroup>
                    <label  htmlFor="period">
                        Period
                    </label>
                        {periodSelect}
                    <p className="help-block">{this.state.errorMessages.get('period')}</p>
                </FormGroup>
                <FormGroup>
                    <label  htmlFor="plo">
                        PLO
                    </label>
                    {ploSelect}
                    <p className="help-block">{this.state.errorMessages.get('plo')}</p>
                </FormGroup>
                <FormGroup>
                    <label  htmlFor="cloCode">
                        CLO
                    </label>
                        <FormTextarea
                            className="form-control"
                            rows={3}
                            name="cloCode"
                            value={this.state.clo.cloCode}
                            onChange={this.handleChange} />
                </FormGroup>
                <FormGroup>
                    <label  htmlFor="description">
                        Deskripsi
                    </label>
                        <FormTextarea
                            className="form-control"
                            rows={3}
                            name="description"
                            value={this.state.clo.description}
                            onChange={this.handleChange} />
                </FormGroup>
                <FormGroup>
                    <div>
                        <Button theme="primary" onClick={this.handleSubmit}>
                            Submit
                        </Button>
                    </div>
                </FormGroup>
            </Form>
        )
    }
}
