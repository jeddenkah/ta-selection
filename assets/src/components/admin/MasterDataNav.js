import React from 'react';
import { Nav, NavItem } from "shards-react";
import {Link, useRouteMatch} from 'react-router-dom';

function NavigationItem({ label, to, activeOnlyWhenExact }) {
    let match = useRouteMatch({
      path: to,
      exact: activeOnlyWhenExact
    });
  
    return (
      <NavItem>
        <Link className={match ? 'nav-link active' : 'nav-link'} to={to}>{label}</Link>
      </NavItem>
    );
  }

export default function MasterDataNav() {
  const homePath = '/admin/database-setting'
  return (
    <Nav tabs className="master-data-nav">
        <NavigationItem label="Admin" to={homePath + "/admin"} />
        <NavigationItem label="Master Role" to={homePath + "/master-role"} />
        <NavigationItem label="KK" to={homePath + "/kk"} />
        <NavigationItem label="Peminatan" to={homePath + "/peminatan"} />
        <NavigationItem label="Lab Riset" to={homePath + "/lab-riset"} />
        <NavigationItem label="Prodi" to={homePath + "/prodi"} />
        <NavigationItem label="Metlit" to={homePath + "/metlit"} />
        <NavigationItem label="JFA" to={homePath + "/jfa"} />
        <NavigationItem label="PLO" to={homePath + "/plo"} />
        <NavigationItem label="CLO" to={homePath + "/clo"} />
        <NavigationItem label="Sub CLO" to={homePath + "/sub-clo"} />
    </Nav>
  );
}