import React from 'react';
import PropTypes from 'prop-types';
import ModelSelect from '../ModelSelect';
import handleFilterChange from '../handleFilterChange';
import { Form, FormGroup, FormSelect } from 'shards-react';

export default function StudentStatusFilter() {
    const searchParams = new URLSearchParams(window.location.search);
    const periodId = searchParams.get('periodId') || '';
    const optionNum = searchParams.get('optionNum') || '';
    return (
        <Form>
            <FormGroup>
                <label htmlFor="optionNum">Opsi</label>
                <FormSelect
                    name="optionNum"
                    value={optionNum}
                    onChange={handleFilterChange}
                >
                    <option value=""></option>
                    <option value={1}>1</option>
                    <option value={2}>2</option>
                </FormSelect>
            </FormGroup>
            <FormGroup>
                <label htmlFor="periodId">Periode</label>
                <ModelSelect
                    name="periodId"
                    modelName="period"
                    value={periodId}
                    onChange={handleFilterChange}
                    displayAttribute={period => `${period.semester}/${period.academicYear}`}
                />
            </FormGroup>
        </Form>
    )
}