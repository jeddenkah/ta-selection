import React from 'react';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import { Formik, Field, ErrorMessage } from 'formik';
import { FormInput, Button, ButtonGroup } from "shards-react";

const emptyRubric = {
    subClo: '',
    rubricCode: '',
    score: '',
    description: ''
}

const RubricSchema = Yup.object().shape({
    subClo: Yup.mixed(),
    rubricCode: Yup.number()
        .required('Wajib diisi'),
    score: Yup.number()
        .min(0, 'Minimal 0')
        .max(100, 'Maksimal 100')
        .required('Wajib diisi'),
    description: Yup.string()
        .required('Wajib diisi')
});

const InputField = ({field, ...props}) => (<FormInput {...field} {...props} />);
const HelpBlock = ({children}) => (<p className="help-block">{children}</p>)

export default function RubricForm({onSubmit, initialValues, mode, onCancel, hidden}) {
    RubricForm.propTypes = {
        onSubmit: PropTypes.func.isRequired,
        initialValues: PropTypes.object,
        mode: PropTypes.oneOf(['NEW', 'EDIT']),
        onCancel: PropTypes.func,
        hidden: PropTypes.bool
    }
    return (
            <Formik
                initialValues={initialValues || emptyRubric}
                validationSchema={RubricSchema}
                onSubmit={(values, { setSubmitting, resetForm }) => {
                    onSubmit(values);
                    resetForm();
                    setSubmitting(false);
                }}
            >
                {({ submitForm }) => (
                    <tr hidden={hidden}>
                        <td>
                            <Field type="number" name="rubricCode" component={InputField}/>
                            <ErrorMessage name="rubricCode" component={HelpBlock} />
                        </td>
                        <td>
                            <Field type="number" name="score" component={InputField}/>
                            <ErrorMessage name="score" component={HelpBlock} />
                        </td>
                        <td>
                            <Field type="text" name="description" component={InputField}/>
                            <ErrorMessage name="description" component={HelpBlock} />
                        </td>
                        <td>
                        {   mode == 'EDIT' ? <EditingButtons onSubmit={submitForm} onCancel={onCancel}/> : <Button onClick={submitForm}>Tambah</Button>}
                        </td>
                    </tr>
                )}
            </Formik>
    )
}

function EditingButtons({onSubmit, onCancel}) {
    EditingButtons.propTypes = {
        onSubmit: PropTypes.func.isRequired,
        onCancel: PropTypes.func.isRequired
    }
    return(
        <ButtonGroup>
            <Button onClick={onSubmit}>Simpan</Button>
            <Button onClick={onCancel}>Batal</Button>
        </ButtonGroup>
    )
}