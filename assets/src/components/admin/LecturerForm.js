import React, {useState, useEffect} from 'react'
import PropTypes from 'prop-types'
import { Formik, Field, ErrorMessage } from 'formik'
import { FormInput, FormGroup, Button, ButtonGroup, Form } from "shards-react"
import LecturerSchema from '../../schemas/Lecturer'
import ModelSelect from '../../components/ModelSelect'
import unpopulateRecord from '../../utils/unpopulateRecord'
import _ from 'lodash'
import axios from 'axios'
import Select from 'react-select'

const InputField = ({field, ...props}) => (<FormInput {...field} {...props} />);
const HelpBlock = ({children}) => (<p className="help-block">{children}</p>);

const emptyLecturer = {
    nik: '',
    name: '',
    email: '',
    jfa: '',
    labRiset: '',
    prodi: '',
    lecturerCode: '',
    roles: []
}

export default function LecturerForm({initialRecord, mode}) {
    LecturerForm.propTypes = {
        initialRecord: PropTypes.object,
        mode: PropTypes.oneOf(['NEW', 'EDIT', '']),
        onCancel: PropTypes.func,
        hidden: PropTypes.bool
    }
    const handleSubmit = (data) => {
        if (mode == 'NEW') {
            axios.post(`/admin/new-lecturer`, data)
                .then(res => {
                    location.reload();
                })
        } else if (mode == 'EDIT') {
            axios.post(`/admin/edit-lecturer`, data)
                .then(res => {
                    location.reload();
                })
        }
    }
    return (
        <Formik
            initialValues={unpopulateRecord(initialRecord) || emptyLecturer}
            validationSchema={LecturerSchema}
            onSubmit={(values, { setSubmitting, resetForm }) => {
                handleSubmit(values);
                resetForm();
                setSubmitting(false);
            }}
        >
            {({ submitForm, handleChange, handleBlur, values }) => (
                <Form>
                    <FormGroup>
                        <label htmlFor="nik">NIK</label>
                        <Field type="text" name="nik" component={InputField}/>
                        <ErrorMessage name="nik" component={HelpBlock} />
                    </FormGroup>
                    <FormGroup>
                        <label htmlFor="name">Nama</label>
                        <Field type="text" name="name" component={InputField}/>
                        <ErrorMessage name="name" component={HelpBlock} />
                    </FormGroup>
                    <FormGroup>
                        <label htmlFor="email">Email</label>
                        <Field type="email" name="email" component={InputField}/>
                        <ErrorMessage name="email" component={HelpBlock} />
                    </FormGroup>
                    <FormGroup>
                        <label htmlFor="jfa">JFA</label>
                        <ModelSelect
                            name="jfa"
                            value={values.jfa}
                            modelName="jfa"
                            displayAttribute="abbrev"
                            onChange={handleChange}
                        />
                        <ErrorMessage name="jfa" component={HelpBlock} />
                    </FormGroup>
                    <FormGroup>
                        <label htmlFor="labRiset">Lab Riset</label>
                        <ModelSelect
                            name="labRiset"
                            value={values.labRiset}
                            modelName="labriset"
                            displayAttribute="abbrev"
                            onChange={handleChange}
                            onBlur={handleBlur}
                        />
                        <ErrorMessage name="labRiset" component={HelpBlock} />
                    </FormGroup>
                    <FormGroup>
                        <label htmlFor="prodi">Prodi</label>
                        <ModelSelect
                            name="prodi"
                            value={values.prodi}
                            modelName="prodi"
                            displayAttribute="abbrev"
                            onChange={handleChange}
                        />
                        <ErrorMessage name="prodi" component={HelpBlock} />
                    </FormGroup>
                    <FormGroup>
                        <label htmlFor="lecturerCode">lecturerCode</label>
                        <Field type="text" name="lecturerCode" component={InputField}/>
                        <ErrorMessage name="lecturerCode" component={HelpBlock} />
                    </FormGroup>
                    <FormGroup>
                        <label htmlFor="roles">Role</label>
                        <RoleSelect
                            name="roles"
                            selectedRoleIds={values.roles}
                            onChange={handleChange}
                        />
                        <ErrorMessage name="lecturerCode" component={HelpBlock} />
                    </FormGroup>
                    <Button onClick={submitForm}>Submit</Button>
                </Form>
            )}
        </Formik>
)
}

function RoleSelect({name, selectedRoleIds, onChange}) {
    const [roles, setRoles] = useState([]);
    useEffect(() => {
        axios.get('/masterrole')
            .then(res => {
                setRoles(res.data)
            });
    }, []);
    const valueLabelPairs = roles.map(r => ({value: r.id, label: r.name}));
    const handleChange = (selectedOptions) => {
        console.log(selectedOptions)
        const roleIds = selectedOptions.map(opt => opt.value);
        console.log({target: {name, value: roleIds}})
        onChange({target: {name, value: roleIds}})
    }
    const currentValues = selectedRoleIds.filter(r => r).map(roleId => {
        const label =  _.find(valueLabelPairs, opt => opt.value == roleId)?.label;
        return ({value: roleId, label})
    });
    return (
        <Select
            name={name}
            value={currentValues}
            options={valueLabelPairs}
            onChange={handleChange}
            isMulti
            isClearable
        />
    )
}