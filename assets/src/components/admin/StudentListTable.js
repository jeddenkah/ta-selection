import PropTypes from 'prop-types'
import React, { Component } from 'react'
//import ReactDataGrid from 'react-data-grid'
import ModelTable from '../ModelTable'
var studentModel = require('../../../../api/models/Student')
import '../../../dependencies/datatables/DataTables-1.10.20/js/jquery.dataTables'
import '../../../dependencies/datatables/DataTables-1.10.20/css/jquery.dataTables.min.css'

export default class StudentListTable extends Component {
    constructor(props) {
        super(props);
        $(document).ready(() => {
            $('#student-table').DataTable()
        })
    }

    componentDidUpdate() {
    }

    render() {
        const students = this.props.students;
        return (
                <ModelTable
                    id="student-table"
                    model={studentModel}
                    modelName="student"
                    records={students}
                    renderColumn={{
                        peminatan: data => data.peminatan?.abbrev,
                        metlit: data => data.metlit?.class,
                    }}
                    hideColumns={
                        [
                            'hashedPassword',
                            'newPasswordToken',
                            'newPasswordTokenExpiresAt',
                            'groups',
                            'ta',
                            'eprt',
                            'form'
                        ]}
                    referencedModelNames={['peminatan', 'metlit']}
                />
        )
    }
}

StudentListTable.propTypes = {
    students: PropTypes.arrayOf(PropTypes.object)
}
