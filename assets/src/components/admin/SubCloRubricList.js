import React from 'react';
import PropTypes from 'prop-types';

export default function SubCloRubricList ({subCloId}) {
    SubCloRubricList.propTypes = {
        subCloId: PropTypes.number.isRequired
    }
    const [subCloRubrics, setSubCloRubrics] = React.useState([]);
    React.useEffect(() => {
        fetch(`/subclorubric?where={"subClo":${subCloId}, "isDeleted": false}`)
            .then(res => res.json())
            .then(res => setSubCloRubrics(res));
    }, [])
    return (
        <ul>
            {subCloRubrics.map(rubric => <li key={rubric.id}>{rubric.rubricCode}: {rubric.description}</li>)}
        </ul>
    )
}