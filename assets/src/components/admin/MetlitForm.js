import React from 'react';
import PropTypes from 'prop-types';
import { Form, FormGroup, FormSelect, FormTextarea, Button } from "shards-react";

export default class metlitForm extends React.Component {
    constructor(props) {
        super(props);
        metlitForm.propTypes = {
            initialRecord: PropTypes.object,
            mode: PropTypes.oneOf(['NEW', 'EDIT', '']),
            onCancel: PropTypes.func,
            hidden: PropTypes.bool
        }
        this.state = {
            metlit: props.initialRecord ? {...props.initialRecord} : {
                class:'',
                period: {id: ''},
                lecturer: {id: ''}
                
            },
            errorMessages: new Map(),
            filterProdiId: props.initialRecord ? props.initialRecord.lecturer.prodi : '',
            //filterPeriodId: props.initialRecord ? props.initialRecord.period : '',
            lecturerList: [],
            prodiList: [],
            periodList:[]
        }
       
        this.handleChange = this.handleChange.bind(this);
        this.handlePeriodChange = this.handlePeriodChange.bind(this);
        this.handleProdiChange = this.handleProdiChange.bind(this);
        this.handleLecturerChange = this.handleLecturerChange.bind(this);
        this.validate = this.validate.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }
    componentDidMount() {
        fetch('/period')
            .then(res => res.json())
            .then(res => this.setState({periodList: res}));
        fetch('/prodi')
            .then(res => res.json())
            .then(res => this.setState({prodiList: res}));
        fetch('/lecturer?limit=1000')
            .then(res => res.json())
            .then(res => this.setState({lecturerList: res}));
           
    }
    handleChange(e) {
        const attributeName = e.target.name;
        const attributeValue = e.target.value;
        this.setState(({metlit}) => {
            const newMetlit = {...metlit, [attributeName]: attributeValue}
            return {metlit: newMetlit}
        });
    }
    handleLecturerChange(e) {
        const lecturerId = e.target.value;
        const lecturer = this.state.lecturerList.find(lecturer => lecturer.id == lecturerId);
        this.setState(({metlit}) => {
            return {metlit: {...metlit, lecturer}}
        })
    }
    handleProdiChange(e) {
        const prodiId = parseInt(e.target.value);
        this.setState(({metlit}) => {
            return {filterProdiId: prodiId, metlit: {...metlit, lecturer: ''}}
        })
    }
    handlePeriodChange(e) {
        const periodId = e.target.value;
        const period = this.state.periodList.find(period => period.id == periodId);
        this.setState(({metlit}) => {
            return {metlit: {...metlit, period}}
        })
    }
    validate() {
        this.setState((state, props) => {
            const errorMessages = getMetlitFormErrors(state);
            return {errorMessages}
        });
        const errorMessages = getMetlitFormErrors(state);
        return errorMessages.size ==  0;
    }
    submitNewMetlit() {
        const unpopulatedMetlit = {...this.state.metlit, period: this.state.metlit.period.id,prodi:this.state.filterProdiId,lecturer: this.state.metlit.lecturer.id};
        fetch('/metlit', {
            method: 'POST',
            body: JSON.stringify(unpopulatedMetlit)
        }).then(res => window.location.reload(true))
    }
    submitEditMetlit() {
        const unpopulatedMetlit = {...this.state.metlit, period: this.state.metlit.period.id,prodi:this.state.filterProdiId,lecturer: this.state.metlit.lecturer.id};
        fetch(`/metlit/${this.state.metlit.id}`, {
            method: 'PATCH',
            body: JSON.stringify(unpopulatedMetlit)
        }).then(res => window.location.reload(true))
    }
    handleSubmit(e) {
        e.preventDefault();
        if (!this.validate) return;
        if (this.props.mode == 'NEW') {
            this.submitNewMetlit();
        } else if (this.props.mode == 'EDIT') {
            this.submitEditMetlit();
        }
    }
    render() {
        const {lecturerList, prodiList, periodList} = this.state;
        const prodiSelectValue = this.state.filterProdiId;
        //const periodSelectValue = this.state.filterPeriodId;
        const periodSelect = (
            <FormSelect className="form-control" name="period" value={this.state.metlit.period.id} onChange={this.handlePeriodChange}>
                <option value=""></option>
                {periodList.map(period => <option key={period.id} value={period.id}>{period.semester+'-'+period.academicYear}</option>)}
            </FormSelect>
        );
        //const filteredProdiList = prodiList.filter(prodi => prodi.id == this.state.filterPeriodId)
        const prodiSelect = (
            <FormSelect className="form-control" name="filterProdiId" value={prodiSelectValue} onChange={this.handleProdiChange} disabled={!this.state.metlit.period.id}>
                <option value=""></option>
                {prodiList.map(prodi => <option key={prodi.id} value={prodi.id}>{prodi.name}</option>)}
            </FormSelect>
        );
        const filteredLecturerList = lecturerList.filter(lecturer => lecturer.prodi.id == this.state.filterProdiId)
        const lecturerSelect = (
            <FormSelect className="form-control" name="lecturer" value={this.state.metlit.lecturer.id} onChange={this.handleLecturerChange} disabled={!this.state.filterProdiId}>
                <option value=""></option>
                {
                    filteredLecturerList.map(lecturer => <option key={lecturer.id} value={lecturer.id}>{lecturer.name}</option>)
                }
            </FormSelect>
        );
        return (
            <Form>
                 <FormGroup>
                    <label  htmlFor="period">
                        Period
                    </label>
                        {periodSelect}
                    <p className="help-block">{this.state.errorMessages.get('period')}</p>
                </FormGroup>
                <FormGroup>
                    <label  htmlFor="prodi">
                        Prodi
                    </label>
                        {prodiSelect}
                    <p className="help-block">{this.state.errorMessages.get('prodi')}</p>
                </FormGroup>
                <FormGroup>
                    <label  htmlFor="lecturer">
                        Lecturer
                    </label>
                    {lecturerSelect}
                    <p className="help-block">{this.state.errorMessages.get('lecturer')}</p>
                </FormGroup>
                <FormGroup>
                    <label  htmlFor="class">
                        Class
                    </label>
                        <FormTextarea
                            className="form-control"
                            rows={3}
                            name="class"
                            value={this.state.metlit.class}
                            onChange={this.handleChange} />
                </FormGroup>
                <FormGroup>
                    <div>
                        <Button theme="primary" onClick={this.handleSubmit}>
                            Submit
                        </Button>
                    </div>
                </FormGroup>
            </Form>
        )
    }
}
