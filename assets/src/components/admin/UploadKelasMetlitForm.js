import React from 'react'
import PropTypes from 'prop-types'
import { Form, FormGroup, FormInput, FormCheckbox, Button, ListGroup, ListGroupItem, ListGroupItemText } from "shards-react";
const Metlit = require('../../../../api/models/Metlit');


const METLIT_COLUMN_CONFIG = [
    ['class', 'string'],
    ['period', 'foreign key'],
    ['prodi', 'foreign key']
];


function UploadResults({results}) {
    UploadResults.propTypes = {
        results: PropTypes.arrayOf(PropTypes.string)
    }
    let successCount = 0, duplicateCount=0, errorMessages = [];
    results.forEach((result, i) => {
        if (result == 'success') successCount++;
        else if (result == 'duplicate') {
            duplicateCount++;
            errorMessages.push(`Di baris ke [${i+2}]: nim/nik sudah terdaftar`);
        }
        else errorMessages.push(`Di baris ke [${i+2}]: ${result}`); //tambah 2 karena mulai dari 0, sama baris header ga diitung
    });
    return (
        <ListGroup className="mt-4">
            <ListGroupItem>Sukses: {successCount} baris.</ListGroupItem>
            {duplicateCount ? <ListGroupItem>Duplikasi: {duplicateCount} baris</ListGroupItem> : ''}
            <br/>
            <ListGroupItemText>
                Daftar error: (Header Tabel dianggap baris ke-1)
                <ul>
                    {errorMessages.map(err => <li>{err}</li>)}
                </ul>
            </ListGroupItemText>
        </ListGroup>
    )
}

class UserCsvForm extends React.Component {
    constructor(props) {
        UserCsvForm.propTypes = {
            userType: PropTypes.string
        }
        super(props);
        this.state = {
            file: '',
            errors: [],
            uploadResults: [],
            shouldUpdateUser: false,
            isSubmitting: false
        }
        this.handleFileChange = this.handleFileChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleToggleUpdateUser = this.handleToggleUpdateUser.bind(this);

    }

    handleToggleUpdateUser(e) {
        this.setState(state => ({shouldUpdateUser: !state.shouldUpdateUser}))
    }


    handleFileChange(e) {
        this.setState({file: e.target.files[0]});
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({recordsCount: null, invalidCount: null, duplicateCount: null, isSubmitting: true});
        const formData = new FormData();
        //formData.append('userType', this.props.userType);
        formData.append('shouldUpdateUser', this.state.shouldUpdateUser);
        formData.append('metlit-table', this.state.file);
        fetch('/admin/input-csv-metlit', {
            method: 'POST',
            body: formData
        }).then(res => {
            const exitCode = res.headers.get('X-Exit');
            switch (exitCode) {
                case "success":
                    res.json().then(({results}) => {
                        this.setState({uploadResults: results, isSubmitting: false});
                    });
                    break;
                default:
                    break;
            }
        })
        .catch(err => console.error(err + 'disini'))
    }

    render() {
        const isFieldEmpty = !(this.state.file);
        return (
            <Form action="/admin/input-csv-metlit" encType="multipart/form-data" method="post" onSubmit={this.handleSubmit}>
                <FormGroup className="form-group">
                    <label htmlFor="file">Tabel CSV</label>
                    <FormInput type="file" name="file" accept=".csv" onChange={this.handleFileChange}/>
                </FormGroup>
                <FormGroup>
                <FormCheckbox
                    name="toggleUpdateUser"
                    toggle
                    checked={this.state.shouldUpdateUser}
                    onChange={this.handleToggleUpdateUser}
                >
                    Update user yang sudah terdaftar
                </FormCheckbox>

                </FormGroup>
                <Button type="submit" className="btn btn-primary" disabled={this.state.isSubmitting || isFieldEmpty}>Upload</Button>
                {this.state.isSubmitting && <p className="help-block">Loading...</p>}
                {this.state.uploadResults.length ? <UploadResults results={this.state.uploadResults} /> : ''}
            </Form>
        )
    }
}

export default class App extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
       
            let Model = Metlit;
            let referencedModelNames = ['period', 'prodi'];
       
        return (
            <>
                    
                        <ColumnList columnConfig={METLIT_COLUMN_CONFIG} />
                       
                    <UserCsvForm />
                </>
        )
    }
}



function ColumnList({columnConfig}) {
    ColumnList.propTypes = {
        columnConfig: PropTypes.arrayOf(PropTypes.array).isRequired // [ [columnName, type], ...]
    }
    return (
        <table className="table table-sm">
            <thead>
                <tr>
                    <th>Header Kolom</th>
                    <th>Tipe Data</th>
                </tr>
            </thead>
            <tbody>
                {columnConfig.map(([columnName, columnType]) => (<tr><td>{columnName}</td><td>{columnType}</td></tr>) )}
            </tbody>
        </table>
    )
}

