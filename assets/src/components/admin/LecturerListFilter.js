import React from 'react';
import PropTypes from 'prop-types';
import ModelSelect from '../ModelSelect';
import handleFilterChange from '../handleFilterChange';
import { Form, FormGroup, } from 'shards-react';

export default class LecturerListFilter extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(e) {
        handleFilterChange(e);
    }
    render() {
        const searchParams = new URLSearchParams(window.location.search);
        const labRisetId = searchParams.get('labRisetId') || '';
        const jfaId = searchParams.get('jfaId') || '';
        return (
            <Form>
                <FormGroup>
                    <label htmlFor="labRisetId">Lab Riset</label>
                    <ModelSelect
                        name="labRisetId"
                        modelName="labriset"
                        value={labRisetId}
                        displayAttribute="abbrev"
                        onChange={this.handleChange}
                    />
                </FormGroup>
                <FormGroup >
                    <label htmlFor="jfaId">Jfa</label>
                    <ModelSelect
                        name="jfaId"
                        modelName="jfa"
                        value={jfaId}
                        displayAttribute="abbrev"
                        onChange={this.handleChange}
                    />
                </FormGroup>
            </Form>
        )
    }
}
