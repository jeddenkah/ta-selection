import React from 'react';
import PropTypes from 'prop-types';
import ModelSelect from '../ModelSelect';
import handleFilterChange from '../handleFilterChange';
import { Form, FormGroup, } from 'shards-react';

export default class LecturerListFilter extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(e) {
        handleFilterChange(e);
    }
    render() {
        const searchParams = new URLSearchParams(window.location.search);
        const peminatanId = searchParams.get('peminatanId') || '';
        return (
            <Form>
                <FormGroup>
                    <label htmlFor="peminatanId">Peminatan</label>
                    <ModelSelect
                        name="peminatanId"
                        modelName="peminatan"
                        value={peminatanId}
                        displayAttribute="abbrev"
                        onChange={this.handleChange}
                    />
                </FormGroup>
            </Form>
        )
    }
}
