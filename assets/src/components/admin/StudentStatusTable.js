import React from 'react'
import Datatable from '../Datatable'

export default function StudentStatusTable({records}) {
    const datatableConfig = {
        data: records,
        columns: [
            {
                data: 'student',
                render: 'nim'
            },
            {
                data: 'student',
                render: 'name'
            },
            {
                data: 'group'
            },
            {
                data: 'optionNum'
            },
            {
                data: 'status'
            },
            {
                data: 'period',
                render: (data, type) => `${data?.semester}/${data?.academicYear}`
            }
        ]
    }
    const htmlId = "student-status-table";
    return(

        <Datatable id={htmlId} options={datatableConfig}> 
            <table id={htmlId}>
                <thead>
                    <tr>
                        <th>NIM</th>
                        <th>Nama</th>
                        <th>ID Group</th>
                        <th>Opsi</th>
                        <th>Status</th>
                        <th>Periode</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </Datatable>

    )
}