import React from 'react';
import PropTypes from 'prop-types';
import RubricForm from './RubricForm';
import { Button, ButtonGroup } from 'shards-react';

export default class EditableRubricRow extends React.Component {
    constructor(props) {
        super(props);
        EditableRubricRow.propTypes = {
            rubric: PropTypes.object,
            onEdit: PropTypes.func.isRequired,
            onDelete: PropTypes.func.isRequired
        }
        this.state = {
            isEditing: false,
            rubric: props.rubric
        }
        this.handleEditButtonClick = this.handleEditButtonClick.bind(this);
        this.handleSaveRubric = this.handleSaveRubric.bind(this);
        this.handleDeleteButtonClick = this.handleDeleteButtonClick.bind(this);
        this.handleCancelButtonClick = this.handleCancelButtonClick.bind(this);
    }
    handleEditButtonClick() {
        this.setState({ isEditing: true });
    }
    handleSaveRubric(rubric) {
        this.setState({ rubric, isEditing: false });
        this.props.onEdit(rubric);
    }
    handleDeleteButtonClick() {
        if (this.state.isEditing) {
            this.setState((state, props) => {
                return { isEditing: false, rubric: props.rubric }
            })
        }
        this.props.onDelete(this.state.rubric.id);
    }
    handleCancelButtonClick() {
        this.setState({isEditing: false});
    }
    render() {
        const rubric = this.state.rubric;
        const editButton = <Button theme="success" onClick={this.handleEditButtonClick}>Ganti</Button>;
        const deleteButton = <Button theme="danger" onClick={this.handleDeleteButtonClick}>x</Button>;
        const rubricForm = (
        <RubricForm
            mode="EDIT"
            onSubmit={this.handleSaveRubric}
            initialValues={this.state.rubric}
            onCancel={this.handleCancelButtonClick}
        />);
        return (
            <>
                {
                    this.state.isEditing && rubricForm
                }
                <tr hidden={this.state.isEditing}>
                    <td>{rubric.rubricCode}</td>
                    <td>{rubric.score}</td>
                    <td>{rubric.description}</td>
                    <td>
                        <ButtonGroup>
                            {editButton}{deleteButton}
                        </ButtonGroup>
                    </td>
                </tr>
            </>
        );
    }
}