import PropTypes from 'prop-types'
import React, { Component } from 'react'
//import ReactDataGrid from 'react-data-grid'
import ModelTable from '../ModelTable'
var lecturerModel = require('../../../../api/models/Lecturer')
import '../../../dependencies/datatables/DataTables-1.10.20/js/jquery.dataTables'
import '../../../dependencies/datatables/DataTables-1.10.20/css/jquery.dataTables.min.css'
import LecturerForm from './LecturerForm'

export default class LecturerTable extends Component {
    constructor(props) {
        super(props);
        $(document).ready(() => {
            $('#lecturer-table').DataTable()
        })
    }

    componentDidUpdate() {
    }

    render() {
        const lecturers = window.SAILS_LOCALS.lecturers
        return (
                <ModelTable
                    id="lecturer-table"
                    model={lecturerModel}
                    modelName="lecturer"
                    records={lecturers}
                    customForm={LecturerForm}
                    hideColumns={
                        [
                            'hashedPassword',
                            'newPasswordToken',
                            'newPasswordTokenExpiresAt',
                            'topics'
                        ]
                    }
                    renderColumn={{
                        labRiset: data => data.labRiset?.abbrev ?? '-', 
                        jfa: data => data.jfa?.abbrev ?? '-',
                        prodi: data => data.prodi?.abbrev ?? '-',
                        roles: data => {
                            return (
                                <ul>
                                    {data.roles?.map(role => <li key={role.id}>{role.name}</li>)}
                                </ul>
                            )
                        }
                    }}
                    referencedModelNames={['labriset:labRiset', 'jfa', 'prodi']}
                />
        )
    }
}

LecturerTable.propTypes = {
    lecturer: PropTypes.arrayOf(PropTypes.object)
}
