import React from 'react';
import PropTypes from 'prop-types';
import RubricForm from './RubricForm';
import EditableRubricRow from './EditableRubricRow';
import { Form, FormGroup, FormInput, FormSelect, FormTextarea, Button } from "shards-react";

export default class SubCloForm extends React.Component {
    constructor(props) {
        super(props);
        SubCloForm.propTypes = {
            mode: PropTypes.oneOf(['NEW', 'EDIT', '']),
            initialRecord: PropTypes.object
        }
        this.state = {
            subClo: props.initialRecord ? {...props.initialRecord} : {
                clo: {id: ''},
                description: '',
                portion: ''
            }, //copy obj
            errorMessages: new Map(),
            deletedRubricIds: [],
            rubrics: [],
            filterPloId: props.initialRecord ? props.initialRecord.clo.plo : '',
            filterPeriodId: props.initialRecord ? props.initialRecord.clo.period : '',
            cloList: [],
            ploList: [],
            periodList:[]
        }
        this.handleAddRubric = this.handleAddRubric.bind(this);
        this.handleEditRubric = this.handleEditRubric.bind(this);
        this.handleDeleteRubric = this.handleDeleteRubric.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handlePloChange = this.handlePloChange.bind(this);
        this.handlePeriodChange = this.handlePeriodChange.bind(this);
        this.handleCloChange = this.handleCloChange.bind(this);
        this.validate = this.validate.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }
    componentDidMount() {
        fetch('/plo')
            .then(res => res.json())
            .then(res => this.setState({ploList: res}));
        fetch('/clo')
            .then(res => res.json())
            .then(res => this.setState({cloList: res}));
        fetch('/period')
            .then(res => res.json())
            .then(res => this.setState({periodList: res}));
        if (this.props.mode == 'EDIT') {
            fetch(`/subclorubric?where={"subClo":${this.state.subClo.id}, "isDeleted": false}`)
                .then(res => res.json())
                .then(res => this.setState({rubrics: res}));
        }

    }
    handleAddRubric(rubric) {
        this.setState(({rubrics, subClo}) => {
            const newRubric = {
                ...rubric,
                subClo: subClo.id,
                status: 'NEW'
            }
            return {
                rubrics: [...rubrics, newRubric],
            }
        })
    }
    handleEditRubric(rubric) {
        this.setState(oldState => {
            const detailedRubric = {...rubric, subClo: oldState.subClo.id, status: 'EDIT'}
            let rubrics = oldState.rubrics;
            const rubricIndex = rubrics.findIndex(rb => rb.id == rubric.id);
            rubrics.splice(rubricIndex, 1, detailedRubric);
            return {rubrics}
        })
    }
    handleDeleteRubric(rubricId) {
        this.setState(oldState => {
            let {rubrics, deletedRubricIds} = oldState;
            const rubricIndex = rubrics.findIndex(rb => rb.id == rubricId);
            rubrics.splice(rubricIndex, 1);
            if ((rubricId+"").substring(0, 3) != 'new') {
                deletedRubricIds.push(rubricId);
            }
            return {rubrics, deletedRubricIds}
            
        })
    }
    handleChange(e) {
        const attributeName = e.target.name;
        const attributeValue = e.target.value;
        this.setState(({subClo}) => {
            const newSubClo = {...subClo, [attributeName]: attributeValue}
            return {subClo: newSubClo}
        });
    }
    handleCloChange(e) {
        const cloId = e.target.value;
        const clo = this.state.cloList.find(clo => clo.id == cloId);
        this.setState(({subClo}) => {
            return {subClo: {...subClo, clo}}
        })
    }
    handlePloChange(e) {
        const ploId = parseInt(e.target.value);
        this.setState(({subClo}) => {
            return {filterPloId: ploId, subClo: {...subClo, clo: ''}}
        })
    }
    handlePeriodChange(e) {
        const periodId = parseInt(e.target.value);
        this.setState(({subClo}) => {
            return {filterPeriodId: periodId, subClo: {...subClo, plo: ''}}
        })
    }
    validate() {
        this.setState((state, props) => {
            const errorMessages = getSubCloFormErrors(state);
            return {errorMessages}
        });
        const errorMessages = getSubCloFormErrors(state);
        return errorMessages.size ==  0;
    }
    submitNewSubClo() {
        const unpopulatedSubClo = {...this.state.subClo, clo: this.state.subClo.clo.id,period:this.state.filterPeriodId};
        fetch('/subclo', {
            method: 'POST',
            body: JSON.stringify(unpopulatedSubClo)
        }).then(res => res.json())
        .then(newSubClo => {
            const submitNewRubrics = this.state.rubrics.map(newRubric => {
                return fetch(`/subclorubric`, {
                    method: 'POST',
                    body: JSON.stringify({...newRubric, subClo: newSubClo.id})
                });
            });
            Promise.all(submitNewRubrics).then(res => window.location.reload(true));
        })
    }
    submitEditSubClo() {
        const unpopulatedSubClo = {...this.state.subClo, clo: this.state.subClo.clo.id,period:this.state.filterPeriodId};
        fetch(`/subclo/${this.state.subClo.id}`, {
            method: 'PATCH',
            body: JSON.stringify(unpopulatedSubClo)
        }).then(res => {
            if (res.ok) {
                const rubrics = this.state.rubrics;
                const newRubrics = rubrics.filter(rubric => rubric.status == 'NEW');
                const submitNewRubrics = newRubrics.map(newRubric => {
                    return fetch(`/subclorubric`, {
                        method: 'POST',
                        body: JSON.stringify(newRubric)
                    });
                });
                const editedRubrics = rubrics.filter(rubric => rubric.status == 'EDIT');
                const submitEditedRubrics = editedRubrics.map(editedRubric => {
                    return fetch(`/subclorubric/${editedRubric.id}`,{ 
                        method: 'PATCH',
                        body: JSON.stringify(editedRubric)
                    });
                })
                const submitDeletedRubrics = this.state.deletedRubricIds.map(rubricId => {
                    return fetch(`/subclorubric/${rubricId}`,{ 
                        method: 'POST',
                        body: JSON.stringify({isDeleted: true})
                    });
                });

                Promise.all([...submitNewRubrics, ...submitEditedRubrics, ...submitDeletedRubrics])
                .then(resArray => window.location.reload(true));
            }
        })
    }
    handleSubmit(e) {
        e.preventDefault();
        if (!this.validate) return;
        if (this.props.mode == 'NEW') {
            this.submitNewSubClo();
        } else if (this.props.mode == 'EDIT') {
            this.submitEditSubClo();
        }
    }
    render() {
        const {cloList, ploList, periodList} = this.state;
        const ploSelectValue = this.state.filterPloId;
        const periodSelectValue = this.state.filterPeriodId;
        const periodSelect = (
            <FormSelect className="form-control" name="filterPeriodId" value={periodSelectValue} onChange={this.handlePeriodChange}>
                <option value=""></option>
                {periodList.map(period => <option key={period.id} value={period.id}>{period.semester+'-'+period.academicYear}</option>)}
            </FormSelect>
        );
        const filteredPloList = ploList.filter(plo => plo.period.id == this.state.filterPeriodId)
        const ploSelect = (
            <FormSelect className="form-control" name="filterPloId" value={ploSelectValue} onChange={this.handlePloChange} disabled={!this.state.filterPeriodId}>
                <option value=""></option>
                {filteredPloList.map(plo => <option key={plo.id} value={plo.id}>{plo.ploCode}</option>)}
            </FormSelect>
        );
        const filteredCloList = cloList.filter(clo => clo.plo.id == this.state.filterPloId)
        const cloSelect = (
            <FormSelect className="form-control" name="clo" value={this.state.subClo.clo.id} onChange={this.handleCloChange} disabled={!this.state.filterPloId}>
                <option value=""></option>
                {
                    filteredCloList.map(clo => <option key={clo.id} value={clo.id}>{clo.cloCode}</option>)
                }
            </FormSelect>
        );
        return (
            <Form>
                 <FormGroup>
                    <label  htmlFor="period">
                        Period
                    </label>
                        {periodSelect}
                    <p className="help-block">{this.state.errorMessages.get('period')}</p>
                </FormGroup>
                <FormGroup>
                    <label  htmlFor="plo">
                        PLO
                    </label>
                        {ploSelect}
                    <p className="help-block">{this.state.errorMessages.get('plo')}</p>
                </FormGroup>
                <FormGroup>
                    <label  htmlFor="clo">
                        CLO
                    </label>
                    {cloSelect}
                    <p className="help-block">{this.state.errorMessages.get('clo')}</p>
                </FormGroup>
                <FormGroup>
                    <label  htmlFor="description">
                        Sub CLO
                    </label>
                        <FormTextarea
                            className="form-control"
                            rows={3}
                            name="description"
                            value={this.state.subClo.description}
                            onChange={this.handleChange} />
                </FormGroup>
                <FormGroup>
                    <label  htmlFor="portion">
                        Bobot (%)
                    </label>
                        <FormInput
                            className="form-control"
                            type="number"
                            name="portion"
                            value={this.state.subClo.portion}
                            mix={0}
                            max={100}
                            onChange={this.handleChange}/
                        >
                </FormGroup>
                <RubricTable
                    rubrics={this.state.rubrics}
                    onAdd={this.handleAddRubric}
                    onEdit={this.handleEditRubric}
                    onDelete={this.handleDeleteRubric}
                />
                <FormGroup>
                    <div>
                        <Button theme="primary" onClick={this.handleSubmit}>
                            Submit
                        </Button>
                    </div>
                </FormGroup>
            </Form>
        )
    }
}

const getSubCloFormErrors = (data) => {
    const errorMessages = new Map();
    if (!data.clo) {
        errorMessages.set('clo', 'CLO tidak boleh kosong')
    } else {
        errorMessages.delete('clo');
    }
    return errorMessages;
}

function RubricTable({rubrics, onAdd, onEdit, onDelete}) {
    RubricTable.propTypes = {
        rubrics: PropTypes.arrayOf(PropTypes.object),
        onAdd: PropTypes.func.isRequired,
        onEdit: PropTypes.func.isRequired,
        onDelete: PropTypes.func.isRequired
    }
    return (
        <div className="table-responsive">
            <table className="table">
                <thead>
                    <tr>
                        <th>Kode Rubrik</th>
                        <th>Nilai</th>
                        <th>Deskripsi</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {rubrics.map(rubric => {
                        return (
                            <EditableRubricRow
                                key={rubric.rubricCode}
                                rubric={rubric}
                                onEdit={onEdit}
                                onDelete={onDelete}
                            />
                        )
                    })}
                    <RubricForm
                        onSubmit={onAdd}
                    />
                </tbody>
            </table>
        </div>
    )
}