const yup = require('yup');

const lecturerSchema = yup.object().shape({
    nik: yup.string().required(),
    name: yup.string().required(),
    email: yup.string().email(),
    jfa: yup.number().integer(),
    labRiset: yup.number().integer().required(),
    prodi: yup.number(),
    lecturerCode: yup.string(),
    roles: yup.mixed()
});

module.exports = lecturerSchema
