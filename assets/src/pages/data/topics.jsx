import React from 'react';
import ReactDOM from 'react-dom';
import ReactDOMServer from 'react-dom/server';
import PropTypes from 'prop-types';
import Navbar, { MENU as MAIN_MENU } from '../../components/Navbar';
import TopicFilter from '../../components/TopicFilter';
import PeriodTag from '../../components/PeriodTag';
import LecturerInfo from '../../components/LecturerInfo';
import Layout from '../../components/layouts';
import { Nav, NavItem, NavLink, Collapse, Modal, ModalBody, ModalHeader, Button, Form, FormGroup, FormInput, Alert } from "shards-react";
import '../../../dependencies/datatables/DataTables-1.10.20/js/jquery.dataTables'
import '../../../dependencies/datatables/DataTables-1.10.20/css/jquery.dataTables.min.css'

class TopicTable extends React.Component {
    constructor(props) {
        super(props);


        $(document).ready(() => {
            $('#topic-table').DataTable({
                data: this.props.topics || [],
                columns: [
                    { data: 'id'}, //id
                    { data: 'name' }, //name
                    { data: 'quota' }, //quota
                    { data: 'deskripsi'}, //DESKRIPSI
                    { data: 'peminatan', render: function(data, type, row){
                        return ( data && data.abbrev != null ) ? data.abbrev : 'Belum Ada Peminatan'
                    } }, //peminatan
                    { data: 'period', render: function(data, type, row){
                        if ( data == null) return "---"
                        return data.semester + '/' + data.academicYear
                    } }, //period
                    { //lecturer
                        data: 'lecturer',
                        /*render: function(data, type, row, meta) {
                            if (type=='display') {
                                const lecturerInfo = (
                                    <LecturerInfo
                                        nik={data.nik}
                                        name={data.name}
                                        lecturerCode={data.lecturerCode}
                                    />
                                );
                                return ReactDOMServer.renderToString(lecturerInfo)
                            }
                        }*/
                        render: 'name'
                    },
                ]

                
            })
            $('#topic-table').find('td:nth-child(3)').click(function() {
                alert($(this).html());

                $('#myModal').modal('show'); //FUNCTION TO SHOW MODAL
                
              });


        })
    }

   

    componentDidUpdate() {
        $(function () {
            $('[data-toggle="popover"]').popover()
        })
    }

    render() {
        return (
            <div>
                
                    <div class="modal" id="myModal">
                        <div class="modal-dialog">
                            <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title">Modal Heading</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="modal-body">
                        Modal body..
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>

                    </div>
                </div>
            </div>

            <table id="topic-table" class="table table-striped table-bordered" >
                <thead>
                    <tr>
                        <th bgcolor="#E8E8E8">Id</th>
                        <th>Topik</th>
                        <th>Quota</th>
                        <th>Deskripsi</th>
                        <th>Peminatan</th>
                        <th>Semester / Periode</th>
                        <th>Dosen</th>
                    </tr>
                </thead>
                <tbody class="wrap">

                </tbody>
            </table>
            </div>
        )
    }
}

TopicTable.propTypes = {
    topics: PropTypes.arrayOf(PropTypes.object)
}

function Page() {
    const { currentPeriod, topics, peminatanList, periods, userType } = window.SAILS_LOCALS;
    const searchParams = new URLSearchParams(window.location.search);
    const periodId = searchParams.get('periodId');
   
    return (
        <div className="page main-content-container px-4 pb-4 container-fluid">
            {/* <Navbar activeMenu={MAIN_MENU.TOPICS} /> */}
            <div className="container">
                <h2 className="display-4">Daftar Topik {!periodId  ? (<PeriodTag period={currentPeriod} />) : ('setelah difilter')} </h2>
                <div>
                    {userType != 'student' ?
                    <TopicFilter peminatanList={peminatanList} periods={periods} /> : <></> }
                    {topics != null ? <TopicTable topics={topics} /> : <Alert theme="danger">
       Belum Ada Peminatan
      </Alert>}
                    
                </div>
            </div>
        </div>
    )
}

ReactDOM.render(<Layout><Page /></Layout>, document.getElementById('root'));
