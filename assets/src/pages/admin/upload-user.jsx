import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types'
import Layout from '../../components/layouts';
import Nav, {MENU as ADMIN_MENU} from '../../components/admin/Nav';
import ModelTable from '../../components/ModelTable';
import handleFilterChange from '../../components/handleFilterChange';
import { Card, CardTitle, CardBody,Form, FormGroup, FormInput, FormSelect, FormCheckbox, Button, ListGroup, ListGroupItem, ListGroupItemText } from "shards-react";
const Student = require('../../../../api/models/Student');
const Lecturer = require('../../../../api/models/Lecturer');

//STUDENT_COLUMN_CONFIG = [ [namakolom: string, tipedata: string, isRequired: bool]]
const STUDENT_COLUMN_CONFIG = [
    ['nim', 'number', true],
    ['name', 'string', true],
    ['email', 'string'],
    ['class', 'string'],
    ['peminatan', 'foreign key', true],
    ['metlit', 'foreign key'],
    ['ipk', 'number decimal'],
];

const LECTURER_COLUMN_CONFIG = [
    ['nik', 'number', true],
    ['name', 'string', true],
    ['email', 'string'],
    ['jfa', 'foreign key'],
    ['labRiset', 'foreign key', true],
    ['lecturerCode', 'string']
];

function UploadResults({uploadResults}) {
    UploadResults.propTypes = {
        response: PropTypes.object
    }
    return (<p>{JSON.stringify(uploadResults)}</p>)
    return (
        <ListGroup className="mt-4">
            <ListGroupItem> Sukses: {successCount} baris.</ListGroupItem>
            {duplicateCount ? <ListGroupItem>Duplikasi: {duplicateCount} baris</ListGroupItem> : ''}
            <br/>
            <ListGroupItemText>
                Daftar error: (Header Tabel dianggap baris ke-1)
                <ul>
                    {invalidMessages.map(err => <li>{err}</li>)}
                </ul>
            </ListGroupItemText>
        </ListGroup>
    )
}

class UserCsvForm extends React.Component {
    constructor(props) {
        UserCsvForm.propTypes = {
            userType: PropTypes.string
        }
        super(props);
        this.state = {
            file: '',
            errors: [],
            uploadResults: '',
            shouldUpdateUser: false,
            isSubmitting: false
        }
        this.handleFileChange = this.handleFileChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleToggleUpdateUser = this.handleToggleUpdateUser.bind(this);

    }

    handleToggleUpdateUser(e) {
        this.setState(state => ({shouldUpdateUser: !state.shouldUpdateUser}))
    }


    handleFileChange(e) {
        this.setState({file: e.target.files[0]});
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({recordsCount: null, invalidCount: null, duplicateCount: null, isSubmitting: true});
        const formData = new FormData();
        formData.append('userType', this.props.userType);
        formData.append('shouldUpdateUser', this.state.shouldUpdateUser);
        formData.append('user-table', this.state.file);
        fetch('/admin/upload-user', {
            method: 'POST',
            body: formData
        }).then(res => {
            this.setState({isSubmitting: false});
            const exitCode = res.headers.get('X-Exit');
            switch (exitCode) {
                case "success":
                    res.json().then((response) => {
                        this.setState({uploadResults: response});
                    });
                    break;
                default:
                    break;
            }
        })
        .catch(err => console.error(err))
    }

    render() {
        const isFieldEmpty = !(this.state.file && this.props.userType);
        return (
            <Form action="/file/upload" encType="multipart/form-data" method="post" onSubmit={this.handleSubmit}>
                <FormGroup className="form-group">
                    <label htmlFor="file">Tabel CSV</label>
                    <FormInput type="file" name="file" accept=".csv" onChange={this.handleFileChange}/>
                </FormGroup>
                <FormGroup>
                <FormCheckbox
                    name="toggleUpdateUser"
                    toggle
                    checked={this.state.shouldUpdateUser}
                    onChange={this.handleToggleUpdateUser}
                >
                    Update user yang sudah terdaftar
                </FormCheckbox>

                </FormGroup>
                <Button type="submit" className="btn btn-primary" disabled={this.state.isSubmitting || isFieldEmpty}>Upload</Button>
                {this.state.isSubmitting && <p className="help-block">Loading...</p>}
                {this.state.uploadResults ? <UploadResults uploadResults={this.state.uploadResults} /> : ''}
            </Form>
        )
    }
}

class App extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const userType = window.SAILS_LOCALS.userType;
        let Model, referencedModelNames;
        if (userType == 'student') {
            Model = Student;
            referencedModelNames = ['peminatan', 'metlit', 'prodi'];
        } else if (userType == 'lecturer') {
            Model = Lecturer;
            referencedModelNames = ['jfa', 'peminatan', 'prodi'];
        }
        return (
            <Card>
                <CardBody>
                    <CardTitle>Upload {userType == 'student' ? 'Mahasiswa' : 'Dosen' }</CardTitle>
                    {userType == 'student' ? 
                        <ColumnList columnConfig={STUDENT_COLUMN_CONFIG} />
                        : <ColumnList columnConfig={LECTURER_COLUMN_CONFIG} /> }
                    <UserCsvForm userType={userType}/>
                </CardBody>
            </Card>
        )
    }
}

const Page = () => {
    return (
        <div className="page">
        <div className="container">
            <div className="content">
                <App />
            </div>
        </div>
    </div>
    )
}

function ColumnList({columnConfig}) {
    ColumnList.propTypes = {
        columnConfig: PropTypes.arrayOf(PropTypes.array).isRequired // [ [columnName, type], ...]
    }
    return (
        <table className="table table-sm">
            <thead>
                <tr>
                    <th>Header Kolom</th>
                    <th>Tipe Data</th>
                    <th>Wajib Diisi</th>
                </tr>
            </thead>
            <tbody>
            {columnConfig
                .map(([columnName, columnType, columnIsRequired]) => 
                    (<tr>
                        <td>{columnName}</td>
                        <td>{columnType}</td>
                        <td>{columnIsRequired ? <i class="material-icons">check_circle</i> : ''}</td></tr>) )}
            </tbody>
        </table>
    )
}

ReactDOM.render(<Layout><Page /></Layout>, document.getElementById('root'));