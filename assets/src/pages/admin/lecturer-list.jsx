import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import LecturerListFilter from '../../components/admin/LecturerListFilter';
import Layout from '../../components/layouts';
import LecturerListTable from  '../../components/admin/LecturerListTable'

function Page() {
    const { lecturer, peminatanList, periods } = window.SAILS_LOCALS;
    return (
        <div className="page main-content-container px-4 pb-4 container-fluid">
            {/* <Navbar activeMenu={ADMIN_MENU.TOPIC_ARCHIVES} /> */}
            <div className="container">
                <h1>Daftar Dosen</h1>
                <div className="content">
                    <LecturerListFilter peminatanList={peminatanList} periods={periods} />
                    <LecturerListTable lecturer={lecturer} />
                </div>
            </div>
        </div>
    )
}

ReactDOM.render(<Layout><Page /></Layout>, document.getElementById('root'))
