import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import StudentListFilter from '../../components/admin/StudentListFilter';
import Layout from '../../components/layouts';
import StudentListTable from  '../../components/admin/StudentListTable'

function Page() {
    const { students } = window.SAILS_LOCALS;
    return (
        <div className="page main-content-container px-4 pb-4 container-fluid">
            {/* <Navbar activeMenu={ADMIN_MENU.TOPIC_ARCHIVES} /> */}
            <div className="container">
                <h1>Daftar Mahasiswa</h1>
                <div className="content">
                    <StudentListFilter/>
                    <StudentListTable students={students} />
                </div>
            </div>
        </div>
    )
}

ReactDOM.render(<Layout><Page /></Layout>, document.getElementById('root'))
