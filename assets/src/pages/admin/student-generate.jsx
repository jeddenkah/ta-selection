import React from 'react';
import ReactDOM from 'react-dom';
import StudentStatusFilter from '../../components/admin/StudentStatusFilter';
import Layout from '../../components/layouts';
import StudentStatusTable from '../../components/admin/StudentStatusTable'
import { Container } from 'shards-react'
import { Button } from "shards-react";
import { Page, Text, View, Document, StyleSheet, Image, Font, PDFViewer, PDFDownloadLink } from '@react-pdf/renderer';

Font.register({
    family: 'Oswald',
    src: 'https://fonts.gstatic.com/s/oswald/v13/Y_TKV6o8WovbUd3m_X9aAA.ttf'
});

const styles = StyleSheet.create({
    image: { flex: 1, flexDirection: 'row', width: 100, alignSelf: 'center' },
    page: { backgroundColor: '#FFF' },
    section: { color: '#000', margin: 30 },
    subTitle: { width: '20%', fontSize: 14 },

    subSpace: { width: '2%', fontSize: 12 },
    title_nomer: { width: '5%', fontSize: 12, borderColor:'black',borderBottomWidth:1,borderTopWidth:1, borderLeftWidth:1},
    title_nama: { 
        width: '20%',
        fontSize: 12,
        borderColor:'black',borderBottomWidth:1,borderTopWidth:1, borderLeftWidth:1
    },
    title_nim: { width: '10%', fontSize: 12, borderColor:'black',borderBottomWidth:1,borderTopWidth:1, borderLeftWidth:1},
    title_kelas: { width: '10%', fontSize: 12, borderColor:'black',borderBottomWidth:1,borderTopWidth:1, borderLeftWidth:1},
    title_topic: { width: '18%', fontSize: 12, borderColor:'black',borderBottomWidth:1,borderTopWidth:1, borderLeftWidth:1},
    title_peminatan: { width: '7%', fontSize: 12, borderColor:'black',borderBottomWidth:1,borderTopWidth:1, borderLeftWidth:1},
    title_dosen: { width: '15%', fontSize: 12, borderColor:'black',borderBottomWidth:1,borderTopWidth:1, borderLeftWidth:1},
    title_dosenpenguji: { width: '15%', fontSize: 12,  borderColor:'black',borderBottomWidth:1,borderTopWidth:1, borderLeftWidth:1, borderRightWidth:1},
    
    nomer: { width: '5%', fontSize: 11, borderColor:'black',borderBottomWidth:1, borderLeftWidth:1},
    nama: { 
        width: '20%',
        fontSize: 11,
        borderColor:'black',borderBottomWidth:1, borderLeftWidth:1
    },
    nim: { width: '10%', fontSize: 11, borderColor:'black',borderBottomWidth:1, borderLeftWidth:1},
    kelas: { width: '10%', fontSize: 11, borderColor:'black',borderBottomWidth:1, borderLeftWidth:1},
    topic: { width: '18%', fontSize: 11, borderColor:'black',borderBottomWidth:1, borderLeftWidth:1},
    peminatan: { width: '7%', fontSize: 11, borderColor:'black',borderBottomWidth:1, borderLeftWidth:1},
    dosen: { width: '15%', fontSize: 11, borderColor:'black',borderBottomWidth:1, borderLeftWidth:1},
    dosenpenguji: { width: '15%', fontSize: 11,  borderColor:'black',borderBottomWidth:1, borderLeftWidth:1, borderRightWidth:1},
    title: {
        fontSize: 14,
        textAlign: 'center',
    },
    subText: { width: '50%' },


});

let i = 1;

const MyDocument = ({ records }) => (
    <Document>
        <Page size="A4" style={styles.page}>
            <View style={styles.section}>
                {/* <Image
                    style={styles.image}
                    src="../../../images/logoTelU.png"
                />
                <Text> </Text>
                <Text style={styles.title}>KEPUTUSAN DEKAN FAKULTAS REKAYASA INDUSTRI </Text>
                <Text style={styles.title}>NOMOR: 451 AKD9/RI-DEK/2019</Text>
                <Text> </Text>
                <Text style={styles.title} >TENTANG</Text>
                <Text> </Text>
                <Text style={styles.title}>PENGANGKATAN DOSEN PEMBINA PROPOSAL TUGAS AKHIR</Text>
                <Text style={styles.title}>SEMESTER GANJIL TAHUN AKADEMIK 2019/2020</Text>
                <Text style={styles.title}>PROGRAM STUDI S1 SISTEM INFORMASI</Text>
                <Text style={styles.title}>FAKULTAS REKAYASA INDUSTRI</Text>
                <Text> </Text>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.subTitle}>Menimbang</Text>
                    <Text style={styles.subSpace}>: </Text>
                    <Text style={styles.subSpace}>Bahwa untuk menyelesaikan Tugas Akhir di Program Studi S1 Sistem Informasi Fakultas Rekayasa Industri Universitas Telkom, mahasiswa diwajibkan untuk membuat Proposal Tugas Akhir dan melaksanakan Desk Evaluation /Seminar</Text>
                </View>
                <Text> </Text>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.subTitle}>Memperhatikan</Text>
                    <Text style={styles.subSpace}> : </Text>
                    <Text style={styles.subSpace}>Aturan Tugas Akhir</Text>
                </View>
                <Text> </Text>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.subTitle}>Mengingat</Text>
                    <Text style={styles.subSpace}> : </Text>
                    <Text style={styles.subSpace} >Perlu adanya Dosen yang akan membina mahasiswa dalam membuat dan menyelesaikan Proposal Tugas Akhir</Text>
                </View>
                <Text> </Text>
                <Text>MEMUTUSKAN</Text>
                <Text> </Text>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.subTitle}>Menetapkan</Text>
                    <Text style={styles.subSpace}> : </Text>
                    <Text style={styles.subSpace}>1. </Text>
                    <Text style={styles.subSpace} >Nama - nama Pembina Proposal Tugas Akhir Program Studi S1 Sistem Informasi Fakultas Rekayasa Industri untuk Semester Ganjil Tahun Akademik 2019/2020.</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.subTitle}></Text>
                    <Text style={styles.subSpace}></Text>
                    <Text style={styles.subSpace}>2. </Text>
                    <Text style={styles.subSpace} >Surat Keputusan ini berlaku sejak tanggal ditetapkan selama 1 (satu) semester dengan ketentuan akan diubah dan ditinjau kembali apabila ternyata terdapat kekeliruan dalam keputusan ini. </Text>
                </View>
                <Text> </Text>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.subText}>Ditetapkan di</Text>
                    <Text style={styles.subText2}> : </Text>
                    <Text >Bandung</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.subText}>Pada Tanggal</Text>
                    <Text style={styles.subText2}> : </Text>
                    <Text >14 Agustus 2020</Text>
                </View>
                <Text>FAKULTAS REKAYASA INDUSTRI</Text>
                <Image
                    style={styles.image}
                    src="../../../images/logoTelU.png"
                />
                <Text>Dr. Ir. Agus Achmad Suhendra, M.T.</Text>
                <Text>Dekan Fakultas Rekayasa Industri</Text>
                <Text> </Text>
                <Text>Keputusan Dekan Fakultas Rekayasa Industri Tentang Pengangkatan Dosen Pembina</Text>
                <Text>Proposal Tugas Akhir Semester Ganjil Tahun Akademik 2019/2020</Text>
                <Text>Nomor : 451/AKD9/RI-DEK/2019</Text>
                <Text>Tanggal : 18 Oktober 2019</Text>
                <Text> </Text> */}
                <Text style={styles.title}>DAFTAR MAHASISWA PROPOSAL TUGAS AKHIR</Text>
                <Text style={styles.title}>PROGRAM STUDI S1 SISTEM INFORMASI</Text>
                <Text style={styles.title}>FAKULTAS REKAYASA INDUSTRI</Text>
                <Text> </Text>
                <View style={{ flexDirection: 'row', textAlign: 'center', }}>
                    <Text style={styles.title_nomer}>No</Text>
                    <Text style={styles.title_nama}>Nama</Text>
                    <Text style={styles.title_nim}>NIM</Text>
                    <Text style={styles.title_kelas}>Kelas</Text>
                    <Text style={styles.title_topic}>Topik</Text>
                    <Text style={styles.title_peminatan}>Peminatan</Text>
                    <Text style={styles.title_dosen}>Dosen Pembimbing</Text>
                    <Text style={styles.title_dosenpenguji}>Dosen Penguji</Text>
                </View>
                {records.map(record =>
                    <View style={{ flexDirection: 'row', textAlign: 'center' }}>
                        <Text style={styles.nomer}>{i++}</Text>
                        <Text style={styles.nama}>{record.student.name}</Text>
                        <Text style={styles.nim}>{record.student.nim}</Text>
                        <Text style={styles.kelas}>{record.student.class}</Text>
                        <Text style={styles.topic}>{record.topic.name}</Text>
                        <Text style={styles.peminatan}>{record.peminatan.abbrev}</Text>
                        <Text style={styles.dosen}>{record.lecturer.name}</Text>
                        <Text style={styles.dosenpenguji}>{record.reviewer != null ? record.reviewer.name : "-"}</Text>
                    </View>
                )}
            </View>
        </Page>
    </Document>
);

function App() {
    const { studentStatus } = window.SAILS_LOCALS;
    console.log("Pdf");
    console.log(studentStatus);
    return (
        <Container fluid>
            <div className="main-content">
                <h1>Generate PDF</h1>

                <PDFViewer style={{ height: "100vh", width: "75vw" }}>
                    <MyDocument records={studentStatus} />
                </PDFViewer>
                <PDFDownloadLink document={<MyDocument records={studentStatus} />} fileName="test.pdf">
                    {({ blob, url, loading, error }) => (loading ? 'Loading document...' : 'Download now!')}
                </PDFDownloadLink>
                {/* <StudentStatusFilter />
                    <StudentStatusTable records={studentStatus} /> */}
            </div>
        </Container>
    )
}


ReactDOM.render(<Layout><App /></Layout>, document.getElementById('root'))
