import React from 'react';
import ReactDOM from 'react-dom';
import StudentStatusFilter from '../../components/admin/StudentStatusFilter';
import Layout from '../../components/layouts';
import StudentStatusTable from '../../components/admin/StudentStatusTable'
import ExportToExcel from '../../components/admin/ExportToExcel'
import {Container} from 'shards-react'
import {Button } from "shards-react";

function Page() {
    const { studentStatus,studentStatusReview } = window.SAILS_LOCALS;
  
        let j = 1;
        
        
        let data =[]
        studentStatusReview.map(records=>{
        let dataList={}
           dataList["No"] = j++
            Object.entries(records.student)
            .map(([ key, val ]) =>
                key=="name" ? dataList["Nama"]=val : 
                key=="nim" ? dataList["NIM"]=val : 
                key=="class" ? dataList["Kelas"]=val : ""
            )
            Object.entries(records.topic)
            .map(([ key2, val2 ]) =>
                key2=="name" ? dataList["Topic"]=val2 : ""
            )
            Object.entries(records.peminatan)
            .map(([ key3, val3 ]) =>
                key3=="abbrev" ? dataList["Peminatan"]=val3 : ""
            )
            Object.entries(records.lecturer)
            .map(([ key4, val4 ]) =>
                key4=="name" ? dataList["Dosen Pembimbing"]=val4 : ""
            )
            if(records.reviewer!=null){
                Object.entries(records.reviewer)
            .map(([ key5, val5 ]) =>
                key5=="name" ? dataList["Dosen Penguji"]=val5 : ""
            )
            }
            else{
                dataList["Dosen Penguji"]="-"
            }
            
            data.push(dataList)
            console.log(dataList)
        })

        console.log(data)
      
    //     const csvReport = {
    //     data: data,
    //     headers: headers,
    //     filename: 'status_mahasiswa.csv'
    //   };
    const fileName = "Status_Mahasiswa"
    return (
        <Container fluid>
                <div className="main-content">
                    <h1>Daftar Mahasiswa</h1>
                    
                    {/* <a href="/admin/student-generate-excel"><Button type="submit" className="btn btn-primary">Generate Excel</Button></a> */}
                    {/* <StudentStatusFilter /> */}
                    <a href="/admin/student-generate"><Button type="submit" className="btn btn-primary">Generate PDF</Button></a>
                     &nbsp;
                    {/* <CSVLink  {...csvReport}> */}
                    <ExportToExcel apiData={data} fileName={fileName}>
                   
                    </ExportToExcel>
                    {/* </ CSVLink > */}
                   <br/>
                    <StudentStatusTable records={studentStatus} />
                </div>
        </Container>
    )
}

ReactDOM.render(<Layout><Page /></Layout>, document.getElementById('root'))
