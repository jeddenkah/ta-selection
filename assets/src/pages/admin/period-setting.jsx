import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import Layout from '../../components/layouts';
import ModelTable from '../../components/ModelTable';
import Period from '../../../../api/models/Period';
import {
    Form,
    FormInput,
    FormGroup,
    Row,
    Col,
    FormSelect,
    Card,
    CardHeader,
    CardTitle,
    CardBody,
    Button,
} from "shards-react";
import ModelSelect from '../../components/ModelSelect';
import {useFormik} from 'formik';

function PeriodForm({initialRecord, mode}) {
    PeriodForm.propTypes = {
        initialRecord: PropTypes.object,
        mode: PropTypes.oneOf(['NEW', 'EDIT'])
    }
    const [yearStart, yearEnd] = initialRecord ? initialRecord.academicYear.split('/') : ['', '']
    const formik = useFormik({
        initialValues: {
            id: initialRecord ? initialRecord.id : '',
            yearStart,
            yearEnd,
            semester: initialRecord ? initialRecord.semester : '',
            perLecturerQuota: initialRecord ? initialRecord.perLecturerQuota : 0,
        },
        onSubmit: values => {
            const academicYear = `${values.yearStart}/${values.yearEnd}`;
            const recordJson = JSON.stringify({academicYear, semester: values.semester, perLecturerQuota: values.perLecturerQuota });
            if (mode == 'NEW') {
                fetch('/period', {
                    method: 'POST',
                    body: recordJson
                }).then(res => {
                    if (res.ok) {
                        window.location.reload(true);
                    }
                })
            } else if (mode == 'EDIT') {
                fetch(`/period/${values.id}`, {
                    method: 'PATCH',
                    body: recordJson
                }).then(res => {
                    if (res.ok) {
                        window.location.reload(true);
                    }
                })
            }
        }
    });
    const currentPeriod = window.SAILS_LOCALS.currentPeriod;
    return (
        <Form onSubmit={formik.handleSubmit}>
            <Row>
                {currentPeriod && (<FormGroup>
                    <label>Periode Sekarang</label>
                    <input
                        type="text"
                        readonly
                        class="form-control-plaintext"
                        id="staticEmail"
                        value={currentPeriod.academicYear + '/' +  currentPeriod.semester}
                    />
                </FormGroup>)}
            </Row>
            <Row>
                <FormGroup>
                    <label htmlFor="#year">Tahun</label>
                    <FormInput
                        name="yearStart"
                        type="number"
                        placeholder="Tahun"
                        value={formik.values.yearStart}
                        onChange={formik.handleChange}
                        inline
                    />
                </FormGroup>
                <FormGroup>
                    <label>Sampai tahun</label>
                    <FormInput
                        name="yearEnd"
                        type="number"
                        placeholder="Tahun"
                        value={formik.values.yearEnd}
                        onChange={formik.handleChange}
                        inline
                    />
                </FormGroup>
            </Row>
            <Row>
                <FormGroup>
                    <label htmlFor="#semester">Semester</label>
                    <FormSelect
                        name="semester"
                        value={formik.values.semester}
                        onChange={formik.handleChange}
                    >
                        <option value=""></option>
                        <option value="GANJIL">Ganjil</option>
                        <option value="GENAP">Genap</option>
                    </FormSelect>
                </FormGroup>
            </Row>
            <Row>
                <FormGroup>
                    <label htmlFor="#perLecturerQuota">Student Per Lecturer Quota</label>
                    <FormInput
                        name="perLecturerQuota"
                        type="number"
                        placeholder="PerLecturerQuota"
                        value={formik.values.perLecturerQuota}
                        onChange={formik.handleChange}
                        inline
                    />
                </FormGroup>
            </Row>
            <Row>
                <Button type="submit" theme="primary">
                    {mode == 'NEW' ? 'Tambah' : 'Ganti'}
                </Button>
            </Row>
        </Form>
    )
}

function ArchiveTopicForm() {
    function handleArchive(e) {
        if (!confirm(`Apakah anda yakin akan mengarsipkan topik`)) return;

        fetch('/admin/archive-topic', {
            method: 'POST'
        }).then(res => {
            if (res.ok) {
                alert('Berhasil mengarsipkan topik!');
                window.location.reload(true);
            }
        });
    }
    return (
    <Card>
        <CardBody>
            <CardTitle>Arsipkan Topik</CardTitle>
            <p>Arsipkan semua topik yang sudah terpilih mahasiswa.</p>
            <Button onClick={handleArchive}>Arsipkan</Button>
        </CardBody>
    </Card>
  )
}

function ChangePeriodForm() {
    const currentPeriod = window.SAILS_LOCALS.currentPeriod;
    const formik = useFormik({
        initialValues: {
            periodId: currentPeriod ? currentPeriod.id : ''
        },
        onSubmit: values => {
            fetch('/admin/change-period', {
                method: 'POST',
                body: JSON.stringify({id: values.periodId})
            }).then(res => {
                if (res.ok) {
                    alert('Berhasil mengganti periode!');
                    window.location.reload(true);
                }
            })
        }
    });
    return(
        <Card>
            <CardBody>
                <CardTitle>Ganti Periode</CardTitle>
                {currentPeriod && <p>Sekarang periode {currentPeriod.academicYear}/{currentPeriod.semester}</p>}
                <ModelSelect
                    name="periodId"
                    modelName="period"
                    displayAttribute={period => period.academicYear + '/' + period.semester}
                    value={formik.values.periodId}
                    onChange={formik.handleChange}
                    isClearable={false}
                />
                <Button onClick={formik.handleSubmit}>Ganti</Button>
            </CardBody>
        </Card>
    )
}

function CreatePeriodForm() {
    return (
        <Card>
            <CardBody>
                <CardTitle>Buat Periode</CardTitle>
                <ModelTable customForm={PeriodForm} model={Period} modelName="period" />
            </CardBody>
        </Card>
    )
}

function Page() {
    return (
        <div className="page main-content-container px-4 pb-4 container-fluid">
            <Row className="card-deck">
                <CreatePeriodForm />
            </Row>
            <Row className="card-deck">
                    <ChangePeriodForm />
                    <ArchiveTopicForm />
            </Row>
        </div>
    )
}
ReactDOM.render(<Layout><Page /></Layout>, document.getElementById('root'));