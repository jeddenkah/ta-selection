import React from 'react';
import ReactDOM from 'react-dom';
import ModelTable from '../../components/ModelTable';
import Layout from '../../components/layouts';
import MasterDataNav from '../../components/admin/MasterDataNav';
import {
    BrowserRouter as Router,
    Switch,
    Route
  } from "react-router-dom";
import SubCloForm from '../../components/admin/SubCloForm';
import UploadMetlitForm from '../../components/admin/UploadKelasMetlitForm';
import SubCloRubricList from '../../components/admin/SubCloRubricList';
import MasterRole from '../../../../api/models/MasterRole';
const Jfa = require('../../../../api/models/Jfa');
const Kk = require('../../../../api/models/Kk');
const Metlit = require('../../../../api/models/Metlit');
const Peminatan = require('../../../../api/models/Peminatan');
const LabRiset = require('../../../../api/models/LabRiset');
const Prodi = require('../../../../api/models/Prodi');
const Plo = require('../../../../api/models/Plo');
const Clo = require('../../../../api/models/Clo');
let SubClo = require('../../../../api/models/SubClo');
SubClo.attributes['subCloRubrics'] = {collection: 'subclorubric'}
const Admin = require('../../../../api/models/Admin');
import PloForm from '../../components/PloForm'
import CloForm from '../../components/CloForm'
import metlitForm from '../../components/admin/MetlitForm';

function Page() {
    const homePath = '/admin/database-setting';
    //const searchParams = new URLSearchParams(window.location.search);
    //const prodiId = searchParams.get('prodiId');
    //const periodId = searchParams.get('periodId');
    const pageURL = window.location.href;
    const tab = pageURL.substr(pageURL.lastIndexOf('/') + 1);   
    //const { plo, prodiList, periods, currentPeriodId, ploList} = window.SAILS_LOCALS;
    //console.log(SubCloList)
    return (
        <Router> 
           
            <div id="page" className = "page main-content-container px-4 pb-4 container-fluid">
                
                    <div className="container">
                    <MasterDataNav />
                    <Switch>
                        <Route path={homePath + "/kk"}>
                            <div>
                                <ModelTable
                                    modelName="kk"
                                    model={Kk}
                                    hideColumns={['labRiset']}
                                />
                            </div>
                        </Route>
                        <Route path={homePath + "/peminatan"}>
                            <div>
                                <ModelTable
                                    modelName="peminatan"
                                    model={Peminatan}
                                    hideColumns={['students', 'topics']}
                                    renderColumn={{
                                        prodi: data => data.prodi?.abbrev ?? '-',
                                        labRisetInduk: data => data.labRisetInduk?.abbrev ?? '-'
                                    }}
                                />
                            </div>
                        </Route>
                        <Route path={homePath + "/lab-riset"}>
                            <div>
                                <ModelTable
                                    modelName="labriset"
                                    model={LabRiset}
                                    renderColumn={
                                        {
                                            'kk': data => data.kk?.abbrev ?? '-'
                                        }
                                    }
                                    hideColumns={['peminatan']}
                                />
                            </div>
                        </Route>
                        <Route path={homePath + "/prodi"}>
                            <div>
                                <ModelTable
                                    modelName="prodi"
                                    model={Prodi}
                                    hideColumns={['lecturers', 'peminatan', 'metlit']}
                                />
                            </div>
                        </Route>
                        <Route path={homePath + "/metlit"}>
                            <div>
                                <ModelTable
                                    modelName="metlit"
                                    model={Metlit}
                                    hideColumns={['students']}
                                    customForm={metlitForm}
                                    uploadForm={UploadMetlitForm}

                                    renderColumn={
                                        {
                                            'metlit': data => data.metlit?.class ?? '-',
                                            'prodi': data => data.prodi?.abbrev ?? '-',
                                            'period': data => data.period?.semester + ' '+ data.period?.academicYear
                                        }
                                    }

                                    referencedModelNames={['prodi', 'period']}
                                />
                            </div>
                        </Route>
                        <Route path={homePath + "/jfa"}>
                            <div>
                                <ModelTable
                                    modelName="jfa"
                                    model={Jfa}
                                />
                            </div>
                        </Route>
                        <Route path={homePath + "/Plo"}>
                            <div>
                            <ModelTable
                                id="plo-table"
                                model={Plo}
                                modelName="plo"
                                customForm={PloForm}
                                hideColumns= {['cloList','isDeleted']}
                                renderColumn={{
                                    prodi: data => data.prodi?.name ?? '-',
                                    period: data => data.period?.academicYear ?? '-',
                                }}
                                referencedModelNames={['prodi', 'period']}
                            />
                            </div>
                        </Route>
                        <Route path={homePath + "/Clo"}>
                            <div>
                                <ModelTable
                                id="clo-table"
                                model={Clo}
                                modelName="clo"
                                customForm={CloForm}
                                hideColumns= {['isDeleted']}
                                renderColumn={{
                                    'plo': data => data.plo?.ploCode ?? '-',
                                    'period': data => data.period?.semester + ' '+ data.period?.academicYear
                                }}
                                referencedModelNames={['plo']}
                            /> 
                            </div>
                            
                        </Route>
                        <Route path={homePath + "/sub-clo"}>
                            <div>
                                <ModelTable
                                    modelName="subclo"
                                    model={SubClo}
                                    referencedModelNames={['clo']}
                                    customForm={SubCloForm}
                                    renderColumn={
                                        {
                                            'clo': data => data.clo?.cloCode ?? '-',
                                            'period': data => data.period?.semester + ' '+ data.period?.academicYear, 
                                            'subCloRubrics': record => 
                                                <SubCloRubricList key={record.id} subCloId={record.id}/>
                                        }
                                    }
                                    shallowDelete={true}
                                    referencedModelNames={['clo']}
                                />
                                {/* {cloList[0].map((subClo,i) =>
                                 subClo.subCloList.map((subClos,i)=>{
                                     if(subClos!=null){
                                        console.dir(subClos)
                                     }
                                 })
                                 )} */}
                            </div>
                        </Route>
                        <Route path={homePath + "/admin"}>
                            <div>
                                <ModelTable
                                    modelName="admin"
                                    model={Admin}
                                />
                            </div>
                        </Route>
                        <Route path={homePath + "/master-role"}>
                            <div>
                                <ModelTable
                                    modelName="masterrole"
                                    model={MasterRole}
                                    renderColumn={{
                                        isCoordinator: data => data.isCoordinator ? <i className="material-icons">check_circle</i> : ''
                                    }}
                                />
                            </div>
                        </Route>
                    </Switch>
                
                </div>
            </div>
        </Router>
    )
}

ReactDOM.render(<Layout><Page /></Layout>, document.getElementById('root'));