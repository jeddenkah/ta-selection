import React from 'react'
import ReactDOM from 'react-dom';
import Layout from '../../components/layouts/';
import {Card, CardBody} from 'shards-react'
import GroupControl from '../../components/group/GroupControl';
import GroupInfo from '../../components/group/GroupInfo';
import LeaveButton from '../../components/group/LeaveButton';

function Page() {
    const user = SAILS_LOCALS.user;
    const group = SAILS_LOCALS.group;
    const userIsOwner = user.id == group?.owner?.id;
    const noGroupView = <p>Anda tidak memiliki group.</p>
    const ownerView = <GroupControl groupId={group?.id} students={group?.students} user={user} />;
    const memberView = (

        <Card>
            <CardBody>
                <GroupInfo
                    isTable={true}
                    students={group?.students}
                    peminatan={group?.owner?.peminatan}
                    ownerId={group?.owner?.id}
                />
                <LeaveButton />
            </CardBody>
        </Card>
    );
    let view;
    if (!group) view = noGroupView;
    else if (userIsOwner) view = ownerView;
    else view = memberView;

    return (
        <Layout>
            <div className="page main-content-container px-4 pb-4 container-fluid">
                <h1 className="mt-2">Setting Group <small>{group ? `#${group.id}` : ''}</small></h1>
                <div className="mt-4">
                {view}
                </div>
            </div>
        </Layout>
    )
}


ReactDOM.render(<Page />, document.getElementById('root'));