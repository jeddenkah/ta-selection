import React from 'react';
import ReactDOM from 'react-dom';
import { Row, Card, CardBody, CardSubtitle, CardTitle, Col, FormInput, Button, Collapse, Form, Progress } from 'shards-react';
import Layout from '../../components/layouts';

class Dashboard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      collapse: false,
      score: props.eprt ? props.eprt.score : null,
      form: props.form,
      ta: props.ta,
      eprt: props.eprt,
      progress: null,
      edit: true,
      editForm: true,
      editTa: true,
      selectedFileEprt: null,
      selectedFileForm: null,
      selectedFileTa: null,
      btnFormBimbingan: true,
      btnTa: true,
      btnEprt: true
    };

    this.handleChange = this.handleChange.bind(this);
    this.editChange = this.editChange.bind(this);
    this.editChangeForm = this.editChangeForm.bind(this);
    this.editChangeTa = this.editChangeTa.bind(this);
    this.toggle = this.toggle.bind(this);

  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }


  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  editChange() {
    this.setState({ edit: false, progress: null })
  }

  editChangeForm() {
    this.setState({ editForm: false, progress: null })
  }

  editChangeTa() {
    this.setState({ editTa: false, progress: null })
  }

  fileHandlerTa = event => {
    this.setState({
      selectedFileTa: event.target.files[0]
    })
  }

  fileHandlerEprt = event => {
    this.setState({
      selectedFileEprt: event.target.files[0]
    })
  }



  uploadHandlerTA = () => {
    const fd = new FormData();
    fd.append('file_ta', this.state.selectedFileTa, this.state.selectedFileTa.name);
    axios.post('/student/new-ta', fd, {
      onUploadProgress: ProgressEvent => {
        this.setState({ progress: Math.round(ProgressEvent.loaded / ProgressEvent.total * 100) })
      }
    }).then(res => {
      this.setState({ ta: res.data, editTa: true })
      console.log(res)
    });
  }

  uploadHandlerEPRT = () => {
    const fd = new FormData();
    fd.append('score', this.state.score);
    fd.append('file_eprt', this.state.selectedFileEprt, this.state.selectedFileEprt.name);

    axios.post('/student/new-eprt', fd, {
      onUploadProgress: ProgressEvent => {
        //console.log('Upload Progress: ' + Math.round(ProgressEvent.loaded / ProgressEvent.total * 100) + '%')
        this.setState({ progress: Math.round(ProgressEvent.loaded / ProgressEvent.total * 100) })
      }
    }).then(res => {
      this.setState({ eprt: res.data, edit: true })
      console.log(res)
    });


  }

  deleteEprt = () => {
    axios.post('/student/delete-eprt', '').then(res => this.setState({ eprt: null }))
  }
  deleteTa = () => {
    axios.post('/student/delete-ta', '')
  }
 


  render() {

    let { ta, eprt, score, form, selectedFileTa, selectedFileEprt, selectedFileForm, progress, editForm, edit, editTa } = this.state;
    // let fileName = eprt ? function (eprt) {
    //   return eprt.eprtUrl.split('\\').pop().split('/').pop();
    // } : '';





    return (
      <div className="main-content-container px-4 pb-4 container-fluid" style={{ paddingTop: '2em' }}>

        {/* Eprt */}
        {
          <Row style={{ paddingTop: '2em' }}>
            <Col md="12" lg="6">
              <Card>
                <CardBody>
                  <Row style={{ marginTop: '-1.09375rem' }}>
                    <Col lg="1" style={{ paddingLeft: 0 }}>
                      <span className="dot2">
                        <i className="material-icons" style={{ fontSize: '2em', alignSelf: 'left' }}>library_books</i>
                      </span>
                    </Col>
                    <Col style={{ alignSelf: 'center', textAlign: 'left' }}><CardTitle style={{ marginTop: '0' }}>Input EPRT</CardTitle></Col>
                  </Row>
                  <Row style={{ paddingTop: '1em' }}>
                    <Col lg="12" >

                      <Form >
                        <div className="uploadBox">
                          <label>
                            {edit ?
                              <FormInput type="file" name="file_eprt" onChange={this.fileHandlerEprt} hidden disabled></FormInput>
                              :
                              <FormInput type="file" name="file_eprt" onChange={this.fileHandlerEprt} accept=".pdf" hidden></FormInput>
                            }
                            <Row style={{ margin: 0, justifyContent: 'center' }}>
                              <i className="material-icons" style={{ fontSize: '2em' }}>library_books</i>
                            </Row>
                            <Row style={{ margin: 0, justifyContent: 'center', paddingTop: '1em' }}>
                              {edit ?
                                <p>{eprt ? eprt.fileName : 'Belum upload berkas'}</p>
                                :
                                <p>{selectedFileEprt == null ? <>Klik disini untuk memilih berkas</> : selectedFileEprt.name}</p>
                              }
                            </Row>
                            <Row style={{ margin: 0, justifyContent: 'center', paddingTop: '1em' }}>
                              {eprt && edit == true ?
                                <p>upload selesai (lihat <a href={eprt.eprtUrl}>file</a>)</p>
                                :
                                <p>{progress == null ? <></> : <>upload {progress} %</>}</p>
                              }

                            </Row>
                          </label>
                        </div>

                        <div style={{ paddingLeft: '1em', paddingRight: '1em' }}>
                          <Row className="formComponentSpace">
                            <CardTitle>Nilai EPRT</CardTitle>
                          </Row>
                          <Row className="formComponentSpace">
                            {edit ?
                              <FormInput size="lg" type="number" name="score" onChange={this.handleChange} value={score} disabled />
                              :
                              selectedFileEprt == null ?
                                <FormInput size="lg" type="text" name="score" onChange={this.handleChange} value="upload file terlebih dahulu untuk mengedit score" disabled />
                                :
                                <FormInput size="lg" type="number" name="score" onChange={this.handleChange} value={score} />


                            }
                          </Row>
                          <Row className="formComponentSpace">
                            {edit ?
                              <Button block className="btn-ijo-outline" onClick={this.editChange} >Edit</Button>
                              :
                              <Button block disabled={!selectedFileEprt} className="btn-ijo" onClick={this.uploadHandlerEPRT} >Simpan</Button>
                            }

                          </Row>
                        </div>
                      </Form>
                    </Col>

                  </Row>

                </CardBody>
              </Card>
            </Col>

          {/* </Row>

        }


        <Row style={{ paddingTop: '2em' }}>
         

          { */}
            <Col md="12" lg="6">
              <Card>
                <CardBody>
                  <Row style={{ marginTop: '-1.09375rem' }}>
                    <Col lg="1" style={{ paddingLeft: 0 }}>
                      <span className="dot2">
                        <i className="material-icons" style={{ fontSize: '2em', alignSelf: 'left' }}>book</i>
                      </span>
                    </Col>
                    <Col style={{ alignSelf: 'center', textAlign: 'left' }}><CardTitle style={{ marginTop: '0' }}>Upload File TA 1</CardTitle></Col>
                  </Row>
                  <Row style={{ paddingTop: '1em' }}>
                    <Col lg="12">
                      <Form >
                        <div className="uploadBox">
                          <label>
                            {editTa ?
                              <FormInput type="file" name="file_ta" onChange={this.fileHandlerTa} hidden disabled></FormInput>
                              :
                              <FormInput type="file" name="file_ta" onChange={this.fileHandlerTa} accept=".pdf" hidden></FormInput>
                            }
                            <Row style={{ margin: 0, justifyContent: 'center' }}>
                              <i className="material-icons" style={{ fontSize: '2em' }}>library_books</i>
                            </Row>
                            <Row style={{ margin: 0, justifyContent: 'center', paddingTop: '1em' }}>
                              {editTa ?
                                <p>{ta ? ta.fileName : 'Belum upload berkas'}</p>
                                :
                                <p>{selectedFileTa == null ? <>Klik disini untuk memilih berkas</> : selectedFileTa.name}</p>
                              }
                            </Row>
                            <Row style={{ margin: 0, justifyContent: 'center', paddingTop: '1em' }}>
                              {ta && editTa == true ?
                                <p>upload selesai (lihat <a href={ta.taUrl}>file</a>)</p>
                                :
                                <p>{progress == null ? <></> : <>upload {progress} %</>}</p>
                              }

                            </Row>
                          </label>
                        </div>

                        <div style={{ paddingLeft: '1em', paddingRight: '1em' }}>
                          <Row className="formComponentSpace">
                            {editTa ?
                              <Button block className="btn-ijo-outline" onClick={this.editChangeTa} >Edit</Button>
                              :
                              <Button block disabled={!selectedFileTa} className="btn-ijo" onClick={this.uploadHandlerTA} >Simpan</Button>
                            }

                          </Row>
                        </div>
                      </Form>

                    </Col>
                  </Row>


                </CardBody>
              </Card>
            </Col>
          

        </Row>

        }

      </div>
    );
  }
}

function Page() {
  const { ta, eprt, form } = window.SAILS_LOCALS;
  return (
    <div className="page main-content-container px-4 pb-4 container-fluid">
      {/* <Navbar /> */}
      <div className="container">

        <Dashboard
          ta={ta}
          eprt={eprt}
          form={form}
        />
      </div>
    </div>
  )
}

ReactDOM.render(<Layout><Page /></Layout>, document.getElementById('root'));



