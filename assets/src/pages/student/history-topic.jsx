import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types'
import PeriodTag from '../../components/PeriodTag';
import GroupInfo from '../../components/group/GroupInfo';
import Layout from '../../components/layouts';
import { Row, Card, CardBody, CardSubtitle, CardTitle, Col, FormInput, Button, Collapse, Form, Progress } from 'shards-react';

function StatusPanel({ topicSelection }) {
    StatusPanel.propTypes = {
        topicSelection: PropTypes.object.isRequired
    }
    let className, statusLabel, warna;
    switch (topicSelection.status) {
        case 'WAITING':
            className = 'panel-default';
            statusLabel = 'Menunggu';
            warna = 'yellow';
            break;
        case 'APPROVED':
            className = 'panel-success';
            statusLabel = 'Disetujui';
            warna = 'green';
            break;
        case 'APPROVED_LAB_RISET':
            className = 'panel-success';
            statusLabel = 'Disetujui';
            warna = 'green';
            break;
        case 'REJECTED':
            className = 'panel-danger';
            statusLabel = 'Ditolak';
            warna = 'orange';
            break;
        case 'AUTO_CANCELED':
            className = 'panel-info';
            className = 'Dibatalkan sebab topik opsi #1 tersetujui'
            break;
        default:
            break;
    }
    const topic = topicSelection.topic;
    const lecturer = topic.lecturer;
    return (
        <div className={`panel ${className}`}>
            <div className="panel-heading">
                <h3 className="panel-title">Opsi {topicSelection.optionNum}</h3>
            </div>
            <div className="panel-body">
                <dl className="dl-horizontal">
                    {/* <dt>Id Topik</dt>
                    <dd>{topic.id}</dd> */}
                    <dt>Nama Topik</dt>
                    <dd>{topic.name}</dd>
                    <dt>Dosen</dt>
                    <dd>
                        {topic.lecturer.name}
                        {/* <LecturerInfo
                            nik={lecturer.nik}
                            name={lecturer.name}
                            lecturerCode={lecturer.lecturerCode}
                        /> */}
                    </dd>
                    <dt>Status</dt>
                    <dd style={{ color: warna }}>{statusLabel}</dd>
                </dl>
            </div>
        </div>
    )
}

function Title({ currentPeriod }) {
    const { semester, academicYear } = currentPeriod;
    return (
        <h1>Status Pemilihan Topik <PeriodTag period={currentPeriod} /></h1>
    )
}

function ReselectButton({ firstStatus, secondStatus }) {
    ReselectButton.propTypes = {
        firstStatus: PropTypes.string.isRequired,
        secondStatus: PropTypes.string
    }
    let isShown = false;
    if (firstStatus != 'WAITING' && secondStatus != 'WAITING') {
        if (firstStatus == 'REJECTED' || secondStatus == 'REJECTED') {
            isShown = true;
        }
    }
    return (
        <div>
            {isShown && <Button theme="success" href="/student/topic-selection/new">Pilih topik lagi</Button>}
        </div>
    )
}

function AppContent({ topicSelection1, topicSelection2, group }) {
    AppContent.propTypes = {
        topicSelection1: PropTypes.object.isRequired,
        topicSelection2: PropTypes.object,
        group: PropTypes.object.isRequired
    }
    return (
        <div className="content">
            <Row>
                <Col md="12" lg="6">
                    <Card style={{ minHeight: 350, maxHeight: 500, maxWidth: 600 }}>
                        <CardBody>
                            <StatusPanel topicSelection={topicSelection1} />
                        </CardBody>
                    </Card>
                </Col>
                {topicSelection2 && (
                <Col md="12" lg="6">
                    <Card style={{ minHeight: 350, maxHeight: 500, maxWidth: 600 }}>
                        <CardBody>
                            <StatusPanel topicSelection={topicSelection2} />
                        </CardBody>
                    </Card>
                </Col>
                )}
            </Row>

            <Row style={{ paddingTop: '2em' }}>
                <Col>
                    <Card>
                        <CardBody>
                            <Row style={{ marginTop: '-1.09375rem' }}>
                                <Col style={{ alignSelf: 'center', textAlign: 'left' }}><CardTitle style={{ marginTop: '0' }}>Data Kelompok</CardTitle></Col>
                            </Row>
                            <Row className="formComponentSpace">
                                <GroupInfo students={group.students} peminatan={group.peminatan} isTable={true} />
                            </Row>
                        </CardBody>
                    </Card>
                </Col>
            </Row>

            <Row style={{ paddingTop: '2em' }} />
            <div className="row">
                <ReselectButton firstStatus={topicSelection1.status} secondStatus={topicSelection2?.status} />
            </div>


        </div>
    )
}

function Page() {
    const { topicSelection1, topicSelection2, currentPeriod, group } = window.SAILS_LOCALS;
    return (
        <div className="page main-content-container px-4 pb-4 container-fluid">
            {/* <Navbar activeMenu={STUDENT_MENU.STATUS} /> */}
            <div className="container">
                <Title currentPeriod={currentPeriod} />
                {(topicSelection1) ?
                    (<AppContent topicSelection1={topicSelection1} topicSelection2={topicSelection2} group={group} />)
                    : <div>
                        <p>Anda belum memilih topik</p>
                        <Button theme="success" size="lg" className="btn btn-primary" href="/student/topic-selection/new">Pilih Topik</Button>
                        <Row style={{ paddingTop: '45em' }}></Row>
                    </div>
                }
            </div>
        </div>
    )
}

ReactDOM.render(<Layout><Page /></Layout>, document.getElementById('root'));
