import React from 'react';
import ReactDOM from 'react-dom';
import { Row, Card, CardBody, CardSubtitle, CardTitle, Col, Badge, Button, Collapse, Form, Progress } from 'shards-react';
import Layout from '../../components/layouts';

export default class Dashboard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      collapse: false
    };


    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  state = {
    selectedFile: null
  }


  fileHandler = event => {
    this.setState({
      selectedFile: event.target.files[0]
    })
  }


  uploadHandlerForm = () => {
    const fd = new FormData();
    fd.append('file_form', this.state.selectedFile, this.state.selectedFile.name);
    axios.post('/student/new-form', fd, {
      onUploadProgress: ProgressEvent => {
        console.log('Upload Progress: ' + Math.round(ProgressEvent.loaded / ProgressEvent.total * 100) + '%')
      }
    }).success(function (res) {
      console.log("UPLOAD SUCCESS" + res);
      document.getElementById('formklikdisini').innerHTML = "File telah di upload";
    }).error(function () {
      console.log("FILE SUDAH ADA")
    })
  }

  uploadHandlerTA = () => {
    const fd = new FormData();
    fd.append('file_ta', this.state.selectedFile, this.state.selectedFile.name);
    axios.post('/student/new-ta', fd, {

    });
  }

  uploadHandlerEPRT = () => {
    const fd = new FormData();
    fd.append('file_eprt', this.state.selectedFile, this.state.selectedFile.name);

    axios.post('student/new-ta', { fd, }, {

    });
  }





  render() {

    const { namaAnggota, topicSelection1, topicSelection2, allTopicSelections, reviewer } = window.SAILS_LOCALS;
    const topicSelections = [topicSelection1, topicSelection2]
    console.log(reviewer);
    const approvedTopicName = topicSelections.find(ts => ts?.status == "APPROVED_LAB_RISET")?.topic.name || '-'
    const approvedTopic = topicSelections.find(ts => ts?.status == "APPROVED")?.topic.name || '-'

    return (
      <div className="main-content-container px-4 pb-4 container-fluid" style={{ paddingTop: '2em' }}>
        <Row >
          <Col>
            <Card>
              <CardBody style={{ paddingBottom: '25px' }}>
                <CardSubtitle>Judul TA</CardSubtitle>
                <div style={{ alignSelf: 'center', textAlign: 'left' }}><CardTitle style={{ marginTop: '0' }}>{approvedTopicName != '-' ? approvedTopicName : approvedTopic != '-' ? approvedTopic : '-' }</CardTitle></div>
                <br />
                <CardSubtitle>Status</CardSubtitle>
                <div style={{ alignSelf: 'center', textAlign: 'left' }}><CardTitle style={{ marginTop: '0' }}>{(topicSelection1 || topicSelection2) ? (topicSelection1.status == 'APPROVED' || topicSelection1.status == 'APPROVED_LAB_RISET') ? <>Disetujui</> : (topicSelection2?.status == 'APPROVED' || topicSelection2?.status == 'APPROVED_LAB_RISET') ? <>Disetujui</> : topicSelection1.status == 'WAITING' ? <>sedang proses approval dosen</> : topicSelection2?.status == 'WAITING' ? <>sedang proses approval dosen</> : topicSelection1.status == 'REJECTED' ? <>Ditolak</> : topicSelection2?.status == 'REJECTED' ? <>Ditolak</> : topicSelection1.status == 'AUTO_CANCELED' ? <>Dibatalkan sebab topik opsi #1 tersetujui</> : topicSelection2?.status == 'AUTO_CANCELED' ? <>Dibatalkan sebab topik opsi #1 tersetujui</> : <a href="/student/topic-selection/new">Anda Belum Memilih Topik</a> : <a href="/student/topic-selection/new">Anda Belum Memilih Topik</a>}</CardTitle></div>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row style={{ paddingTop: '2em' }}>
          <Col md="12" lg="4">
            <Card>
              <CardBody style={{ paddingBottom: '25px' }}>
                <CardSubtitle>Dosen Pembimbing</CardSubtitle>
                <CardTitle style={{ marginBottom: '0', marginTop: '0.35em' }}>{(topicSelection1 || topicSelection2) ? (topicSelection1.status == 'APPROVED' || topicSelection1.status == 'APPROVED_LAB_RISET') ? topicSelection1.topic.lecturer.name : (topicSelection2?.status == 'APPROVED' || topicSelection2?.status == 'APPROVED_LAB_RISET') ? topicSelection2?.topic.lecturer.name : <>-</> : <>-</>}</CardTitle>

              </CardBody>
            </Card>
          </Col>
          <Col md="12" lg="4">
            <Card>
              <CardBody style={{ paddingBottom: '25px' }}>
                <CardSubtitle>Dosen Penguji</CardSubtitle>
                <CardTitle style={{ marginBottom: '0', marginTop: '0.35em' }}>{ reviewer?.lecturer?.name || '-'}</CardTitle>

              </CardBody>
            </Card>
          </Col>
          <Col md="12" lg="4">
            <Card className="btn" style={{ padding: 0 }} onClick={this.toggle}>
              <CardBody style={{ paddingBottom: '25px' }}>
                <Row style={{ marginTop: '-1.09375rem' }}>
                  <Col><CardSubtitle style={{ marginTop: '0' }}>Anggota Kelompok</CardSubtitle></Col>
                  <Col style={{ textAlign: '-webkit-right' }}>{this.state.collapse == false ? <i className="material-icons" style={{ fontSize: '2em', alignSelf: 'left', color: '#0ACF83' }}>arrow_drop_down_circle</i> : <div className="dontSeeAnggota" ></div>}</Col>
                </Row>

                <CardTitle style={{ marginBottom: '0', marginTop: '0.35em', textAlign: '-webkit-left' }}><span className="dot">1 </span> {namaAnggota[0]}</CardTitle>
                <Collapse open={this.state.collapse}>
                  {namaAnggota.map((nama, idx) =>
                    idx == 0 ? <></> :
                      <CardTitle key={idx} style={{ marginBottom: '0', marginTop: '0.35em', textAlign: '-webkit-left' }}><span className="dot">{idx + 1}</span> {nama}</CardTitle>
                  )}
                </Collapse>
              </CardBody>
            </Card>
          </Col>


        </Row>
        <Row style={{ paddingTop: '2em' }}>
          <Col>
            <CardSubtitle><h4>Riwayat</h4></CardSubtitle>
            {namaAnggota.length == 0 ? <p>Anda belum memilih topik TA Selection, silahkan <a href="/student/topic-selection/new">pilih topik</a>  </p> : allTopicSelections.map((value, index) => {
              return (<Card style={{ marginBottom: '20px' }}>
                <CardBody>
                  Group #{value.group} - {value.topic.name} <Badge theme={value.status == "WAITING" ? 'warning' : (value.status == "APPROVED" || value.status == "APPROVED_LAB_RISET") ? 'success' : value.status == 'REJECTED' ? 'danger' : 'danger'}>{value.status}</Badge> <a href={`/student/history-topic/${value.group}`} style={{ float: 'right' }}> <i className="material-icons" style={{ fontSize: '2em', alignSelf: 'left', color: 'black' }}>keyboard_arrow_right</i></a>
                </CardBody>
              </Card>)
            })}


          </Col>
        </Row>




      </div>
    );
  }
}

ReactDOM.render(<Layout role={window.SAILS_LOCALS.role} ><Dashboard /></Layout>, document.getElementById('root'));



