import React from 'react';
import ReactDOM from 'react-dom';
import { Row, Card, CardBody, CardSubtitle, CardTitle, Col, FormInput, Button, Collapse, Form } from 'shards-react';
import Layout from '../../components/layouts';

export default class Notification extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      collapse: false
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }


  render() {
   

    return (
      <div className="main-content-container px-4 pb-4 container-fluid" style={{ paddingTop: '2em' }}>

        <Col style={{paddingTop: '5em'}} >
            <Row style={{justifyContent: 'center'}}>
          <img src="/images/notif.svg"/></Row>
          <Row style={{justifyContent: 'center'}}>
          <p>tidak ada pemberitahuan</p>
          </Row>
        </Col>



      </div>
    );
  }
}

ReactDOM.render(<Layout><Notification /></Layout>, document.getElementById('root'));



