import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import ReactModal from 'react-modal';
import Pagination from '../../components/Pagination';
import PeriodTag from '../../components/PeriodTag';
import Layout from '../../components/layouts'
import { Row, Card, CardBody, CardTitle, Col, Button, Alert } from 'shards-react';
import EditableGroupTable from '../../components/group/EditableGroupTable';
import DeletableGroupRow from '../../components/group/DeletableGroupRow';
import NimForm from '../../components/group/NimForm';

global.$ = require('jquery');


function FinalDeletableGroupRow({ student, hasDeleteButton = false, onDeleteStudent }) {
    DeletableGroupRow.propTypes = {
        student: PropTypes.object.isRequired,
        hasDeleteButton: PropTypes.bool,
        onDeleteStudent: PropTypes.func.isRequired
    }
    const handleDeleteClick = () => {
        onDeleteStudent(student.id);
    }
    return (
        <tr>
            <td>{student.nim}</td>
            <td>{student.name}</td>
            <td>{student.class}</td>
            <td>{student.peminatan.abbrev}</td>
        </tr>
    )
}

class FinalGroupTable extends React.Component {
    constructor(props) {
        super(props);
        EditableGroupTable.propTypes = {
            user: PropTypes.object.isRequired,
            students: PropTypes.arrayOf(PropTypes.object).isRequired,
            onAddStudent: PropTypes.func.isRequired,
            onDeleteStudent: PropTypes.func.isRequired
        }
        this.handleHasStudent = this.handleHasStudent.bind(this);

    }

    handleHasStudent(nim) {
        const student = this.props.students.find(student => student.nim == nim);
        return !!student;
    }

    render() {
        const { students, user, onAddStudent, onDeleteStudent } = this.props;
        return (

            <div>
                <Card>
                    <CardBody>
                        <Row style={{ marginTop: '-1.09375rem' }}>
                            <Col style={{ alignSelf: 'center', textAlign: 'left' }}><CardTitle style={{ marginTop: '0' }}>Daftar Anggota Kelompok</CardTitle></Col>
                        </Row>
                        <Row className="formComponentSpace">
                        <table className="table">
                    <thead>
                        <tr>
                            <th>NIM</th>
                            <th>Nama</th>
                            <th>Kelas</th>
                            <th>Peminatan</th>
                        </tr>

                    </thead>
                    <tbody>
                        {students.map(student => {
                            return (
                                <DeletableGroupRow
                                    key={student.id}
                                    student={student}
                                    onDeleteStudent={onDeleteStudent}
                                    hasDeleteButton={student.id != user.id}
                                    step={3}
                                />
                            )
                        })}
                    </tbody>
                </table>
                        </Row>
                    </CardBody>
                </Card>
                
            </div>
        )
    }
}

function SelectButton({ topic, topicOptionNum, selectTopic }) {
    SelectButton.propTypes = {
        topic: PropTypes.object.isRequired,
        topicOptionNum: PropTypes.oneOf([1, 2]),
        selectTopic: PropTypes.func.isRequired
    }
    const handleChange = (e) => {
        const optionNum = e.target.value;
        selectTopic(optionNum, topic);
    }
    return (
        <select className="" value={topicOptionNum || ""} onChange={handleChange}>
            <option value="" defaultValue>Pilih</option>
            <option value="1">Opsi #1</option>
            <option value="2">Opsi #2</option>
        </select>
    )
}

function SelectableTopicRow({ topic, topicOptionNum, selectTopic }) {
    SelectableTopicRow.propTypes = {
        topic: PropTypes.object.isRequired,
        topicOptionNum: PropTypes.oneOf([1, 2]),
        selectTopic: PropTypes.func.isRequired,
    }
    return (
        <tr>
            <td>{topic.name}</td>
            <td className='wrap'>{topic.deskripsi}</td>
            <td>
                {topic.lecturer.name}
                {/* <LecturerInfo
                    nik={topic.lecturer.nik}
                    name={topic.lecturer.name}
                    lecturerCode={topic.lecturer.lecturerCode}
                /> */}
            </td>
            <td>{topic.quota}</td>
            <td>
                <SelectButton
                    topic={topic}
                    topicOptionNum={topicOptionNum}
                    selectTopic={selectTopic}
                />
            </td>
        </tr>
    );
}

class SelectableTopicTable extends React.Component {
    constructor(props) {
        super(props);
        SelectableTopicTable.propTypes = {
            peminatanId: PropTypes.number.isRequired,
            totalStudents: PropTypes.number.isRequired,
            selectTopic: PropTypes.func.isRequired,
            topicOpt1Id: PropTypes.number,
            topicOpt2Id: PropTypes.number,
        }
        SelectableTopicTable.defaultProps = {
            totalStudents: 0
        }
        this.state = {
            topics: [],
            pageNum: 1,
            maxPage: 10,
            maxRow: 25,
            isLoading: false
        }
        this.handleMaxRowChange = this.handleMaxRowChange.bind(this);
        this.handlePageNumChange = this.handlePageNumChange.bind(this);
    }

    fetchTopics() {
        this.setState({ isLoading: true });
        const { pageNum, maxRow } = this.state;
        const { totalStudents, peminatanId } = this.props;
        const getTopicsParams = new URLSearchParams(
            {
                pageNum,
                maxRow,
                minQuota: totalStudents,
                peminatanId: peminatanId
            }
        );
        fetch('/data/active-topics?' + getTopicsParams.toString()).then(res => res.json())
            .then(res => this.setState({ topics: res.topics, isLoading: false }));
    }

    componentDidMount() {
        this.fetchTopics();
        const { totalStudents, peminatanId } = this.props;
        const getTopicsParams = new URLSearchParams(
            {
                minQuota: totalStudents,
                peminatanId: peminatanId
            }
        );
        const maxRow = this.state.maxPage;
        const getMaxPage = topicsCount => Math.floor(topicsCount / maxRow) || 1;
        fetch('/data/topics/count?' + getTopicsParams.toString())
            .then(res => res.json())
            .then(({ topicsCount }) => this.setState({ maxPage: getMaxPage(topicsCount) }));
        $(function () {
            $('[data-toggle="popover"]').popover()
        })
    }

    componentDidUpdate() {
        $(function () {
            $('[data-toggle="popover"]').popover()
        })
    }

    handlePageNumChange(pageNum) {
        this.fetchTopics();
        this.setState({ pageNum });
    }

    handleMaxRowChange(maxRow) {
        this.fetchTopics();
        this.setState({ maxRow });
    }

    render() {
        const { topicOpt1Id, topicOpt2Id, selectTopic } = this.props;
        return (
            <div className="selectable-topic-table">
                <div className="">
                    
                    
                <Card>
                    <CardBody>
                        
                        <Row className="formComponentSpace">
                        <table id="topic-selection-table" className="table">
                        <thead>
                            <tr>
                                <th width="400px">Topik</th>
                                <th width="400px">Deskripsi</th>
                                <th>Dosen</th>
                                <th>Quota Topik</th>
                                <th>Pilih</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.topics && this.state.topics.map(topic => {
                                let topicOptionNum;
                                if (topic.id == topicOpt1Id) topicOptionNum = 1;
                                else if (topic.id == topicOpt2Id) topicOptionNum = 2;
                                return (
                                    <SelectableTopicRow
                                        key={topic.id}
                                        topic={topic}
                                        topicOptionNum={topicOptionNum}
                                        selectTopic={selectTopic}
                                    />
                                )
                            })}
                        </tbody>
                    </table>
                        </Row>
                    </CardBody>
                    
                </Card>
                </div>
                <Row style={{ paddingTop: '2em' }}></Row>
                <Pagination
                    pageNum={this.state.pageNum}
                    maxPage={this.state.maxPage}
                    maxRow={this.state.maxRow}
                    onPageNumChange={this.handlePageNumChange}
                    onMaxRowChange={this.handleMaxRowChange}
                />
            </div>
        );
    }
}


function TopicSelectionControl({ topicOpt1, topicOpt2, selectTopic }) {
    TopicSelectionControl.propTypes = {
        topicOpt1: PropTypes.object,
        topicOpt2: PropTypes.object,
        selectTopic: PropTypes.func.isRequired
    }
    const handleSwitch = (e) => {
        selectTopic(1, topicOpt2);
        selectTopic(2, topicOpt1);
    }
    const handleDeleteOpt1 = () => {
        selectTopic(1, null);
    }
    const handleDeleteOpt2 = () => {
        selectTopic(2, null);
    }
    return (


        
        <div className="form-horizontal">

            <Row style={{ paddingTop: '1em' }}>
                <Col md="12" lg="9">
                    <Card>
                        <CardBody>
                            <Row style={{ marginTop: '-1.09375rem' }}>
                                <Col style={{ alignSelf: 'center', textAlign: 'left' }}><CardTitle style={{ marginTop: '0' }}>Topik Terpilih</CardTitle></Col>
                            </Row>
                            <Row className="formComponentSpace">
                                <label className="control-label col-sm-2" htmlFor="topicOpt1">Opsi #1</label>
                            </Row>
                            <Row className="formComponentSpace">
                            <Col><p className="form-control-static">{topicOpt1 ? topicOpt1.name : 'Belum memilih'}</p></Col>
                            <Col>{topicOpt1 && <Button theme="danger" className="btn btn-default" type="button" onClick={handleDeleteOpt1} >Hapus</Button>}</Col>
                            </Row>
                            <Row className="formComponentSpace">
                                <label className="control-label col-sm-2" htmlFor="topicOpt1">Opsi #2</label>
                            </Row>
                            <Row className="formComponentSpace">
                            <Col><p className="form-control-static">{topicOpt2 ? topicOpt2.name : 'Belum memilih'}</p></Col>
                            <Col>{topicOpt2 && <Button theme="danger" className="btn btn-default" type="button" onClick={handleDeleteOpt2} >Hapus</Button>}</Col>
                            </Row>

                            <Row className="formComponentSpace">
                                <Col><Button className={`btn btn-default ${(topicOpt1 && topicOpt2) ? '' : 'sr-only'}`} type="button" onClick={handleSwitch}>Tukar</Button></Col>
                            </Row>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
            


            
        </div>
    )
}

function TopicSelectionReview({
    topicOpt1,
    topicOpt2,
    students,
    onConfirm,
    onCancel
}) {
    TopicSelectionReview.propTypes = {
        topicOpt1: PropTypes.object.isRequired,
        topicOpt2: PropTypes.object.isRequired,
        students: PropTypes.arrayOf(PropTypes.object).isRequired,
        onConfirm: PropTypes.func.isRequired,
        onCancel: PropTypes.func.isRequired
    }
    return (
        <div className="container text-center">
            <h1>Periksa Pilihan</h1>

            <div className="text-center">
                <table class="table">
                    <thead>
                        <tr>
                            <th>NIM</th>
                            <th>Nama</th>
                            <th>Kelas</th>
                            <th>Peminatan</th>
                        </tr>
                    </thead>
                    <tbody>
                        {students.map(student => (
                            <tr>
                                <td>{student.nim}</td>
                                <td>{student.name}</td>
                                <td>{student.class}</td>
                                <td>{student.peminatan.abbrev}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>

                <div className="row">
                    <table id="final-topic-table" class="table">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Topik</th>
                                <th>Dosen</th>
                                <th>Quota Topik</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>1</th>
                                <th>{topicOpt1.name}</th>
                                <th>{topicOpt1.lecturer.name}</th>
                                <th>{topicOpt1.quota}</th>
                                <th>1</th>
                            </tr>
                            {topicOpt2 && (
                                <tr>
                                    <th>2</th>
                                    <th>{topicOpt2.name}</th>
                                    <th>{topicOpt2.lecturer.name}</th>
                                    <th>{topicOpt2.quota}</th>
                                    <th>2</th>
                                </tr>
                            )}
                        </tbody>
                    </table>
                    {/* <dl className="dl-horizontal">
                        <dt>Topik opsi #1</dt>
                        <dd>{topicOpt1.name}</dd>
                        <dt>Topik opsi #2</dt>
                        <dd>{topicOpt2.name}</dd>
                    </dl> */}
                </div>


                <p>Apakah anda yakin?</p>
                <div className="form-inline float-md-left">
                    <Button size="lg" type="button" className="btn btn-default" onClick={onCancel} theme="danger">Tidak</Button>
                </div>
                <div className="form-inline float-md-right">
                    <Button size="lg" type="button" className="btn btn-primary" onClick={onConfirm} theme="success">Yakin</Button>
                </div>
                
            </div>
        </div>
    )
}

function FormErrorMessages({ errorMessages }) {
    FormErrorMessages.propTypes = {
        errorMessages: PropTypes.object
    }
    return (
        <div>
            {
                Object.entries(errorMessages).map(([field, message]) => {
                    return <p key={field} className="text-danger">{message}</p>
                })
            }
        </div>
    )
}

class TopicSelectionForm extends React.Component {
    constructor(props) {
        super(props);
        TopicSelectionForm.propTypes = {
            user: PropTypes.object.isRequired,
            prevGroupStudents: PropTypes.arrayOf(PropTypes.object),
        }
        this.state = {
            step: 1,
            topicOpt1: undefined,
            topicOpt2: undefined,
            students: this.props.prevGroupStudents ? this.props.prevGroupStudents : [this.props.user],
            errorMessages: {},
            isReviewModalOpen: false
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleAddStudent = this.handleAddStudent.bind(this);
        this.handleDeleteStudent = this.handleDeleteStudent.bind(this);
        this.hasStudent = this.hasStudent.bind(this);
        this.selectTopic = this.selectTopic.bind(this);
        this.validate = this.validate.bind(this);
        this.showReviewModal = this.showReviewModal.bind(this);
        this.closeReviewModal = this.closeReviewModal.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillMount() {
        this.setState((state, props) => {
            if (props.prevGroupStudents) {
                return { students: props.prevGroupStudents }
            }
        })
    }

    handleChange(e) {
        const name = e.target.name;
        const value = e.target.value;
        const parsedValue = isNaN(Number(value)) ? value : Number(value); //parse value kalo bisa diparse ke integer
        this.setState({ [name]: parsedValue });
    }

    handleAddStudent(student) {
        this.setState(prevState => ({ students: [student, ...prevState.students] }))
    }


    handleDeleteStudent(studentId) {
        console.log("DELETE")
        this.setState(prevState => {
            let students = prevState.students;
            const deleteIndex = students.findIndex(student => student.id == studentId);
            students.splice(deleteIndex, 1);
            return { students }
        });
    }

    selectTopic(optionNum, topic) {
        this.setState({ [`topicOpt${optionNum}`]: topic });
    }

    validate() {
        const { topicOpt1, topicOpt2, students } = this.state;
        this.setState({
            errorMessages: {
                topicOpt1: !topicOpt1 && 'Anda harus memilih topik opsi #1',
            }
        });
        if (!topicOpt1) return false;
        else return true;
    }

    showReviewModal() {
        if (this.validate()) {
            this.setState({ isReviewModalOpen: true });
        }
    }

    closeReviewModal() {
        this.setState({ isReviewModalOpen: false })
    }

    hasStudent(nim) {
        const student = this.state.students.find(student => student.nim == nim);
        return !!student;
    }

    handleSubmit(e) {
        const {
            topicOpt1,
            topicOpt2,
            students
        } = this.state;
        const studentIds = students.map(s => s.id);
        fetch('/student/new-topic-selection', {
            method: "POST",
            body: JSON.stringify({
                topicOpt1Id: topicOpt1 && topicOpt1.id,
                topicOpt2Id: topicOpt2 && topicOpt2.id,
                studentIds
            }),
        })
            .then(res => {
                const exitCode = res.headers.get('X-Exit');
                switch (exitCode) {
                    case 'success':
                        window.location.href = res.url;
                        break;
                    case 'someAlreadySubmitted':
                        this.closeReviewModal();
                        alert('Tidak bisa mengajukan topik. Ada anggota kelompok yang sudah mengajukan topik.');
                        break;
                    case 'topicQuotaExceeded':
                        this.closeReviewModal();
                        alert('Tidak bisa mengajukan topik. Jumlah anggota kelompok lebih besar dari topik yang anda pilih.');
                        break;
                    default:
                        alert('Generic error: ' + exitCode);
                }
            })
    }

    nextStep = () => {
        const { step } = this.state;
        this.setState({
            step: step + 1
        });
    }

    prevStep = () => {
        const { step } = this.state;
        this.setState({
            step: step - 1
        });
    }

    continue = e => {
        e.preventDefault();
        this.nextStep();
    }





    render() {
        const { user } = this.props;
        const { step } = this.state;
        if (step === 1)
            return (

                <div className="main-content-container px-4 pb-4 container-fluid" style={{ paddingTop: '2em' }}>
                    <h2>Step 1: Tambah Anggota</h2>
                    <div className="main-content-container px-4 pb-4 container-fluid" style={{ paddingTop: '2em' }}>
                        <Row style={{paddingTop: '2em'}}>
                            <div className="col-sm-12 col-md-6">
                                <NimForm
                                    peminatanId={this.props.user.peminatan.id}
                                    hasStudent={this.hasStudent}
                                    onAddStudent={this.handleAddStudent}
                                    validation={(student, queryNim) => {
                                        if (student.waitingOrApprovedTopics && (student.waitingOrApprovedTopics.length > 0) ) {
                                            const nama1 = student.name;
                                            return `${nama1} / ${queryNim} sudah memiliki pengajuan Topik `;
                                        }
                                    }}
                                />
                            </div>
                        </Row>
                        <Row style={{marginTop: '2rem'}}></Row>
                        <EditableGroupTable
                            ownerId={user.id}
                            students={this.state.students}
                            onAddStudent={this.handleAddStudent}
                            onDeleteStudent={this.handleDeleteStudent}
                            peminatan={user.peminatan}
                        />
                    </div>
                    <div align="right">
                    <Button size="lg" theme="dark" onClick={this.nextStep} >
                        Next
            </Button>
            </div>
                </div>

            );
        if (step === 2)
            return (

                <div className="main-content-container px-4 pb-4 container-fluid" style={{ paddingTop: '2em' }}>
                    <h2>Step 2: Pemilihan Topik</h2>

                    <SelectableTopicTable
                        peminatanId={user.peminatan.id}
                        topicOpt1Id={this.state.topicOpt1 && this.state.topicOpt1.id}
                        topicOpt2Id={this.state.topicOpt2 && this.state.topicOpt2.id}
                        totalStudents={this.state.students.length}
                        selectTopic={this.selectTopic}
                    />

                    
                    
                    <Button size="lg" className="btn-ijo" onClick={this.prevStep}>
                        Back
                    </Button>

                    <Button size="lg" theme="dark float-right" onClick={this.nextStep}> Next </Button>

                </div>

            );
        if(step===3)
        return (
            <div className="container">
                <h1>Step 3: Hasil Akhir</h1>

                <h3>Hasil Akhir Anggota</h3>

                

                    <FinalGroupTable
                        user={user}
                        students={this.state.students}
                        onAddStudent={this.handleAddStudent}
                        onDeleteStudent={this.handleDeleteStudent}
                    />
                    <Row style={{ paddingTop: '2em' }}></Row>
                    <h3>Hasil Akhir Ajukan Topik</h3>

                    <TopicSelectionControl
                        topicOpt1={this.state.topicOpt1}
                        topicOpt2={this.state.topicOpt2}
                        selectTopic={this.selectTopic}
                    />

                <Row style={{ paddingTop: '2em' }} ></Row>
                <Button size="lg" className="btn-ijo" onClick={this.prevStep}>Back</Button>
                <Button size="lg" theme="dark float-right" onClick={this.showReviewModal}> Ajukan </Button>

                <form>
                        <div className="col-xs-10">
                            <FormErrorMessages errorMessages={this.state.errorMessages} />
                        </div>

                    {
                        <ReactModal
                            appElement={document.getElementById('root')}
                            isOpen={this.state.isReviewModalOpen}
                            onRequestClose={this.closeReviewModal}
                            className="mymodal"
                            overlayClassName="myoverlay"
                        >
                            <TopicSelectionReview
                                students={this.state.students}
                                topicOpt1={this.state.topicOpt1}
                                topicOpt2={this.state.topicOpt2}
                                onConfirm={this.handleSubmit}
                                onCancel={this.closeReviewModal}
                            />
                        </ReactModal>
                    }
                </form>

            </div>


        );


        // return (
        //     <div className="container">
        //         <form>
        //                 <div className="col-xs-10">
        //                     <FormErrorMessages errorMessages={this.state.errorMessages} />
        //                 </div>

        //             {
        //                 <ReactModal
        //                     appElement={document.getElementById('root')}
        //                     isOpen={this.state.isReviewModalOpen}
        //                     onRequestClose={this.closeReviewModal}
        //                 >
        //                     <TopicSelectionReview
        //                         students={this.state.students}
        //                         topicOpt1={this.state.topicOpt1}
        //                         topicOpt2={this.state.topicOpt2}
        //                         onConfirm={this.handleSubmit}
        //                         onCancel={this.closeReviewModal}
        //                     />
        //                 </ReactModal>
        //             }
        //         </form>
        //     </div>
        // );
    }
}

function Page() {
    const { prevGroupStudents, currentPeriod, user, hasSelectedTopic } = window.SAILS_LOCALS;
    return (
        <div className="page main-content-container px-4 pb-4 container-fluid">
            {/* <Navbar activeMenu={STUDENT_MENU.SELECT_TOPIC} /> */}
                <h1>Pemilihan Topik <PeriodTag period={currentPeriod} /></h1>

                {hasSelectedTopic
                    ? 'Anda sudah mengajukan pilihan topik' :
                    user.peminatan ? <TopicSelectionForm
                    user={user}
                    prevGroupStudents={prevGroupStudents}
                /> : <Alert theme="danger">
                Belum Ada Peminatan
               </Alert>
                }
            
        </div>
    )
}
ReactDOM.render(
    <Layout>
    <Page
    /></Layout>,
    document.getElementById("root")
);
