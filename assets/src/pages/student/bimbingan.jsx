import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import Layout from '../../components/layouts';
import {
    Card,
    CardTitle,
    CardBody,
    Button,
    Modal,
    ModalHeader
} from "shards-react";


class BimbinganForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: props.defaultBimbingan && props.defaultBimbingan.id,
            tanggal: props.defaultBimbingan && props.defaultBimbingan.tanggal,
            catatan: props.defaultBimbingan && props.defaultBimbingan.catatan,
            studentId : window.SAILS_LOCALS.studentId,
            status : props.defaultBimbingan && 'PROCESSED'
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }
    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }
    handleSubmit(e) {
        e.preventDefault();
        if (this.props.mode == 'NEW') {
            fetch('/bimbingan', {
                method: 'POST',
                body: JSON.stringify(this.state)
            }).then(res => {
                window.location.reload(true);
            })
        } else if (this.props.mode == 'EDIT') {
            const data = { ...this.state, id: this.props.id }
            fetch(`/bimbingan/${this.props.id}`, {
                method: 'PATCH',
                body: JSON.stringify(data)
            }).then(res => {
                window.location.reload(true);
            })
        }
    }
    render() {
        return (
            <form
                className="form-horizontal"
                method="POST"
                onSubmit={this.handleSubmit}
            >
                <div className="form-group">
                    <label className="col-sm-2 control-label" htmlFor="tanggal">
                        Tanggal
                    </label>
                    <div className="col-sm-6">
                        <input
                            className="form-control"
                            type="date"
                            name="tanggal"
                            value={this.state.tanggal}
                            
                            onChange={this.handleChange}
                        />
                    </div>
                </div>
                <div className="form-group">
                    <label className="col-sm-2 control-label" htmlFor="catatan">
                       Catatan
                    </label>
                    <div className="col-sm-6">
                        <textarea
                            className="form-control"
                            rows={3}
                            name="catatan"
                            value={this.state.catatan}
                            onChange={this.handleChange}
                        />
                    </div>
                </div>
                <div className="form-group">
                    <div className="col-sm-offset-2 col-sm-10">
                        <Button type="submit">
                            Submit
                        </Button>
                    </div>
                </div>
            </form>
        )
    }
}
BimbinganForm.propTypes = {
    mode: PropTypes.oneOf(['NEW', 'EDIT', '']),
    id: PropTypes.number,
    defaultBimbingan: PropTypes.object
}

class EditableBimbinganTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            mode: '',
            rowToEdit: {},
        }
        this.handleNew = this.handleNew.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.closeModal = this.closeModal.bind(this);

    }
    handleNew() {
        this.setState({ mode: 'NEW', rowToEdit: {} });
    }
    handleEdit(bimbinganId) {
        const rowToEdit = this.props.bimbinganList.find(bimbingan => bimbingan.id == bimbinganId);
        this.setState({ rowToEdit, mode: 'EDIT' });
    }

    handleDelete(bimbinganId) {
        const bimbingan = this.props.bimbinganList.find(bimbingan => bimbingan.id == bimbinganId);
        const shouldDelete = confirm(`Apakah anda yakin akan menghapus data bimbingan?`);
        if (shouldDelete) {
            fetch('/student/delete-bimbingan', {
                method: 'POST',
                body: JSON.stringify({ id: bimbinganId })
            }).then(res => {
                const exitCode = res.headers.get('X-Exit');
                if (exitCode == 'success') {
                    window.location.reload(true);
                }
            })
        }
    }
    closeModal() {
        this.setState({ mode: '', rowToEdit: {} });
    }
    render() {
        const bimbinganList = this.props.bimbinganList;
        const bimbinganIds = bimbinganList.map(bimbingan => bimbingan.id);
        const bimbinganTuples = bimbinganList.map(bimbingan => [bimbingan.tanggal, bimbingan.catatan, bimbingan.status]);
        const approveBtnClass =  'btn-success';
        const rejectBtnClass = 'btn-danger';
        return (
            <div>
                <Button onClick={this.handleNew}>Tambah Catatan Bimbingan</Button>
                {/* <EditableTable
                    columns={['Tanggal', 'Deskripsi', 'Status']}
                    tuples={bimbinganTuples}
                    rowIds={bimbinganIds}
                    onRowDelete={this.handleDelete}
                    onRowEdit={this.handleEdit}
                /> */}
                
                <div className="table-scroll">
                <table class="table" id="table-bimbingan">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Catatan</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    {/* {bimbinganList.map(bimbingan)=>{
                            <tr>{bimbingan.tanggal}</tr>
                            <tr>{bimbingan.deskripsi}</tr>
                            <tr>{bimbingan.status}</tr>
                        }} */}
                        {bimbinganList.map(bimbingan=>{
                            return(
                                <tr>
                                <td>{bimbingan.tanggal}</td>
                                <td>{bimbingan.catatan}</td>
                                <td>{bimbingan.status}</td>
                                                        
                               {bimbingan.status=="PROCESSED" ?
                                <td className="btn-group" role="group">
                                <button type="button" className={`btn ${approveBtnClass}`} onClick={()=>this.handleEdit(bimbingan.id)} >Ubah</button>
                                <button type="button" className={`btn ${rejectBtnClass}`} onClick={()=>this.handleDelete(bimbingan.id)} >Hapus</button>
                                </td>
                                :
                                <td>
                                    <button type="button" className={`btn ${rejectBtnClass}`} onClick={()=>this.handleDelete(bimbingan.id)} >Hapus</button>
                                </td>
                                }

                            </tr>
                            )
                            
                        })}
                    </tbody>
                </table>
                
             </div>
                <Modal
                    appElement={document.getElementById('root')}
                    open={!!this.state.mode}
                    toggle={this.closeModal}
                    modalClassName="modal"
                >
                
                <ModalHeader>Bimbingan Online</ModalHeader>
                    <BimbinganForm
                        mode={this.state.mode}
                        id={this.state.rowToEdit.id}
                        defaultBimbingan={this.state.rowToEdit}
                        // defaultGroupStudent = {SAILS_LOCALS.groupStudentId}
                    />
                </Modal>
            </div>
        )
    }
}

EditableBimbinganTable.propTypes = {
    bimbinganList: PropTypes.arrayOf(PropTypes.object)
}

function Page() {
    const bimbinganList = window.SAILS_LOCALS.bimbingan;
    return (
        <div id="page" className="page main-content-container px-4 pb-4 container-fluid">
         <div className="container">
         <Card>
            <CardBody>
                <CardTitle>Bimbingan Online</CardTitle>
                <EditableBimbinganTable bimbinganList={bimbinganList} />
            </CardBody>
        </Card>
     </div>
     </div>
    )
}
ReactDOM.render(<Layout role={window.SAILS_LOCALS.role} ><Page/></Layout>, document.getElementById('root'));