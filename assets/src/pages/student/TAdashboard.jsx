import React from 'react';
import ReactDOM from 'react-dom';
import { Row, Card, CardBody, CardSubtitle, CardTitle, Col, FormInput, Button, Collapse, Form, Progress } from 'shards-react';
import Layout from '../../components/layouts';

class TADashboard extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="main-content-container px-4 pb-4 container-fluid" style={{ paddingTop: '2em' }}>
        <Row>
          <Col md="12">
            <Card>
              <CardBody style={{ paddingBottom: '25px' }}>
                <CardSubtitle>Daftar Topik</CardSubtitle>
                <CardTitle style={{ marginBottom: '0', marginTop: '0.35em' }}>-</CardTitle>
                <table id="topic-table">
                <thead>
                    <tr>
                        <th bgcolor="#E8E8E8">Topik</th>
                    </tr>
                </thead>
  
            </table>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row style={{ paddingTop: '2em' }}>
          <Col md="12" lg="6">
            <Card>
              <CardBody style={{ paddingBottom: '25px' }}>
                <CardSubtitle>Status</CardSubtitle>
                <CardTitle style={{ marginBottom: '0', marginTop: '0.35em' }}>Dr. Edi Suheri, ST, S.Kom (ESH)</CardTitle>

              </CardBody>
            </Card>
          </Col>
          <Col md="12" lg="6">
            <Card>
              <CardBody style={{ paddingBottom: '25px' }}>
                <CardSubtitle>Pilih Topik</CardSubtitle>
                <CardTitle style={{ marginBottom: '0', marginTop: '0.35em' }}>ANDA BELUM MEMILIH TOPIK</CardTitle>

              </CardBody>
            </Card>
          </Col>
        </Row>

      </div>
    )
  }
}

ReactDOM.render(<Layout><TADashboard /></Layout>, document.getElementById('root'));



