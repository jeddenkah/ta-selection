import React from 'react';
import ReactDOM from 'react-dom';
import Layout from '../../components/layouts';
import {  Button } from 'shards-react';
import RadioButtonScore from '../../components/lecturer/RadioButtonScore';
import {
    BrowserRouter as Router,
    Switch,
    Redirect
} from "react-router-dom";
  
  class EvaluationScore extends React.Component {
    render() {
        const {studentId,lecturer,cloAll, scoreAll} = this.props;
        const number = 0;
        const subCloTuples = cloAll.map(clo => [clo]);
        
        const scoreTuples = scoreAll.map(score => [score]);
       
        const url = document.referrer
        return(
            <RadioButtonScore scoreTuples={scoreTuples} subCloTuples={subCloTuples} lecturer={lecturer} student={studentId} url={url}/>
            //console.log(subCloTuples)
        )       
        }
  }

  function Page() {
    const {studentId,lecturer,cloAll,subCloAll,ta,scoreAll,bimbingan} = window.SAILS_LOCALS;
    const fileTaUrl = ta[0]?.taUrl; 
    return (
        <React.Fragment>
            {/* <Navbar activeMenu={LECTURER_MENU.LIST}/> */}
            <div className="container main-content-container px-4 pb-4 container-fluid">
                
                {ta.length>0 ? bimbingan.length>7 ?
                <>
                <h1>File TA</h1>
                <a href={fileTaUrl}><Button className="btn btn-primary">Lihat File TA</Button></a>
                <h1>Nilai</h1>
                <EvaluationScore studentId={studentId} lecturer={lecturer} cloAll={cloAll} scoreAll={scoreAll}/>
                </>
                :
                <>{
                    alert("Mahasiswa Belum Memenuhi Kriteria Jumlah Bimbingan")
                }
                <Router forceRefresh={true}>
                <Switch>
                <Redirect to="/lecturer/dosen-pembimbing"/>
                </Switch>
                </Router>
                </>
                :
                <>{
                    alert("Mahasiswa Belum Mengunggah Berkas TA")
                }
                <Router forceRefresh={true}>
                <Switch>
                <Redirect to="/lecturer/dosen-pembimbing"/>
                </Switch>
                </Router>
                </>
                }
            </div>
        </React.Fragment>
    )
}
  
  /*
   * Render the above component into the div#app
   */
  ReactDOM.render(
    <Layout>
       <Page/></Layout>,
    document.getElementById("root")
);