import React from 'react';
import ReactDOM from 'react-dom';
import ReactModal from 'react-modal';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import Navbar, {MENU as NAVBAR_MENU} from '../../components/Navbar';
import Nav, {MENU as LECTURER_MENU} from '../../components/lecturer/Nav';
import PeriodSelect from '../../components/PeriodSelect';
import ProdiSelect from '../../components/ProdiSelect';
import BobotDosen from '../../components/lecturer/BobotDosen';
import handleFilterChange from '../../components/handleFilterChange';
import Layout from '../../components/layouts';
import { Row, Card, CardBody, CardSubtitle, CardTitle, Col, FormInput, Button, Collapse, Form, Progress,FormTextarea, FormGroup } from 'shards-react';
const Plo = require('../../../../api/models/Plo');
const Clo = require('../../../../api/models/Clo');
let SubClo = require('../../../../api/models/SubClo');
SubClo.attributes['subCloRubrics'] = {collection: 'subclorubric'}
import ModelTable from '../../components/ModelTable';
import CloForm from '../../components/CloForm';
import SubCloRubricList from '../../components/admin/SubCloRubricList';
import EditableTable from '../../components/EditableTable';
var ploModel = require('../../../../api/models/Plo')
var cloModel = require('../../../../api/models/Clo')
import PloForm from '../../components/PloForm'

class KoorMetlit extends React.Component {
    constructor(props) {
        super(props);
        const searchParams = new URLSearchParams(window.location.search);
        const periodId = searchParams.get('periodId');
        const prodiId= searchParams.get('prodiId');
        const currentPeriodId = window.SAILS_LOCALS.currentPeriodId;
        const currentBobot = periodId ? props.bobotlist?.find(bobotlist => bobotlist.period == periodId) : props.bobotlist?.find(bobotlist => bobotlist.period == currentPeriodId);
        
        this.state = {
            //props.bobot ? 
            bobotPembina: currentBobot?.dosenPembimbing ?? 0,
            bobotPenguji: currentBobot?.dosenPenguji ?? 0,
            // bobotPembina : props.bobot && props.bobot.dosenPembimbing,
            // bobotPenguji: props.bobot && props.bobot.dosenPenguji,
        };

        this.showReviewModal = this.showReviewModal.bind(this);
        this.closeReviewModal = this.closeReviewModal.bind(this);
        // this.handleChange = this.handleChange.bind(this);
        
        this.handleChangePeriod = this.handleChangePeriod.bind(this);
        //this.validate = this.validate.bind(this);
        //this.handleSubmit = this.handleSubmit.bind(this);
    }

    showReviewModal() {
        if (this.validate()) {
            this.setState({ isReviewModalOpen: true });
        }
    }

    closeReviewModal() {
        this.setState({ isReviewModalOpen: false,
            name: '',
            quota: 5,
            peminatanId: '',
            deskripsi:'',
        })
        
    }


    handleChangePeriod(e) {
        handleFilterChange(e)
    }
        
    render() {
        const {currentPeriod,bobotlist} = this.props;
        const {bobotPembina, bobotPenguji} = this.state;
        const searchParams = new URLSearchParams(window.location.search);
        const prodiId = searchParams.get('prodiId');
        const periodId = searchParams.get('periodId');
        const homePath = '/lecturer/koor-metlit';
        const pageURL = window.location.href;
        const tab = pageURL.substr(pageURL.lastIndexOf('/') + 1);
        const currentPeriodId = window.SAILS_LOCALS.currentPeriodId;    
        const { plo,ploList } = this.props;
        const ploIds = ploList.map(plo => plo.id);
        const ploTuples = ploList.map(plo => [plo.ploCode,plo.description]);  
        const cloIds = ploList.map(clo => clo.cloList.id);
        const cloListAll= ploList.map(clo => clo.cloList);
        //var listcoba = []
        var cloList=[]
        cloListAll.forEach((listAll,i)=>{
            cloListAll[i].forEach((list,j)=>{
              cloList.push(cloListAll[i][j])
            })
          })
        
        return (

            <div>
                
                <form>
                <div className="form-group">
                    <label htmlFor="peminatanId">Prodi</label>
                    <ProdiSelect
                        name="prodiId"
                        value={prodiId}
                        prodiList={this.props.prodiList}
                        onChange={handleFilterChange}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="periodId">Periode</label>
                    <PeriodSelect
                        name="periodId"
                        value={periodId}
                        periods={this.props.periods}
                        onChange={handleFilterChange}
                    />
                </div>
                </form>
                <Row>
                <Col>
                <Row className="container">
                <div style={{paddingLeft: '1em',paddingTop: '2em'}}>
                    <Button outline pill tag="a"  href={homePath+'/CLO'} theme="success" onClick={this.handleChangeTab} active={tab == 'CLO'}>
                        CLO
                    </Button>
                    </div>
                    <div style={{paddingLeft: '1em',paddingTop: '2em'}}>
                    <Button outline pill tag="a"  href={homePath+'/Plo'} theme="success" onClick={this.handleChangeTab} active={tab == 'PLO'}>
                        PLO
                    </Button>
                    </div>
                    <div style={{paddingLeft: '1em',paddingTop: '2em'}}>
                    <Button outline pill tag="a"  href={homePath+'/bobot'} theme="success" onClick={this.handleChangeTab} active={tab == 'bobot'}>
                        Bobot nilai dosen
                    </Button>
                    </div>
                </Row>
                
                <Switch>
                    <Route path={homePath + "/Clo"}>
                        <div style={{paddingTop: '2em'}}>
                        <ModelTable
                            id="clo-table"
                            model={Clo}
                            modelName="clo"
                            records={cloList}
                            customForm={CloForm}
                            hideColumns= {['isDeleted']}
                            renderColumn={{
                                plo: data => data.plo?.ploCode ?? '-'
                            }}
                            referencedModelNames={['plo']}
                        /> 
                        {/* {console.log(listcoba)} */}
                        </div>
                        </Route>
                        <Route path={homePath + "/Plo"}>
                        <div style={{paddingTop: '2em'}}>
                        <ModelTable
                            id="plo-table"
                            model={Plo}
                            modelName="plo"
                            records={ploList}
                            customForm={PloForm}
                            hideColumns= {['cloList','isDeleted']}
                            renderColumn={{
                                prodi: data => data.prodi?.name ?? '-',
                                period: data => data.period?.academicYear ?? '-',
                            }}
                            referencedModelNames={['prodi', 'period']}
                        />    
                        </div>
                        </Route>
                        <Route path={homePath + "/bobot"}>
                            <div style={{paddingTop: '2em'}}>
                                {(bobotPembina !=0 && bobotPenguji !=0 )  ? <BobotDosen bobotPembina={bobotPembina} bobotPenguji={bobotPenguji} periodId={periodId}/> :  <BobotDosen periodId={periodId}/>}
                            </div>
                        </Route>
                       
                        
                    </Switch>
                </Col>
            </Row>
            </div>
            

            
        );
    }
}

const Page = () => {
   
    return (
        <div className="page main-content-container px-4 pb-4 container-fluid">
            {/* <Navbar activeMenu={LECTURER_MENU.NEW} /> */}
            <div className="container" style={{paddingTop: '1em'}}>
                <h2>Komponen Penilaian</h2>
                
                <KoorMetlit
                    currentPeriod={window.SAILS_LOCALS.currentPeriod}
                    bobotlist={window.SAILS_LOCALS.bobotlist}
                    periods={window.SAILS_LOCALS.periods}
                    prodiList={window.SAILS_LOCALS.prodiList}
                    currentPeriodId={window.SAILS_LOCALS.currentPeriodId}
                    ploList={window.SAILS_LOCALS.ploList}
                    plo={window.SAILS_LOCALS.plo}
                />
            </div>
        </div>
    )
}

ReactDOM.render(
    <Layout><Page /></Layout>,
    document.getElementById("root")
);
