import React from 'react';
import ReactDOM from 'react-dom';
import { Breadcrumb, BreadcrumbItem, Row, Card, CardBody, CardSubtitle, CardTitle, Col, FormInput, Button, Collapse, Form, Badge } from 'shards-react';
import Layout from '../../components/layouts';
import { Link } from 'react-router-dom';
import { Progress } from 'react-sweet-progress';
import {
  CircularProgressbar,
  CircularProgressbarWithChildren,
  buildStyles
} from "react-circular-progressbar";

import AnimatedProgressProvider from '../../components/AnimatedProgressProvider'


export default class Dashboard extends React.Component {
  render() {
    return (
      <div className="main-content-container px-4 pb-4 container-fluid" style={{ paddingTop: '2em' }}>


        <Row>
          <Col style={{ fontFamily: 'Roboto', fontStyle: 'normal', fontWeight: 500, fontSize: '24px', lineHeight: '28px', color: '#000000' }}>
          <h4><strong>Dashboard</strong></h4>
            <h5>Selamat Datang di Dashboard Desk Evaluation</h5>
          </Col>
          
        </Row>
        <br/>
        <Row>
          <Col style={{maxWidth: '294px'}}>
            <Card style={{ paddingBottom: '1em' }}>
              <CardBody>

                <CardTitle><Badge outline pill>
        Pembina
      </Badge></CardTitle>

                <AnimatedProgressProvider valueStart={0} valueEnd={66}>
                  {(value) =>
                    <CircularProgressbar value={value} text={`12/14`} />
                  }</AnimatedProgressProvider>


              </CardBody>
              <Link style={{ paddingLeft: '2em' }}>LIHAT NILAI</Link>
            </Card>
          </Col>
          <Col style={{maxWidth: '294px'}}>
            <Card style={{ paddingBottom: '1em' }}>
              <CardBody>

                <CardTitle> <Badge outline pill theme="danger">
        Penguji
      </Badge></CardTitle>
                <AnimatedProgressProvider valueStart={0} valueEnd={66}>
        {(value)=>
      <CircularProgressbar value={value} text={`12/14`} />
        }</AnimatedProgressProvider>

              </CardBody>
              <Link style={{ paddingLeft: '2em' }}>LIHAT NILAI</Link>
            </Card>
          </Col>
          <Col style={{maxWidth: '294px'}}>
            <Card style={{ paddingBottom: '1em' }}>
              <CardBody>

                <CardTitle><Badge outline pill theme="success">
        Metlit
      </Badge></CardTitle>
                <AnimatedProgressProvider valueStart={0} valueEnd={80}>
        {(value)=>
      <CircularProgressbar value={value} text={`13/14`} />
        }</AnimatedProgressProvider>

              </CardBody>
              <Link style={{ paddingLeft: '2em' }}>LIHAT NILAI</Link>
            </Card>
          </Col>
          
        </Row>

      </div>
    );
  }
}

ReactDOM.render(<Layout ><Dashboard /></Layout>, document.getElementById('root'));



