import React from 'react';
import ReactDOM from 'react-dom';
import { Breadcrumb, BreadcrumbItem, Row, Card, CardBody, CardSubtitle, CardTitle, Col, FormInput, Button, Collapse, Form } from 'shards-react';
import Layout from '../../components/layouts';
import { Link } from 'react-router-dom';
import { Progress } from 'react-sweet-progress';

export default class Dashboard extends React.Component {
  render() {
    return (
      <div className="main-content-container px-4 pb-4 container-fluid" style={{ paddingTop: '2em' }}>
        
   
        <Row>
            <Col style={{fontFamily: 'Roboto', fontStyle: 'normal', fontWeight: 500, fontSize: '24px', lineHeight: '28px', color: '#000000'}}>
                <h5>Daftar Kelas yang diampu</h5>
            </Col>
            <Col style={{textAlign: 'right'}}>
                <Link>Download nilai semua kelas</Link>
            </Col>
        </Row>
        <Row>
            <Col>
                <Card style={{paddingBottom: '1em'}}>
                    <CardBody>
                        <CardSubtitle>Kelas</CardSubtitle>
                        <CardTitle>SI-41-04</CardTitle>
                        
                        <div style={{textAlign: '-webkit-center'}}>
                            <Progress
                                type="circle"
                                width={100}
                                percent={99}
                                theme={{
                                    color: '#2ECC71'
                                }}
                            />
                            </div>
                        
                        
                    </CardBody>
                    <Link style={{paddingLeft: '2em'}}>LIHAT NILAI</Link>
                </Card>
            </Col>
            <Col >
                <Card style={{paddingBottom: '1em'}}>
                    <CardBody>
                        <CardSubtitle>Kelas</CardSubtitle>
                        <CardTitle>SI-41-05</CardTitle>
                        <div style={{textAlign: '-webkit-center'}}>
                        <Progress
                                type="circle"
                                width={100}
                                percent={67}
                                theme={{
                                    color: '#2ECC71'
                                }}
                            />
                            </div>
                        
                       
                    </CardBody>
                    <Link style={{paddingLeft: '2em'}}>LIHAT NILAI</Link>
                </Card>
            </Col>
            <Col>
                <Card style={{paddingBottom: '1em'}}>
                    <CardBody>
                        <CardSubtitle>Kelas</CardSubtitle>
                        <CardTitle>SI-41-06</CardTitle>
                        <div style={{textAlign: '-webkit-center'}}>
                        <Progress
                                type="circle"
                                width={100}
                                percent={0}
                                theme={{
                                    color: '#2ECC71'
                                }}
                            />
                              </div>
                      
                    </CardBody>
                    <Link style={{paddingLeft: '2em'}}>LIHAT NILAI</Link>
                </Card>
            </Col>
            <Col>
                <Card style={{paddingBottom: '1em'}}>
                    <CardBody>
                        <CardSubtitle>Kelas</CardSubtitle>
                        <CardTitle>SI-41-07</CardTitle>
                        <div style={{textAlign: '-webkit-center'}}>
                        <Progress
                                type="circle"
                                width={100}
                                percent={65}
                                theme={{
                                    color: '#2ECC71'
                                }}
                            />
                        </div>
                      
                    </CardBody>
                    <Link style={{paddingLeft: '2em'}}>LIHAT NILAI</Link>
                </Card>
            </Col>
        </Row>

      </div>
    );
  }
}

ReactDOM.render(<Layout><Dashboard /></Layout>, document.getElementById('root'));



