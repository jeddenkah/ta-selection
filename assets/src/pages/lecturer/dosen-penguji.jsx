import React from 'react';
import ReactDOM from 'react-dom';

import Cookies from 'js-cookie';
import Navbar from '../../components/Navbar';
import Nav, { MENU as LECTURER_MENU } from '../../components/lecturer/Nav';
import Layout from '../../components/layouts';
import ScorePembimbing from '../../components/lecturer/ScorePembimbing';
import { Row, Card, CardBody, CardSubtitle, CardTitle, Col, FormInput, Button, Collapse, Form, Progress } from 'shards-react';

function PlottingRow({ student, number,nilailist } ) {
    const nilai = nilailist ? nilailist.find(nilailist=> nilailist.student.id == student.id) : {};
    return (
        <tr>
            <td>{number}</td>
            <td>{student.name}</td>
            <td>{student.nim}</td>
            <td>{student.class}</td>
            <td>{student.peminatan}</td>
            <td>{student.kelompok}</td>
            <td>{student.topic}</td>
            <td>{student.dospim}</td>
            <td>{nilai ?
             <ScorePembimbing nilaiList={nilailist} student={student.id}/>
             : '-'}</td>
             <td>{nilai ?
             <a href={`/lecturer/evaluation-score/${student.id}`}>Ubah Nilai</a>
             : <a href={`/lecturer/evaluation-score/${student.id}`}>Lakukan Penilaian</a>}</td>
        </tr>
    )
}

class PlottingTable extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { plotting } = this.props;
        console.log(plotting);
        let number = 0;
        return (
            <div>
                <Card>
                    <CardBody>
                        <Row className="formComponentSpace">
                            <table className="table" >
                                <thead thead-dark>
                                    <tr>
                                        <th bgcolor="#E8E8E8">NO.</th>
                                        <th bgcolor="#E8E8E8">Nama</th>
                                        <th bgcolor="#E8E8E8">Nim</th>
                                        <th bgcolor="#E8E8E8">Kelas</th>
                                        <th bgcolor="#E8E8E8">Peminatan</th>
                                        <th bgcolor="#E8E8E8">Kelompok</th>
                                        <th bgcolor="#E8E8E8">Topic</th>
                                        <th bgcolor="#E8E8E8">Pembimbing</th>
                                        <th bgcolor="#E8E8E8">Nilai</th>
                                        <th bgcolor="#E8E8E8">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {plotting.map((student, index) => {
                                        number++;
                                        return(
                                            <PlottingRow student={student} number={number} nilailist={student.nilai} />
                                        )
                                    })}
                                </tbody>
                            </table>
                        </Row>
                    </CardBody>

                </Card>
            </div>
        )
    }
}

function Page({ topicSelections }) {
    console.log(topicSelections);
    // console.log(lecturer);
    const data_fix = [];
    topicSelections.map(ts => {
        ts.group.students.map(student => {
            if(student.group_student.length != 0){
                student.group_student.map(groupStudent=>{
                    const data = {
                        "id" : student.id,
                        "name" : student.name,
                        "nim" : student.nim,
                        "class" : student.class,
                        "peminatan" : student.peminatan.abbrev,
                        "dospim" : ts.lecturer.name,
                        "topic" : ts.topic.name,
                        "kelompok" : ts.group.id,
                        "nilai" : groupStudent.score
                    }
                    data_fix.push(data);
                })

            }
        })
    })
    
    console.log(data_fix);
    return (
        <React.Fragment>
            {/* <Navbar activeMenu={LECTURER_MENU.LIST}/> */}
            <div className="container main-content-container px-4 pb-4 container-fluid">
                <h1>List Mahasiswa Reviewer</h1>
                {/* {topics.length ? <TopicTable topics={topics} /> : <p>Anda belum membuat topik.</p>} */}
                {data_fix.length ? <PlottingTable plotting={data_fix} /> : <p>Belum ada mahasiswa yang akan diuji.</p>}
            </div>
        </React.Fragment>
    )
}

ReactDOM.render(
    <Layout>
        <Page
            topicSelections={window.SAILS_LOCALS.topicSelections}
            currentPeriod={window.SAILS_LOCALS.currentPeriod}
        /></Layout>,
    document.getElementById("root")
);
