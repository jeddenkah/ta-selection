import React from 'react';
import ReactDOM from 'react-dom';

import Cookies from 'js-cookie';
import Navbar from '../../components/Navbar';
import Nav, { MENU as LECTURER_MENU } from '../../components/lecturer/Nav';
import Layout from '../../components/layouts';
import { Row, Card, CardBody, CardSubtitle, CardTitle, Col, FormInput, Button, Collapse, Form, Progress } from 'shards-react';

let no = 1;

function PlottingRow({ student, number, peminatan, lecturer, topic, topicSelectionId, length, index }) {
    const handleReject = () => {
        const promptMessage = `Apakah anda yakin ingin menolak topik "${topic.name}" (id: ${topic.id})?`
        if (!confirm(promptMessage)) return;
        fetch('/lecturer/update-plotting-dosen-pembimbing', {
            method: 'POST',
            body: JSON.stringify({ id: topicSelectionId, status: 'REJECTED' })
        }).then(res => {
            if (res.redirected) location.href = res.url;
        })
    }

    const handleApprove = () => {
        fetch('/lecturer/update-plotting-dosen-pembimbing', {
            method: 'POST',
            body: JSON.stringify({ id: topicSelectionId, status: 'APPROVED_LAB_RISET' })
        }).then(res => {
            if (res.redirected) location.href = res.url;
        })
    }

    return (
        <tr>
            <td>{number}</td>
            <td>{student.name}</td>
            <td>{student.nim}</td>
            <td>{peminatan.abbrev}</td>
            <td>{topic.name}</td>
            <td>{lecturer.name}</td>
            {index == 0 ? 
            <td rowspan={length}>
                <Button outline theme="success" className="btn btn-default" onClick={handleApprove}>Acc</Button>
                <Button outline theme="danger" className="btn btn-default" onClick={handleReject}>Tolak</Button>
            </td>
            : <></>}
        </tr>
    )
}

class PlottingTable extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { plotting } = this.props;
        //console.log(plotting);
        let number = 0;
        return (
            <div>
                <Card>
                    <CardBody>
                        <Row className="formComponentSpace">
                            <p>Kelompok {plotting.group.id} | {plotting.topic.name}</p>
                            <table className="table" >
                                <thead thead-dark>
                                    <tr>
                                        <th bgcolor="#E8E8E8">NO.</th>
                                        <th bgcolor="#E8E8E8">NAMA</th>
                                        <th bgcolor="#E8E8E8">NIM</th>
                                        <th bgcolor="#E8E8E8">PEMINATAN</th>
                                        <th bgcolor="#E8E8E8">TOPIK</th>
                                        <th bgcolor="#E8E8E8">NAMA PEMBIMBING</th>
                                        <th bgcolor="#E8E8E8">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {plotting.group.students.map((student, index) => {
                                        number++;
                                        return(
                                            <PlottingRow topicSelectionId={plotting.id}  topic={plotting.topic} student={student} number={number} peminatan={plotting.peminatan} lecturer={plotting.lecturer} length={plotting.group.students.length} index={index} />
                                        )
                                    })}
                                </tbody>
                            </table>
                        </Row>
                    </CardBody>

                </Card>
            </div>
        )
    }
}

function Page({ lecturer, topicSelections }) {
    const isKoorLabRiset =    Cookies.get('koorLabRiset');
    const isKoorKk = Cookies.get('koorKK');

    const topicSelectionFix = isKoorKk ? window.SAILS_LOCALS.topicSelectionByKk : isKoorLabRiset ? window.SAILS_LOCALS.topicSelectionByLabRiset : undefined;
    console.log("KOORLAB "+isKoorLabRiset);
    console.log(window.SAILS_LOCALS.topicSelections);
    
    return (
        <React.Fragment>
            {/* <Navbar activeMenu={LECTURER_MENU.LIST}/> */}
            <div className="container main-content-container px-4 pb-4 container-fluid">
                <h1>Plotting Dosen Pembimbing</h1>
                {/* {topics.length ? <TopicTable topics={topics} /> : <p>Anda belum membuat topik.</p>} */}
                {topicSelectionFix.length ? topicSelectionFix.map(topicSelection => <PlottingTable plotting={topicSelection} />) : <p>Belum ada topik yang di approve.</p>}
            </div>
        </React.Fragment>
    )
}

ReactDOM.render(
    <Layout>
        <Page
            topicSelections={window.SAILS_LOCALS.topicSelections}
            currentPeriod={window.SAILS_LOCALS.currentPeriod}
            lecturer={window.SAILS_LOCALS.lecturer}
        /></Layout>,
    document.getElementById("root")
);
