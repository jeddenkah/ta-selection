import React from 'react';
import ReactDOM from 'react-dom';
import ReactModal from 'react-modal';
import Navbar, {MENU as NAVBAR_MENU} from '../../components/Navbar';
import Nav, {MENU as LECTURER_MENU} from '../../components/lecturer/Nav';
import PeminatanSelect from '../../components/PeminatanSelect';
import Layout from '../../components/layouts';
import { Row, Card, CardBody, CardSubtitle, CardTitle, Col, FormInput, Button, Collapse, Form, Progress,FormTextarea, FormGroup, FormCheckbox } from 'shards-react';
import LecturerSelect from '../../components/lecturer/LecturerSelect';

class TopicCreate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            quota: 5,
            peminatanId: '',
            deskripsi:'',
            isReviewModalOpen: false,
            lecturer: {},
            asPembimbing: true,
            errorMessages: {
                name: '',
                quota: '',
                peminatanId: '',
                deskripsi:'',
            }
        };

        this.showReviewModal = this.showReviewModal.bind(this);
        this.closeReviewModal = this.closeReviewModal.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeAsPembimbing = this.handleChangeAsPembimbing.bind(this);
        this.validate = this.validate.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    showReviewModal() {
        if (this.validate()) {
            this.setState({ isReviewModalOpen: true });
        }
    }

    closeReviewModal() {
        this.setState({ isReviewModalOpen: false,
            name: '',
            quota: 5,
            peminatanId: '',
            deskripsi:'',
            lecturer: {}
        })
        
    }

    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    handleChangeAsPembimbing(e) {
        this.setState(state => ({asPembimbing: !state.asPembimbing}));
    }

    validate() {
        this.setState(state => {
            const {name, quota, peminatanId,deskripsi, lecturer, asPembimbing} = state;
            return {
                errorMessages: {
                    name: name? '' : 'Kolom topik tidak boleh kosong',
                    deskripsi: deskripsi? '' : 'Kolom deskripsi tidak boleh kosong',
                    peminatanId: peminatanId? '' : 'Kolom peminatan tidak boleh kosong',
                    lecturer: lecturer?.id || asPembimbing ? "" : "Kolom Dosen tidak boleh kosong" 
                }
            }
        });
        const {name, peminatanId, deskripsi, lecturer, asPembimbing} = this.state;
        if (name && peminatanId && deskripsi && (lecturer?.id || asPembimbing)) return true;
        else return false;
    }

    handleSubmit(e) {
        e.preventDefault();
        const {lecturer, asPembimbing} = this.state;
        if (this.validate()) {
            fetch('/lecturer/new-topic', {
                method: 'POST',
                body: JSON.stringify({...this.state, lecturerId: asPembimbing ? this.props.user.id : lecturer.id})
            }).then(res => {
                // res.redirected ? location.href = res.url : null;
                this.setState({ isReviewModalOpen: true });
            })
        }
    }

    render() {
        const {peminatanList, currentPeriod, isKetuaLabRiset, user} = this.props;
        const {name, quota, peminatanId, deskripsi, errorMessages, asPembimbing, lecturer} = this.state;
        const lecturerSelect = isKetuaLabRiset && (
            <FormGroup className={errorMessages.lecturer ? 'has-error' : ''}>
                <label className="col-sm-2 control-label" htmlFor="lecturer">
                    Dosen
                </label>
                <div className="col-sm-12">
                    <FormCheckbox
                        name="asPembimbing"
                        inline
                        checked={asPembimbing}
                        onChange={this.handleChangeAsPembimbing}
                    >
                        Pilih diri sendiri
                    </FormCheckbox>
                </div>
                <div className="col-sm-6">
                    <LecturerSelect
                        name={"lecturer"}
                        value={lecturer}
                        prodiId={user.prodi}
                        onChange={this.handleChange}
                        disabled={asPembimbing}
                    />
                    <span className="help-block errorMsg">
                        {errorMessages.lecturer}
                    </span>
                </div>
            </FormGroup>
        )
        return (

            <div>
                <Row style={{ paddingTop: '2em' }}>
                <Col>
                <Card>
                    <CardBody>
                    <Row style={{ marginTop: '-1.09375rem' }}>
                                <Col style={{ alignSelf: 'center', textAlign: 'left' }}><CardTitle style={{ marginTop: '0' }}>INPUT TOPIK BARU</CardTitle></Col>
                    </Row>
                    <form
                className="form-horizontal"
                method="POST"
                onSubmit={this.handleSubmit}
            >
                <div className={`form-group ${errorMessages.name ? 'has-error' : ''}`}>
                
                    <label className="col-sm-2 control-label" htmlFor="name">
                        Nama Topik TA
                    </label>
                    <div className="col-sm-6">
                        <FormTextarea
                            className="form-control"
                            type="text"
                            name="name"
                            value={name}
                            onChange={this.handleChange}
                            placeholder="Masukkan Nama Topik TA"
                        />
                        <span className="help-block errorMsg">
                            {errorMessages.name}
                        </span>
                    </div>
                </div>

                <div className={`form-group ${errorMessages.name ? 'has-error' : ''}`}>
                    <label className="col-sm-2 control-label" htmlFor="deskripsi">
                        Deskripsi
                    </label>
                    <div className="col-sm-6">
                        <FormTextarea
                            className="form-control"
                            type="text"
                            name="deskripsi"
                           value={deskripsi} // INI GABISA LINE INI
                            onChange={this.handleChange}
                            placeholder="Masukkan Deskripsi TA"
                        />
                        <span className="help-block errorMsg">
                            {errorMessages.deskripsi}
                        </span>
                    </div>
                </div>

                <div className="form-group">
                    <label className="col-sm-2 control-label" htmlFor="quota">
                        Kuota
                    </label>
                    <div className="col-sm-2">
                        <FormInput
                            className="form-control"
                            type="number"
                            name="quota"
                            min="1"
                            value={quota}
                            onChange={this.handleChange}
                        />
                    </div>
                </div>
                <div className={`form-group ${errorMessages.peminatanId ? 'has-error' : ''}`}>
                    <label className="col-sm-2 control-label" htmlFor="peminatanId">
                        Peminatan
                    </label>
                    <div className="col-sm-6">
                        <PeminatanSelect peminatanList={peminatanList} name="peminatanId" value={peminatanId} onChange={this.handleChange} />
                        <span className="help-block errorMsg">
                            {errorMessages.peminatanId}
                        </span>
                    </div>
                </div>
                <FormGroup>
                    <label className="col-sm-2 control-label" htmlFor="staticPeriod">
                        Periode
                    </label>
                    <div className="col-sm-6">
                        <input
                            type="text"
                            readOnly
                            class="form-control-plaintext"
                            name="staticPeriod"
                            value={currentPeriod.semester + '/' + currentPeriod.academicYear}></input>
                    </div>
                </FormGroup>
                {lecturerSelect}
                


                <div className="form-group">
                    <Row className="col-sm-12">
                        <Button size='lg' block className="btn-ijo" type="submit">
                            Submit
                        </Button>
                    </Row>
                </div>
                {
                <ReactModal
                            appElement={document.getElementById('root')}
                            isOpen={this.state.isReviewModalOpen}
                            onRequestClose={this.closeReviewModal}
                            className="mymodal400"
                            overlayClassName="myoverlay"
                            >
                                <div className="container text-center">
                                <img
                                    className="checklist rounded-circle mr-2"
                                    src="/images/checklist.png"
                                    alt="checklist"
                                    width="40px"
                                    height="40px"
                                />
                               
                            </div>
                                
                            <div className="container text-center">
                                <p className="display-4" style={{fontSize:'2em'}}>Berhasil Tersimpan</p>
                            </div>

                                <div className="container text-center">
                                <Button theme="Success" className="btn btn-ijo" onClick={this.closeReviewModal} > OK </Button>
                                </div>
                                
                                
                        </ReactModal>
                        }

            </form>
                    </CardBody>
                </Card>
                </Col>
            </Row>
            </div>
            

            
        );
    }
}

const Page = () => {
    return (
        <div id="page" className="page main-content-container px-4 pb-4 container-fluid">
            {/* <Navbar activeMenu={LECTURER_MENU.NEW} /> */}
            <div className="container">
                <h2>Buat Topik</h2>
                <TopicCreate
                    peminatanList={window.SAILS_LOCALS.peminatanList}
                    currentPeriod={window.SAILS_LOCALS.currentPeriod}
                    isKetuaLabRiset={window.SAILS_LOCALS.isKetuaLabRiset}
                    user={window.SAILS_LOCALS.user}
                />
            </div>
        </div>
    )
}

ReactDOM.render(
    <Layout><Page /></Layout>,
    document.getElementById("root")
);
