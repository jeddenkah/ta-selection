import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import Layout from '../../components/layouts';
import { Row, Card, CardBody, Button } from 'shards-react';
import ReactModal from 'react-modal';

class ButtonModal extends React.Component {
    constructor(props) {
      super(props);
      ButtonModal.propTypes = { 
        openModal: PropTypes.func.isRequired,
        student : PropTypes.arrayOf(PropTypes.array),
        studentName : PropTypes.string
    }
    
    }
    
    toggle() {
      const {openModal,student,studentName} = this.props
      openModal(student,studentName)
    }
    render() {
      
      
        return(
            
        <Button onClick={this.toggle.bind(this)}>Lihat Catatan Bimbingan</Button>
        )
    }
  }


function PlottingRow({ student, number, peminatan, lecturer, topic, topicSelectionId, length, index, openModal,closeModal,addStudent }) {
   
   
    return (
        <tr>
            <td>{number}</td>
            <td>{student.name}</td>
            <td>{student.nim}</td>
            <td>{student.class}</td>
            <td>{peminatan.abbrev}</td>
            {/* <td>{student.id}</td> */}
            
            <td><ButtonModal studentName={student.name} student={student.id} openModal={openModal}/></td>
        </tr>
    )
}

class PlottingTable extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { plotting,openModal,closeModal,handleAddStudent } = this.props;
        //console.log(plotting);
        let number = 0;
        return (
            <div>
                <Card>
                    <CardBody>
                        <Row className="formComponentSpace">
                            <p>Kelompok {plotting.group.id} | {plotting.topic.name}</p>
                            <table className="table" >
                                <thead thead-dark>
                                    <tr>
                                        <th bgcolor="#E8E8E8">NO.</th>
                                        <th bgcolor="#E8E8E8">NAMA</th>
                                        <th bgcolor="#E8E8E8">NIM</th>
                                        <th bgcolor="#E8E8E8">KELAS</th>
                                        <th bgcolor="#E8E8E8">PEMINATAN</th>
                                        <th bgcolor="#E8E8E8">AKSI</th>
                                        <></>
                                    </tr>
                                </thead>
                                <tbody>
                                    {plotting.group.students.map((student, index) => {
                                        number++;
                                        return(
                                            <PlottingRow topicSelectionId={plotting.id}  topic={plotting.topic} student={student} 
                                            number={number} peminatan={plotting.group.peminatan} lecturer={plotting.lecturer} 
                                            length={plotting.group.students.length} index={index}
                                            openModal={openModal}
                                            closeModal={closeModal}
                                            addStudent={handleAddStudent}
                                            />
                                        )
                                    })}
                                </tbody>
                            </table>
                        </Row>
                    </CardBody>

                </Card>
            </div>
        )
    }
}

class BimbinganList extends React.Component {
    constructor(props) {
        super(props);
        BimbinganList.propTypes = {
            plotting: PropTypes.arrayOf(PropTypes.object),
            bimbingan: PropTypes.arrayOf(PropTypes.object),
        }
        this.state = {
            isBimbinganModalOpen: false,
            student:0
        };
       
        this.showBimbinganModal = this.showBimbinganModal.bind(this);
        this.closeBimbinganModal = this.closeBimbinganModal.bind(this);
        this.handleAcc = this.handleAcc.bind(this);
        this.handleReject = this.handleReject.bind(this);

    }

    // handleAddStudent(studentId){
    //     this.setState({student:studentId})
    // }
    showBimbinganModal(studentId,studentName) {
        this.setState({ 
            studentName:studentName,
            student:studentId,
            isBimbinganModalOpen: true 
        
        });
    }

    closeBimbinganModal() {
        this.setState({ isBimbinganModalOpen: false })
    }

    handleAcc(bimbinganId){
        
            fetch('/lecturer/update-bimbingan', {
                method: 'POST',
                body: JSON.stringify({ id: bimbinganId, status: 'APPROVED' })
            }).then(res => {
                if (res.redirected) location.href = res.url;
            })
        
    }

    handleReject(bimbinganId){
        
        fetch('/lecturer/update-bimbingan', {
            method: 'POST',
            body: JSON.stringify({ id: bimbinganId, status: 'REJECTED' })
        }).then(res => {
            if (res.redirected) location.href = res.url;
        })
    
}
   

    render() {
       
        return (
            <div className="container">
               

                    <PlottingTable
                        plotting ={this.props.plotting}
                        handleAddStudent = {this.handleAddStudent}
                        openModal ={this.showBimbinganModal}
                        closeModal ={this.closeBimbinganModal}
                    />
                    <Row style={{ paddingTop: '2em' }}></Row>

                <form>
                       

                    {
                        <ReactModal
                            appElement={document.getElementById('root')}
                            isOpen={this.state.isBimbinganModalOpen}
                            onRequestClose={this.closeBimbinganModal}
                            className="mymodal"
                            overlayClassName="myoverlay"
                        >
                            <BimbinganTable
                                bimbingan={this.props.bimbingan}
                                handleAcc={this.handleAcc}
                                handleReject={this.handleReject}
                                student={this.state.student}
                                studentName={this.state.studentName}
                            />
                        </ReactModal>
                    }
                </form>

            </div>


        );

    }
}

function BimbinganTable({
  studentName,bimbingan,student,handleAcc,handleReject
}) {
    BimbinganTable.propTypes = {
       studentName: PropTypes.string,
        bimbingan: PropTypes.arrayOf(PropTypes.object).isRequired,
        student: PropTypes.number.isRequired,
        handleAcc: PropTypes.func.isRequired,
        handleReject: PropTypes.func.isRequired
    }
    
    const result = Object.values(bimbingan).filter(data=> data.studentId == student)
    const jumlahBimbingan = Object.values(bimbingan).filter(data=> data.studentId == student && data.status === "APPROVED" )
  
    return (
        <div className="container">
            <center><h1>Catatan Bimbingan</h1></center>
            <h6>Mahasiswa : {studentName}</h6>
            <h6>Jumlah Bimbingan Terverifikasi : {jumlahBimbingan.length}</h6>
            <div className="table-scroll">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Catatan</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        {result.map(bimbingan => (
                            <tr>
                                <td>{bimbingan.tanggal}</td>
                                <td>{bimbingan.catatan}</td>
                                <td>{bimbingan.status}</td>
                                {bimbingan.status=="PROCESSED" ?
                               <td><ApprovalButton onApprove={handleAcc} onReject={handleReject} bimbinganId={bimbingan.id}/></td>
                                : ''}
                            </tr>
                        ))}
                    </tbody>
                </table>
                
            </div>
        </div>
  
    )
}

function ApprovalButton({ onApprove, onReject, bimbinganId }) {
    ApprovalButton.propTypes = {
        onApprove: PropTypes.func.isRequired,
        onReject: PropTypes.func.isRequired,
        bimbinganId: PropTypes.number.isRequired
    }
    const handleApproveClick = () => {
        onApprove(bimbinganId);
    }
    const handleRejectClick = () => {
        onReject(bimbinganId);
    }
    const approveBtnClass =  'btn-success';
    const rejectBtnClass = 'btn-danger';
    return (
        <div className="btn-group" role="group" aria-label="approval-button">
            <button type="button" className={`btn ${approveBtnClass}`} onClick={handleApproveClick} >Setujui</button>
            <button type="button" className={`btn ${rejectBtnClass}`} onClick={handleRejectClick} >Tolak</button>
        </div>
    );
}

function Page({ topicSelections,bimbingan }) {
    //console.log(topicSelections);
    // console.log(lecturer);
    
    return (
        <React.Fragment>
            {/* <Navbar activeMenu={LECTURER_MENU.LIST}/> */}
            <div className="container main-content-container px-4 pb-4 container-fluid">
                <h1>List Mahasiswa Pembimbingan</h1>
                {/* {topics.length ? <TopicTable topics={topics} /> : <p>Anda belum membuat topik.</p>} */}
                {topicSelections.length ? topicSelections.map(topicSelection => <BimbinganList plotting={topicSelection} bimbingan={bimbingan}/>) : <p>Belum ada mahasiswa yang akan dibimbing.</p>}
            </div>
        </React.Fragment>
    )
}

ReactDOM.render(
    <Layout>
        <Page
            topicSelections={window.SAILS_LOCALS.topicSelections}
            currentPeriod={window.SAILS_LOCALS.currentPeriod}
            bimbingan = {window.SAILS_LOCALS.bimbingan}
        /></Layout>,
    document.getElementById("root")
);
