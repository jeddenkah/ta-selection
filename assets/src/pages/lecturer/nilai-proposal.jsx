import React from 'react';
import ReactDOM from 'react-dom';
import {
    Switch,
    Route
  } from "react-router-dom";
import PeriodSelect from '../../components/PeriodSelect';
import KelasSelect from '../../components/KelasSelect';
import handleFilterChange from '../../components/handleFilterChange';
import Layout from '../../components/layouts';
import { Row, Col, Button } from 'shards-react';
let SubClo = require('../../../../api/models/SubClo');
SubClo.attributes['subCloRubrics'] = {collection: 'subclorubric'}
import ScoreTable from '../../components/lecturer/ScoreTable'
import ScorePembimbingTable from '../../components/lecturer/ScorePembimbingTable'
import ScorePengujiTable from '../../components/lecturer/ScorePengujiTable'

class NilaiProposal extends React.Component {
    constructor(props) {
        super(props);
        const searchParams = new URLSearchParams(window.location.search);
        const kelasId = searchParams.get('kelas');
        
        this.state = {
            
        };

        this.showReviewModal = this.showReviewModal.bind(this);
        this.closeReviewModal = this.closeReviewModal.bind(this);
      
        
        this.handleChangePeriod = this.handleChangePeriod.bind(this);
       
    }

    showReviewModal() {
        if (this.validate()) {
            this.setState({ isReviewModalOpen: true });
        }
    }

    closeReviewModal() {
        this.setState({ isReviewModalOpen: false,
            name: '',
            quota: 5,
            peminatanId: '',
            deskripsi:'',
        })
        
    }


    handleChangePeriod(e) {
        handleFilterChange(e)
    }
        
    render() {
        //const {currentPeriod,bobotlist} = this.props;
        //const {bobotPembina, bobotPenguji} = this.state;
        
        const searchParams = new URLSearchParams(window.location.search);
        const kelasId = searchParams.get('kelasId');
        const periodId = searchParams.get('periodId');
        const homePath = '/lecturer/nilai-proposal';
        const pageURL = window.location.href;
        const tab = pageURL.substr(pageURL.lastIndexOf('/') + 1);
        //const currentPeriodId = window.SAILS_LOCALS.currentPeriodId;    
        const { bobotlist,currentPeriodId,periods,kelasList,scorePembimbing,scorePenguji,studentList} = this.props; 
        const prodiList= kelasList.map(kelas => kelas.prodi);
        const metlitList = [];
        const studentMetlit =[];
        prodiList[0].map((prodi,i) =>
            prodi.metlit.map((metlit,i)=>{
                if(metlit!=null){
                
                    metlitList.push(metlit);
                    
                }
            })
        )
        studentList.map(student=>{
          return metlitList.map(metlit=>{
                if(student.metlit==metlit.id){
                    return(
                        studentMetlit.push(student)
                    )
                }
           })
        })
        console.log(metlitList)
        console.log(studentMetlit)
        return (

            <div><form>
                <div className="form-group">
                    <label htmlFor="peminatanId">Kelas</label>
                    <KelasSelect
                        name="kelasId"
                        value={kelasId}
                        kelasList={metlitList}
                        onChange={handleFilterChange}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="periodId">Periode</label>
                    <PeriodSelect
                        name="periodId"
                        value={periodId || this.props.currentPeriodId}
                        periods={periods}
                        onChange={handleFilterChange}
                    />
                </div>
                </form>
                <Row>
                <Col>
                <Row className="container">
                <div style={{paddingLeft: '1em',paddingTop: '2em'}}>
                    <Button outline pill tag="a"  href={homePath+'/All'} theme="success" onClick={this.handleChangeTab} active={tab == 'All'}>
                        Nilai Desk Evaluation
                    </Button>
                    </div>
                    <div style={{paddingLeft: '1em',paddingTop: '2em'}}>
                    <Button outline pill tag="a"  href={homePath+'/Pembimbing'} theme="success" onClick={this.handleChangeTab} active={tab == 'Pembimbing'}>
                        Nilai Pembimbing
                    </Button>
                    </div>
                    <div style={{paddingLeft: '1em',paddingTop: '2em'}}>
                    <Button outline pill tag="a"  href={homePath+'/Penguji'} theme="success" onClick={this.handleChangeTab} active={tab == 'Penguji'}>
                        Nilai Penguji
                    </Button>
                    </div>
                </Row>
                
                <Switch>
                    <Route path={homePath + "/All"}>
                        <div style={{paddingTop: '2em'}}>
                        <ScoreTable
                        scorePembimbing={scorePembimbing}
                        scorePenguji={scorePenguji}
                        student={studentMetlit}
                        view='all'
                        />
                        </div>
                        </Route>
                        <Route path={homePath + "/Pembimbing"}>
                        <div style={{paddingTop: '2em'}}>
                        <ScorePembimbingTable
                        scoreList={scorePembimbing}
                        studentList={studentMetlit}
                        view='Pembimbing'
                        />
                        </div>
                        </Route>
                        <Route path={homePath + "/Penguji"}>
                        <div style={{paddingTop: '2em'}}>
                        <ScorePengujiTable
                            scoreList={scorePenguji}
                            studentList={studentMetlit}
                            view='Penguji'
                        />
                        </div>
                        </Route>
                       
                        
                    </Switch>
                </Col>
            </Row>
            </div>
            

            
        );
    }
}

const Page = () => {
    const{scorePembimbingPopulated,scorePengujiPopulated} = window.SAILS_LOCALS;
   
    
    return (
        <div className="page main-content-container px-4 pb-4 container-fluid">
            {/* <Navbar activeMenu={LECTURER_MENU.NEW} /> */}
            <div className="container" style={{paddingTop: '1em'}}>
                <h2>Nilai Proposal</h2>
                
                <NilaiProposal
                    currentPeriod={window.SAILS_LOCALS.currentPeriod}
                    //pist={window.SAILS_LOCALS.bobotlist}
                    periods={window.SAILS_LOCALS.periods}
                    kelasList={window.SAILS_LOCALS.kelasList}
                    currentPeriodId={window.SAILS_LOCALS.currentPeriodId}
                    //scorePopulated = {scoreAll}
                    scorePembimbing = {scorePembimbingPopulated}
                    scorePenguji = {scorePengujiPopulated}
                    bobotlist={window.SAILS_LOCALS.bobotlist}
                    studentList={window.SAILS_LOCALS.studentList}
                />
            </div>
        </div>
    )
}

ReactDOM.render(
    <Layout><Page /></Layout>,
    document.getElementById("root")
);
