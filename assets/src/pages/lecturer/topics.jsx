import React from 'react';
import ReactDOM from 'react-dom';

import Navbar from '../../components/Navbar';
import Nav, {MENU as LECTURER_MENU} from '../../components/lecturer/Nav';
import Layout from '../../components/layouts';
import { Row, Card, CardBody, CardSubtitle, CardTitle, Col, FormInput, Button, Collapse, Form, Progress } from 'shards-react';


function TopicRow({topic}) {
    const promptMessage = `Apakah anda yakin ingin menghapus topik "${topic.name}" (id: ${topic.id})?`
    const handleDelete = () => {
        if (!confirm(promptMessage)) return;
        fetch('/lecturer/delete-topic', {
            method: 'POST',
            body: JSON.stringify({topicId: topic.id})
        }).then(res => {
            if (res.redirected) location.href = res.url;
            else if (res.headers.get('X-Edit') == 'alreadyHasStudent') {
                alert('Tidak bisa menghapus topik. Topik sudah dipilih mahasiswa.');
            }
        })
    }
    const shortAcademicYear = topic.period.academicYear.split('-')
        .map(year => year.substring(2,4))
        .join('-');
    return (


        <tr>
            <td width="400px">{topic.name}</td>
            <td >{topic.quota}</td>
            {/* <td>{topic.kk.abbrev}</td> */}
            <td>KK</td>
            <td>{topic.peminatan?.abbrev}</td>
            <td>{topic.period.semester}</td>
            <td>{shortAcademicYear}</td>
            <td >
                <Button outline theme="danger" className="btn btn-default" href={`/lecturer/topic/edit/${topic.id}`}  style={{margin:10}} >Edit</Button>
                <Button outline theme="danger" className="btn btn-default" onClick={handleDelete}>Hapus</Button>
            </td>
        </tr>
    )
}

class TopicTable extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {topics} = this.props;
        return(
            <div>
                <Card>
                    <CardBody>
                        <Row className="formComponentSpace">
            <table className="table" >
                <thead thead-dark>
                    <tr>
                        <th bgcolor="#E8E8E8">Nama Topik</th>
                        <th bgcolor="#E8E8E8">Kuota</th>
                        <th bgcolor="#E8E8E8">KK</th>
                        <th bgcolor="#E8E8E8">Peminatan</th>
                        <th bgcolor="#E8E8E8">Semester</th>
                        <th bgcolor="#E8E8E8">Tahun</th>
                        <th bgcolor="#E8E8E8">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {topics.map(topic => <TopicRow key={topic.id} topic={topic} />)}
                </tbody>
            </table>
            </Row>
        </CardBody>
                    
    </Card>
            </div>
        )
    }
}

function Page({topics}) {
    return (
        <React.Fragment>
            {/* <Navbar activeMenu={LECTURER_MENU.LIST}/> */}
            <div className="container main-content-container px-4 pb-4 container-fluid">
                <h1>Daftar Topik Anda</h1>
                {topics.length? <TopicTable topics={topics}/> : <p>Anda belum membuat topik.</p>}
            </div>
        </React.Fragment>
    )
}

ReactDOM.render(
    <Layout>
    <Page
        topics={window.SAILS_LOCALS.topics} 
        currentPeriod={window.SAILS_LOCALS.currentPeriod}
    /></Layout>,
    document.getElementById("root")
);
