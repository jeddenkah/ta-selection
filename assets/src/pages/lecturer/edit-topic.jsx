import React from 'react';
import ReactDOM from 'react-dom';
import { Row, Card, CardBody, CardSubtitle, CardTitle, Col, Button, Collapse, Form, FormInput, FormGroup, FormCheckbox, FormTextarea} from 'shards-react';
import LecturerSelect from '../../components/lecturer/LecturerSelect';
import Navbar from '../../components/Navbar';
import Nav from '../../components/lecturer/Nav';
import PeminatanSelect from '../../components/PeminatanSelect';
import Layout from '../../components/layouts';
import ReactModal from 'react-modal';
import Cookies from "js-cookie";

class TopicEditForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.topic.id,
            name: this.props.topic.name,
            quota: this.props.topic.quota,
            peminatanId: this.props.topic.peminatan,
            deskripsi: this.props.topic.deskripsi,
            lecturer: this.props.topic.lecturer,
            isReviewModalOpen: false,
            errorMessages: {
                quota: ''
            },
            asPembimbing: Cookies.get("userIdVal") == this.props.topic.lecturer.id
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.validate = this.validate.bind(this);
        this.showReviewModal = this.showReviewModal.bind(this);
        this.closeReviewModal = this.closeReviewModal.bind(this);
    }

    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    showReviewModal() {
        if (this.validate()) {
            this.setState({ isReviewModalOpen: true });
        }
    }

    closeReviewModal() {
        this.setState({ isReviewModalOpen: false })
    }

    validate() {
        this.setState(state => {
            const {name, quota, peminatanId,deskripsi, lecturer, asPembimbing} = state;
            return {
                errorMessages: {
                    name: name? '' : 'Kolom topik tidak boleh kosong',
                    deskripsi: deskripsi? '' : 'Kolom deskripsi tidak boleh kosong',
                    peminatanId: peminatanId? '' : 'Kolom peminatan tidak boleh kosong',
                    lecturer: lecturer?.id || asPembimbing ? "" : "Kolom Dosen tidak boleh kosong" 
                }
            }
        });
        const {name, peminatanId, deskripsi, lecturer, asPembimbing} = this.state;
        if (name && peminatanId && deskripsi && (lecturer?.id || asPembimbing)) return true;
        else return false;
    }

    handleSubmit(e) {
        e.preventDefault();
        if (this.validate()) {
            fetch('/lecturer/edit-topic', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({...this.state, lecturerId: this.state.lecturer?.id })
                
            }).then(res => {
                this.setState({ isReviewModalOpen: true });
                if (res.redirected) location.href = res.url;
                else if (res.headers.get('X-Exit') == 'quotaConflict') {
                    res.json().then(({minQuota})=> {
                        this.setState(state => {
                            let newErrorMessages = state.errorMessages;
                            newErrorMessages.quota = 'Kuota harus tidak kurang dari ' + minQuota + ', yaitu jumlah anggota kelompok yang sudah memilih topik ini';
                            return {errorMessages: newErrorMessages}
                        })
                    })
    
                }
            });
        }
    }

    render() {
        const {topic, peminatanList, user} = this.props;
        const {name, quota, peminatanId,deskripsi, lecturer, errorMessages, asPembimbing} = this.state;
        const lecturerSelect = this.props.isKetuaLabRiset && (
            <FormGroup className={errorMessages.lecturer ? 'has-error' : ''}>
                <label className="col-sm-2 control-label" htmlFor="lecturer">
                    Dosen
                </label>
                <div className="col-sm-12">
                    <FormCheckbox
                        name="asPembimbing"
                        inline
                        checked={asPembimbing}
                        onChange={this.handleChangeAsPembimbing}
                    >
                        Pilih diri sendiri
                    </FormCheckbox>
                </div>
                <div className="col-sm-6">
                    <LecturerSelect
                        name={"lecturer"}
                        value={lecturer}
                        prodiId={user.prodi}
                        onChange={this.handleChange}
                        disabled={asPembimbing}
                    />
                    <span className="help-block errorMsg">
                        {errorMessages.lecturer}
                    </span>
                </div>
            </FormGroup>
        )
        return (
            <div className="container">
                <Row style={{ paddingTop: '2em' }}>
                <Col>
                <Card>
                    <CardBody>
                    <Row style={{ marginTop: '-1.09375rem' }}>
                                <Col style={{ alignSelf: 'center', textAlign: 'left' }}><CardTitle style={{ marginTop: '0' }}>EDIT TOPIK</CardTitle></Col>
                    </Row>
                <Form
                    className="form-horizontal"
                    onSubmit={this.handleSubmit}
                >
                    
                    <div className={`form-group ${errorMessages.name && 'has-error'}`}>
                        <label htmlFor="name" className="col-sm-2 control-label">
                            Nama Topik TA
                        </label>
                        <div className="col-sm-10">
                            <FormTextarea
                                className="form-control"
                                type="text"
                                name="name"
                                value={name}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-sm-offset-2 col-sm-10">
                            <p className="help-block errorMsg">{errorMessages.name}</p>
                        </div>
                    </div>
                    <div className={`form-group ${errorMessages.deskripsi && 'has-error'}`}>
                        <label htmlFor="deskripsi" className="col-sm-2 control-label">
                            Deskripsi
                        </label>
                        <div className="col-sm-10">
                            <FormTextarea
                                className="form-control"
                                type="text"
                                name="deskripsi"
                                value={deskripsi}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-sm-offset-2 col-sm-10">
                            <p className="help-block errorMsg">{errorMessages.deskripsi}</p>
                        </div>
                    </div>
                    <div className={`form-group ${errorMessages.quota && 'has-error'}`}>
                        <label className="col-sm-2 control-label" htmlFor="quota">
                            Kuota
                        </label>
                        <div className="col-sm-2">
                            <FormInput
                                className="form-control"
                                type="number"
                                name="quota"
                                value={quota}
                                onChange={this.handleChange}
                                min={1}
                            />
                        </div>
                        <p className="help-block errorMsg">{errorMessages.quota}</p>
                    </div>
                    <div className={`form-group ${errorMessages.peminatanId && 'has-error'}`}>
                        <label className="col-sm-2 control-label" htmlFor="peminatanId">
                            Peminatan
                        </label>
                        <div className="col-sm-2">
                            <PeminatanSelect name="peminatanId" peminatanList={peminatanList} value={peminatanId} onChange={this.handleChange}/>
                        </div>
                        <p className="help-block errorMsg">{errorMessages.peminatanId}</p>
                    </div>
                    {lecturerSelect}
                    <div className="form-group">
                        <div className="col-sm-offset-2 col-sm-10">
                            <Button size='lg' block type="submit" className="btn-ijo">
                                Submit
                            </Button>
                        </div>
                        {
                <ReactModal
                            appElement={document.getElementById('root')}
                            isOpen={this.state.isReviewModalOpen}
                            onRequestClose={this.closeReviewModal}
                            className="mymodal400"
                            overlayClassName="myoverlay"
                            >
                                <div className="container text-center">
                                <img
                                    className="checklist rounded-circle mr-2"
                                    src="/images/checklist.png"
                                    alt="checklist"
                                    width="40px"
                                    height="40px"
                                />
                               
                            </div>
                                
                            <div className="container text-center">
                                <p className="display-4" style={{fontSize:'2em'}}>Berhasil Tersimpan</p>
                            </div>

                                <div className="container text-center">
                                <Button theme="Success" className="btn btn-ijo" onClick={this.closeReviewModal} > OK </Button>
                                </div>  
                        </ReactModal>
                        }
                    </div>
                </Form>
                </CardBody>
                </Card>
                </Col>
            </Row>

            
            </div>
        );
    }
}

function Page() {
    return (
        <div className="page main-content-container px-4 pb-4 container-fluid">
            {/* <Navbar /> */}
            <div className="container">
                <h1>Edit Topik TA</h1>
                <TopicEditForm
                    topic={window.SAILS_LOCALS.topic}
                    peminatanList={window.SAILS_LOCALS.peminatanList}
                    isKetuaLabRiset={window.SAILS_LOCALS.isKetuaLabRiset}
                    user={window.SAILS_LOCALS.user}
                />
            </div>
        </div>
    )
}

ReactDOM.render(
    <Layout>
    <Page
    /></Layout>,
    document.getElementById("root")
);
