import React, {useState} from 'react';
import ReactDOM from 'react-dom';

import Cookies from 'js-cookie';
import Navbar from '../../components/Navbar';
import Nav, { MENU as LECTURER_MENU } from '../../components/lecturer/Nav';
import Layout from '../../components/layouts';
import ModelSelect from '../../components/ModelSelect'
import Select from 'react-select'
import { Row, Card, CardBody, CardSubtitle, CardTitle, Col, FormInput, Button, Collapse, Form, Progress } from 'shards-react';

let no = 1;

function PlottingRow({ student, number, lecturerFix, topic,peminatan, lecturer }) {
    const [dosen, setDosen] = useState("");

    const handleDelete = () => {
        const promptMessage = `Apakah anda yakin ingin menghapus penguji ini "${student.group_student[0].lecturer.name}"?`
        if (!confirm(promptMessage)) return;
        fetch('/lecturer/update-plotting-dosen-penguji', {
            method: 'POST',
            body: JSON.stringify({ id: student.group_student[0].id, lecturer_id: 0 })
        }).then(res => {
            if (res.redirected) location.href = res.url;
        })
    }

    const handleSave = () => {
        fetch('/lecturer/update-plotting-dosen-penguji', {
            method: 'POST',
            body: JSON.stringify({ id: student.group_student[0].id, lecturer_id: dosen.target.value })
        }).then(res => {
            if (res.redirected) location.href = res.url;
        })
    }
    //console.log(window.SAILS_LOCALS.lecturer);
    // console.log(student.group_student.length > 0 ? student.group_student[0].lecturer.name : "a");
    return (
        <tr>
            <td>{number}</td>
            <td>{student.name}</td>
            <td>{student.nim}</td>
            <td>{student.peminatan.abbrev}</td>
            <td>{topic.name}</td>
            <td>{lecturer.name}</td>
            <td>{student.group_student[0].lecturer !== undefined ? student.group_student[0].lecturer.name : 
                <ModelSelect
                    name="lecturer"
                    value={student.lecturer}
                    modelName="lecturer"
                    displayAttribute="name"
                    onChange={value => setDosen(value)}
                />}
            </td>
            <td>
                {student.group_student[0].lecturer !== undefined ?
                    <Button outline theme="danger" className="btn btn-default" onClick={handleDelete}>Hapus</Button>
                :
                    <Button outline theme="primary" className="btn btn-default" onClick={handleSave}>Simpan</Button>
                    
                }
                
                
            </td>
        </tr>
    )
}

class PlottingTable extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { plotting, lecturer } = this.props;
        // console.log(lecturer);
        let number = 0;
        return (
            <div>
                <Card>
                    <CardBody>
                        <Row className="formComponentSpace">
                            <p>Kelompok {plotting.group.id} | {plotting.topic.name}</p>
                            <table className="table" >
                                <thead thead-dark>
                                    <tr>
                                        <th bgcolor="#E8E8E8">NO.</th>
                                        <th bgcolor="#E8E8E8">NAMA</th>
                                        <th bgcolor="#E8E8E8">NIM</th>
                                        <th bgcolor="#E8E8E8">PEMINATAN</th>
                                        <th bgcolor="#E8E8E8">TOPIK</th>
                                        <th bgcolor="#E8E8E8">NAMA PEMBIMBING</th>
                                        <th bgcolor="#E8E8E8">NAMA PENGUJI</th>
                                        <th bgcolor="#E8E8E8">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {plotting.group.students.map(student => {
                                        number++;
                                        return(
                                            <PlottingRow topicSelectionId={plotting.id}  topic={plotting.topic} student={student} number={number} lecturerFix={lecturer} lecturer={plotting.lecturer} peminatan={plotting.peminatan}/>
                                        )
                                    })}
                                </tbody>
                            </table>
                        </Row>
                    </CardBody>

                </Card>
            </div>
        )
    }
}

function Page({ lecturer, topicSelections }) {
    const isKoorLabRiset = Cookies.get('koorLabRiset');
    const isKoorKk = Cookies.get('koorKk');

    const topicSelectionFix = isKoorKk ? window.SAILS_LOCALS.topicSelectionByKk : isKoorLabRiset ? window.SAILS_LOCALS.topicSelectionByLabRiset : undefined;
    console.log(topicSelectionFix);

    // console.log(topicSelections);
    // console.log(lecturer);
    return (
        <React.Fragment>
            {/* <Navbar activeMenu={LECTURER_MENU.LIST}/> */}
            <div className="container main-content-container px-4 pb-4 container-fluid">
                <h1>Plotting Dosen Penguji</h1>
                {/* {topics.length ? <TopicTable topics={topics} /> : <p>Anda belum membuat topik.</p>} */}
                {topicSelectionFix.length ? topicSelectionFix.map(topicSelection => <PlottingTable plotting={topicSelection} lecturer={lecturer} />) : <p>Belum ada topik yang sudah di approve.</p>}
            </div>
        </React.Fragment>
    )
}

ReactDOM.render(
    <Layout>
        <Page
            topicSelections={window.SAILS_LOCALS.topicSelections}
            lecturer={window.SAILS_LOCALS.lecturer}
        /></Layout>,
    document.getElementById("root")
);
