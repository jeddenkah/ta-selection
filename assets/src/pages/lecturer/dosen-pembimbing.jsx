import React from 'react';
import ReactDOM from 'react-dom';

import Cookies from 'js-cookie';
import Navbar from '../../components/Navbar';
import Nav, { MENU as LECTURER_MENU } from '../../components/lecturer/Nav';
import Layout from '../../components/layouts';
import ScorePembimbing from '../../components/lecturer/ScorePembimbing';
import { Row, Card, CardBody, CardSubtitle, CardTitle, Col, FormInput, Button, Collapse, Form, Progress } from 'shards-react';

function PlottingRow({ student, number, peminatan, lecturer, topic, topicSelectionId, length, index, nilailist }) {
   
    const nilai = nilailist ? nilailist.find(nilailist=> nilailist.student.id == student.id):{};
    return (
        <tr>
            <td>{number}</td>
            <td>{student.name}</td>
            <td>{student.nim}</td>
            <td>{student.class}</td>
            <td>{peminatan.abbrev}</td>
            {/* <td>{student.id}</td> */}
            <td>{nilai ?
             <ScorePembimbing nilaiList={nilailist} student={student.id}/>
             : '-'}</td>
             <td>{nilai ?
             <a href={`/lecturer/evaluation-score/${student.id}/`}>Ubah Nilai</a>
             : <a href={`/lecturer/evaluation-score/${student.id}/`}>Lakukan Penilaian</a>}</td>
        </tr>
    )
}

class PlottingTable extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { plotting } = this.props;
        //console.log(plotting);
        let number = 0;
        return (
            <div>
                <Card>
                    <CardBody>
                        <Row className="formComponentSpace">
                            <p>Kelompok {plotting.group.id} | {plotting.topic.name}</p>
                            <table className="table" >
                                <thead thead-dark>
                                    <tr>
                                        <th bgcolor="#E8E8E8">NO.</th>
                                        <th bgcolor="#E8E8E8">NAMA</th>
                                        <th bgcolor="#E8E8E8">NIM</th>
                                        <th bgcolor="#E8E8E8">KELAS</th>
                                        <th bgcolor="#E8E8E8">PEMINATAN</th>
                                        <th bgcolor="#E8E8E8">NILAI</th>
                                        <th bgcolor="#E8E8E8">AKSI</th>
                                        <></>
                                    </tr>
                                </thead>
                                <tbody>
                                    {plotting.group.students.map((student, index) => {
                                        number++;
                                        return(
                                            <PlottingRow topicSelectionId={plotting.id}  topic={plotting.topic} student={student} 
                                            number={number} peminatan={plotting.group.peminatan} lecturer={plotting.lecturer} 
                                            length={plotting.group.students.length} index={index} nilailist={plotting.score}
                                            />
                                        )
                                    })}
                                </tbody>
                            </table>
                        </Row>
                    </CardBody>

                </Card>
            </div>
        )
    }
}

function Page({ topicSelections }) {
    //console.log(topicSelections);
    // console.log(lecturer);
    
    return (
        <React.Fragment>
            {/* <Navbar activeMenu={LECTURER_MENU.LIST}/> */}
            <div className="container main-content-container px-4 pb-4 container-fluid">
                <h1>List Mahasiswa Pembimbingan</h1>
                {/* {topics.length ? <TopicTable topics={topics} /> : <p>Anda belum membuat topik.</p>} */}
                {topicSelections.length ? topicSelections.map(topicSelection => <PlottingTable plotting={topicSelection} />) : <p>Belum ada mahasiswa yang akan dibimbing.</p>}
            </div>
        </React.Fragment>
    )
}

ReactDOM.render(
    <Layout>
        <Page
            topicSelections={window.SAILS_LOCALS.topicSelections}
            currentPeriod={window.SAILS_LOCALS.currentPeriod}
        /></Layout>,
    document.getElementById("root")
);
