const express = require("express");
const { default: Axios } = require("axios");
const path = require("path");
const childProcess = require("child_process");
const {promisify} = require("util");
const exec = promisify(childProcess.exec); 
const {CronJob} = require("cron");
const { BUILDER_CONFIG, RECEIVER_CONFIG, BUILD_FILENAME } = require("./webhook.config");

const cronJob = new CronJob(
	"0 */10 * * * *",
	async () => {
		await execBuild("production");
		await execBuild("development");
	}
);
cronJob.start();

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.get("/build", getBuild);
app.post("/build", requestBuild);

app.listen(BUILDER_CONFIG.port, BUILDER_CONFIG.hostname);
console.log(`Builder listening at ${BUILDER_CONFIG.baseUrl}`);


const receiver = Axios.create({
	baseURL: RECEIVER_CONFIG.baseUrl
});


async function requestBuild(req, res) {
	res.status(200).send();
	const env = req.headers["x-env"];
	await execBuild(env);
	const reqOptions = {
		headers: {
			"x-env": env
		}
	}
	receiver.post("/build/finished", reqOptions)
		.catch(handleReqError);
}

async function getBuild(req, res) {
	const env = req.headers["x-env"];
	let filename;
	if (env == "production") filename = BUILD_FILENAME.production;
	else filename = BUILD_FILENAME.development;
	const buildPath = path.resolve(__dirname, ".tmp", filename + ".tar.gz");
	res.sendFile(buildPath, (err) => {
		if (err) {
			console.error("error send file:\n", err);
			res.status(400).end();
		}
		else {
			console.log("Sucessfully sent build!")
		}
	});
}

async function execBuild(env) {
	console.log("Executing build");
	const beginTime = Date.now();
	let webpackConfig, filename, branch;
	if (env == "production") {
		webpackConfig = "webpack.prod.js";
		filename = BUILD_FILENAME.production;
		branch = "master"
	} else {
		webpackConfig = "webpack.dev.js";
		filename = BUILD_FILENAME.development;
		branch = "qa"
	}
	const jobs = [
		"git checkout " + branch,
		"git pull",
		"npm i",
		`rm -rf .tmp/${filename}`,
		`npx webpack --config ${webpackConfig} --output-path .tmp/${filename}`,
		`tar -zcvf .tmp/${filename}.tar.gz .tmp/${filename}`,
	];
	try {
		for (job of jobs) {
			console.log("exec: " + job);
			const process = await exec(job);
			process.stdout && console.log("stdout: " + process.stdout);
			process.stderr && console.log("stderr: " + process.stderr);
		}
	} catch (e) {
		return console.error(e);
	}
	console.log("Build finished!");
	console.log(`Execution time: ${(Date.now() - beginTime)/1000}s`)
}


function handleReqError(error) {
	if (error.response) {
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log('Error', error.message);
    }
} 