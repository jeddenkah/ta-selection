/* Replace with your SQL commands */


CREATE TABLE `score_penguji` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `student_id` int(11) NOT NULL,
  `lecturer_id` int(11) NOT NULL,
  `rubric_id` int(11) NOT NULL,
  `sub_clo_id` int(11) NOT NULL,
  `period_id` int(11) NOT NULL,
  `nilai_per_rubric` int(11) NOT NULL,
  `portion` int(11) NOT NULL,
  `nilai_calculated_portion` float NOT NULL,
  `nilai_calculated_bobot` float NOT NULL,
  `nilai_calculated_bobot_portion` float NOT NULL,
  `clo_id` int(11) NOT NULL,
  `plo_id` int(11) NOT NULL
)
