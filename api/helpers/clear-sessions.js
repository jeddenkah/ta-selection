module.exports = {


	friendlyName: 'Clear sessions',
  
  
	description: 'Menghapus semua session',
  
  
	inputs: {
	  req: {
		type: 'ref',
		description: 'The current incoming request (req).',
		required: true
	  }
	},
  
  
	exits: {
  
	  success: {
		description: 'All done.',
	  },
  
	},
  
  
	fn: function (inputs, exits) {
		const req = inputs.req;
		delete req.session.studentId;
		delete req.session.lecturerId;
		delete req.session.adminId;
		delete req.session.userType;
		delete req.session.koorMetlit;
		return exits.success();
	}
  
  
  };
  
  