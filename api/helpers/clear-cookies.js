module.exports = {


  friendlyName: 'Clear cookies',


  description: 'Menghapus semua cookie',


  inputs: {
    res: {
      type: 'ref',
      required: true,
      description: 'The current response (res).',
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: function (inputs, exits) {
    const res = inputs.res;
    res.clearCookie('userName');
    res.clearCookie('userIdVal');
    res.clearCookie('userType');
    res.clearCookie('koorMetlit');
    res.clearCookie('dosenPembina');
    res.clearCookie('dosenPenguji');
    res.clearCookie('koorKk');
    res.clearCookie('koorLabRiset');
    res.clearCookie('role')
    return exits.success();
  }


};

