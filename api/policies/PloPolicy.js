module.exports = async function (req, res, proceed) {
    if (req.session.adminId || req.session.koorMetlit) {
      return proceed();
    }
    await sails.helpers.clearCookies(res);
    return res.redirect('/login');
  
  };