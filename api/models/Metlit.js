module.exports = {
    attributes: {
        class: {
            type: 'string'
        },
        lecturer: {
            columnName: 'lecturer_id',
            model: 'lecturer',
        },
        period: {
            columnName: 'period_id',
            model: 'period',
        },
        prodi: {
            columnName: 'prodi_id',
            model: 'prodi'
        },
        students: {
            collection: 'student',
            via: 'metlit'
        }
    }
}