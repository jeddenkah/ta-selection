module.exports = {
    tableName: 'eprt',
    attributes: {
        score: {
            type: 'number',
            required: true
        },
        eprtUrl: {
            type: 'string',
            required: true
        },
        eprtFd: {
            type: 'string',
            required: true
        },
        fileName: {
            columnName: 'file_name',
            type: 'string',
            required: true
        },
        owner: {
            columnName: 'student_id',
            model: 'student',
            unique: true
        }
    }
};