module.exports = {
    attributes: {
        name: {
            type: 'string'
        },
        abbrev: {
            type: 'string'
        },
        lecturers: {
            collection: 'lecturer',
            via: 'prodi'
        },
        peminatan: {
            collection: 'peminatan',
            via: 'prodi'
        },
        metlit: {
            collection: 'metlit',
            via: 'prodi'
        }
    },
}