module.exports = {
    
    findWaitingOrApprovedTopics: async function(studentId) {
        const periodSetting = await AppSetting.findOne({ name: 'period_id' });
        const activePeriodId = periodSetting.settingValue;
        const groupsRecords = await GroupStudent.find({
            where: {
                student: studentId,
            },
            select: ['group']
        }).populate('group')
        .sort('id DESC');
        const topicSelections = await Promise.all(groupsRecords.map(record => TopicSelection.find(
            { group: record.group.id,
              period: activePeriodId,
              status: ['WAITING', 'APPROVED'] })
          ));
        const topicSels = [].concat(...topicSelections); // flatten
        return topicSels;
    }
}