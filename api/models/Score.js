module.exports = {
    tableName: 'score',
    attributes: {
        student: {
            columnName: 'student_id',
            model: 'student'
        },
        lecturer:{
            columnName: 'lecturer_id',
            model: 'lecturer'
        },
        rubric:{
            columnName: 'rubric_id',
            model: 'SubCloRubric'
        },
        subClo:{
            columnName: 'sub_clo_id',
            model: 'SubClo'
        }
    },
}
