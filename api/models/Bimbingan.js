module.exports = {
    tableName: 'bimbingan',
    attributes: {
        tanggal: {
            columnName: 'tanggal',
            type:'string'
        },
        catatan: {
            columnName: 'catatan',
            type:'string'
        },
        studentId: {
            columnName: 'student_id',
            model: 'student'
        },
        status: {
            columnName: 'status',
            type: 'string'
        }
    },
}