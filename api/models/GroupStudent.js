module.exports = {
    tableName: 'group_student',
    attributes: {
        group: {
            columnName: "group_id",
            model: 'group'
        },
        student: {
            columnName: "student_id",
            model: 'student',
        },
        lecturer: {
            columnName: "lecturer_id",
            model: 'lecturer',
        }
    },
    getStudentGroupIds: async function (studentId) {
        //const periodId = await AppSetting.getPeriodId();
        const groupsRecords = await GroupStudent.find({
            where: {
                student: studentId,
            },
            select: ['id']
        });
        const groupIds = groupsRecords.map(gs => gs.id);
        return groupIds;
    }
};
