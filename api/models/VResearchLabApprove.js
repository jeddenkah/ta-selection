module.exports = {
    tableName: 'v_reserach_lab_approve',
    migrate: 'safe',
    attributes: {
        student: {
            model: 'student'
        },
        group: {
            model: 'group' 
        },
        peminatan: {
            model: 'peminatan' 
        },
        lecturer: {
            model: 'lecturer'
        },
        topic: {
            model: 'topic'
        }
    }
}