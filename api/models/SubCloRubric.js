module.exports = {
    tableName: 'sub_clo_rubric',
    attributes: {
        subClo: {
            columnName: 'sub_clo_id',
            model: 'subclo',
        },
        rubricCode: {
            columnName: 'rubric_code',
            type: 'number',
            description: 'skala (antara 1-5) penilaian subclo.'
        },
        score: {
            columnName: 'score_per_rubric',
            type: 'number',
            min: 0,
            max: 100,
            description: 'nilai rubrik'
        },
        description: {
            type: 'string',
        },
        isDeleted: {
            columnName: 'is_deleted',
            type: 'boolean',
            defaultsTo: false
        }
    },
}