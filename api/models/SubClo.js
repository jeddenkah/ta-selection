module.exports = {
    tableName: 'sub_clo',
    attributes: {
        clo: {
            columnName: 'clo_id',
            model: 'clo',
        },
        description: {
            type: 'string',
        },
        portion: {
            type: 'number',
            min: 0,
            max: 100,
            description: 'bobot penilaian sub clo terhadap keseluruhan nilai, dalam persen'
        },
        isDeleted: {
            columnName: 'is_deleted',
            type: 'boolean',
            defaultsTo: false
        },
        period: {
            columnName: 'period_id',
            model: 'period',
        }
    },
}