module.exports = {
    tableName: 'peminatan',
    attributes: {
        name: {
            type: 'string',
        },
        abbrev: {
            type:'string',
            unique: true,
        },
        prodi: {
            columnName: 'prodi_id',
            model: 'prodi'
        },
        students: {
            collection: 'student',
            via: 'peminatan'
        },
        topics: {
            collection: 'topic',
            via: 'peminatan'
        },
        labRisetInduk: {
            model: 'labriset'
        }
    }
}