module.exports = {
    tableName: 'app_setting',
    attributes: {
        name: {
            type: 'string',
            unique: true
        },
        settingValue: {
            columnName: 'setting_value',
            type: 'string'
        },
    },
    getPeriodId: async function () {
        const periodSetting = await AppSetting.findOne({ name: 'period_id' });
        return periodSetting ? periodSetting.settingValue : null;
    },
    getPeriod: async function () {
        const periodSetting = await AppSetting.findOne({ name: 'period_id' });
        const periodId = periodSetting.settingValue;
        if (periodId) return await Period.findOne({id: periodId});
    }
}