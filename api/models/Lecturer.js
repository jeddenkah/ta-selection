module.exports = {
    attributes: {
        nik: {
            type: 'string',
            unique: true
        },
        name: {
            type: 'string',
        },
        email: {
            type: 'string',
            allowNull: true
        },
        jfa: {
            columnName: 'jfa_id',
            model: 'jfa'
        },
        labRiset: {
            columnName: 'lab_riset_id',
            model: 'labriset'
        },
        prodi: {
            columnName: 'prodi_id',
            model: 'prodi'
        },
        topics: {
            collection: 'topic',
            via: 'lecturer'
        },
        groupstudent: {
            collection: 'groupstudent',
            via: 'lecturer',
        },
        hashedPassword: {
            columnName: 'hashed_password',
            type: 'string',
            allowNull: true
        },
        newPasswordToken: {
            columnName: 'new_password_token',
            type: 'string',
            allowNull: true,
            description: 'Token yang digunakan untuk membuat password baru'
        },
        newPasswordTokenExpiresAt: {
            columnName: 'new_password_token_expires_at',
            type: 'ref',
        },
        lecturerCode: {
            columnName: 'lecturer_code',
            type: 'string'
        },
        roles: {
            collection: 'masterrole'
        },
    },
    customToJSON: function () {
        return _.omit(this, sails.config.custom.lecturerPrivateCols);
    }
}