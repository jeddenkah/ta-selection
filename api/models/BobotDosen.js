module.exports = {
    tableName: 'bobot_dosen',
    attributes: {
        dosenPembimbing: {
            columnName: 'dosen_pembimbing',
            type: 'number'
        },
        dosenPenguji: {
            columnName: 'dosen_penguji',
            type: 'number'
        },
        period: {
            columnName: 'period_id',
            model: 'period'
        },
    },
}
