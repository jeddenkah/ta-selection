module.exports = {
    attributes: {
        name: {
            type: 'string'
        },
        quota: {
            type: 'number',
            description: 'Quota per group. Maksimum jumlah anggota per kelompok.'
        },
        deskripsi: {
            type:'string'
        },
        peminatan: {
            columnName: 'peminatan_id',
            model: 'peminatan'
        },
        period: {
            columnName: 'period_id',
            model: 'period'
        },
        lecturer: {
            columnName: 'lecturer_id',
            model: 'lecturer'
        },
        isDeleted: {
            columnName: 'is_deleted',
            type: 'boolean',
            defaultsTo: false
        },
        isArchived: {
            columnName: 'is_archived',
            type: 'boolean',
            defaultsTo: false
        },
        groups: {
            collection: 'group',
            through: 'topicselection',
            via: 'topic'
        }
    }
}