module.exports = {
    tableName: 'score_pembimbing',
    attributes: {
        student: {
            columnName: 'student_id',
            model: 'student'
        },
        lecturer:{
            columnName: 'lecturer_id',
            model: 'lecturer'
        },
        rubric:{
            columnName: 'rubric_id',
            model: 'SubCloRubric'
        },
        subClo:{
            columnName: 'sub_clo_id',
            model: 'SubClo'
        },
        period:{
            columnName: 'period_id',
            model: 'period'
        },
        nilaiPerRubric:{
            columnName: 'nilai_per_rubric',
            type: 'number'
        },
        portion:{
            columnName: 'portion',
            type: 'number'
        },
        nilaiCalculatedPortion:{
            columnName: 'nilai_calculated_portion',
            type: 'number'
        },
        nilaiCalculatedBobot:{
            columnName: 'nilai_calculated_bobot',
            type: 'number'
        },
        nilaiCalculatedBobotPortion:{
            columnName: 'nilai_calculated_bobot_portion',
            type: 'number'
        },
        clo:{
            columnName: 'clo_id',
            model: 'clo'
        },
        plo:{
            columnName: 'plo_id',
            model: 'plo'
        }
    },
}
