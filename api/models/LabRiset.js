module.exports = {
    tableName: 'lab_riset',
    attributes: {
        name: {
            type: 'string',
        },
        abbrev: {
            type:'string',
            unique: true,
        },
        kk: {
            columnName: 'kk_id',
            model: 'kk'
        },
        peminatan: {
            collection: 'peminatan',
            via: 'labRisetInduk'
        }
    }
}