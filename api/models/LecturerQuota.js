//const Topic = require("./Topic");

module.exports = {
    attributes: {
        lecturer: {
            columnName: 'lecturer_id',
            model: 'lecturer'
        },
        period: {
            columnName: 'period_id',
            model: 'period'
        },
        lecturerQuota: {
            columnName: 'lecturer_quota',
            type: 'number',
            allowNull: true
        },
        
    },
    customToJSON: function () {
        const  lecturerQuotaPublicColumns  =  ['id', 'lecturer', 'period', 'lecturer_quota'];
        return _.pick(this, lecturerQuotaPublicColumns);
    },
    findApprovedTopics: async function(lecturerId, periodId) {
        const topics = await Topic.find(
            {
                lecturer: lecturerId,
                period: periodId
            }
        );
        const topicSelections = await Promise.all(topics.map(record => TopicSelection.find(
            { topic: record.id,
              period: periodId,
              status: ['APPROVED'] }).populate('group')
          ));
        const topicSels = [].concat(...topicSelections); // flatten
        return topicSels;
    },
    findApprovedGroups: async function(lecturerId, periodId) {
        const topicSels = await LecturerQuota.findApprovedTopics(lecturerId,periodId);
        const groups = await Promise.all( topicSels.map ( r => Group.findOne( { id: r.group.id }).populate('students')));
        const groupsResult = new Map(groups.map(obj => [obj.id, obj.students.length]));
        let obj = {};
        groupsResult.forEach(function(value, key){
            obj[key] = value
        });
        return obj;
        //const students = groups.map ( r => ({ group_id : r.id, members : r.students, count : r.students.length}));
    },
    findQuotaForLecturer: async function(lecturerId, periodId) {
        const lecturerQuota = await LecturerQuota.find( {
            lecturer: lecturerId,
            period: periodId
        });
        var currentQuota = 0;
        const currentPeriod = await Period.findOne( { id: periodId });
        if (currentPeriod && currentPeriod.perLecturerQuota)
            currentQuota = currentPeriod.perLecturerQuota;
        if (lecturerQuota && lecturerQuota.lecturer_quota) 
            currentQuota = lecturerQuota.lecturer_quota;
        return currentQuota;
    }
}