module.exports = {
    tableName: 'v_student_report_reviewer',
    migrate: 'safe',
    attributes: {
        student: {
            model: 'student'
        },
        group: {
            model: 'group' 
        },
        optionNum: {
            type: 'number'
        },
        status: {
            type: 'string',
            isIn: ['WAITING', 'APPROVED', 'REJECTED', 'AUTO_CANCELLED']
        },
        period: {
            model: 'period'
        },
        lecturer: {
            model: 'lecturer'
        },
        reviewer: {
            columnName: 'reviewer',
            model: 'lecturer'
        },
        topic: {
            columnName: 'topic',
            model: 'topic'
        },
        peminatan: {
            columnName: 'peminatan',
            model: 'peminatan'
        }
    }
}