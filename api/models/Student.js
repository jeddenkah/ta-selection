module.exports = {
    attributes: {
        nim: {
            type: 'number',
            unique: true
        },
        name: {
            type: 'string'
        },
        email: {
            type: 'string',
        },
        class: {
            type: 'string'
        },
        peminatan: {
            columnName: 'peminatan_id',
            model: 'peminatan'
        },
        metlit: {
            columnName: 'metlit_id',
            model: 'metlit',
        },
        hashedPassword: {
            columnName: 'hashed_password',
            type: 'string',
            allowNull: true
        },
        newPasswordToken: {
            columnName: 'new_password_token',
            type: 'string',
            allowNull: true,
            description: 'Token yang digunakan untuk membuat password baru'
        },
        newPasswordTokenExpiresAt: {
            columnName: 'new_password_token_expires_at',
            type: 'ref',
        },
        ipk: {
            type: 'number',
            columnType: 'DECIMAL(6,2)'
        },
        groups: {
            collection: 'group',
            through: 'groupstudent',
            via: 'student',
        },
        groupstudent: {
            collection: 'groupstudent',
            via: 'student',
        },
        ta: {
            columnName: 'ta_id',
            model: 'ta'
        },
        eprt: {
            columnName: 'eprt_id',
            model: 'eprt'
        },
        form: {
            columnName: 'form_id',
            model: 'form'
        },
    },
    customToJSON: function () {
        const { studentPublicColumns } = sails.config.custom;
        if (this.waitingOrApprovedTopics) {
            const studentPublicColumnsExtended = ['waitingOrApprovedTopics', ...studentPublicColumns];
            return _.pick(this, studentPublicColumnsExtended);
        }
        return _.pick(this, studentPublicColumns);
    }
    
}