module.exports = {
    attributes: {
        ploCode: {
            columnName: 'plo_code',
            type: 'string',
        },
        description: {
            type: 'string'
        },
        isDeleted: {
            columnName: 'is_deleted',
            type: 'boolean'
        },
        prodi: {
            columnName: 'prodi_id',
            model: 'prodi',
        },
        period: {
            columnName: 'period_id',
            model: 'period',
        }
    },
}
