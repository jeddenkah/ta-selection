module.exports = {
    tableName: 'master_role',
    attributes: {
        name: {
            type: 'string',
            required: true
        },
        resourceId: {
            type: 'number',
            isInteger: true,
            allowNull: true
        },
        resourceType: {
            type: 'string',
            isIn: ['metlit', 'labriset']
        },
        isCoordinator: {
            type: 'boolean'
        }
    }
};
