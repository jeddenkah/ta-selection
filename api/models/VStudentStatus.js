module.exports = {
    tableName: 'v_student_status',
    migrate: 'safe',
    attributes: {
        student: {
            model: 'student'
        },
        group: {
            model: 'group' 
        },
        optionNum: {
            type: 'number'
        },
        status: {
            type: 'string',
            isIn: ['WAITING', 'APPROVED', 'REJECTED', 'AUTO_CANCELLED']
        },
        period: {
            model: 'period'
        }
    }
}