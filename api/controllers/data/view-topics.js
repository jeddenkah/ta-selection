module.exports = {
    friendlyName: 'View topics',

    description: 'mengirim daftar topik pada tahun ajaran yang aktif',

    inputs: {
        peminatanId: {
            type: 'string'
        },
        periodId: {
            type: 'number'
        }
    },

    exits: {
        success: {
            viewTemplatePath: 'pages/data/topics'
        }
    },

    fn: async function (inputs, exits) {
        const MAX_ROWS = 500;
        const currentPeriodId = await AppSetting.getPeriodId();
        // const userType = this.req.cookies.userType;
        const userType = this.req.session.userType;
        const peminatanList = await Peminatan.find();
        const periods = await Period.find();
        const currentPeriod = await Period.findOne({ id: currentPeriodId });

        if( this.req.session.studentId ) {
            const student =  await Student.findOne({id: this.req.session.studentId})
            const topics = student.peminatan ? await Topic.find({
                where: {
                peminatan: student.peminatan,
                isDeleted: false,
                isArchived: false,
                period: currentPeriodId
                }
            }).populate('lecturer')
            .populate('peminatan')
            .populate('period')
            : null


//            console.log(topics)
           
        return exits.success({ topics, currentPeriod, peminatanList, periods, userType });
        }else{
        const topics = await Topic.find({
            where: {
                peminatan: inputs.peminatanId ? inputs.peminatanId : undefined,
                isDeleted: false,
                isArchived: false,
                period: inputs.periodId ? inputs.periodId : undefined
            },
            limit: MAX_ROWS
        }).populate('lecturer')
            .populate('peminatan')
            .populate('period');
        
        return exits.success({ topics, currentPeriod, peminatanList, periods, userType });
    }
      
        
     
        
    }
}
