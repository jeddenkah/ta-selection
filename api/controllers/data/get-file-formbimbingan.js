module.exports = {
    friendlyName: 'Download form bimbingan',

    description: '',

    inputs: {
        studentId: { required: true, type: 'number' }
    },

    exits: {
        success: {
            outputDescription: '',
            outputType: 'ref'
        },
        notFound: {
            responType: 'notFound'
        }
    },

    fn: async function (inputs, exits) {

        var form = await Form.findOne({ owner: inputs.studentId });


        if (!form) {
            throw 'notFound'
        }

        this.res.type('application/pdf')

        var downloading = await sails.startDownload(form.formFd)

        return exits.success(downloading)
    }
}