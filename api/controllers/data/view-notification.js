module.exports = {
    friendlyName: 'View Notification',

    description: 'Menampilkan Notification',

    exits: {
        success: { viewTemplatePath: 'pages/data/notification' },
        userNotFound: {}
    },

    fn: async function (inputs, exits) {
        const studentId = this.req.session.studentId;
        const student = await Student.findOne({ id: studentId })
        if (student) {
            return exits.success();

        }
        return exits.userNotFound();
    }
    
}