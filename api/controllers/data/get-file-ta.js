module.exports = {
    friendlyName: 'Download ta',

    description: '',

    inputs: {
        studentId: { required: true, type: 'number' }
    },

    exits: {
        success: {
            outputDescription: '',
            outputType: 'ref'
        },
        notFound: {
            responType: 'notFound'
        }
    },

    fn: async function (inputs, exits) {

        var ta = await Ta.findOne({ owner: inputs.studentId });


        if (!ta) {
            throw 'notFound'
        }

        this.res.type('application/pdf')

        var downloading = await sails.startDownload(ta.taFd)

        return exits.success(downloading)
    }
}