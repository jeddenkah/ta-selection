module.exports = {
    friendlyName: 'Download EPRT',

    description: '',

    inputs: {
        studentId: { required: true, type: 'number' },
        fileName: { required: true, type: 'string' }
    },

    exits: {
        success: {
            outputDescription: '',
            outputType: 'ref'
        },
        notFound: {
            responType: 'notFound'
        }
    },

    fn: async function (inputs, exits) {

        var eprt = await Eprt.findOne({ owner: inputs.studentId, fileName: inputs.fileName });

        if (!eprt) {
            throw 'notFound'
        }

        this.res.type('application/pdf')

        var downloading = await sails.startDownload(eprt.eprtFd)

        return exits.success(downloading)
    }
}