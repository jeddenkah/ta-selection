module.exports = async function setUserRole(req, res, user, userType) {
	await sails.helpers.clearCookies(res);
	await sails.helpers.clearSessions(req)
	req.session[userType.toLowerCase() + 'Id'] = user.id;
	req.session['userType'] = userType;
	res.cookie('userName', user.name);
	res.cookie('userIdVal', user.id);
	res.cookie('userType', userType);
	if (userType == "lecturer") {
		let roleName = '';
		const roleLength = user.roles.length;
		const role = roleLength != 0 ? user.roles.map((r, i) => {
			console.log(`Role: ${r.name}`);
			if (r.resourceType == 'metlit' && r.isCoordinator) {
				req.session.koorMetlit = true;
				res.cookie('koorMetlit', true)
			}
			if (r.name == 'Dosen Pembina') {
				res.cookie('dosenPembina', true);
			}
			if (r.name == 'Dosen Penguji') {
				res.cookie('dosenPenguji', true)
			}
			if (r.resourceType == 'kk' && r.isCoordinator) {
				res.cookie('koorKk', user.labRiset.kk)
			}
			if (r.resourceType == 'labriset' && r.isCoordinator) {
				res.cookie('koorLabRiset', user.labRiset.id)
			}
			if (r.name == 'Dosen Metlit') {
				res.cookie('dosenMetlit', true)
			}

			return roleName += r.name + (roleLength - 1 == i ? '' : ', ')

		}) : 'Dosen';
		roleName = roleName != '' ? roleName : role;
		res.cookie("role", roleName);
	}
}