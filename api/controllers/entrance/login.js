const bcrypt = require('bcrypt');
const setUserRole = require('./set-user-role');

module.exports = {


  friendlyName: 'Login',


  description: 'Login mahasiswa/dosen.',


  inputs: {
    password: { required: true, type: 'string' },
    userIdVal: { required: true, type: 'number' }, //nik atau nim
    userType: { required: true, type: 'string' },
  },


  exits: {
    redirect: { responseType: 'redirect' },
    userNotFound: {},
    passwordNotSet: {},
    incorrectPassword: {},
    badRequest: {
      statusCode: 404
    }
  },


  fn: async function (inputs, exits) {
    this.res.setHeader('Access-Control-Allow-Credentials', 'true');
    const { password, userIdVal, userType } = inputs;
    let userIdKey, User;
    switch (userType) {
      case 'student':
        userIdKey = 'nim';
        User = Student;
        break;
      case 'lecturer':
        userIdKey = 'nik';
        User = Lecturer;
        break;
      case 'admin':
        User = Admin;
        userIdKey = 'id';
        break;
      default:
        return exits.badRequest();
    }
    let user;
    if (userType == "lecturer") user = await User.findOne({ [userIdKey]: parseInt(userIdVal) }).populate("labRiset").populate("roles");
    else user = await User.findOne({ [userIdKey]: parseInt(userIdVal) })
    if (!user) return exits.userNotFound();
    if (!user.hashedPassword) return exits.passwordNotSet();
    const isPasswordMatch = await bcrypt.compare(password, user.hashedPassword);
    //const isPasswordMatch = password == user.hashedPassword ? true : false;
    if (!isPasswordMatch) return exits.incorrectPassword();
    await setUserRole(this.req, this.res, user, userType);
    return exits.redirect('/');
  }


};
