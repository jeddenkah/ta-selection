module.exports = {
    friendlyName: 'New plo',

    description: 'Membuat plo baru',

    inputs: {
        ploCode: { required: true, type: 'string' },
        description: { required: true, type: 'string' },
        prodi: { required: true, type: 'number' },
        period: { required: true, type: 'number' },
    },

    exits: {
        success: {}
    },

    fn: async function (inputs, exits) {
        await Plo.create({ ploCode: inputs.ploCode, description: inputs.description, prodi : inputs.prodi, period:inputs.period, isDeleted: false });
        return exits.success()
    }
}
