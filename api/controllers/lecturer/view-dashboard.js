module.exports = {
    friendlyName: 'View dashboard',

    description: 'Menampilkan dashboard mahasiswa',

    exits: {
        success: { viewTemplatePath: 'pages/lecturer/dashboard' },
        redirect: {responseType: 'redirect'},
        userNotFound: {}
    },

    fn: async function (inputs, exits) {
        const lecturerId = this.req.session.lecturerId;
        const lecturer = await Lecturer.findOne({ id: lecturerId }).populate('roles').populate('labRiset');
        if (lecturer) {
            return exits.success();
        }
        return exits.userNotFound();
    }
}