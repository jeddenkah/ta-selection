module.exports = {
    friendlyName: "Update Ploting Dosen Pembimbing",

    description: "Mengupdate status dari topic selections",

    inputs: {
        id: {
            required: true,
            type: 'number'
        },
        status: {
            required: true,
            type: "string"
        }
    },

    exits: {
        success: {
            responseType: 'redirect'
        }
    },

    fn: async function(inputs, exits) {
        await TopicSelection.updateOne({ id: inputs.id }).set({
            status: inputs.status
        })
        exits.success('/lecturer/plotting-dosen-pembimbing')
        // All done.
        return;
    }
};
