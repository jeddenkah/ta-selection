module.exports = {

    friendlyName: 'View input score DE',

    description: 'Menampilkan form input nilai DE',

    inputs: {
        studentId: { required: true, type: 'string' }
    },

    exits: {
        success: { viewTemplatePath: 'pages/lecturer/evaluation-score' }
    },

    fn: async function (inputs, exits) {
        const lecturer = this.req.session.lecturerId;
        const studentId = inputs.studentId;
        
        const clo = await Clo.find();
        //const clo = await Clo.find();

        const cloAll = await Promise.all(clo.map(async c => {
            let subCloList = await SubClo.find({clo: c.id});
            subCloList = await Promise.all(subCloList.map(async sc => {
                const subCloRubricList = await SubCloRubric.find({subClo: sc.id});
                return Object.assign(sc, {subCloRubricList})
            }))
            return Object.assign(c, {subCloList});
        }));

        const ta = await Ta.find({owner : studentId});
        //console.dir(ta)
        
        const scoreAll = await Score.find({student: studentId, lecturer : lecturer})
        .populate('rubric');
        const bimbingan = await Bimbingan.find({studentId: studentId,status:"APPROVED"})
        //console.dir(score)

        // const scoreAll = await Promise.all(score.map(async scr => {
        //     const subCloRubric = await SubCloRubric.find({id: scr.rubric});
           
        //     return Object.assign(scr, {subCloRubric});
        // }));

        return exits.success({ bimbingan,studentId,lecturer, cloAll, ta , scoreAll});

    }
}