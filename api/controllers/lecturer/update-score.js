module.exports = {
    friendlyName: 'Edit Score',

    description: 'Mengedit score',

    inputs: {
        rubric: { required: true, type: 'number' },
        subClo: { required: true, type: 'number' },
        nilaiPerRubric : { required: true, type: 'number' },
        portion : { required: true, type: 'number' },
        lecturer: { required: true, type: 'number' },
        student: { required: true, type: 'number' },
    },

    exits: {
        success: {
            responseType: 'redirect'
        }
    },

    fn: async function (inputs, exits) {
        let cek=[];
    const groupStudent = await GroupStudent.find({lecturer:inputs.lecturer,student:inputs.student});
    groupStudent.map((group)=>{
        cek.push(group.student)
    })
        const currentPeriodId = await AppSetting.getPeriodId();
        const bobotlist = await BobotDosen.find({
         where: {
             period: currentPeriodId
         }
         })
         
         const bobotPembimbing = bobotlist.map(bobot=>bobot.dosenPembimbing);
         const bobotPenguji = bobotlist.map(bobot=>bobot.dosenPenguji);
         const nilaiperRubric = inputs.nilaiPerRubric;
       const portion = inputs.portion;
        var subCloObj = await SubClo.findOne(inputs.subClo).populate('clo');
        var clo_id = subCloObj.clo.id;
        var plo_id = subCloObj.clo.plo;
        const newScore = await Score.update({ student: inputs.student, lecturer: inputs.lecturer,subClo: inputs.subClo })
            .set({ rubric: inputs.rubric}).fetch();
            var k = {student: inputs.student, lecturer: inputs.lecturer, subClo: inputs.subClo};
            var nilaiValues = {rubric: inputs.rubric,
                nilaiPerRubric : nilaiperRubric,
                nilaiCalculatedPortion : nilaiperRubric*portion/100,
                nilaiCalculatedBobot : nilaiperRubric*bobotPenguji/100,
                nilaiCalculatedBobotPortion : nilaiperRubric*portion/100*bobotPenguji/100,
                clo: clo_id,
                plo: plo_id, period: currentPeriodId};   
            if(newScore && cek.length>0){
                var r = await ScorePenguji.findOne(k);
                if (!r) 
                    await ScorePenguji.create({...k, ...nilaiValues});
                else
                    await ScorePenguji.update(k).set(nilaiValues);
                exits.success('/lecturer/dosen-penguji')
            }else{
                var r = await ScorePembimbing.findOne(k);
                if (!r)
                    await ScorePembimbing.create({...k, ...nilaiValues});
                else
                    await ScorePembimbing.update(k).set(nilaiValues);
                exits.success('/lecturer/dosen-pembimbing');        
            }
    }
}
