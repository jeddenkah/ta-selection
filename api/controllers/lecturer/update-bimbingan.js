module.exports = {
    friendlyName: "Approval Bimbingan",

    description: "Mengupdate status dari bimbingan",

    inputs: {
        id: {
            required: true,
            type: 'number'
        },
        status: {
            required: true,
            type: "string"
        }
    },

    exits: {
        success: {
            responseType: 'redirect'
        }
    },

    fn: async function(inputs, exits) {
        await Bimbingan.updateOne({ id: inputs.id }).set({
            status: inputs.status
        })
        exits.success('/lecturer/bimbingan')
        // All done.
        return;
    }
};
