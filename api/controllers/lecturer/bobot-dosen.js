// const { inputCSS } = require("react-select/src/components/Input");

module.exports = {
    friendlyName: 'bobot dosen',

    description: 'Mengatur bobot dosen',

    inputs: {

        bobot_pembimbing: {
            type: 'number',
            required: true
        },

        bobot_penguji: {
            type: 'number',
            required: true
        },
        period_id:{
            type: 'number',
            required:true
        }

    },

    exits: {
        badRequest: {
            description: 'Bad request'
        },
        success: {
            viewTemplatePath: 'pages/lecturer/koor-metlit/bobot'
        }
    },

    fn: async function (inputs, exits) {
        const currentPeriodId = await AppSetting.getPeriodId();
        const dataBobot = {
            dosenPembimbing: inputs.bobot_pembimbing,
            dosenPenguji: inputs.bobot_penguji,
            period: inputs.period_id
        }

        var newBobot = await BobotDosen.findOrCreate({period: inputs.period_id},dataBobot).exec(async(err, bobot, wasCreated)=> {
            if (err) { return res.serverError(err); }
          
            if(wasCreated) {
                return this.res.ok(bobot);
            }
            else {
              const updateBobot = await BobotDosen.updateOne({period: inputs.period_id}).set({
                dosenPembimbing: inputs.bobot_pembimbing,
                dosenPenguji: inputs.bobot_penguji,
              })

              return this.res.ok(updateBobot);
            }
          });
        
        
        //exits.success({ newEprt });
    }

}