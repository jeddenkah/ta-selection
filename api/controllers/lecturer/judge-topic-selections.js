async function approveSelection(topicSelectionId) {
    const topicSelection = await TopicSelection.updateOne({ id: topicSelectionId })
        .set({ status: 'APPROVED' });
    if (topicSelection.optionNum == 1) {
        await TopicSelection.updateOne({ group: topicSelection.group, optionNum: 2 })
            .set({ status: 'AUTO_CANCELLED' });
    }
}
async function rejectSelections(topicSelectionId) {
    const topicSelection = await TopicSelection.updateOne({ id: topicSelectionId })
        .set({ status: 'REJECTED' });
    if (topicSelection.optionNum == 1) {
        await TopicSelection.updateOne({ group: topicSelection.group, optionNum: 2, status: 'AUTO_CANCELLED' })
            .set({ status: 'WAITING' });
    }
}

module.exports = {
    friendlyName: 'Judge topic',

    description: 'menyutujui atau menolak topik selection',

    inputs: {
        approvedIds: {
            required: true,
            type: 'ref',
            description: 'topic-selection id yang disetujui'
        },
        rejectedIds: {
            required: true,
            type: 'ref',
            description: 'topic-selection id yang ditolak'
        }
    },

    exits: {
        success: { responseType: 'redirect' },
        quotaExceeded: {}
    },

    fn: async function (inputs, exits) {
        const { approvedIds, rejectedIds } = inputs;
        let lecturerMap = new Map()
        var thePeriod = 0;
        for (const topicSelectionId of approvedIds) {
            const topicSel = await TopicSelection.findOne({ id: topicSelectionId }).populate("topic");
            const lecturer = topicSel.topic.lecturer;
            var periodKey = "P" + topicSel.period + "L" + lecturer;
            if (!lecturerMap.has(periodKey))
                lecturerMap.set(periodKey,{ groupMap: new Map(), period: topicSel.period, lecturer: lecturer});
            if (!topicSel.group) continue;
            var group = await Group.findOne( { id: topicSel.group }).populate('students');
            var groupStudents = group.students;
            let groupMapping = lecturerMap.get(periodKey).groupMap;
            groupMapping.set(group.id,groupStudents.length);
        }
        sails.log('lecturerMap');
        sails.log(lecturerMap);
        for (const [periodKey, mapEntry] of lecturerMap) {
            const lecturer = mapEntry.lecturer;
            const period = mapEntry.period;
            const existingStudents = await LecturerQuota.findApprovedGroups(lecturer,period);
            const existingStudentsMap = new Map(Object.entries(existingStudents));
            existingStudentsMap.forEach(
                function(groupCnt, groupId){
                    mapEntry.groupMap.set(parseInt(groupId),groupCnt);
                }
            );
        }
        sails.log('lecturerMap');
        sails.log(lecturerMap);
        for (const topicSelectionId of rejectedIds) {
            const topicSel = await TopicSelection.findOne({ id: topicSelectionId }).populate("topic");
            const lecturer = topicSel.topic.lecturer;
            var periodKey = "P" + topicSel.period + "L" + lecturer;
            if (!lecturerMap.has(periodKey))
                lecturerMap.set(periodKey,{ groupMap: new Map(), period: topicSel.period, lecturer: lecturer});
            var group = await Group.findOne( { id: topicSel.group }).populate('students');
            var groupStudents = group.students;
            let groupMapping = lecturerMap.get(periodKey).groupMap;
            groupMapping.set(group.id,0); // set to zero on rejecteds
        }
        sails.log('lecturerMap');
        sails.log(lecturerMap);
        for (const [periodKey, mapEntry] of lecturerMap) {
            const lecturer = mapEntry.lecturer;
            const period = mapEntry.period;
            const students = mapEntry.groupMap;
            var totalStudents = 0;
            sails.log('group data for lecturer '+lecturer +' period '+period);
            sails.log(students);
            students.forEach(
                function(groupCnt, groupId){
                    totalStudents += groupCnt;
                }
            );
            sails.log(' total students = ' + totalStudents);
            var lecturerQuota =  await LecturerQuota.findQuotaForLecturer(lecturer,period);
            sails.log("lecturer quota = ");
            sails.log(lecturerQuota);
            if (totalStudents > lecturerQuota)
                return exits.quotaExceeded();
        }    
        
        for (const topicSelectionId of approvedIds) {
            await approveSelection(topicSelectionId);
        }
        for (const topicSelectionId of rejectedIds) {
            await rejectSelections(topicSelectionId);
        }
        return exits.success('/lecturer/topic-approvals');
    }
}
