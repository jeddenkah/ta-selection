module.exports = {

    friendlyName : 'View new topic',

    description : 'Menampilkan form untuk membuat topik baru',

    exits: {
        success: {
            responseType: 'view',
            viewTemplatePath: 'pages/lecturer/new-topic',
        }
    },

    fn: async function(inputs,exits) {
        const peminatanList = await Peminatan.find({});
        const currentPeriod = await AppSetting.getPeriod();
        const lecturer = await Lecturer.findOne({id: this.req.session.lecturerId}).populate('roles');
        const isKetuaLabRiset = lecturer.roles.filter(r => r.resourceType == "labriset" && r.isCoordinator).length > 0;
        const user = await Lecturer.findOne({id: this.req.session.lecturerId});
        return exits.success({peminatanList, currentPeriod, isKetuaLabRiset, user});
    }
}