module.exports = {


    friendlyName: 'View list catatan bimbingan',
  
  
    description: 'Menampilkan catatan bimbingan',
  
  
    exits: {
      success: {
        viewTemplatePath: 'pages/lecturer/bimbingan-approvals'
      }
  
    },
  
  
    fn: async function (inputs,exits) {
        const currentPeriodId = await AppSetting.getPeriodId();
        const lecturerId = this.req.session.lecturerId; 
  
        const topicSelections = await TopicSelection.find({
          status: "APPROVED_LAB_RISET",
          period: currentPeriodId
        })
        .populate('period')
        .populate('topic')
        .populate('group');
  
        const topicSelectionsPopulated = await Promise.all(topicSelections.map(async ts => {
            //populate 'students' property of topicSelection
            const groupPopulated = await Group.findOne({ id: ts.group.id })
            .populate('students', {
                select: sails.config.custom.studentPublicColumns
            })
            .populate('peminatan');
            const lecturerPopulated = await Lecturer.findOne({ id: ts.topic.lecturer }).populate('labRiset');

            const peminatanPopulated = await Peminatan.findOne({id: ts.topic.peminatan}).populate('labRisetInduk');
            //score
           
            return Object.assign(ts, { group: groupPopulated, lecturer: lecturerPopulated, topic: peminatanPopulated});      
        }));
  
        const topicSelectionByLecturer = await Promise.all(topicSelectionsPopulated.map(async tsp => {
            if (tsp.lecturer.id == lecturerId) {
              return Object.assign(tsp);
            }
        }));

        const bimbingan = await Bimbingan.find();

        //const clo = await Clo.find();
  
        return exits.success({topicSelections: topicSelectionByLecturer,bimbingan});
        // Respond with view.
    
      }
  
  
  };