module.exports = {

    friendlyName : 'View koor metlit',

    description : 'Menampilkan tampilan koor metlit',
    
    inputs: {
        periodId: {
            type: 'number'
        },
        prodiId: {
            type: 'number'
        }
    },

    exits: {
        success: {
            responseType: 'view',
            viewTemplatePath: 'pages/lecturer/koor-metlit',
        }
    },

    fn: async function(inputs,exits) {
        //console.log(inputs.periodId)
        const currentPeriod = await AppSetting.getPeriod();
        const currentPeriodId = await AppSetting.getPeriodId();
        const periods = await Period.find();
        const prodiList = await Prodi.find();
       // const bobotlist = await BobotDosen.findOne({ period : inputs.periodId });
       //const bobotlistAll = await BobotDosen.find();
        //const bobotlist = inputs.periodId ? await BobotDosen.findOne({period: inputs.periodId}): null;
        const bobotlist = await BobotDosen.find({
             where: {
                 period: inputs.periodId ? inputs.periodId : undefined
             }
         });
         
        const plo = await Plo.find({
            where: {
                period: inputs.periodId ? inputs.periodId : undefined,
                prodi: inputs.prodiId ? inputs.prodiId : undefined
            }
        })
        .populate('prodi')
        .populate('period');
        const ploList= await Promise.all(plo.map(async pl => {
            let cloList = await Clo.find({plo:pl.id}).populate('plo');
            cloList = await Promise.all(cloList.map(async cl => {
                const subCloList = await SubClo.find({clo:cl.id})
                return Object.assign(cl, {subCloList});
            }))
            return Object.assign(pl, {cloList});
        }))
        return exits.success({currentPeriodId, currentPeriod, periods, prodiList, bobotlist, plo,ploList});
    }
}