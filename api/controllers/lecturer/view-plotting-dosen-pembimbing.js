module.exports = {


    friendlyName: 'View Plotting Dosen Pembimbing',
  
  
    description: 'Display plotting lecturer that is created by Koor Lab Research.',
  
  
    exits: {
      success: {
        responseType: 'view',
        viewTemplatePath: 'pages/lecturer/plotting-dosen-pembimbing',
      }
  
    },
  
  
    fn: async function (inputs,exits) {
      const currentPeriodId = await AppSetting.getPeriodId();
      const lecturerId = this.req.session.lecturerId;

      const lecturer = await Lecturer.findOne({id: lecturerId}).populate('labRiset');

      const topicSelections = await TopicSelection.find({
        status: "APPROVED",
        period: currentPeriodId
      })
      .populate('period')
      .populate('topic')
      .populate('group');

      const topicSelectionsPopulated = await Promise.all(topicSelections.map(async ts => {
        //populate 'students' property of topicSelection
        const groupPopulated = await Group.findOne({ id: ts.group.id })
            .populate('students', {
                select: sails.config.custom.studentPublicColumns
            })
            .populate('peminatan');
        const lecturerPopulated = await Lecturer.findOne({ id: ts.topic.lecturer }).populate('labRiset');

        const peminatanPopulated = await Peminatan.findOne({id: ts.topic.peminatan}).populate('labRisetInduk');
        return Object.assign(ts, { group: groupPopulated, lecturer: lecturerPopulated, peminatan: peminatanPopulated });
      }));

      const topicSelectionByLabRiset = await Promise.all(topicSelectionsPopulated.map(async tsp => {
        if (tsp.peminatan.labRisetInduk.id == lecturer.labRiset.id) {
          return Object.assign(tsp);
        }      
      }));

      const topicSelectionByKk = await Promise.all(topicSelectionsPopulated.map(async tsp => {
        if (tsp.peminatan.labRisetInduk.kk == lecturer.labRiset.kk) {
          return Object.assign(tsp);
        }
      }));

      return exits.success({topicSelections: topicSelectionsPopulated, lecturer: lecturer, topicSelectionByKk: topicSelectionByKk, topicSelectionByLabRiset: topicSelectionByLabRiset});
      // Respond with view.
  
    }
  
  
  };
  