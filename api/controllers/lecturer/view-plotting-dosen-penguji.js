module.exports = {


    friendlyName: 'View Plotting Dosen Penguji',
  
  
    description: 'Display plotting reviewer that is created by Koor Lab Research.',
  
  
    exits: {
      success: {
        responseType: 'view',
        viewTemplatePath: 'pages/lecturer/plotting-dosen-penguji',
      }
  
    },
  
  
    fn: async function (inputs,exits) {
      const currentPeriodId = await AppSetting.getPeriodId();
      const lecturerId = this.req.session.lecturerId;

      const lecturerUser = await Lecturer.findOne({id: lecturerId}).populate('labRiset');

      const lecturer = await Lecturer.find();

      const topicSelections = await TopicSelection.find({
        status: "APPROVED_LAB_RISET",
        period: currentPeriodId
      })
      .populate('period')
      .populate('topic')
      .populate('group');

      const topicSelectionsPopulated = await Promise.all(topicSelections.map(async ts => {
        //populate 'students' property of topicSelection
        const groupPopulated = await Group.findOne({ id: ts.group.id })
            .populate('students', {
                select: sails.config.custom.studentPublicColumns
            })
            .populate('peminatan');
        const lecturerPopulated = await Lecturer.findOne({ id: ts.topic.lecturer });

        const peminatanPopulated = await Peminatan.findOne({id: ts.topic.peminatan}).populate('labRisetInduk');

        return Object.assign(ts, { group: groupPopulated, lecturer: lecturerPopulated, peminatan: peminatanPopulated });
      }));

      await Promise.all(topicSelectionsPopulated.map(async tss => {
        const student = await Promise.all(tss.group.students.map(async ts => {
          //populate 'students' property of topicSelection
          const pemintanPopulated = await Peminatan.findOne({ id: ts.peminatan })
          .populate('labRisetInduk');

          const group_student = await Student.findOne({id: ts.id}).populate('groupstudent');

          const group_studentfix = await Promise.all(group_student.groupstudent.map(async gs => {
            if (gs.group == tss.group.id) {
              const reviewerData = await Lecturer.findOne({id: gs.lecturer}).populate('labRiset')

              return Object.assign(gs, { lecturer: reviewerData});
            }
          }))

          // const lecturerReviewer = await Lecturer.findOne({id: 57}).populate('labRiset');
          
          const kkId =  pemintanPopulated.labRisetInduk.kk;

          const groupstudent = (group_studentfix == []) ? null : group_studentfix;
          return Object.assign(ts, { peminatan: pemintanPopulated, group_student: groupstudent, kkId: kkId })
        }));
        return Object.assign(tss.group, {students : student});
      }));

      const topicSelectionByLabRiset = await Promise.all(topicSelectionsPopulated.map(async tsp => {
        if (tsp.peminatan.labRisetInduk.id == lecturerUser.labRiset.id) {
          return Object.assign(tsp);
        }      
      }));

      const topicSelectionByKk = await Promise.all(topicSelectionsPopulated.map(async tsp => {
        if (tsp.peminatan.labRisetInduk.kk == lecturerUser.labRiset.kk) {
          return Object.assign(tsp);
        }
      }));

      return exits.success({topicSelections: topicSelectionByLabRiset, lecturer: lecturer, topicSelectionByKk: topicSelectionByKk, topicSelectionByLabRiset: topicSelectionByLabRiset});
      // Respond with view.
  
    }
  
  
  };
  