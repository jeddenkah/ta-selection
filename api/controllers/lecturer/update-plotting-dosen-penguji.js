module.exports = {
    friendlyName: "Update Ploting Dosen Reviewer",

    description: "Mengupdate reviwer untuk proses DE",

    inputs: {
        id: {
            required: true,
            type: 'number'
        },
        lecturer_id: {
            required: true,
            type: 'number'
        }
    },

    exits: {
        success: {
            responseType: 'redirect'
        }
    },

    fn: async function(inputs, exits) {
        if (inputs.lecturer_id === 0) {
            await GroupStudent.updateOne({ id: inputs.id }).set({
                lecturer: null
            })    
        } else {
            await GroupStudent.updateOne({ id: inputs.id }).set({
                lecturer: inputs.lecturer_id
            })
        }
        
        exits.success('/lecturer/plotting-dosen-penguji')
        // All done.
        return;
    }
};
