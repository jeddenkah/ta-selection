module.exports = {
    friendlyName: "New topic",

    description: "Membuat topik TA baru dan menyimpannya di database",

    inputs: {
        rubric: { required: true, type: 'number' },
        subClo: { required: true, type: 'number' },
        nilaiPerRubric : { required: true, type: 'number' },
        portion : { required: true, type: 'number' },
        lecturer: { required: true, type: 'number' },
        student: { required: true, type: 'number' },
        clo:{ required: true, type: 'number' },
        plo:{ required: true, type: 'number' }
    },

    exits: {
        success: {
            responseType: 'redirect'
        }
    },

    fn: async function(inputs, exits) {
    let cek=[];
    const groupStudent = await GroupStudent.find({lecturer:inputs.lecturer,student:inputs.student});
    groupStudent.map((group)=>{
        cek.push(group.student)
    })
       
       const currentPeriodId = await AppSetting.getPeriodId();
       const bobotlist = await BobotDosen.find({
        where: {
            period: currentPeriodId
        }
        })
        
        const bobotPembimbing = bobotlist.map(bobot=>bobot.dosenPembimbing);
        const bobotPenguji = bobotlist.map(bobot=>bobot.dosenPenguji);

       const nilaiperRubric = inputs.nilaiPerRubric;
       const portion = inputs.portion;
       console.log(cek)
        const newScore = await Score.create({
            rubric: inputs.rubric,
            subClo: inputs.subClo,
            lecturer: inputs.lecturer,
            student: inputs.student,
        }).fetch();
        if(newScore && cek.length>0){
            await ScorePenguji.create({
                period:currentPeriodId,
                subClo: inputs.subClo,
                rubric: inputs.rubric,
                lecturer: inputs.lecturer,
                student: inputs.student,
                nilaiPerRubric : nilaiperRubric,
                portion:inputs.portion,
                nilaiCalculatedPortion : nilaiperRubric*portion/100,
                nilaiCalculatedBobot : nilaiperRubric*bobotPenguji/100,
                nilaiCalculatedBobotPortion : nilaiperRubric*portion/100*bobotPenguji/100,
                clo : inputs.clo,
                plo : inputs.plo
            }).fetch()
            exits.success('/lecturer/dosen-penguji')
        }else if(newScore && cek.length==0){
            await ScorePembimbing.create({
                period:currentPeriodId,
                subClo: inputs.subClo,
                rubric: inputs.rubric,
                lecturer: inputs.lecturer,
                student: inputs.student,
                nilaiPerRubric : nilaiperRubric,
                portion:inputs.portion,
                nilaiCalculatedPortion : nilaiperRubric*portion/100,
                nilaiCalculatedBobot : nilaiperRubric*bobotPembimbing/100,
                nilaiCalculatedBobotPortion : nilaiperRubric*portion/100*bobotPembimbing/100,
                clo : inputs.clo,
                plo : inputs.plo
            }).fetch()
            exits.success('/lecturer/dosen-pembimbing')
        }
    }
};
