module.exports = {


    friendlyName: 'View list dosen pembimbing',
  
  
    description: 'Menampilkan list dosen pembimbing',
  
  
    exits: {
      success: {
        viewTemplatePath: 'pages/lecturer/dosen-pembimbing'
      }
  
    },
  
  
    fn: async function (inputs,exits) {
        const currentPeriodId = await AppSetting.getPeriodId();
        const lecturerId = this.req.session.lecturerId; 
  
        const topicSelections = await TopicSelection.find({
          status: "APPROVED_LAB_RISET",
          period: currentPeriodId
        })
        .populate('period')
        .populate('topic')
        .populate('group');
  
        const topicSelectionsPopulated = await Promise.all(topicSelections.map(async ts => {
            //populate 'students' property of topicSelection
            const groupPopulated = await Group.findOne({ id: ts.group.id })
            .populate('students', {
                select: sails.config.custom.studentPublicColumns
            })
            .populate('peminatan');
            const lecturerPopulated = await Lecturer.findOne({ id: ts.topic.lecturer }).populate('labRiset');

            const peminatanPopulated = await Peminatan.findOne({id: ts.topic.peminatan}).populate('labRisetInduk');
            //score
            let scored = await Score.find({
              lecturer: ts.topic.lecturer
            })
            .populate('student')
            .populate('rubric');
            scored = await Promise.all(scored.map(async sc => {
              let subCloRubricList = await SubCloRubric.find({id: sc.rubric.id}).populate('subClo');
              return Object.assign(sc, {subCloRubricList})
            }))
            return Object.assign(ts, { group: groupPopulated, lecturer: lecturerPopulated, topic: peminatanPopulated,score : scored});      
        }));
  
        const topicSelectionByLecturer = await Promise.all(topicSelectionsPopulated.map(async tsp => {
            if (tsp.lecturer.id == lecturerId) {
              return Object.assign(tsp);
            }
        }));

        //const clo = await Clo.find();
  
        return exits.success({topicSelections: topicSelectionByLecturer});
        // Respond with view.
    
      }
  
  
  };
  