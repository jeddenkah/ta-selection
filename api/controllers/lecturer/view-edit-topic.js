module.exports = {
    friendlyName: "View edit topic",

    description: "Show edit page of final project topic",

    inputs: {
        topicId: {required: true, type: 'string'}
    },

    exits: {
        success: {
            viewTemplatePath: 'pages/lecturer/edit-topic'
        }
    },

    fn: async function(inputs, exits) {
        const peminatanList = await Peminatan.find({});
        const topic = await Topic.findOne({id: inputs.topicId}).populate("lecturer");
        const lecturer = await Lecturer.findOne({id: this.req.session.lecturerId}).populate('roles');
        const isKetuaLabRiset = lecturer.roles.filter(r => r.resourceType == "labriset" && r.isCoordinator).length > 0;
        const user = await Lecturer.findOne({id: this.req.session.lecturerId});
        return exits.success({peminatanList, topic, isKetuaLabRiset, user});
    }
};
