module.exports = {
    friendlyName: "Edit topic",

    description: "Edit topik dosen",

    inputs: {
        id: {
            required: true,
            type: 'number'
        },
        name: {
            required: true,
            type: "string"
        },
        deskripsi: {
            required: true,
            type: "string"
        },
        quota: {
            required: true,
            type: "number"
        },
        peminatanId: {
            required: true,
            type: 'number'
        },
        lecturerId: {
            required: true,
            type: "number"
        }
    },

    exits: {
        success: {
            responseType: 'redirect',
        },
        quotaConflict: {
            description: 'kuota lebih kecil daripada jumlah anggota kelompok yang terdaftar',
        }
    },

    fn: async function (inputs, exits) {
        const topicSelections = await TopicSelection.find({ topic: inputs.id }).populate('group');
        const maxTotalStudents = topicSelections.reduce((maxTotalStudents = 0, ts) => {
            return Math.max(ts.group.totalStudents, maxTotalStudents);
        }, 0);
        if (maxTotalStudents && inputs.quota < maxTotalStudents) {
            return exits.quotaConflict({ minQuota: maxTotalStudents });
        }

        const peminatan = await Peminatan.findOne({ id: inputs.peminatanId });
        const lecturer = await Lecturer.findOne({id: this.req.session.lecturerId}).populate('roles');
        const isKetuaLabRiset = lecturer.roles.filter(r => r.resourceType == "labriset" && r.isCoordinator).length > 0;
        await Topic.updateOne({ id: inputs.id }).set({

            name: inputs.name,
            quota: inputs.quota,
            deskripsi: inputs.deskripsi,
            kk: peminatan.kk,
            peminatan: inputs.peminatanId,
            lecturer: isKetuaLabRiset ? inputs.lecturerId : this.req.session.lecturerId 
        })
        // All done.
        return exits.success('/lecturer/topics');
    }
};
