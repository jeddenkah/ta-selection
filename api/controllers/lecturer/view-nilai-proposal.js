module.exports = {

    friendlyName : 'View nilai',

    description : 'Menampilkan nilai',
    
    inputs: {
        periodId: {
            type: 'number'
        },
        kelasId:{
            type: 'number'
        }
    },

    exits: {
        success: {
            responseType: 'view',
            viewTemplatePath: 'pages/lecturer/nilai-proposal',
        }
    },

    fn: async function(inputs,exits) {
        const currentPeriodId = await AppSetting.getPeriodId();
        const periods = await Period.find();
        const lecturerList = await Lecturer.find({id : this.req.session.lecturerId 
        }).populate('prodi')

        
        const kelasList = await Promise.all(lecturerList.map(async lc => {
            let prodi = await Prodi.find({id: lc.prodi.id});
                prodi = await Promise.all(prodi.map(async pr => {	
                    if(this.req.cookies.dosenMetlit=="true"){
                        const metlit = await Metlit.find({prodi:pr.id,lecturer:this.req.session.lecturerId})
                        return Object.assign(pr, {metlit}); 
                        }
                       else{
                        const metlit = await Metlit.find({prodi:pr.id})
                        return Object.assign(pr, {metlit}); 
                       }
                }))
            return Object.assign(lc,{prodi});
        }));

        
        const student = await Student.find({metlit:inputs.kelasId ? inputs.kelasId : undefined})

        const studentList = await Promise.all(student.map(async st=>{
            let groupStudent = await GroupStudent.find({student:st.id}).populate('lecturer')
            groupStudent = await Promise.all(groupStudent.map(async gs=>{
                let group = await Group.find({id:gs.group})
                group = await Promise.all(group.map(async gp=>{
                    let topicSelection = await TopicSelection.find({group:gp.id,status: "APPROVED_LAB_RISET"})
                    topicSelection = await Promise.all(topicSelection.map(async ts=>{
                        const topic = await Topic.find({id:ts.topic}).populate('lecturer')
                        return Object.assign(ts,{topic});
                    }))
                    return Object.assign(gp,{topicSelection});
                }))
                return Object.assign(gs,{group});
            }))
            return Object.assign(st,{groupStudent});
        }))

        
        const scorePembimbingPopulated = await ScorePembimbing.find({period: inputs.periodId ? inputs.periodId : currentPeriodId})
        .populate('student')
        .populate('lecturer')
        .populate('plo')
        .populate('clo')
        .populate('subClo')
        
        // const scorePembimbingPopulated = await Promise.all(scorePembimbing.map(async spb=>{
        //     const studentBimbingan = await Student.find({id:spb.student.id,metlit:inputs.kelasId ? inputs.kelasId : undefined})
        //     return Object.assign(spb,{studentBimbingan});
        // }))
        
        const scorePengujiPopulated = await ScorePenguji.find({period:inputs.periodId? inputs.periodId : currentPeriodId})
        .populate('student')
        .populate('lecturer')
        .populate('plo')
        .populate('clo')
        .populate('subClo')
        
        // const scorePengujiPopulated = await Promise.all(scorePenguji.map(async spj=>{
        //     const studentReview = await Student.find({id:spj.student.id,metlit:inputs.kelasId ? inputs.kelasId : undefined})
        //     return Object.assign(spj,{studentReview});
        // }))
    return exits.success({studentList,currentPeriodId,periods, kelasList,scorePembimbingPopulated,scorePengujiPopulated});
    }
}