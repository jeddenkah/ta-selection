module.exports = {

    friendlyName : 'View dashboard dosenmetlit ',

    description : 'Menampilkan dashboard dosenmetlit',

    exits: {
        success: {
            responseType: 'view',
            viewTemplatePath: 'pages/lecturer/dosen-metlit',
        }
    },

    fn: async function(inputs,exits) {
        //const peminatanList = await Peminatan.find({});
        return exits.success({});
    }
}