module.exports = {
    friendlyName: 'Delete TA',

    description: 'Menghapus ta dari mahasiswa',

    inputs: {
    },

    exits: {
        success: {},
        groupNotFound: {},
    },

    fn: async function (inputs, exits) {
        const studentId = this.req.session.studentId;
        await Ta.destroyOne({ owner: studentId })

        return exits.success();
    }
}