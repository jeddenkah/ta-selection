module.exports = {
    friendlyName: 'Edit group',

    description: 'Edit anggota dari kelompok mahasiswa yang sudah ada',

    inputs: {
        studentIds: {required: true, type: 'ref'}
    },
    
    exits: {
        success: {},
        groupNotFound: {},
        locked: {
            description: 'TopicSelection group sudah di approve'
        },
        forbidden: {
            description: 'user bukan owner dari group'
        },
        someAlreadySubmitted: {},
    },

    fn: async function(inputs, exits) {
        const studentId = this.req.session.studentId;
        const groupIds = await Group.getStudentGroupIds(studentId);
        const groupId = _.last(groupIds);
        if (!groupId) return exits.groupNotFound();
        const group = await Group.findOne({id: groupId}).populate('students');
        if (studentId != group.owner) return exits.forbidden();
        const approvedTopics = await TopicSelection.find({group: groupId, status: 'APPROVED'});
        if (approvedTopics.length > 0 ) return exits.locked();

        const oldStudentIds = group.students.map(s => s.id);
        const newStudentIds = _.difference(inputs.studentIds, oldStudentIds);

        for (const oneStudent of newStudentIds) {
            let topics = await StudentService.findWaitingOrApprovedTopics(oneStudent);
            if (topics.length > 0)
                return exits.someAlreadySubmitted()
        }
        
        await Group.replaceCollection(groupId, 'students').members(inputs.studentIds);
        return exits.success();
    }
}