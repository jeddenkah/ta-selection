module.exports = {

    friendlyName: 'New topic selection',


    description: '',


    inputs: {
        studentIds: { type: 'ref' },
        topicOpt1Id: { required: true, type: 'number' },
        topicOpt2Id: { type: 'number' },
    },


    exits: {
        success: { responseType: 'redirect' },
        topicQuotaExceeded: {},
        someAlreadySubmitted: {}
    },


    fn: async function (inputs, exits) {
        const {studentIds, topicOpt1Id, topicOpt2Id} = inputs;

        const student = await Student.findOne({id: this.req.session.studentId});
        for (const oneStudent of studentIds) {
            let topics = await StudentService.findWaitingOrApprovedTopics(oneStudent);
            if (topics.length > 0)
                return exits.someAlreadySubmitted()   
        }

        const topicOpt1 = await Topic.findOne({id: inputs.topicOpt1Id});
        if (studentIds.length > topicOpt1.quota) {
            return exits.topicQuotaExceeded()
        }
        
        const currentPeriodId = await AppSetting.getPeriodId();
        const newGroup = await Group.create({
            totalStudents: studentIds.length,
            period: currentPeriodId,
            peminatan: student.peminatan,
            owner: student.id
        }).fetch();
        
        await Group.addToCollection(newGroup.id, 'students').members(studentIds);
        const newTopicSelection1 = await TopicSelection.create({
            topic: topicOpt1.id,
            optionNum: 1,
            period: currentPeriodId,
            status: 'WAITING',
            group: newGroup.id
        });

        if (topicOpt2Id) {
            const topicOpt2 = await Topic.findOne({id: topicOpt2Id});
            if (studentIds.length > topicOpt2.quota) {
                return exits.topicQuotaExceeded()
            }
            const newTopicSelection2 = await TopicSelection.create({
                topic: topicOpt2.id,
                optionNum: 2,
                period: currentPeriodId,
                status: 'WAITING',
                group: newGroup.id
            });
        }
        return exits.success('/student/topic-selection/status')
    }


};
