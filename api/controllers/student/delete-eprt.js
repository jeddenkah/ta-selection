module.exports = {
    friendlyName: 'Delete EPRT',

    description: 'Menghapus eprt dari mahasiswa',

    inputs: {
    },

    exits: {
        success: {},
        groupNotFound: {},
    },

    fn: async function (inputs, exits) {
        const studentId = this.req.session.studentId;
        await Eprt.destroyOne({ owner: studentId })

        return exits.success();
    }
}