module.exports = {
    friendlyName: 'File EPRT',

    description: 'Mengatur file eprt yang diupload mahasiswa',

    files: ['file_eprt'],

    inputs: {

        file_eprt: {
            type: 'ref',
            required: true
        },

        score: {
            type: 'string',
            required: true
        },

    },

    exits: {
        badRequest: {
            description: 'Bad request'
        },
        success: {
            viewTemplatePath: 'pages/student/dedashboard'
        }
    },

    fn: async function (inputs, exits) {
        var info = await sails.uploadOne(inputs.file_eprt, {
            maxBytes: 3000000
        });
        var url = require('url');
        const path = require('path');

        const fileName = path.parse(info.filename).name;

        const score = inputs.score;
        if (!info) {
            throw 'badRequest'
        }


        const dataEprt = {
            score: score,
            eprtUrl: '/get-eprt/' + this.req.session.studentId + '/' + fileName,
            eprtFd: info.fd,
            fileName: fileName,
            owner: this.req.session.studentId
        }

        var newEprt = await Eprt.findOrCreate({ owner: this.req.session.studentId }, dataEprt).exec(async (err, eprt, wasCreated) => {
            if (err) { return res.serverError(err); }

            if (wasCreated) {
                return this.res.ok(eprt);
            }
            else {
                const updatedEprt = await Eprt.updateOne({ owner: this.req.session.studentId }).set({
                    score: score,
                    eprtUrl: '/get-eprt/' + this.req.session.studentId + '/' + fileName,
                    eprtFd: info.fd,
                    fileName: fileName
                })

                return this.res.ok(updatedEprt);
            }
        });


        //exits.success({ newEprt });
    }

}