module.exports = {
    friendlyName: 'Leave group',

    description: 'Hapus user dari group terbaru',

    inputs: {
    },
    
    exits: {
        success: {},
        locked: {
            description: 'TopicSelection group sudah di approve'
        },
        cantLeaveOwnedGroup: {}
    },

    fn: async function(inputs, exits) {
        const userId = this.req.session.studentId;
        const periodId = await AppSetting.getPeriod();
        const groupIds = await Group.getStudentGroupIds(userId);
        const groupId = _.last( groupIds );

        const approvedTopics = await TopicSelection.find({group: groupId, status: 'APPROVED'});
        if (approvedTopics.length) return exits.locked();

        const group = await Group.findOne({id: groupId});
        if (userId == group.owner) return exits.cantLeaveOwnedGroup();

        await Group.removeFromCollection(groupId, 'students').members([userId]);
        return exits.success();
    }
}