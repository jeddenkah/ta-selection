module.exports = {
    friendlyName: 'File EPRT',

    description: 'Mengatur file ta yang diupload mahasiswa',

    files: ['file_form'],

    inputs: {

        file_form: {
            type: 'ref',
            required: true
        },
    },

    exits: {
        badRequest: {
            description: 'Bad request'
        },
        success: {
            viewTemplatePath: 'pages/student/dashboard'
        }
    },

    fn: async function (inputs, exits) {
        var url = require('url');
        const path = require('path');
        var info = await sails.uploadOne(inputs.file_form, {
            maxBytes: 3000000
        });

        if (!info) {
            throw 'badRequest'
        }

        const fileName = path.parse(info.filename).name;

        var dataForm = {
            formUrl: '/get-form/' + this.req.session.studentId + '/' + fileName,
            formFd: info.fd,
            owner: this.req.session.studentId,
            fileName: fileName
        }

        var newForm = await Form.findOrCreate({ owner: this.req.session.studentId }, dataForm).exec(async (err, form, wasCreated) => {
            if (err) { return res.serverError(err); }

            if (wasCreated) {
                return this.res.ok(form);
            }
            else {
                const updatedForm = await Form.updateOne({ owner: this.req.session.studentId }).set({
                    formUrl: '/get-form/' + this.req.session.studentId + '/' + fileName,
                    formFd: info.fd,
                    fileName: fileName
                })

                return this.res.ok(updatedForm);
            }
        });


    }

}