module.exports = {


    friendlyName: 'Group setting',


    description: '',

    exits: {
        success: { viewTemplatePath: 'pages/student/group-setting' }
    },


    fn: async function (inputs) {
        const userId = this.req.session.studentId;
        const user = await Student.findOne({ id: userId }).populate('peminatan');

        const groupIds = await Group.getStudentGroupIds(userId);
        const groupId = _.last(groupIds);
        const periodId = await AppSetting.getPeriodId()
        if (!groupId) return {user, group: undefined}
        const group = await Group.findOne({id: groupId})
            .populate('students')
            .populate('owner');
        if (group.period != periodId) return {user, group: undefined}

        return { user, group };
    }


};
