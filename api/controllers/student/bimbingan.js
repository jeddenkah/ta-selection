module.exports = {

    friendlyName: 'View Bimbingan Online',

    description: 'Menampilkan Catatan Bimbingan',

    inputs: {
    },

    exits: {
        success: {viewTemplatePath: 'pages/student/bimbingan'}
    },

    fn: async function(inputs, exits) {
        const studentId = this.req.session.studentId;
        //const currentPeriodId = await AppSetting.getPeriodId();
        const groupStudent = await GroupStudent.getStudentGroupIds(studentId); 
        const bimbingan = await Bimbingan.find({studentId:studentId});
        const groupStudentId = groupStudent[0];
        return exits.success({bimbingan,studentId});
        
    }
}