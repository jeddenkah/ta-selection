module.exports = {
    friendlyName: 'New bimbingan',

    description: 'Membuat catatan bimbingan',

    inputs: {
        tanggal: { required: true, type: 'string' },
        catatan: { required: true, type: 'string' },
        groupStudentId : { required: true, type: 'number' }
    },

    exits: {
        success: {}
    },

    fn: async function (inputs, exits) {
        await Bimbingan.create({ tanggal: inputs.tanggal, catatan: inputs.catatan, groupStudentId: inputs.groupStudentId, isDeleted: false });
        return exits.success()
    }
}
