module.exports = {
    friendlyName: 'Delete Bimbingan',

    description: 'Menghapus Bimbingan',

    inputs: {
        id: { required: true, type: 'number' },
    },

    exits: {
        success: {}
    },

    fn: async function (inputs, exits) {
        await Bimbingan.destroyOne({id: inputs.id})
        exits.success('/student/bimbingan')
    }
}
