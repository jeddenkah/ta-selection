module.exports = {
    friendlyName: 'File Ta',

    description: 'Mengupload file ta yang diupload mahasiswa',

    files: ['file_ta'],

    inputs: {

        file_ta: {
            type: 'ref',
            required: true
        }
    },


    exits: {
        badRequest: {
            description: 'Bad request'
        },
        success: {
            viewTemplatePath: 'pages/student/dedashboard'
        }
    },

    fn: async function (inputs, exits) {
        var url = require('url');
        const path = require('path');
        var info = await sails.uploadOne(inputs.file_ta, {
            maxBytes: 3000000
        });

        if (!info) {
            throw 'badRequest'
        }

        const fileName = path.parse(info.filename).name;

        var dataTa = {
            taUrl: '/get-ta/' + this.req.session.studentId + '/' + fileName,
            taFd: info.fd,
            owner: this.req.session.studentId,
            fileName: fileName
        }

        var newTa = await Ta.findOrCreate({ owner: this.req.session.studentId }, dataTa).exec(async (err, ta, wasCreated) => {
            if (err) { return res.serverError(err); }

            if (wasCreated) {
                return this.res.ok(ta);
            }
            else {
                const updatedTa = await Ta.updateOne({ owner: this.req.session.studentId }).set({
                    taUrl: '/get-ta/' + this.req.session.studentId + '/' + fileName,
                    taFd: info.fd,
                    fileName: fileName
                })

                return this.res.ok(updatedTa);
            }
        });


    }

}