module.exports = {
    friendlyName: 'Delete group',

    description: 'Menghapus topic selection dan group mahasiswa',

    inputs: {
    },
    
    exits: {
        success: {},
        groupNotFound: {},
        forbidden: {
            description: 'user bukan owner dari group'
        },
        locked: {
            description: 'TopicSelection group ada yang sudah di approve/reject'
        },
    },

    fn: async function(inputs, exits) {
        const studentId = this.req.session.studentId;
        const groupIds = await Group.getStudentGroupIds(studentId);
        const groupId = _.last(groupIds);
        if (!groupId) return exits.groupNotFound();
        const group = await Group.findOne({id: groupId});
        if (studentId != group.owner) return exits.forbidden();

        const approvedTopics = await TopicSelection.find({group: groupId, status: 'APPROVED'});
        if (approvedTopics.length > 0) return exits.locked();
        await Group.destroyOne({id: groupId});

        return exits.success();
    }
}