async function populateLecturer(topicSelection) {
    const lecturer = await Lecturer.findOne({ id: topicSelection.topic.lecturer }).populate('topics');
    const populatedTopic = { ...topicSelection.topic, lecturer }
    return { ...topicSelection, topic: populatedTopic };
}

module.exports = {
    friendlyName: 'View dashboard',

    description: 'Menampilkan dashboard mahasiswa',

    exits: {
        success: { viewTemplatePath: 'pages/student/dashboard' },
        userNotFound: {}
    },

    fn: async function (inputs, exits) {
        const studentId = this.req.session.studentId;

        const student = await Student.findOne({ id: studentId });
        if (!student) return exits.userNotFound();
        const groupIds = await Group.getStudentGroupIds(studentId);
        const activeGroupId = _.last(groupIds);
        const activePeriod = await AppSetting.getPeriod();
        if (!activeGroupId) return exits.success({ namaAnggota: [], topicSelection1: undefined, topicSelection2: undefined });
        const activeGroup = await Group.findOne({ id: activeGroupId }).populate('students');
        const topicSelections = await TopicSelection.find({ group: activeGroup.id, period: activePeriod.id }).populate('topic');
        let topicSelection1 = topicSelections.find(ts => ts.optionNum == 1);
        topicSelection1 = topicSelection1 ? await populateLecturer(topicSelection1) : undefined;
        let topicSelection2 = topicSelections.find(ts => ts.optionNum == 2);
        topicSelection2 = topicSelection2 ? await populateLecturer(topicSelection2) : undefined;
        const anggotaKelompok = activeGroup.students;
        const reviewer = await GroupStudent.findOne({group: activeGroupId, student: studentId}).populate('lecturer');

        let namaAnggota = anggotaKelompok.map(student => {
            return student.name;
        })

        // let topicApproved = topicSelection.filter(row => {
        //         return row.status == 'APPROVED'

        //   })

        // console.log(topicApproved);

        // let nameLecturerApproved = topicApproved.length != 0 ? await Lecturer.findOne({id: topicApproved[0].topic.lecturer}) : null;

        // let populateTopik = topicApproved.length != 0 && nameLecturerApproved != null ? {...topicApproved,nameLecturerApproved} : null;

        //topicApproved = nameLecturerApproved != null ? topicApproved.push(nameLecturerApproved.name)  : topicApproved;

        // console.log('ini '+ populateTopik)

        // history topic
        const allTopicSelections = await TopicSelection.find({ group: groupIds, period: activePeriod.id }).populate('topic').sort('group DESC');
        return exits.success({ namaAnggota, topicSelection1, topicSelection2, topicSelections, allTopicSelections, reviewer, role:'Mahasiswa' });
    }
}