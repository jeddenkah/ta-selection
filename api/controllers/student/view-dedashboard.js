async function populateLecturer(topicSelection) {
    const lecturer = await Lecturer.findOne({ id: topicSelection.topic.lecturer });
    const populatedTopic = { ...topicSelection.topic, lecturer }
    return { ...topicSelection, topic: populatedTopic };
}

module.exports = {
    friendlyName: 'View de dashboard',

    description: 'Menampilkan dashboard de mahasiswa',

    exits: {
        success: { viewTemplatePath: 'pages/student/dedashboard' },
        userNotFound: {}
    },

    fn: async function (inputs, exits) {
        const studentId = this.req.session.studentId;
        const eprt = await Eprt.findOne({ owner: studentId });
        const ta = await Ta.findOne({ owner: studentId });
        const form = await Form.findOne({ owner: studentId });


        return exits.success({ eprt, ta, form });
    }
}