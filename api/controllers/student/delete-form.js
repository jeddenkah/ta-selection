module.exports = {
    friendlyName: 'Delete form bimbingan',

    description: 'Menghapus form bimbingan dari mahasiswa',

    inputs: {
    },

    exits: {
        success: {},
        groupNotFound: {},
    },

    fn: async function (inputs, exits) {
        const studentId = this.req.session.studentId;
        await Form.destroyOne({ owner: studentId })

        return exits.success();
    }
}