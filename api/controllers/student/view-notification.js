module.exports = {


    friendlyName: 'View notification',
  
  
    description: 'Display "Notification" page.',
  

    exits: {
  
      success: {
        viewTemplatePath: 'pages/student/notification'
      }
  
    },
  
  
    fn: async function (inputs, exits) {
     
      return exits.success();
    }
  
  
  };
  