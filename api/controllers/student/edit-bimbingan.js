module.exports = {
    friendlyName: 'New plo',

    description: 'Mengedit plo',

    inputs: {
        id: { required: true, type: 'number' },
        tanggal: { type: 'string' },
        catatan: { type: 'string' }
    },

    exits: {
        success: {}
    },

    fn: async function (inputs, exits) {
        await Bimbingan.update({ id: inputs.id })
            .set({ tanggal: inputs.tanggal, catatan: inputs.catatan });
        return exits.success();
    }
}
