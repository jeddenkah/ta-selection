module.exports = {
    friendlyName: 'New tabel lecturer',

    description: 'Membuat dosen baru',

    inputs: {
        nik: { required: true, type: 'string' },
        name: { required: true, type: 'string' },
        email: { required: true, type: 'string', email: true },
        jfa: { required: true, type: 'number' },
        labRiset: { required: true, type: 'number' },
        prodi: { required: true, type: 'number' },
        lecturerCode: {required: true, type: 'string'},
        roles: { type: 'ref' },
        hashedPassword: {type: 'string'},
    },

    exits: {
        success: {},
        failed: {}
    },

    fn: async function (inputs, exits) {
        const bcrypt = require('bcrypt');
        const { nik, name, email, jfa, labRiset, prodi, hashedPassword, lecturerCode, roles = [] } = inputs;
        //const hashed_password = await bcrypt.hash(password, sails.config.custom.saltRounds);
        const newLecturer = await Lecturer.create({
            nik,
            name,
            email,
            jfa,
            labRiset,
            prodi,
            hashedPassword,
            lecturerCode,
        }).fetch().intercept((err)=>{
            // Return a modified error here (or a special exit signal)
            // and .create() will throw that instead
            err.message = 'Uh oh: '+err.message;
            console.log(err);
           });

        if(newLecturer.id){
            await Lecturer.addToCollection(newLecturer.id, 'roles')
                .members(roles);
            return exits.success();
        }
        else return exits.failed();
    }
}