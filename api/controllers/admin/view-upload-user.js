module.exports = {


    friendlyName: 'View upload user',
  
  
    description: 'Display "Upload user data" page.',
  
    inputs: {
      userType: {require: false, type: 'string'}
    },
  
    exits: {
  
      success: {
        viewTemplatePath: 'pages/admin/upload-user'
      }
  
    },
  
  
    fn: async function (inputs) {
      // Respond with view.
      return {userType: inputs.userType}
  
    }
  
  
  };
  