module.exports = {
    friendlyName: 'New tabel lecturer',

    description: 'Membuat dosen baru',

    inputs: {
        id: { required: true, type: 'number' },
        nik: { required: true, type: 'string' },
        name: { required: true, type: 'string' },
        email: { type: 'string', email: true },
        jfa: { type: 'number' },
        labRiset: { type: 'number' },
        prodi: { type: 'number' },
        lecturerCode: {required: true, type: 'string'},
        roles: { type: 'ref' },
        hashedPassword: {type: 'string'},
    },

    exits: {
        success: {},
        failed: {}
    },

    fn: async function (inputs, exits) {
        const bcrypt = require('bcrypt');
        const { id, nik, name, email, jfa, labRiset, prodi, hashedPassword, lecturerCode, roles = [] } = inputs;
        await Lecturer.updateOne({id})
            .set({
                nik,
                name,
                email,
                jfa,
                labRiset,
                prodi,
                hashedPassword,
                lecturerCode,
            }).intercept((err)=>{
                err.message = 'Uh oh: '+err.message;
                console.log(err);
            });
        await Lecturer.replaceCollection(id, 'roles').members(roles);
        return exits.success();
    }
}