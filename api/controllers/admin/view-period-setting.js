module.exports = {


  friendlyName: 'View period setting',


  description: 'Display "Period setting" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/admin/period-setting'
    }

  },


  fn: async function () {
    const currentPeriodId = await AppSetting.getPeriodId();
    const currentPeriod = await Period.findOne({id: currentPeriodId});
    // Respond with view.
    return {currentPeriod};

  }


};
