module.exports = {


  friendlyName: 'View kk setting',


  description: 'Display "Kk setting" page.',
  
  inputs: {
    periodId: {
        type: 'number'
    },
    prodiId: {
        type: 'number'
    }
  },
  
  exits: {

    success: {
      responseType: 'view',
      viewTemplatePath: 'pages/admin/database-setting'
    }

  },


  fn: async function (inputs,exits) {
    const currentPeriodId = await AppSetting.getPeriodId();
    const periods = await Period.find();
    const prodiList = await Prodi.find();

    const plo = await Plo.find({
      where: {
          period: inputs.periodId ? inputs.periodId : undefined,
          prodi: inputs.prodiId ? inputs.prodiId : undefined
      }
    })
    .populate('prodi')
    .populate('period');
    const ploList= await Promise.all(plo.map(async pl => {
      let cloList = await Clo.find({plo:pl.id}).populate('plo');
      cloList = await Promise.all(cloList.map(async cl => {
          const subCloList = await SubClo.find({clo:cl.id})
          return Object.assign(cl, {subCloList});
      }))
      return Object.assign(pl, {cloList});
  }))
    return exits.success({currentPeriodId, periods, prodiList, plo, ploList});

  }


};
