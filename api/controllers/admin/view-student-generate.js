const _ = require('lodash');

module.exports = {


    friendlyName: 'View student generate',


    description: 'Display "Student generate" page.',

    inputs: {
        optionNum: {
            type: 'number'
        },
        periodId: {
            type: 'number'
        }
    },


    exits: {

        success: {
            viewTemplatePath: 'pages/admin/student-generate'
        }

    },


    fn: async function (inputs, exits) {
        const filters = {
            optionNum: inputs.optionNum,
            period: inputs.periodId
        }
        // const noNullFilters = _.pickBy(filters, _.identity);
        const studentStatus = await VStudentReportReviewer.find()
            .populate('student')
            .populate('lecturer')
            .populate('reviewer')
            .populate('topic')
            .populate('peminatan')
            .populate('period');
        // Respond with view.
        return exits.success({ studentStatus });

    }


};
