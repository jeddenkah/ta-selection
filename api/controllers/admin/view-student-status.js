const _ = require('lodash');

module.exports = {


    friendlyName: 'View student status',


    description: 'Display "Student status" page.',

    inputs: {
        optionNum: {
            type: 'number'
        },
        periodId: {
            type: 'number'
        }
    },


    exits: {

        success: {
            viewTemplatePath: 'pages/admin/student-status'
        }

    },


    fn: async function (inputs, exits) {
        const filters = {
            optionNum: inputs.optionNum,
            period: inputs.periodId
        }
        const noNullFilters = _.pickBy(filters, _.identity);
        const studentStatus = await VStudentStatus.find(noNullFilters)
            .populate('student')
            .populate('period');
        // Respond with view.
        const studentStatusReview = await VStudentReportReviewer.find()
            .populate('student')
            .populate('lecturer')
            .populate('reviewer')
            .populate('topic')
            .populate('peminatan')
            .populate('period');
        return exits.success({ studentStatus,studentStatusReview }); 

    }


};
