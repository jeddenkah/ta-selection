module.exports = {
    friendlyName: 'View dosen',

    description: 'Menampilkan list mahasiswa sesuai filter',

    inputs: {
        peminatanId: {
            type: 'string'
        }
    },

    exits: {
        success: {
            viewTemplatePath: 'pages/admin/student-list'
        },
    },
    fn: async function (inputs, exits) {
        const students = await Student.find({ peminatan: inputs.peminatanId })
            .populate('peminatan')
            .populate('metlit');
        return exits.success({ students });
    }
}
