module.exports = {


    friendlyName: 'Change period',


    description: ` 
        Ganti periode aktif.
    `,


    inputs: {
        id: {required: true, type: 'number'}
    },


    exits: {

    },


    fn: async function (inputs) {
        await changePeriod(inputs.id);
        return;
    }


};

async function changePeriod(periodId) {

    const activePeriodId = await AppSetting.getPeriodId();
    if (activePeriodId) {
        await AppSetting.updateOne({ name: 'period_id' }).set({ settingValue: periodId });
    }
    else {
        await AppSetting.create({name: 'period_id', settingValue: periodId});
    }
}