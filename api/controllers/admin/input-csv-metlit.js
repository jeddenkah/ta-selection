const yup = require('yup');
const skipperCsv = require('skipper-csv');



const metlitSchema = yup.object().shape({
    class: yup.string(),
    period: yup.number().integer(),
    prodi: yup.number().integer()
});

module.exports = {
    friendlyName: 'Upload user',
    
    description: 'Menerima data user dalam bentuk tabel lalu memasukkan ke database',

    inputs: {
        
        shouldUpdateUser: {
            type: 'boolean'
        },
    },

    exits: {
        success: {},
    },
    
    fn: async function(inputs, exits) {
        let Model, modelSchema;
        Model = Metlit;
        modelSchema = metlitSchema;
        let recordsCount = 0, duplicateCount = 0, invalidCount = 0, successCount = 0;
        const promises = [];
        const rowHandler = async (row) => {
            recordsCount++;
            if (row.ipk) {
                row.ipk = row.ipk.replace(',','.')
            }
            const promise = new Promise(async (resolve, reject) => {
                try {
                    const castedRow = await modelSchema.validate(row);
                    try {
                        await Model.create(castedRow);
                        resolve('success');
                    } catch (err) {
                        if (err.code == 'E_UNIQUE') {
                            if (inputs.shouldUpdateUser) {
                                try {
                                    
                                        await Model.updateOne({id: castedRow.id}).set(castedRow);
                                    
                                }
                                catch(err) {this.res.serverError(err)}
                                resolve('success');
                            } else {
                                resolve('duplicate');
                            }
                        }
                    }
                } catch (err) {
                    if (err.name == 'ValidationError') {
                        resolve('invalid. ' + err.message);
                    }
                }
            });
            promises.push(promise);
        }
        this.req.file('metlit-table').upload(
            {
                adapter: skipperCsv,
                csvOptions: {delimiter: ';', columns: true},
                rowHandler
            },
            async (err, files) => {
                if (err) {
                    console.log('err disini')
                    this.res.serverError(err);
                }
                const results = await Promise.all(promises);
                return exits.success({results});
            }
        );
    }
}