const yup = require('yup');
const skipperCsv = require('skipper-csv');
const _ = require('lodash')

const studentSchema = yup.object().shape({
    nim: yup.number().integer().required(),
    name: yup.string().required(),
    email: yup.string().email(),
    class: yup.string(),
    peminatan: yup.number().integer().required(),
    metlit: yup.number().integer(),
    ipk: yup.number(),
});

const lecturerSchema = yup.object().shape({
    nik: yup.number().integer().required(),
    name: yup.string().required(),
    email: yup.string().email(),
    jfa: yup.number().integer(),
    labRiset: yup.number().integer().required(),
    prodi: yup.number(),
    lecturerCode: yup.string(),
});

module.exports = {
    friendlyName: 'Upload user',
    
    description: 'Menerima data user dalam bentuk tabel lalu memasukkan ke database',

    inputs: {
        userType: {
            required: true,
            type: 'string',
            isIn: ['student', 'lecturer']
        },
        shouldUpdateUser: {
            type: 'boolean'
        },
    },

    exits: {
        success: {},
    },
    
    fn: async function(inputs, exits) {
        let Model, modelSchema, userIdentifier;
        switch (inputs.userType) {
            case 'student': 
                Model = Student;
                modelSchema = studentSchema;
                userIdentifier = 'nim';
                break;
            case 'lecturer':
                Model = Lecturer;
                modelSchema = lecturerSchema;
                userIdentifier = 'nik';
                break;
            default:
                Model = {};
                break;
        }
        let recordsCount = 0;
        const promises = [];
        const rowHandler = async (row) => {
            recordsCount++;
            if (row.ipk) {
                row.ipk = row.ipk.replace(',','.')
            }
            const promise = new Promise(async (resolve, reject) => {
                try {
                    const castedRow = await modelSchema.validate(cleanEmptyString(row));
                    resolve(['success', castedRow]);
                }
                catch (err) {
                    if (err.name == 'ValidationError') {
                        resolve(['invalid', `Invalid at ${userIdentifier}=${row[userIdentifier]}. ` + err.message]);
                    }
                }
            });
            promises.push(promise);
        }
        this.req.file('user-table').upload(
            {
                adapter: skipperCsv,
                csvOptions: {delimiter: ';', columns: true},
                rowHandler
            },
            async (err, files) => {
                if (err) {
                    this.res.serverError(err);
                }
                const results = await Promise.all(promises);
                const validatedRecords = results.filter(res => res[0] == 'success')
                    .map(res => res[1]);
                const invalidMessages = results.filter(res => res[0] == 'invalid').map(res => res[1]);
                const recordsToCreate = validatedRecords;
                let successCount = 0, duplicateIds = [], errorMessages = []
                for (const record of recordsToCreate) {
                    // buat copy record, karena record bakal diubah sama fungsi create (bug)
                    const recordCopy = {...record};
                    try {
                        await Model.create(record);
                        successCount++;
                    } catch (err) {
                        // Update user jika user sudah ada
                        if (err.code == "E_UNIQUE") {
                            if (inputs.shouldUpdateUser) {
                                await Model.updateOne({[userIdentifier]: recordCopy[userIdentifier]}).set(recordCopy);
                                successCount++;
                            } else duplicateIds.push(recordCopy[userIdentifier]);
                        }
                        else {
                            errorMessages.push(err.toJSON())
                        }
                    }
                }
                return exits.success({invalidMessages, successCount, duplicateCount: duplicateIds.length, duplicateIds, errorMessages});
            }
        );
    }
}

function cleanEmptyString(object) {
    let newObject = {}
        Object.keys(object).forEach(key => {
            newObject[key] = object[key] || undefined 
        });
        return newObject
    }