module.exports = {


    friendlyName: 'archive topic',


    description: ` 
        Soft-delete semua topic yang pernah diambil mahasiswa.
    `,


    inputs: {
    },


    exits: {

    },


    fn: async function (inputs) {
        const approvedTopicSelections = await TopicSelection.find({
            where: {
                status: ['APPROVED', 'APPROVED_LAB_RISET']
            },
        });
        const approvedTopicIds = approvedTopicSelections.map(ts => ts.topic);
        await Topic.update({ id: approvedTopicIds, isDeleted: false }).set({ isArchived: true });
        return;

    }


};