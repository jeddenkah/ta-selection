module.exports = {
    friendlyName: 'View dosen',

    description: 'Menampilkan list dosen sesuai filter',

    inputs: {
        labRisetId: {
            type: 'string'
        },
        jfaId: {
            type: 'string'
        }
    },

    exits: {
        success: {
            viewTemplatePath: 'pages/admin/lecturer-list'
        },
    },
    fn: async function (inputs, exits) {
        const lecturers = await Lecturer.find({ labRiset: inputs.labRisetId, jfa: inputs.jfaId })
            .populate('jfa')
            .populate('labRiset')
            .populate('prodi')
            .populate('roles')
        return exits.success({ lecturers });
    }
}
