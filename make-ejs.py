#BIKIN FILE EJS BERDASARKAN assets/src/pages/**/*.jsx
# python make_ejs.py

import os
ejsDir = 'views/pages'
reactDir = 'assets/src/pages'
ejsPagesPath = set()
for root, dirs, files in os.walk(ejsDir):
    relativeRoot = os.path.relpath(root, ejsDir)
    for filename in files:
        filename, extension = os.path.splitext(filename)
        ejsPagesPath.add(os.path.join(relativeRoot, filename))
reactPagesPath = set()
for root, dirs, files in os.walk(reactDir):
    relativeRoot = os.path.relpath(root, reactDir)
    for filename in files:
        filename, extension = os.path.splitext(filename)
        reactPagesPath.add(os.path.join(relativeRoot, filename))
newFiles = reactPagesPath - ejsPagesPath
for newFile in newFiles:
    f = open(os.path.join(ejsDir, newFile + '.ejs'), "x")
    ejsText = f"""
    <div id="root"></div>
    <%- exposeLocalsToBrowser() %>
    <script src="/dist/pages/{newFile}.js"></script>
    """
    f.write(ejsText)
    f.close()
oldFiles = ejsPagesPath - reactPagesPath
for oldFile in oldFiles:
    os.remove(os.path.join(ejsDir, oldFile + '.ejs'))