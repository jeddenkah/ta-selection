module.exports.RECEIVER_CONFIG = {
	baseUrl: "http://virtualfri.id:8084",
	port: 8084,
	hostname: "virtualfri.id"
} 
module.exports.BUILDER_CONFIG = {
	baseUrl: "http://taproposal.bubat-dev.com:8085",
	port: 8085,
	hostname: "taproposal.bubat-dev.com"
};
module.exports.APP_CONFIG = {
	development: {
		path: "/var/www/aplikasita1-dev",
		processName: "aplikasita1-dev" //pm2 name
	},
	production: {
		path: "/var/www/aplikasita1",
		processName: "ta1"
	}
}
module.exports.SECRET = "c3ks@tud%wa";
module.exports.BUILD_FILENAME = {
	development: "www.dev",
	production: "www.prod"
}